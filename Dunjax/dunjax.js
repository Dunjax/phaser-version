var Dunjax;
(function (Dunjax) {
    var Animations;
    (function (Animations) {
        var Animation = (function () {
            function Animation() {
            }
            Object.defineProperty(Animation.prototype, "currentImage", {
                get: function () {
                    return this._frameSequence != null
                        ? this._frameSequence.getFrame(this.currentFrameIndex).image : null;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Animation.prototype, "frameSequence", {
                get: function () { return this._frameSequence; },
                set: function (value) {
                    if (!this.isDone && this._frameSequence === value)
                        return;
                    this._frameSequence = value;
                    this.isDone = false;
                    this.currentFrameIndex = 0;
                    this.frameStartTime = Dunjax.Program.clock.time;
                },
                enumerable: true,
                configurable: true
            });
            Animation.prototype.checkForAdvance = function () {
                var sequence = this._frameSequence;
                if (this.isDone || sequence == null)
                    return false;
                var advanced = false;
                if (this.isTimeToAdvanceToNextFrame()) {
                    if (this.hasMoreFramesToPlay()) {
                        if (sequence.requiresNodToAdvance && !this.isNodGivenToAdvance)
                            return false;
                        this.advanceToNextFrame();
                        advanced = true;
                        this.isNodGivenToAdvance = false;
                    }
                    else
                        advanced = this.onPlayedLastFrame();
                }
                return advanced;
            };
            Animation.prototype.isTimeToAdvanceToNextFrame = function () {
                var frame = this._frameSequence.getFrame(this.currentFrameIndex);
                return Dunjax.Program.clock.time - this.frameStartTime >= frame.duration;
            };
            Animation.prototype.hasMoreFramesToPlay = function () {
                return this.currentFrameIndex < this._frameSequence.framesCount - 1;
            };
            Animation.prototype.advanceToNextFrame = function () {
                this.currentFrameIndex++;
                this.frameStartTime = Dunjax.Program.clock.time;
            };
            Animation.prototype.onPlayedLastFrame = function () {
                var advanced = false;
                if (this._frameSequence.isRepeating) {
                    this.restart();
                    advanced = true;
                }
                else
                    this.isDone = true;
                return advanced;
            };
            Animation.prototype.restart = function () {
                this.currentFrameIndex = 0;
                this.frameStartTime = Dunjax.Program.clock.time;
                this.isDone = false;
            };
            return Animation;
        })();
        Animations.Animation = Animation;
    })(Animations = Dunjax.Animations || (Dunjax.Animations = {}));
})(Dunjax || (Dunjax = {}));
var Dunjax;
(function (Dunjax) {
    var Animations;
    (function (Animations) {
        var Frame = (function () {
            function Frame(image) {
                this.image = image;
                this.duration = 125;
            }
            return Frame;
        })();
        Animations.Frame = Frame;
    })(Animations = Dunjax.Animations || (Dunjax.Animations = {}));
})(Dunjax || (Dunjax = {}));
var Dunjax;
(function (Dunjax) {
    var Time;
    (function (Time) {
        var Clock = (function () {
            function Clock(game) {
                this.game = game;
            }
            Object.defineProperty(Clock.prototype, "time", {
                get: function () { return this.game.time.now; },
                enumerable: true,
                configurable: true
            });
            Clock.prototype.start = function () {
                this.game.time.reset();
            };
            return Clock;
        })();
        Time.Clock = Clock;
    })(Time = Dunjax.Time || (Dunjax.Time = {}));
})(Dunjax || (Dunjax = {}));
var Dunjax;
(function (Dunjax) {
    var Sounds;
    (function (Sounds) {
        var Sound = (function () {
            function Sound(fileName, shouldPlayOnlyOneClipAtATime) {
                if (shouldPlayOnlyOneClipAtATime === void 0) { shouldPlayOnlyOneClipAtATime = false; }
                this.fileName = fileName;
                this.shouldPlayOnlyOneClipAtATime = shouldPlayOnlyOneClipAtATime;
                Sound.sounds.push(this);
                var game = Dunjax.Program.game;
                if (Sound.soundsEnabled)
                    game.load.audio(fileName, "sounds/" + fileName + ".mp3");
            }
            Sound.prototype.play = function () {
                if (!Sound.soundsEnabled)
                    return;
                if (!this.sound.isPlaying || !this.shouldPlayOnlyOneClipAtATime)
                    this.sound.play();
            };
            Object.defineProperty(Sound.prototype, "isPlaying", {
                get: function () {
                    if (!Sound.soundsEnabled)
                        return false;
                    return this.sound.isPlaying;
                },
                enumerable: true,
                configurable: true
            });
            Sound.prototype.stop = function () {
                if (!Sound.soundsEnabled)
                    return;
                this.sound.stop();
            };
            Sound.soundsEnabled = true;
            return Sound;
        })();
        Sounds.Sound = Sound;
    })(Sounds = Dunjax.Sounds || (Dunjax.Sounds = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="Sound.ts" />
var Dunjax;
(function (Dunjax) {
    var Sounds;
    (function (Sounds) {
        var AllSounds = (function () {
            function AllSounds() {
                this.sounds = Sounds.Sound.sounds = [];
                this.playerMove = new Sounds.Sound("playerMove");
                this.playerJump = new Sounds.Sound("playerJump");
                this.playerThrust = new Sounds.Sound("playerThrust");
                this.playerLand = new Sounds.Sound("playerLand");
                this.playerSlide = new Sounds.Sound("playerSlide");
                this.playerSwitchGuns = new Sounds.Sound("playerSwitchGuns");
                this.playerClimb = new Sounds.Sound("playerClimb", true);
                this.playerDeath = new Sounds.Sound("playerDeath");
                this.playerHit = new Sounds.Sound("playerHit");
                this.playerWhirlwinded = new Sounds.Sound("playerWhirlwinded", true);
                this.playerWebbed = new Sounds.Sound("playerWebbed", true);
                this.monsterAttack = new Sounds.Sound("monsterAttack");
                this.monsterDeath = new Sounds.Sound("monsterDeath");
                this.greenGargoyleFly = new Sounds.Sound("greenGargoyleFly");
                this.stalagmiteWake = new Sounds.Sound("stalagmiteWake");
                this.spiderWake = new Sounds.Sound("spiderWake");
                this.masterWake = new Sounds.Sound("masterWake");
                this.slimeJump = new Sounds.Sound("slimeJump");
                this.pulseFire = new Sounds.Sound("pulseFire");
                this.grapeFire = new Sounds.Sound("grapeFire");
                this.outOfAmmo = new Sounds.Sound("outOfAmmo", true);
                this.halberdFire = new Sounds.Sound("halberdFire");
                this.webFire = new Sounds.Sound("webFire");
                this.webImpact = new Sounds.Sound("webImpact");
                this.shotImpact = new Sounds.Sound("shotImpact");
                this.fireballShot = new Sounds.Sound("fireballShot");
                this.fireballImpact = new Sounds.Sound("fireballImpact");
                this.slimeballShot = new Sounds.Sound("slimeballShot");
                this.slimeShotImpact = new Sounds.Sound("slimeballImpact");
                this.levelEndReached = new Sounds.Sound("levelEndReached", true);
                this.itemTaken = new Sounds.Sound("itemTaken");
                this.door = new Sounds.Sound("door");
            }
            AllSounds.prototype.addSounds = function () {
                if (this.sounds[0].sound != null)
                    return;
                this.sounds.forEach(function (s) {
                    s.sound = Dunjax.Program.game.add.audio(s.fileName);
                    s.sound.allowMultiple = !s.shouldPlayOnlyOneClipAtATime;
                });
            };
            return AllSounds;
        })();
        Sounds.AllSounds = AllSounds;
    })(Sounds = Dunjax.Sounds || (Dunjax.Sounds = {}));
})(Dunjax || (Dunjax = {}));
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Dunjax;
(function (Dunjax) {
    var GameStates;
    (function (GameStates) {
        var AllSounds = Dunjax.Sounds.AllSounds;
        var TitleState = (function (_super) {
            __extends(TitleState, _super);
            function TitleState() {
                _super.apply(this, arguments);
                this.notices = [
                    "Version 3.0.0",
                    "",
                    "Copyright 1991-2015 Jeff Mather",
                ];
                this.creditsRoles = [
                    "Programming, Game Concept",
                    "Game Design, Level Design",
                    "Graphics",
                    "Music",
                    "Additional Level Design",
                    "Logo",
                ];
                this.creditsNames = [
                    "Jeff Mather",
                    "David Niecikowski",
                    "Gavin Corneveaux",
                    "Edward Niecikowski",
                    "George Corneveaux",
                    "Howard Kistler",
                ];
            }
            TitleState.prototype.preload = function () {
                var game = Dunjax.Program.game;
                var style = { font: "bold 12pt Arial", fill: "#888888", align: "center" };
                var text = this.loadingText = game.add.text(0, 0, "Loading...", style);
                text.x = game.width / 2 - text.width / 2;
                text.y = game.height / 2 - text.height / 2;
                game.load.image('logo', 'images/logo.png');
                game.load.image('tiles', 'maps/tiles.png');
                game.load.image('armorStat', 'images/armorStat.png');
                game.load.image('keysStat', 'images/entities/items/key1.png');
                game.load.image('pulseAmmoStat', 'images/entities/items/pulseAmmoBig1.png');
                game.load.image('grapeshotAmmoStat', 'images/entities/items/grapeshotGun1.png');
                Dunjax.Program.emptyImage = new Dunjax.Images.Image("empty");
                GameStates.createFrameSequences();
                if (Dunjax.Program.sounds == null)
                    Dunjax.Program.sounds = new AllSounds();
            };
            TitleState.prototype.create = function () {
                var game = Dunjax.Program.game;
                this.loadingText.destroy();
                var centerX = game.width / 2;
                var logo = game.add.image(0, 40, 'logo');
                logo.x = centerX - logo.width / 2;
                var y = 160;
                var style = { font: "bold 12pt Arial", fill: "#888888", align: "center" };
                this.notices.forEach(function (line) {
                    var text = game.add.text(0, y, line, style);
                    text.x = centerX - text.width / 2,
                        y += text.height;
                });
                var creditsCenterX = centerX + 40;
                var creditsStartY = 270;
                y = creditsStartY;
                this.creditsRoles.forEach(function (line) {
                    var text = game.add.text(0, y, line, style);
                    text.x = creditsCenterX - 20 - text.width,
                        y += text.height;
                });
                y = creditsStartY;
                var emphasisStyle = { font: "bold 12pt Arial", fill: "#cccccc", align: "center" };
                this.creditsNames.forEach(function (line) {
                    var text = game.add.text(0, y, line, emphasisStyle);
                    text.x = creditsCenterX + 20;
                    y += text.height;
                });
                var text = game.add.text(0, game.height - 120, "Press [Enter] (or touch device screen) to start", style);
                text.x = centerX - text.width / 2;
                this.startGameKey = this.game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
            };
            TitleState.prototype.update = function () {
                if (this.startGameKey.isDown || this.game.input.pointer1.isDown)
                    this.onStateExited();
            };
            return TitleState;
        })(Phaser.State);
        GameStates.TitleState = TitleState;
    })(GameStates = Dunjax.GameStates || (Dunjax.GameStates = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="time/Clock.ts" />
/// <reference path="sounds/AllSounds.ts" />
/// <reference path="gamestates/TitleState.ts" />
var Dunjax;
(function (Dunjax) {
    var Clock = Dunjax.Time.Clock;
    var TitleState = Dunjax.GameStates.TitleState;
    var Program = (function () {
        function Program() {
            var phaserGame = Program.game =
                new Phaser.Game(800, 600, Phaser.AUTO, 'content');
            Program.clock = new Clock(phaserGame);
        }
        Program.prototype.start = function () {
            var _this = this;
            var game = Program.game;
            var stateManager = game.state;
            var state = this.titleState = new TitleState();
            state.onStateExited = function () { return _this.onTitleStateExited.call(_this); };
            var key = state.key = "Title";
            stateManager.add(key, state);
            stateManager.start(key);
        };
        Program.prototype.onTitleStateExited = function () {
            var _this = this;
            var game = new Dunjax.Game();
            game.onGameOver = function () { return _this.onGameOver.call(_this); };
            game.startCurrentLevel();
        };
        Program.prototype.onGameOver = function () {
            var game = Program.game;
            var stateManager = game.state;
            stateManager.start(this.titleState.key);
        };
        Program.random = new Phaser.RandomDataGenerator([new Date().getTime()]);
        return Program;
    })();
    Dunjax.Program = Program;
})(Dunjax || (Dunjax = {}));
window.onload = function () {
    var program = new Dunjax.Program();
    program.start();
};
/// <reference path="../Program.ts" />
var Dunjax;
(function (Dunjax) {
    var Images;
    (function (Images) {
        var Image = (function () {
            function Image(path) {
                this.key = path;
                var game = Dunjax.Program.game;
                game.load.image(this.key, "images/" + path + ".png");
                var self = this;
                game.load.onLoadComplete.addOnce(function () { return self.image = game.cache.getImage(self.key); });
            }
            Object.defineProperty(Image.prototype, "width", {
                get: function () { return this.image.width; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Image.prototype, "height", {
                get: function () { return this.image.height; },
                enumerable: true,
                configurable: true
            });
            return Image;
        })();
        Images.Image = Image;
    })(Images = Dunjax.Images || (Dunjax.Images = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="../images/Image.ts" />
var Dunjax;
(function (Dunjax) {
    var Animations;
    (function (Animations) {
        var Image = Dunjax.Images.Image;
        var FrameSequence = (function () {
            function FrameSequence(imagePrefix, length, isRepeating, repeatedFrames, images) {
                this.imagePrefix = imagePrefix;
                this.isRepeating = isRepeating;
                this.frames = [];
                for (var i = 0; i < length; i++) {
                    var image = this.getFrameImage(i, repeatedFrames, images, this.frames);
                    if (image == null
                        && (images == null || i >= images.length - 1))
                        break;
                    var frame = new Animations.Frame(image);
                    this.frames.push(frame);
                }
                if (this.frames.length === 0)
                    throw new Error("No frames found for sequence with prefix " + imagePrefix);
            }
            FrameSequence.prototype.getFrame = function (index) { return this.frames[index]; };
            Object.defineProperty(FrameSequence.prototype, "framesCount", {
                get: function () { return this.frames.length; },
                enumerable: true,
                configurable: true
            });
            FrameSequence.prototype.getFrameImage = function (frameIndex, repeatedFrames, images, fromFrames) {
                var image;
                if (images != null && frameIndex < images.length) {
                    image = images[frameIndex];
                }
                else if (repeatedFrames != null
                    && frameIndex < repeatedFrames.length
                    && repeatedFrames[frameIndex] >= 0) {
                    image = fromFrames[repeatedFrames[frameIndex]].image;
                }
                else
                    image = new Image(this.imagePrefix + (frameIndex + 1));
                return image;
            };
            FrameSequence.prototype.setDurationOfAllFrames = function (duration) {
                this.frames.forEach(function (f) { return f.duration = duration; });
            };
            return FrameSequence;
        })();
        Animations.FrameSequence = FrameSequence;
    })(Animations = Dunjax.Animations || (Dunjax.Animations = {}));
})(Dunjax || (Dunjax = {}));
var Dunjax;
(function (Dunjax) {
    var Animations;
    (function (Animations) {
        Animations.invisibleFrameSequence;
        function createMiscFrameSequences() {
            if (Animations.invisibleFrameSequence == null)
                Animations.invisibleFrameSequence =
                    new Animations.FrameSequence("invisible", 1, true, null, [Dunjax.Program.emptyImage]);
        }
        Animations.createMiscFrameSequences = createMiscFrameSequences;
    })(Animations = Dunjax.Animations || (Dunjax.Animations = {}));
})(Dunjax || (Dunjax = {}));
var Dunjax;
(function (Dunjax) {
    var Maps;
    (function (Maps) {
        (function (TerrainFeature) {
            TerrainFeature[TerrainFeature["None"] = 0] = "None";
            TerrainFeature[TerrainFeature["LockedDoor"] = 1] = "LockedDoor";
            TerrainFeature[TerrainFeature["ClosedDoor"] = 2] = "ClosedDoor";
            TerrainFeature[TerrainFeature["OpenDoor"] = 3] = "OpenDoor";
            TerrainFeature[TerrainFeature["StartCave"] = 4] = "StartCave";
            TerrainFeature[TerrainFeature["ExitCave"] = 5] = "ExitCave";
            TerrainFeature[TerrainFeature["Ladder"] = 6] = "Ladder";
            TerrainFeature[TerrainFeature["SlideLeft"] = 7] = "SlideLeft";
            TerrainFeature[TerrainFeature["SlideRight"] = 8] = "SlideRight";
            TerrainFeature[TerrainFeature["Stalagmite"] = 9] = "Stalagmite";
            TerrainFeature[TerrainFeature["SlimePool"] = 10] = "SlimePool";
            TerrainFeature[TerrainFeature["Spikes"] = 11] = "Spikes";
        })(Maps.TerrainFeature || (Maps.TerrainFeature = {}));
        var TerrainFeature = Maps.TerrainFeature;
        ;
        Maps.pixelsPerTenFeet = 32;
    })(Maps = Dunjax.Maps || (Dunjax.Maps = {}));
})(Dunjax || (Dunjax = {}));
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        (function (Side) {
            Side[Side["Left"] = 0] = "Left";
            Side[Side["Right"] = 1] = "Right";
            Side[Side["NeitherSide"] = 2] = "NeitherSide";
        })(Entities.Side || (Entities.Side = {}));
        var Side = Entities.Side;
        ;
        (function (VerticalSide) {
            VerticalSide[VerticalSide["Top"] = 0] = "Top";
            VerticalSide[VerticalSide["Bottom"] = 1] = "Bottom";
            VerticalSide[VerticalSide["NeitherVerticalSide"] = 2] = "NeitherVerticalSide";
        })(Entities.VerticalSide || (Entities.VerticalSide = {}));
        var VerticalSide = Entities.VerticalSide;
        ;
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="../maps/IMap.ts" />
/// <reference path="IMapEntity.ts" />
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var pixelsPerTenFeet = Dunjax.Maps.pixelsPerTenFeet;
        var Animation = Dunjax.Animations.Animation;
        var MapEntity = (function () {
            function MapEntity(x, y) {
                this._x = 0;
                this._y = 0;
                this._width = 1;
                this._height = 1;
                this._left = 0;
                this._right = 0;
                this._top = 0;
                this._bottom = 0;
                this.isInOrNearViewport = false;
                this.animation = new Animation();
                this.isPermanentlyInactive = false;
                this.shouldCheckForFall = false;
                this.createFrameworkObject(x, y);
            }
            Object.defineProperty(MapEntity.prototype, "x", {
                get: function () { return this._x; },
                set: function (value) {
                    this.sprite.x = value;
                    if (this.sprite.body != null)
                        this.sprite.body.shape.pos.x = value;
                    this.updateX();
                },
                enumerable: true,
                configurable: true
            });
            MapEntity.prototype.updateX = function () {
                this._x = this.sprite.body != null ? this.sprite.body.shape.pos.x : this.sprite.x;
                var halfWidth = this._width / 2;
                this._left = this._x - halfWidth;
                this._right = this._x + halfWidth - 1;
            };
            Object.defineProperty(MapEntity.prototype, "y", {
                get: function () { return this._y; },
                set: function (value) {
                    this.sprite.y = value;
                    if (this.sprite.body != null)
                        this.sprite.body.shape.pos.y = value;
                    this.updateY();
                },
                enumerable: true,
                configurable: true
            });
            MapEntity.prototype.updateY = function () {
                this._y = this.sprite.body != null ? this.sprite.body.shape.pos.y : this.sprite.y;
                var halfHeight = this._height / 2;
                this._top = this._y - halfHeight;
                this._bottom = this._y + halfHeight - 1;
            };
            Object.defineProperty(MapEntity.prototype, "left", {
                get: function () { return this._left; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(MapEntity.prototype, "right", {
                get: function () { return this._right; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(MapEntity.prototype, "top", {
                get: function () { return this._top; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(MapEntity.prototype, "bottom", {
                get: function () { return this._bottom; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(MapEntity.prototype, "width", {
                get: function () { return this._width; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(MapEntity.prototype, "height", {
                get: function () { return this._height; },
                enumerable: true,
                configurable: true
            });
            MapEntity.prototype.updateSize = function () {
                this._width = Math.abs(this.sprite.width);
                this._height = this.sprite.height;
                this.updateX();
                this.updateY();
            };
            Object.defineProperty(MapEntity.prototype, "shouldBeDrawn", {
                get: function () { return this.sprite.visible; },
                set: function (value) { this.sprite.visible = value; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(MapEntity.prototype, "frameworkObject", {
                get: function () { return this.sprite; },
                set: function (value) { this.sprite = value; },
                enumerable: true,
                configurable: true
            });
            MapEntity.prototype.createFrameworkObject = function (x, y) {
                var sprite = this.sprite = new Phaser.Sprite(Dunjax.Program.game, x, y);
                sprite.anchor.setTo(0.5, 0.5);
                this.updateX();
                this.updateY();
                Dunjax.Program.game.add.existing(sprite);
            };
            MapEntity.prototype.act = function () {
                if (this.isPermanentlyInactive)
                    return;
                this.checkForAnimationAdvanceOrFinish();
                if (this.map == null)
                    return;
                if (this.shouldCheckForFall)
                    this.checkForFall();
                this.actAfterAnimationChecked();
                if (this.sprite.game != null) {
                    this.determineAnimation();
                    this.determineImage();
                }
                this.onActed();
            };
            MapEntity.prototype.actAfterAnimationChecked = function () { };
            MapEntity.prototype.onActed = function () { };
            MapEntity.prototype.getDefaultImage = function () {
                throw new Error("A subtype needs to implement this.");
            };
            MapEntity.prototype.determineImage = function () {
                var old = this.image;
                var current = this.animation.frameSequence != null
                    ? this.animation.currentImage
                    : this.getDefaultImage();
                if (current !== old) {
                    this.sprite.loadTexture(current.key);
                    this.image = current;
                    this.updateSize();
                }
            };
            MapEntity.prototype.checkForAnimationAdvanceOrFinish = function () {
                var animation = this.animation;
                if (animation == null)
                    return;
                var wasDone = animation.isDone;
                animation.checkForAdvance();
                if (!wasDone && animation.isDone) {
                    var sequence = animation.frameSequence;
                    this.onAnimationDone(sequence);
                    if (sequence.switchToWhenDone != null)
                        animation.frameSequence = sequence.switchToWhenDone;
                }
            };
            MapEntity.prototype.onAnimationDone = function (sequence) { };
            MapEntity.prototype.getSideEntityIsOn = function (entity, tolerance) {
                if (tolerance === void 0) { tolerance = 0; }
                if (Math.abs(entity.x - this.x) <= 0.5 + tolerance)
                    return Entities.Side.NeitherSide;
                return entity.x < this.x ? Entities.Side.Left : Entities.Side.Right;
            };
            MapEntity.prototype.getVerticalSideEntityIsOn = function (entity, tolerance) {
                if (tolerance === void 0) { tolerance = 0; }
                if (Math.abs(entity.y - this.y) <= 0.5 + tolerance)
                    return Entities.VerticalSide.NeitherVerticalSide;
                return entity.y < this.y ? Entities.VerticalSide.Top : Entities.VerticalSide.Bottom;
            };
            MapEntity.prototype.isOnGround = function () {
                return !this.map.isPassible(this.x, this.bottom + 1);
            };
            MapEntity.prototype.isCeilingDwelling = function () { return false; };
            MapEntity.prototype.isGroundDwelling = function () { return true; };
            MapEntity.prototype.alignWithSurroundings = function () {
                if (this.shouldAlignWithGroundOrCeiling()) {
                    this.moveUpUntilAboveImpassible();
                    this.moveDownUntilBelowImpassible();
                    if (this.isGroundDwelling())
                        this.moveDownUntilOnGround();
                    if (this.isCeilingDwelling())
                        this.moveUpUntilContactingCeiling();
                }
                this.alignSpecially();
            };
            MapEntity.prototype.shouldAlignWithGroundOrCeiling = function () { return true; };
            MapEntity.prototype.alignSpecially = function () { };
            MapEntity.prototype.moveUpUntilAboveImpassible = function () {
                var limit = this.top - 4 * pixelsPerTenFeet;
                while (!this.map.isPassible(this.x, this.bottom, false) && this.top > limit)
                    this.y--;
            };
            MapEntity.prototype.moveDownUntilBelowImpassible = function () {
                var limit = this.top + 4 * pixelsPerTenFeet;
                while (!this.map.isPassible(this.x, this.top, false) && this.top < limit)
                    this.y++;
            };
            MapEntity.prototype.moveDownUntilOnGround = function () {
                var limit = this.top + 4 * pixelsPerTenFeet;
                while (this.map.isFallThroughable(this.x, this.bottom + 1) && this.top < limit)
                    this.y++;
            };
            MapEntity.prototype.moveUpUntilContactingCeiling = function () {
                var limit = this.top - 4 * pixelsPerTenFeet;
                while (this.map.isPassible(this.x, this.top - 1, false) && this.top > limit)
                    this.y--;
            };
            MapEntity.prototype.isPassibleToSide = function (side, dx, considerBeings) {
                if (considerBeings === void 0) { considerBeings = true; }
                var x = side === Entities.Side.Left ? this.left - dx : this.right + dx;
                var map = this.map;
                return map.isVerticalLinePassible(x, this.y, this.height, considerBeings);
            };
            MapEntity.prototype.isPassibleToVerticalSide = function (side, dy, considerBeings) {
                if (considerBeings === void 0) { considerBeings = true; }
                var y = side === Entities.VerticalSide.Top ? this.top - dy : this.bottom + dy;
                var map = this.map;
                return map.isHorizontalLinePassible(this.x, y, this.width, considerBeings);
            };
            MapEntity.prototype.removeFromPlay = function () {
                this.isPermanentlyInactive = true;
                this.map.entities.removeEntity(this);
                this.sprite.destroy();
            };
            MapEntity.prototype.determineAnimation = function () {
                var animation = this.animation;
                var oldSequence = animation.frameSequence;
                var sequence = this.getFrameSequenceForState();
                if (sequence !== oldSequence) {
                    if (oldSequence != null && !animation.isDone)
                        this.onAnimationDone(oldSequence);
                    animation.frameSequence = sequence;
                    animation.restart();
                    this.onAnimationChanged();
                }
            };
            MapEntity.prototype.onAnimationChanged = function () { };
            MapEntity.prototype.getFrameSequenceForState = function () {
                throw new Error("Must override this.");
            };
            MapEntity.prototype.playIfExists = function (sound) {
                if (sound != null)
                    sound.play();
            };
            MapEntity.prototype.determineIsInOrNearViewport = function () {
                this.updateX();
                this.updateY();
                var camera = Dunjax.Program.game.camera;
                var maxDistance = 100;
                this.isInOrNearViewport =
                    Dunjax.Geometry.intersects(this.left, this.top, this.width, this.height, camera.x - maxDistance, camera.y - maxDistance, camera.width + 2 * maxDistance, camera.height + 2 * maxDistance);
            };
            MapEntity.prototype.checkForFall = function () {
                var fallAmount = this.canFall();
                if (fallAmount > 0)
                    this.y += fallAmount;
                else {
                    this.shouldCheckForFall = false;
                    this.onFallBlocked();
                }
            };
            MapEntity.prototype.canFall = function () {
                var x = this.x;
                var y = this.bottom;
                var width = this.width;
                var map = this.map;
                var check = function (dy) {
                    return map.isPassible(x - width / 4, y + dy)
                        && map.isPassible(x + width / 4, y + dy);
                };
                var accelerationInPixelsPerMilliSecond = .55;
                var timeFalling = Math.max(Dunjax.Program.clock.time - this.currentFallStartTime, 1);
                var speed = accelerationInPixelsPerMilliSecond * timeFalling;
                var dy = Dunjax.getMovementForSpeed(speed);
                if (check(dy))
                    return dy;
                if (check(1))
                    return 1;
                return 0;
            };
            MapEntity.prototype.onFallBlocked = function () { };
            return MapEntity;
        })();
        Entities.MapEntity = MapEntity;
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="MapEntity.ts" />
/// <reference path="../animations/Animation.ts" />
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var AnimationEntity = (function (_super) {
            __extends(AnimationEntity, _super);
            function AnimationEntity(type, sequence, x, y) {
                _super.call(this, x, y);
                this.animationEntityType = type;
                this.animation.frameSequence =
                    sequence != null ? sequence : Dunjax.Animations.invisibleFrameSequence;
            }
            AnimationEntity.prototype.shouldAlignWithGroundOrCeiling = function () { return false; };
            AnimationEntity.prototype.getFrameSequenceForState = function () {
                return this.animation.frameSequence;
            };
            return AnimationEntity;
        })(Entities.MapEntity);
        Entities.AnimationEntity = AnimationEntity;
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="../animations/FrameSequence.ts" />
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var FrameSequences = (function () {
            function FrameSequences() {
                this.imagesPrefix = "";
            }
            FrameSequences.prototype.createFrameSequence = function (args) {
                var fullName = "";
                var prefix = this.imagesPrefix;
                var name = args.name;
                if (name != null)
                    fullName = this.imagesPath + prefix +
                        (prefix.length > 0 ? name.makeFirstLetterUppercase() : name);
                return new Dunjax.Animations.FrameSequence(fullName, args.length, args.isRepeating, args.repeatedFrames, args.images);
            };
            FrameSequences.prototype.args = function (name, length, others) {
                if (length === void 0) { length = 1; }
                if (others === void 0) { others = {}; }
                others.name = name;
                others.length = length;
                if (others.isRepeating == null)
                    others.isRepeating = false;
                return others;
            };
            FrameSequences.prototype.create = function (name, length) {
                return this.createFrameSequence(this.args(name, length));
            };
            FrameSequences.prototype.createWith = function (name, length, others) {
                return this.createFrameSequence(this.args(name, length, others));
            };
            return FrameSequences;
        })();
        Entities.FrameSequences = FrameSequences;
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="../MapEntity.ts" />
/// <reference path="../FrameSequences.ts" />
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Beings;
        (function (Beings) {
            var MapEntity = Entities.MapEntity;
            var Being = (function (_super) {
                __extends(Being, _super);
                function Being(x, y) {
                    _super.call(this, x, y);
                    this.isFacingLeft = false;
                    this.isMoving = false;
                    this.isTurning = false;
                    this.isHit = false;
                    this.isDying = false;
                    this.isDead = false;
                    this.isPassible = false;
                }
                Being.prototype.getDefaultImage = function () {
                    return this.beingFrameSequences.still.getFrame(0).image;
                };
                Being.prototype.onActed = function () {
                    this.sprite.scale.x = this.isFacingLeft ? -1 : 1;
                };
                Being.prototype.onDamaged = function (amount) {
                    this.health -= amount;
                    this.health = Math.max(this.health, 0);
                    if (this.health <= 0)
                        this.die();
                };
                Being.prototype.onStruck = function (damage, x, y) {
                    if (x === void 0) { x = this.x; }
                    if (y === void 0) { y = this.y; }
                    if (this.isVulnerableAt(x, y))
                        this.onStruckWhereVulnerable(x, y, damage);
                };
                Being.prototype.onStruckWhereVulnerable = function (x, y, damage) {
                    this.onDamaged(damage);
                    if (!this.isDying)
                        this.survivedStrike(x, y);
                };
                Being.prototype.beforeSurvivedStrike = function () { };
                Being.prototype.survivedStrike = function (x, y) {
                    this.beforeSurvivedStrike();
                    if (this.hasSpatterSequence())
                        this.showSpatterSequence(x, y);
                    if (this.beingFrameSequences.hit != null)
                        this.isHit = true;
                    this.playIfExists(this.beingSounds.hit);
                };
                Being.prototype.hasSpatterSequence = function () {
                    return this.beingFrameSequences.spatter != null;
                };
                Being.prototype.showSpatterSequence = function (x, y) {
                    var onLeftSide = x < this.x;
                    var spatter = new Entities.Spatter(this.beingFrameSequences.spatter, 0, 0);
                    spatter.determineImage();
                    spatter.x =
                        onLeftSide ? this.left - spatter.width / 2 : this.right + spatter.width / 2;
                    spatter.y = this.bottom - spatter.height / 2;
                    this.map.entities.addEntity(spatter);
                };
                Being.prototype.die = function () {
                    this.beforeDying();
                    this.isDying = true;
                    this.playIfExists(this.beingSounds.death);
                };
                Being.prototype.beforeDying = function () { };
                Being.prototype.isStrikableAt = function (x, y) {
                    if (this.isDead || this.isDying || !Dunjax.Geometry.rectContains(this, x, y))
                        return false;
                    return this.isStrikableAtContainedLocation(x, y);
                };
                Being.prototype.isStrikableAtContainedLocation = function (x, y) {
                    return true;
                };
                Being.prototype.isVulnerableAt = function (x, y) {
                    if (!this.isStrikableAt(x, y))
                        return false;
                    return this.isVulnerableAtStrikableLocation(x, y);
                };
                Being.prototype.isVulnerableAtStrikableLocation = function (x, y) {
                    return true;
                };
                Being.prototype.onAnimationDone = function (sequence) {
                    this.onBeforeAnimationDoneAsBeing(sequence);
                    var sequences = this.beingFrameSequences;
                    if (sequence === sequences.turn || sequence === sequences.ceilingTurn) {
                        this.isTurning = false;
                    }
                    if (sequence === sequences.hit) {
                        this.isHit = false;
                    }
                    if (sequence === sequences.death
                        || sequence === sequences.ceilingDeath)
                        this.onDied();
                };
                Being.prototype.onBeforeAnimationDoneAsBeing = function (sequence) { };
                Being.prototype.onDied = function () {
                    this.isDying = false;
                    this.isDead = true;
                    if (!this.doesLeaveCarcass())
                        this.removeFromPlay();
                    else {
                        this.shouldCheckForFall = true;
                        this.currentFallStartTime = Dunjax.Program.clock.time;
                    }
                };
                Being.prototype.doesLeaveCarcass = function () {
                    return this.beingFrameSequences.carcass != null;
                };
                Being.prototype.getFrameSequenceForState = function () {
                    var sequences = this.beingFrameSequences;
                    var sequence;
                    if (this.isDying)
                        sequence = sequences.death;
                    else if (this.isDead)
                        sequence = sequences.carcass;
                    else if (this.isHit)
                        sequence = sequences.hit;
                    else if (this.isTurning)
                        sequence = sequences.turn;
                    else if (this.isMoving)
                        sequence = sequences.move;
                    else
                        sequence = sequences.still;
                    return this.getFrameSequenceForStateAsBeingSubtype(sequence);
                };
                Being.prototype.getFrameSequenceForStateAsBeingSubtype = function (beingSequenceChosen) {
                    return beingSequenceChosen;
                };
                Being.prototype.actAfterAnimationChecked = function () {
                    this.actAfterAnimationCheckedAsBeingSubtype();
                };
                Being.prototype.actAfterAnimationCheckedAsBeingSubtype = function () { };
                Being.prototype.onFallBlocked = function () {
                    if (this.isDead)
                        this.isPermanentlyInactive = true;
                };
                return Being;
            })(MapEntity);
            Beings.Being = Being;
            var BeingSounds = (function () {
                function BeingSounds() {
                }
                return BeingSounds;
            })();
            Beings.BeingSounds = BeingSounds;
            var BeingFrameSequences = (function (_super) {
                __extends(BeingFrameSequences, _super);
                function BeingFrameSequences() {
                    _super.apply(this, arguments);
                }
                return BeingFrameSequences;
            })(Entities.FrameSequences);
            Beings.BeingFrameSequences = BeingFrameSequences;
        })(Beings = Entities.Beings || (Entities.Beings = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
var Dunjax;
(function (Dunjax) {
    var Time;
    (function (Time) {
        var Duration = (function () {
            function Duration(length, representsPeriod) {
                if (length === void 0) { length = 0; }
                if (representsPeriod === void 0) { representsPeriod = true; }
                this.startTime = 0;
                this.length = length;
                this.representsPeriod = representsPeriod;
            }
            Object.defineProperty(Duration.prototype, "isDone", {
                get: function () {
                    var done = Dunjax.Program.clock.time - this.startTime >= this.length;
                    if (done && this.representsPeriod)
                        this.start();
                    return done;
                },
                enumerable: true,
                configurable: true
            });
            Duration.prototype.start = function () {
                var clock = Dunjax.Program.clock;
                if (this.representsPeriod
                    && this.startTime + this.length >= clock.time - this.length / 2)
                    this.startTime += this.length;
                else
                    this.startTime = clock.time;
                this.afterStart();
            };
            Duration.prototype.afterStart = function () { };
            Object.defineProperty(Duration.prototype, "lengthLeft", {
                get: function () {
                    return this.startTime + this.length - Dunjax.Program.clock.time;
                },
                enumerable: true,
                configurable: true
            });
            Duration.prototype.makeDone = function () {
                this.startTime = Dunjax.Program.clock.time - this.length;
            };
            return Duration;
        })();
        Time.Duration = Duration;
    })(Time = Dunjax.Time || (Dunjax.Time = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="../Being.ts" />
/// <reference path="../../../time/Duration.ts" />
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Beings;
        (function (Beings) {
            var Monsters;
            (function (Monsters) {
                var Being = Beings.Being;
                var BeingSounds = Beings.BeingSounds;
                var Side = Entities.Side;
                var Duration = Dunjax.Time.Duration;
                var Monster = (function (_super) {
                    __extends(Monster, _super);
                    function Monster(x, y) {
                        _super.call(this, x, y);
                        this.isAttacking = false;
                        this.isSleeping = false;
                        this.isWaking = false;
                        this.isOnCeiling = false;
                        this.betweenAttacksDuration = new Duration(500, false);
                    }
                    Monster.prototype.startAttack = function () {
                        this.isFacingLeft =
                            this.getSideEntityIsOn(this.map.entities.player) === Side.Left;
                        this.isAttacking = true;
                        this.playIfExists(this.monsterSounds.attack);
                    };
                    Monster.prototype.canStartAttack = function () {
                        if (this.isAttacking)
                            return false;
                        if (this.isSleeping || this.isWaking)
                            return false;
                        if (!this.betweenAttacksDuration.isDone)
                            return false;
                        if (this.map.entities.player.isDead)
                            return false;
                        return this.isCloseEnoughToAttack();
                    };
                    Monster.prototype.onBeforeAnimationDoneAsBeing = function (sequence) {
                        this.onBeforeAnimationDoneAsMonster(sequence);
                        if (this.isAttacking)
                            this.finishAttack();
                    };
                    Monster.prototype.onBeforeAnimationDoneAsMonster = function (sequence) { };
                    Monster.prototype.finishAttack = function () {
                        if (this.isCloseEnoughToAttack())
                            this.onPlayerStruck();
                        this.isAttacking = false;
                        this.betweenAttacksDuration.start();
                    };
                    Monster.prototype.onPlayerStruck = function () {
                        var player = this.map.entities.player;
                        if (this.attackDamage > 0)
                            player.onStruck(this.attackDamage, player.x, player.y);
                        if (!player.isDead)
                            this.onPlayerHit(player);
                    };
                    Monster.prototype.isCloseEnoughToAttack = function () {
                        var player = this.map.entities.player;
                        var sideBeingIsOn = this.getSideEntityIsOn(player);
                        var maxDistance = 4;
                        if ((sideBeingIsOn === Side.Left && this.left - player.right > maxDistance)
                            || sideBeingIsOn === Side.Right && player.left - this.right > maxDistance) {
                            return false;
                        }
                        var isDirectlyAboveOrBelow = this.x >= player.left && this.x <= player.right;
                        var margin = isDirectlyAboveOrBelow ? 4 : 0;
                        if (this.top > player.bottom + margin || this.bottom < player.top - margin)
                            return false;
                        return true;
                    };
                    Monster.prototype.canMove = function () {
                        if (!this.canMoveInCurrentState())
                            return false;
                        return true;
                    };
                    Monster.prototype.canMoveInCurrentState = function () {
                        return !this.isAttacking && !this.isSleeping && !this.isWaking && !this.isHit;
                    };
                    Monster.prototype.move = function () {
                        var moved = this.currentMovementMethod.moveMonster();
                        if (!moved && !this.isCloseEnoughToAttack())
                            this.changeMovementMethod();
                    };
                    Monster.prototype.changeMovementMethod = function () { };
                    Monster.prototype.actAfterAnimationCheckedAsBeingSubtype = function () {
                        this.actBeforeActingAsMonster();
                        if (!this.isDead && !this.isDying)
                            this.actWhenAlive();
                    };
                    Monster.prototype.actBeforeActingAsMonster = function () { };
                    Monster.prototype.actWhenAlive = function () {
                        if (this.canStartAttack())
                            this.startAttack();
                        if (this.canMove())
                            this.move();
                    };
                    Monster.prototype.onPlayerHit = function (player) { };
                    Monster.prototype.getFrameSequenceForStateAsBeingSubtype = function (beingSequenceChosen) {
                        var sequences = this.monsterFrameSequences;
                        var sequence = beingSequenceChosen;
                        var isOnCeiling = this.isOnCeiling;
                        if (this.isDead || this.isHit)
                            ;
                        else if (this.isDying && isOnCeiling)
                            sequence = sequences.ceilingDeath;
                        else if (this.isDying)
                            ;
                        else if (this.isSleeping)
                            sequence = sequences.sleep;
                        else if (this.isWaking)
                            sequence = sequences.wake;
                        else if (this.isAttacking && isOnCeiling)
                            sequence = sequences.ceilingAttack;
                        else if (this.isAttacking)
                            sequence = sequences.attack;
                        else if (this.isTurning && isOnCeiling)
                            sequence = sequences.ceilingTurn;
                        else if (this.isMoving && isOnCeiling)
                            sequence = sequences.ceilingMove;
                        else if (!this.isMoving && isOnCeiling)
                            sequence = sequences.ceilingStill;
                        return this.getFrameSequenceForStateAsMonsterSubtype(sequence);
                    };
                    Monster.prototype.getFrameSequenceForStateAsMonsterSubtype = function (monsterSequenceChosen) {
                        return monsterSequenceChosen;
                    };
                    Object.defineProperty(Monster.prototype, "hasTurnSequence", {
                        get: function () {
                            var sequences = this.monsterFrameSequences;
                            return this.isOnCeiling
                                ? sequences.ceilingTurn != null
                                : sequences.turn != null;
                        },
                        enumerable: true,
                        configurable: true
                    });
                    return Monster;
                })(Being);
                Monsters.Monster = Monster;
                var MonsterSounds = (function (_super) {
                    __extends(MonsterSounds, _super);
                    function MonsterSounds() {
                        _super.call(this);
                        this.death = Dunjax.Program.sounds.monsterDeath;
                    }
                    return MonsterSounds;
                })(BeingSounds);
                Monsters.MonsterSounds = MonsterSounds;
                var MonsterFrameSequences = (function (_super) {
                    __extends(MonsterFrameSequences, _super);
                    function MonsterFrameSequences() {
                        _super.call(this);
                        this.imagesPath = "entities/monsters/";
                    }
                    return MonsterFrameSequences;
                })(Beings.BeingFrameSequences);
                Monsters.MonsterFrameSequences = MonsterFrameSequences;
            })(Monsters = Beings.Monsters || (Beings.Monsters = {}));
        })(Beings = Entities.Beings || (Entities.Beings = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Beings;
        (function (Beings) {
            var Monsters;
            (function (Monsters) {
                var Movement;
                (function (Movement) {
                    var MonsterMovementMethod = (function () {
                        function MonsterMovementMethod(monster) {
                            this.monster = monster;
                        }
                        MonsterMovementMethod.prototype.moveMonster = function () {
                            throw new Error('Must override');
                        };
                        MonsterMovementMethod.prototype.isPlayerToSide = function (tolerance) {
                            if (tolerance === void 0) { tolerance = 0; }
                            var monster = this.monster;
                            return monster.getSideEntityIsOn(monster.map.entities.player, tolerance)
                                !== Entities.Side.NeitherSide;
                        };
                        MonsterMovementMethod.prototype.isPlayerOnLeft = function () {
                            var monster = this.monster;
                            return monster.getSideEntityIsOn(monster.map.entities.player) === Entities.Side.Left;
                        };
                        return MonsterMovementMethod;
                    })();
                    Movement.MonsterMovementMethod = MonsterMovementMethod;
                })(Movement = Monsters.Movement || (Monsters.Movement = {}));
            })(Monsters = Beings.Monsters || (Beings.Monsters = {}));
        })(Beings = Entities.Beings || (Entities.Beings = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="MonsterMovementMethod.ts" />
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Beings;
        (function (Beings) {
            var Monsters;
            (function (Monsters) {
                var Movement;
                (function (Movement) {
                    var Duration = Dunjax.Time.Duration;
                    var MoveAlongGroundOrCeiling = (function (_super) {
                        __extends(MoveAlongGroundOrCeiling, _super);
                        function MoveAlongGroundOrCeiling(monster) {
                            _super.call(this, monster);
                            this.isWandering = false;
                            this.wanderDuration = new Duration(0, false);
                        }
                        MoveAlongGroundOrCeiling.prototype.moveMonster = function () {
                            this.checkForWanderingEnd();
                            this.checkForWanderingStart();
                            var monster = this.monster;
                            if (this.shouldTurn())
                                this.turn();
                            else if (this.shouldChangeFacingWithoutTurn())
                                monster.isFacingLeft = !monster.isFacingLeft;
                            if (monster.isTurning)
                                return true;
                            if (this.shouldMoveToSide()) {
                                var amount = this.canMoveToSide();
                                if (amount > 0) {
                                    this.moveToSide(amount);
                                    return true;
                                }
                                this.stopMovement();
                            }
                            else
                                monster.isMoving = false;
                            return false;
                        };
                        MoveAlongGroundOrCeiling.prototype.shouldMoveToSide = function () {
                            return !this.monster.isTurning && (this.isPlayerToSide() || this.isWandering);
                        };
                        MoveAlongGroundOrCeiling.prototype.canMoveToSide = function () {
                            var monster = this.monster;
                            var side = monster.isFacingLeft ? Entities.Side.Left : Entities.Side.Right;
                            var check = function (dx) {
                                return monster.isPassibleToSide(side, dx)
                                    && !monster.map.isFallThroughable((side === Entities.Side.Left ? monster.left - dx : monster.right + dx), (monster.isOnCeiling ? monster.top - 1 : monster.bottom + 1));
                            };
                            var dx = Dunjax.getMovementForSpeed(monster.moveSpeed);
                            if (check(dx))
                                return dx;
                            if (check(1))
                                return 1;
                            return 0;
                        };
                        MoveAlongGroundOrCeiling.prototype.moveToSide = function (amount) {
                            var monster = this.monster;
                            monster.x += amount * (monster.isFacingLeft ? -1 : 1);
                            monster.isMoving = true;
                        };
                        MoveAlongGroundOrCeiling.prototype.onMoveBlocked = function () {
                            this.stopMovement();
                            if (this.isWandering)
                                this.isWandering = false;
                            else if (Dunjax.Program.random.integerInRange(1, 50) === 1)
                                this.startWandering();
                        };
                        MoveAlongGroundOrCeiling.prototype.stopMovement = function () {
                            this.monster.isMoving = false;
                        };
                        MoveAlongGroundOrCeiling.prototype.shouldTurn = function () {
                            var monster = this.monster;
                            return !this.isWandering
                                && !monster.isTurning
                                && monster.hasTurnSequence
                                && this.isPlayerToSide()
                                && monster.isFacingLeft !== this.isPlayerOnLeft();
                        };
                        MoveAlongGroundOrCeiling.prototype.shouldChangeFacingWithoutTurn = function () {
                            var monster = this.monster;
                            return !this.isWandering
                                && !monster.hasTurnSequence
                                && this.isPlayerToSide()
                                && monster.isFacingLeft !== this.isPlayerOnLeft();
                        };
                        MoveAlongGroundOrCeiling.prototype.turn = function () {
                            var monster = this.monster;
                            monster.isTurning = true;
                            monster.isFacingLeft = !monster.isFacingLeft;
                            if (monster.isMoving)
                                this.stopMovement();
                        };
                        MoveAlongGroundOrCeiling.prototype.checkForWanderingEnd = function () {
                            if (!this.isWandering)
                                return;
                            if (this.wanderDuration.isDone)
                                this.isWandering = false;
                        };
                        MoveAlongGroundOrCeiling.prototype.checkForWanderingStart = function () {
                            if (this.isWandering)
                                return;
                            var monster = this.monster;
                            if (!this.isPlayerToSide(monster.width / 2 - 1)
                                && !monster.isCloseEnoughToAttack()
                                && Dunjax.Program.random.integerInRange(1, 100) === 1)
                                this.startWandering();
                        };
                        MoveAlongGroundOrCeiling.prototype.startWandering = function () {
                            this.isWandering = true;
                            var monster = this.monster;
                            this.wanderDuration.length = 1000 + Dunjax.Program.random.integerInRange(1, 5000);
                            this.wanderDuration.start();
                            if (monster.hasTurnSequence)
                                this.turn();
                            else
                                monster.isFacingLeft = !monster.isFacingLeft;
                        };
                        return MoveAlongGroundOrCeiling;
                    })(Movement.MonsterMovementMethod);
                    Movement.MoveAlongGroundOrCeiling = MoveAlongGroundOrCeiling;
                })(Movement = Monsters.Movement || (Monsters.Movement = {}));
            })(Monsters = Beings.Monsters || (Beings.Monsters = {}));
        })(Beings = Entities.Beings || (Entities.Beings = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="Monster.ts" />
/// <reference path="movement/MoveAlongGroundOrCeiling.ts" />
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Beings;
        (function (Beings) {
            var Monsters;
            (function (Monsters) {
                var MoveAlongGroundOrCeiling = Monsters.Movement.MoveAlongGroundOrCeiling;
                var BlueGargoyle = (function (_super) {
                    __extends(BlueGargoyle, _super);
                    function BlueGargoyle(x, y) {
                        _super.call(this, x, y);
                        this.monsterType = Monsters.MonsterType.BlueGargoyle;
                        this.currentMovementMethod = new MoveAlongGroundOrCeiling(this);
                        this.health = 10;
                        this.betweenAttacksDuration.length = 1000;
                        this.attackDamage = 15;
                        this.moveSpeed = 167;
                        this.beingFrameSequences = this.monsterFrameSequences =
                            BlueGargoyleFrameSequences.singleton;
                        this.beingSounds = this.monsterSounds = new Monsters.MonsterSounds();
                    }
                    return BlueGargoyle;
                })(Monsters.Monster);
                Monsters.BlueGargoyle = BlueGargoyle;
                var BlueGargoyleFrameSequences = (function (_super) {
                    __extends(BlueGargoyleFrameSequences, _super);
                    function BlueGargoyleFrameSequences() {
                        var _this = this;
                        _super.call(this);
                        this.imagesPrefix = "blueGargoyle";
                        var create = function (n, l) { return _this.create.call(_this, n, l); };
                        var createWith = function (n, l, o) {
                            return _this.createWith.call(_this, n, l, o);
                        };
                        var goToStill = function (s) { return s.switchToWhenDone = _this.still; };
                        this.still = create("still", 1);
                        this.move = createWith("move", 6, { isRepeating: true });
                        this.attack = createWith("attack", 4, { repeatedFrames: [-1, -1, -1, 2] });
                        goToStill(this.attack);
                        this.turn = create("turn", 2);
                        goToStill(this.turn);
                        this.carcass = create("carcass", 1);
                        this.death = create("death", 5);
                        this.death.switchToWhenDone = this.carcass;
                    }
                    BlueGargoyleFrameSequences.create = function () {
                        BlueGargoyleFrameSequences.singleton = new BlueGargoyleFrameSequences();
                    };
                    return BlueGargoyleFrameSequences;
                })(Monsters.MonsterFrameSequences);
                Monsters.BlueGargoyleFrameSequences = BlueGargoyleFrameSequences;
            })(Monsters = Beings.Monsters || (Beings.Monsters = {}));
        })(Beings = Entities.Beings || (Entities.Beings = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="Monster.ts" />
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Beings;
        (function (Beings) {
            var Monsters;
            (function (Monsters) {
                var FlyingMonster = (function (_super) {
                    __extends(FlyingMonster, _super);
                    function FlyingMonster(x, y) {
                        _super.call(this, x, y);
                        this.isFlying = true;
                        this.isFlyTurning = false;
                        this.isLanding = false;
                    }
                    FlyingMonster.prototype.onBeforeAnimationDoneAsMonster = function (sequence) {
                        var sequences = this.flyingMonsterFrameSequences;
                        if (this.isFlyTurning) {
                            this.isFlyTurning = false;
                            this.isFlying = true;
                            this.isFacingLeft = !this.isFacingLeft;
                        }
                        else if (this.isLanding) {
                            this.isLanding = false;
                            this.currentMovementMethod = this.groundMovementMethod;
                        }
                        else if (sequence === sequences.flyDeath)
                            this.onDied();
                    };
                    FlyingMonster.prototype.changeMovementMethod = function () {
                        if (!this.isFlying)
                            this.currentMovementMethod = this.flyMovementMethod;
                    };
                    FlyingMonster.prototype.getFrameSequenceForStateAsMonsterSubtype = function (monsterSequenceChosen) {
                        var sequences = this.flyingMonsterFrameSequences;
                        var sequence = monsterSequenceChosen;
                        if (this.isDead)
                            ;
                        else if (this.isDying && this.isFlying)
                            sequence = sequences.flyDeath;
                        else if (this.isDying || this.isAttacking)
                            ;
                        else if (this.isFlyTurning)
                            sequence = sequences.flyTurn;
                        else if (this.isLanding)
                            sequence = sequences.land;
                        else if (this.isFlying)
                            sequence = sequences.fly;
                        return sequence;
                    };
                    return FlyingMonster;
                })(Monsters.Monster);
                Monsters.FlyingMonster = FlyingMonster;
                var FlyingMonsterSounds = (function (_super) {
                    __extends(FlyingMonsterSounds, _super);
                    function FlyingMonsterSounds() {
                        _super.apply(this, arguments);
                    }
                    return FlyingMonsterSounds;
                })(Monsters.MonsterSounds);
                Monsters.FlyingMonsterSounds = FlyingMonsterSounds;
                var FlyingMonsterFrameSequences = (function (_super) {
                    __extends(FlyingMonsterFrameSequences, _super);
                    function FlyingMonsterFrameSequences() {
                        _super.apply(this, arguments);
                    }
                    return FlyingMonsterFrameSequences;
                })(Monsters.MonsterFrameSequences);
                Monsters.FlyingMonsterFrameSequences = FlyingMonsterFrameSequences;
            })(Monsters = Beings.Monsters || (Beings.Monsters = {}));
        })(Beings = Entities.Beings || (Entities.Beings = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="MonsterMovementMethod.ts" />
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Beings;
        (function (Beings) {
            var Monsters;
            (function (Monsters) {
                var Movement;
                (function (Movement) {
                    var Duration = Dunjax.Time.Duration;
                    var FlyMonsterMovementMethod = (function (_super) {
                        __extends(FlyMonsterMovementMethod, _super);
                        function FlyMonsterMovementMethod(monster) {
                            _super.call(this, monster);
                            this.mayFlyDiagonally = true;
                            this.diagonalFlightDuration = new Duration(1000);
                            this.durationBetweenFlightSounds = new Duration(270);
                            this.isFlyingVerticallyToPassObstacle = false;
                            this.isFlyingHorizontallyToPassObstacle = false;
                            this.takeoffDistanceLeft = 0;
                            this.flyingMonster = monster;
                        }
                        FlyMonsterMovementMethod.prototype.moveMonster = function () {
                            var monster = this.flyingMonster;
                            if (monster.isLanding)
                                return true;
                            if (!monster.isFlying) {
                                this.startFlight();
                                return true;
                            }
                            if (this.shouldLand()) {
                                this.land();
                                return true;
                            }
                            return this.flyMove();
                        };
                        FlyMonsterMovementMethod.prototype.shouldLand = function () {
                            var monster = this.flyingMonster;
                            monster.y = Math.round(monster.y);
                            return !this.isFlyingHorizontallyToPassObstacle
                                && !this.isFlyingVerticallyToPassObstacle
                                && this.takeoffDistanceLeft <= 0
                                && !monster.map.isPassible(monster.x, monster.bottom + 1);
                        };
                        FlyMonsterMovementMethod.prototype.land = function () {
                            var monster = this.flyingMonster;
                            monster.isFlying = monster.isFlyTurning = false;
                            monster.isLanding = true;
                        };
                        FlyMonsterMovementMethod.prototype.startFlight = function () {
                            var monster = this.flyingMonster;
                            monster.isFlying = true;
                            this.isFlyingHorizontallyToPassObstacle = false;
                            this.isFlyingVerticallyToPassObstacle = false;
                            this.verticalFlightDirection = Entities.VerticalSide.Top;
                            this.takeoffDistanceLeft = 6;
                        };
                        FlyMonsterMovementMethod.prototype.flyMove = function () {
                            var monster = this.flyingMonster;
                            var movedHorizontally = false;
                            var flyMoved = false;
                            var amountMoved = 0;
                            if (this.shouldFlyTurn()) {
                                monster.isFlyTurning = true;
                                flyMoved = true;
                            }
                            else if (this.shouldFlyMoveHorizontally()) {
                                var amount = this.canFlyMoveHorizontally();
                                if (amount > 0) {
                                    this.flyMoveHorizontally(amount);
                                    flyMoved = movedHorizontally = true;
                                    amountMoved = amount;
                                }
                                else
                                    this.onHorizontalFlyMoveBlocked();
                            }
                            if (this.diagonalFlightDuration.isDone)
                                this.mayFlyDiagonally = !this.mayFlyDiagonally;
                            this.checkShouldFlyVerticallyToPassObstacle(movedHorizontally);
                            this.checkToDetermineVerticalFlightDirection();
                            if (this.shouldFlyMoveVertically(movedHorizontally)) {
                                var amount = this.canFlyMoveVertically();
                                if (amount > 0) {
                                    this.flyMoveVertically(amount);
                                    flyMoved = true;
                                    amountMoved = Math.max(amount, amountMoved);
                                }
                                else
                                    this.onVerticalFlyMoveBlocked(movedHorizontally);
                            }
                            if (flyMoved && this.shouldPlayFlightSound())
                                monster.flyingMonsterSounds.fly.play();
                            if (flyMoved && this.takeoffDistanceLeft > 0)
                                this.takeoffDistanceLeft -= amountMoved;
                            return flyMoved;
                        };
                        FlyMonsterMovementMethod.prototype.shouldFlyTurn = function () {
                            var monster = this.flyingMonster;
                            return !monster.isFlyTurning
                                && !this.isFlyingHorizontallyToPassObstacle
                                && this.isPlayerToSide(monster.width / 2)
                                && monster.isFacingLeft !== this.isPlayerOnLeft();
                        };
                        FlyMonsterMovementMethod.prototype.shouldFlyMoveHorizontally = function () {
                            var monster = this.flyingMonster;
                            return !monster.isFlyTurning
                                && (this.isPlayerToSide() || this.isFlyingHorizontallyToPassObstacle)
                                && !(this.isFlyingHorizontallyToPassObstacle
                                    && this.isPlayerToVerticalSide(monster.height / 2 - 1)
                                    && this.canFlyMoveVertically() > 0);
                        };
                        FlyMonsterMovementMethod.prototype.checkToDetermineVerticalFlightDirection = function () {
                            var monster = this.flyingMonster;
                            if (!this.isFlyingVerticallyToPassObstacle && this.isPlayerToVerticalSide())
                                this.verticalFlightDirection =
                                    monster.getVerticalSideEntityIsOn(monster.map.entities.player);
                        };
                        FlyMonsterMovementMethod.prototype.shouldFlyMoveVertically = function (movedHorizontally) {
                            var monster = this.flyingMonster;
                            return (this.isFlyingVerticallyToPassObstacle
                                || ((this.mayFlyDiagonally || !movedHorizontally)
                                    && (this.isPlayerToVerticalSide()
                                        || (!movedHorizontally && !monster.isCloseEnoughToAttack()))));
                        };
                        FlyMonsterMovementMethod.prototype.shouldPlayFlightSound = function () {
                            var monster = this.flyingMonster;
                            var player = monster.map.entities.player;
                            return this.durationBetweenFlightSounds.isDone
                                && monster.map.isLocationVisibleFrom(player.x, player.y, monster.x, monster.y);
                        };
                        FlyMonsterMovementMethod.prototype.canFlyMoveHorizontally = function () {
                            var monster = this.flyingMonster;
                            if (monster.isFlyTurning)
                                return 0;
                            var check = function (dx) {
                                return monster.isPassibleToSide(monster.isFacingLeft ? Entities.Side.Left : Entities.Side.Right, dx);
                            };
                            var dx = Dunjax.getMovementForSpeed(monster.flyMoveSpeed);
                            if (check(dx))
                                return dx;
                            if (check(1))
                                return 1;
                            return 0;
                        };
                        FlyMonsterMovementMethod.prototype.flyMoveHorizontally = function (amount) {
                            var monster = this.flyingMonster;
                            monster.x += amount * (monster.isFacingLeft ? -1 : 1);
                        };
                        FlyMonsterMovementMethod.prototype.onHorizontalFlyMoveBlocked = function () {
                            var monster = this.flyingMonster;
                            var dx = Dunjax.getMovementForSpeed(monster.flyMoveSpeed);
                            if (this.isFlyingHorizontallyToPassObstacle)
                                this.isFlyingHorizontallyToPassObstacle = false;
                            else if (this.canFlyMoveVertically() === 0
                                && !monster.isCloseEnoughToAttack()
                                && Dunjax.randomInt(0, 1) === 0
                                && monster.isPassibleToSide(monster.isFacingLeft ? Entities.Side.Right : Entities.Side.Left, dx)) {
                                monster.isFlyTurning = true;
                                this.isFlyingHorizontallyToPassObstacle = true;
                            }
                        };
                        FlyMonsterMovementMethod.prototype.canFlyMoveVertically = function () {
                            var _this = this;
                            var monster = this.flyingMonster;
                            var check = function (dy) {
                                return monster.isPassibleToVerticalSide(_this.verticalFlightDirection, dy);
                            };
                            var dy = Dunjax.getMovementForSpeed(monster.flyMoveSpeed);
                            if (check.call(this, dy))
                                return dy;
                            if (check.call(this, 1))
                                return 1;
                            return 0;
                        };
                        FlyMonsterMovementMethod.prototype.checkShouldFlyVerticallyToPassObstacle = function (movedHorizontally) {
                            var monster = this.flyingMonster;
                            if (!this.isFlyingVerticallyToPassObstacle
                                && !this.isFlyingHorizontallyToPassObstacle
                                && !movedHorizontally
                                && !this.isPlayerToVerticalSide(monster.height / 2 - 1)
                                && !monster.isFlyTurning
                                && !monster.isCloseEnoughToAttack()) {
                                this.isFlyingVerticallyToPassObstacle = true;
                                this.verticalFlightDirection =
                                    Dunjax.randomInt(0, 1) === 1 ? Entities.VerticalSide.Top : Entities.VerticalSide.Bottom;
                            }
                        };
                        FlyMonsterMovementMethod.prototype.flyMoveVertically = function (amount) {
                            var monster = this.flyingMonster;
                            monster.y += amount * (this.verticalFlightDirection === Entities.VerticalSide.Top ? -1 : 1);
                            if (!monster.isFlyTurning)
                                monster.isFlying = true;
                        };
                        FlyMonsterMovementMethod.prototype.onVerticalFlyMoveBlocked = function (movedHorizontally) {
                            var monster = this.flyingMonster;
                            if (this.isFlyingVerticallyToPassObstacle)
                                this.isFlyingVerticallyToPassObstacle = false;
                            else if (!movedHorizontally && !monster.isCloseEnoughToAttack())
                                this.flyToPassObstacleToVerticalFlight();
                        };
                        FlyMonsterMovementMethod.prototype.flyToPassObstacleToVerticalFlight = function () {
                            var monster = this.flyingMonster;
                            var dx = Dunjax.getMovementForSpeed(monster.flyMoveSpeed);
                            if (!this.isPlayerToSide() || Dunjax.randomInt(0, 2) > 0) {
                                this.isFlyingHorizontallyToPassObstacle = true;
                                if (monster.isPassibleToSide(monster.isFacingLeft ? Entities.Side.Left : Entities.Side.Right, dx)
                                    && Dunjax.randomInt(0, 2) > 0) { }
                                else if (monster.isPassibleToSide(monster.isFacingLeft ? Entities.Side.Right : Entities.Side.Left, dx))
                                    monster.isFlyTurning = true;
                                else
                                    this.isFlyingHorizontallyToPassObstacle = false;
                            }
                            else if (Dunjax.randomInt(0, 1) === 0) {
                                this.isFlyingVerticallyToPassObstacle = true;
                                this.verticalFlightDirection =
                                    (this.verticalFlightDirection === Entities.VerticalSide.Top
                                        ? Entities.VerticalSide.Bottom : Entities.VerticalSide.Top);
                            }
                        };
                        FlyMonsterMovementMethod.prototype.isPlayerToVerticalSide = function (tolerance) {
                            if (tolerance === void 0) { tolerance = 0; }
                            var monster = this.flyingMonster;
                            return monster.getVerticalSideEntityIsOn(monster.map.entities.player, tolerance)
                                !== Entities.VerticalSide.NeitherVerticalSide;
                        };
                        return FlyMonsterMovementMethod;
                    })(Movement.MonsterMovementMethod);
                    Movement.FlyMonsterMovementMethod = FlyMonsterMovementMethod;
                })(Movement = Monsters.Movement || (Monsters.Movement = {}));
            })(Monsters = Beings.Monsters || (Beings.Monsters = {}));
        })(Beings = Entities.Beings || (Entities.Beings = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="movement/MoveAlongGroundOrCeiling.ts" />
/// <reference path="movement/FlyMonsterMovementMethod.ts" />
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Beings;
        (function (Beings) {
            var Monsters;
            (function (Monsters) {
                var MoveAlongGroundOrCeiling = Monsters.Movement.MoveAlongGroundOrCeiling;
                var FlyMonsterMovementMethod = Monsters.Movement.FlyMonsterMovementMethod;
                var GreenGargoyle = (function (_super) {
                    __extends(GreenGargoyle, _super);
                    function GreenGargoyle(x, y) {
                        _super.call(this, x, y);
                        this.monsterType = Monsters.MonsterType.GreenGargoyle;
                        this.groundMovementMethod = new MoveAlongGroundOrCeiling(this);
                        this.currentMovementMethod = this.flyMovementMethod =
                            new FlyMonsterMovementMethod(this);
                        this.health = 1;
                        this.betweenAttacksDuration.length = 300;
                        this.attackDamage = 2;
                        this.moveSpeed = 165;
                        this.flyMoveSpeed = 125;
                        this.beingFrameSequences = this.monsterFrameSequences =
                            this.flyingMonsterFrameSequences = GreenGargoyleFrameSequences.singleton;
                        var sounds = this.beingSounds = this.monsterSounds = this.flyingMonsterSounds =
                            new Monsters.FlyingMonsterSounds();
                        sounds.fly = Dunjax.Program.sounds.greenGargoyleFly;
                    }
                    return GreenGargoyle;
                })(Monsters.FlyingMonster);
                Monsters.GreenGargoyle = GreenGargoyle;
                var GreenGargoyleFrameSequences = (function (_super) {
                    __extends(GreenGargoyleFrameSequences, _super);
                    function GreenGargoyleFrameSequences() {
                        var _this = this;
                        _super.call(this);
                        this.imagesPrefix = "greenGargoyle";
                        var create = function (n, l) { return _this.create.call(_this, n, l); };
                        var createWith = function (n, l, o) {
                            return _this.createWith.call(_this, n, l, o);
                        };
                        var goToStill = function (s) { return s.switchToWhenDone = _this.still; };
                        this.still = create("still", 1);
                        this.move = createWith("move", 6, { isRepeating: true });
                        this.attack = createWith("attack", 4, { repeatedFrames: [-1, -1, -1, 0] });
                        goToStill(this.attack);
                        this.turn = create("turn", 2);
                        goToStill(this.turn);
                        this.carcass = create("carcass", 1);
                        this.death = create("death", 6);
                        this.fly =
                            createWith("fly", 4, { repeatedFrames: [-1, -1, 0, -1], isRepeating: true });
                        this.flyTurn = createWith("flyTurn", 4, { repeatedFrames: [-1, -1, 0, -1] });
                        this.flyTurn.switchToWhenDone = this.fly;
                        this.land = create("land", 1);
                        this.flyDeath = create("flyDeath", 3);
                    }
                    GreenGargoyleFrameSequences.create = function () {
                        GreenGargoyleFrameSequences.singleton = new GreenGargoyleFrameSequences();
                    };
                    return GreenGargoyleFrameSequences;
                })(Monsters.FlyingMonsterFrameSequences);
                Monsters.GreenGargoyleFrameSequences = GreenGargoyleFrameSequences;
                var GreenGargoyleMonsterSounds = (function (_super) {
                    __extends(GreenGargoyleMonsterSounds, _super);
                    function GreenGargoyleMonsterSounds() {
                        _super.call(this);
                        this.fly = Dunjax.Program.sounds.greenGargoyleFly;
                    }
                    return GreenGargoyleMonsterSounds;
                })(Monsters.FlyingMonsterSounds);
                Monsters.GreenGargoyleMonsterSounds = GreenGargoyleMonsterSounds;
            })(Monsters = Beings.Monsters || (Beings.Monsters = {}));
        })(Beings = Entities.Beings || (Entities.Beings = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="../FrameSequences.ts" />
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Shots;
        (function (Shots) {
            var ShotFrameSequences = (function (_super) {
                __extends(ShotFrameSequences, _super);
                function ShotFrameSequences() {
                    _super.call(this);
                    this.imagesPath = "entities/shots/";
                }
                return ShotFrameSequences;
            })(Entities.FrameSequences);
            Shots.ShotFrameSequences = ShotFrameSequences;
        })(Shots = Entities.Shots || (Entities.Shots = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="../MapEntity.ts" />
/// <reference path="ShotFrameSequences.ts" />
/// <reference path="../../time/Duration.ts" />
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Shots;
        (function (Shots) {
            var Duration = Dunjax.Time.Duration;
            var pixelsPerTenFeet = Dunjax.Maps.pixelsPerTenFeet;
            var Shot = (function (_super) {
                __extends(Shot, _super);
                function Shot(shotType, x, y, directionX, directionY) {
                    _super.call(this, x, y);
                    this.directionX = 0;
                    this.directionY = 0;
                    this.isAMover = true;
                    this.isImpacter = true;
                    this.targetBeingType = TargetBeingType.Player;
                    this.impactSound = Dunjax.Program.sounds.shotImpact;
                    this.shotType = shotType;
                    this.isPassible = true;
                    if (directionX !== 0 || directionY !== 0) {
                        var u = Dunjax.Geometry.getUnitVector(0, 0, directionX, directionY);
                        _a = [u.x, u.y], this.directionX = _a[0], this.directionY = _a[1];
                        if (directionY === 0)
                            this.angle = directionX > 0 ? 0 : Math.PI;
                        else
                            this.angle = Math.atan2(this.directionY, this.directionX);
                    }
                    this.distanceUntilDone = this.getRange();
                    this.existenceDuration = new Duration(Number.MAX_VALUE, false);
                    this.existenceDuration.start();
                    var _a;
                }
                Shot.prototype.actAfterAnimationChecked = function () {
                    if (!this.frameSequence)
                        this.frameSequence =
                            this.getFrameSequenceForDirection(this.directionX, this.directionY);
                    this.actBeforeMove();
                    var distanceMoved = 0;
                    if (this.isAMover)
                        distanceMoved = this.moveInCurrentDirection();
                    this.checkForFinish(distanceMoved);
                };
                Shot.prototype.actBeforeMove = function () { };
                Shot.prototype.getFrameSequenceForState = function () { return this.frameSequence; };
                Shot.prototype.getFrameSequenceForDirection = function (dirX, dirY) {
                    var slope = (dirX !== 0) ? dirY / dirX :
                        1000 * (dirY > 0 ? 1 : -1);
                    var sequences = this.shotFrameSequences;
                    this.sprite.scale.x = dirX < 0 ? -1 : 1;
                    if (dirX === 0 && dirY === 0) {
                        return sequences.stationary;
                    }
                    if (slope >= -0.5 && slope <= 0.5) {
                        return sequences.level;
                    }
                    if (slope >= 2.0 || slope <= -2.0) {
                        return dirY < 0 ? sequences.up : sequences.down;
                    }
                    if ((slope > 0.5 && slope < 2.0)
                        || (slope < -0.5 && slope > -2.0)) {
                        return dirY < 0 ? sequences.upAngle : sequences.downAngle;
                    }
                    throw new Error("Direction corresponds to no condition");
                };
                Shot.prototype.getTargetBeingStruck = function () {
                    var entities = this.map.entities;
                    var player = entities.player;
                    var being = (this.targetBeingType === TargetBeingType.Player
                        ? (Dunjax.Geometry.rectContains(player, this.x, this.y) ? player : null)
                        : entities.getMonsterAt(this.x, this.y));
                    if (being == null)
                        return null;
                    return being.isStrikableAt(this.x, this.y) ? being : null;
                };
                Shot.prototype.moveInCurrentDirection = function () {
                    var magnitude = this.speed * Dunjax.Program.game.time.physicsElapsed;
                    this.x += magnitude * Math.cos(this.angle);
                    this.y += magnitude * Math.sin(this.angle);
                    return magnitude;
                };
                Shot.prototype.checkForFinish = function (distanceMovedThisTurn) {
                    if (this.isImpacter && this.checkForTargetBeingStruck())
                        return;
                    if (this.isImpacter && !this.map.isPassible(this.x, this.y)) {
                        this.impacted(null);
                        return;
                    }
                    this.distanceUntilDone -= distanceMovedThisTurn;
                    if (this.distanceUntilDone <= 0) {
                        this.removeFromPlay();
                        return;
                    }
                    if (this.existenceDuration.isDone) {
                        this.removeFromPlay();
                        return;
                    }
                };
                Shot.prototype.checkForTargetBeingStruck = function () {
                    var being = this.getTargetBeingStruck();
                    if (being != null) {
                        this.targetBeingStruck(being);
                        return true;
                    }
                    return false;
                };
                Shot.prototype.targetBeingStruck = function (being) {
                    if (this.impactDamage > 0)
                        being.onStruck(this.impactDamage, this.x, this.y);
                    this.impacted(being);
                };
                Shot.prototype.impacted = function (beingHit) {
                    this.playIfExists(this.impactSound);
                    this.preRemovedFromPlayDueToImpact(beingHit);
                    this.removeFromPlay();
                };
                Shot.prototype.preRemovedFromPlayDueToImpact = function (beingHit) { };
                Shot.prototype.getRange = function () { return 25 * pixelsPerTenFeet; };
                return Shot;
            })(Entities.MapEntity);
            Shots.Shot = Shot;
            (function (TargetBeingType) {
                TargetBeingType[TargetBeingType["Player"] = 0] = "Player";
                TargetBeingType[TargetBeingType["Monster"] = 1] = "Monster";
            })(Shots.TargetBeingType || (Shots.TargetBeingType = {}));
            var TargetBeingType = Shots.TargetBeingType;
            ;
        })(Shots = Entities.Shots || (Entities.Shots = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="Shot.ts" />
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Shots;
        (function (Shots) {
            var FireballShot = (function (_super) {
                __extends(FireballShot, _super);
                function FireballShot(x, y, directionX, directionY) {
                    _super.call(this, Shots.ShotType.FireballShot, x, y, directionX, directionY);
                    this.speed = 333;
                    this.impactDamage = 50;
                    this.shotFrameSequences = FireballShotFrameSequences.singleton;
                    this.targetBeingType = Shots.TargetBeingType.Player;
                    this.impactSound = Dunjax.Program.sounds.fireballImpact;
                }
                FireballShot.prototype.preRemovedFromPlayDueToImpact = function (beingHit) {
                    if (beingHit != null)
                        return;
                    var shot = new Shots.FireballHit(this.x, this.y);
                    this.map.entities.addEntity(shot);
                };
                return FireballShot;
            })(Shots.Shot);
            Shots.FireballShot = FireballShot;
            var FireballShotFrameSequences = (function (_super) {
                __extends(FireballShotFrameSequences, _super);
                function FireballShotFrameSequences() {
                    var _this = this;
                    _super.call(this);
                    this.imagesPrefix = "fireballShot";
                    var create = function (name, length) {
                        return _this.createWith(name, length, { isRepeating: true });
                    };
                    this.downAngle = create("downAngle", 1);
                }
                FireballShotFrameSequences.create = function () {
                    FireballShotFrameSequences.singleton = new FireballShotFrameSequences();
                };
                return FireballShotFrameSequences;
            })(Shots.ShotFrameSequences);
            Shots.FireballShotFrameSequences = FireballShotFrameSequences;
        })(Shots = Entities.Shots || (Entities.Shots = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="Shot.ts" />
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Shots;
        (function (Shots) {
            var SlimeShot = (function (_super) {
                __extends(SlimeShot, _super);
                function SlimeShot(x, y, directionX, directionY) {
                    _super.call(this, Shots.ShotType.SlimeShot, x, y, directionX, directionY);
                    this.speed = 250;
                    this.impactDamage = 50;
                    this.shotFrameSequences = SlimeShotFrameSequences.singleton;
                    this.targetBeingType = Shots.TargetBeingType.Player;
                    this.impactSound = Dunjax.Program.sounds.slimeShotImpact;
                }
                SlimeShot.prototype.preRemovedFromPlayDueToImpact = function (beingHit) {
                    if (beingHit != null)
                        return;
                    var shot = new Shots.SlimeShotHit(this.x, this.y);
                    this.map.entities.addEntity(shot);
                };
                return SlimeShot;
            })(Shots.Shot);
            Shots.SlimeShot = SlimeShot;
            var SlimeShotFrameSequences = (function (_super) {
                __extends(SlimeShotFrameSequences, _super);
                function SlimeShotFrameSequences() {
                    var _this = this;
                    _super.call(this);
                    var create = function (name, length) {
                        return _this.createWith(name, length, { isRepeating: true });
                    };
                    this.imagesPrefix = "slimeShot";
                    this.upAngle = create("upAngle", 1);
                }
                SlimeShotFrameSequences.create = function () {
                    SlimeShotFrameSequences.singleton = new SlimeShotFrameSequences();
                };
                return SlimeShotFrameSequences;
            })(Shots.ShotFrameSequences);
            Shots.SlimeShotFrameSequences = SlimeShotFrameSequences;
        })(Shots = Entities.Shots || (Entities.Shots = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="Monster.ts" />
/// <reference path="../../shots/FireballShot.ts" />
/// <reference path="../../shots/SlimeShot.ts" />
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Beings;
        (function (Beings) {
            var Monsters;
            (function (Monsters) {
                var FireballShot = Entities.Shots.FireballShot;
                var SlimeShot = Entities.Shots.SlimeShot;
                var Master = (function (_super) {
                    __extends(Master, _super);
                    function Master(x, y) {
                        _super.call(this, x, y);
                        this.isFiring = false;
                        this.isSliming = false;
                        this.isPostFiring = false;
                        this.isPostSliming = false;
                        this.isRoaring = false;
                        this.monsterType = Monsters.MonsterType.Master;
                        this.currentMovementMethod = this;
                        this.health = 40;
                        this.betweenAttacksDuration.length = 1500;
                        this.attackDamage = 30;
                        this.isFacingLeft = true;
                        this.isSleeping = true;
                        this.beingFrameSequences = this.monsterFrameSequences =
                            MasterFrameSequences.singleton;
                        this.beingSounds = this.monsterSounds = new Monsters.MonsterSounds();
                    }
                    Master.prototype.onActed = function () { };
                    Master.prototype.moveMonster = function () {
                        return true;
                    };
                    Master.prototype.onBeforeAnimationDoneAsMonster = function (sequence) {
                        if (this.isFiring)
                            this.launchFireballShot();
                        else if (this.isSliming)
                            this.launchSlimeShot();
                        else if (this.isWaking) {
                            this.isWaking = false;
                            this.isRoaring = true;
                            Dunjax.Program.sounds.masterWake.play();
                        }
                        else if (this.isRoaring) {
                            this.isRoaring = false;
                            if (Dunjax.randomInt(0, 1))
                                this.isFiring = true;
                            else
                                this.isSliming = true;
                        }
                        else if (this.isPostFiring) {
                            this.isPostFiring = false;
                            if (!this.map.entities.player.isDead)
                                this.isSliming = true;
                        }
                        else if (this.isPostSliming) {
                            this.isPostSliming = false;
                            this.isFiring = true;
                        }
                        else if (this.isHit)
                            this.finishedBeingHit();
                        else if (this.isDying)
                            this.onKilled();
                    };
                    Master.prototype.launchFireballShot = function () {
                        var dx = -(8 + Dunjax.randomInt(0, 5));
                        var dy = dx + 20;
                        var shot = new FireballShot(this.left - 1, this.y + 16, dx, dy);
                        this.map.entities.addEntity(shot);
                        Dunjax.Program.sounds.fireballShot.play();
                        this.isFiring = false;
                        this.isPostFiring = true;
                    };
                    Master.prototype.launchSlimeShot = function () {
                        var dx = -(8 + Dunjax.randomInt(0, 5));
                        var dy = -(dx + 20);
                        var shot = new SlimeShot(this.left - 1, this.y - this.height / 4 - 16, dx, dy);
                        this.map.entities.addEntity(shot);
                        Dunjax.Program.sounds.slimeballShot.play();
                        this.isSliming = false;
                        this.isPostSliming = true;
                    };
                    Master.prototype.finishedBeingHit = function () {
                        if (this.isSleeping)
                            this.wake();
                        else if (Dunjax.randomInt(0, 1))
                            this.isPostFiring = true;
                        else
                            this.isPostSliming = true;
                    };
                    Master.prototype.wake = function () {
                        this.isSleeping = false;
                        this.isWaking = true;
                    };
                    Master.prototype.isStrikableAtContainedLocation = function (x, y) {
                        if (this.isWaking || this.isRoaring)
                            return false;
                        return y < this.top + this.height / 4 || x > this.left + this.width / 4;
                    };
                    Master.prototype.isVulnerableAtStrikableLocation = function (x, y) {
                        return x > this.left + this.width / 4
                            && y >= this.top + this.height / 4
                            && y <= this.y + 4;
                    };
                    Master.prototype.shouldAlignWithGroundOrCeiling = function () { return false; };
                    Master.prototype.getFrameSequenceForStateAsMonsterSubtype = function (monsterSequenceChosen) {
                        var sequences = MasterFrameSequences.singleton;
                        var sequence = monsterSequenceChosen;
                        if (this.isDead || this.isDying || this.isHit)
                            ;
                        else if (this.isRoaring)
                            sequence = sequences.roar;
                        else if (this.isPostFiring)
                            sequence = sequences.postFire;
                        else if (this.isPostSliming)
                            sequence = sequences.postSlime;
                        else if (this.isFiring)
                            sequence = sequences.fire;
                        else if (this.isSliming)
                            sequence = sequences.slime;
                        return sequence;
                    };
                    Master.prototype.beforeDying = function () {
                        this.isFiring = false;
                        this.isSliming = false;
                        this.isPostFiring = false;
                        this.isPostSliming = false;
                    };
                    Master.prototype.actBeforeActingAsMonster = function () {
                        if (this.isSleeping && this.isCloseEnoughToAttack())
                            this.wake();
                    };
                    return Master;
                })(Monsters.Monster);
                Monsters.Master = Master;
                var MasterFrameSequences = (function (_super) {
                    __extends(MasterFrameSequences, _super);
                    function MasterFrameSequences() {
                        var _this = this;
                        _super.call(this);
                        this.imagesPrefix = "master";
                        var create = function (n, l) { return _this.create.call(_this, n, l); };
                        var createWith = function (n, l, o) {
                            return _this.createWith.call(_this, n, l, o);
                        };
                        this.sleep = create("sleep", 1);
                        this.wake = create("wake", 4);
                        this.still = create("still", 1);
                        this.fire = create("fire", 5);
                        this.slime = create("slime", 4);
                        var images = [
                            this.slime.getFrame(0).image,
                            this.slime.getFrame(1).image,
                        ];
                        this.roar = createWith("roar", 2, { images: images });
                        this.roar.getFrame(1).duration = 1200;
                        images = [
                            this.fire.getFrame(2).image,
                            this.fire.getFrame(1).image,
                            this.fire.getFrame(0).image,
                            this.still.getFrame(0).image,
                        ];
                        this.postFire = createWith("postFire", images.length, { images: images });
                        var restDuration = 3000;
                        this.postFire.getFrame(3).duration = restDuration;
                        images = [
                            this.slime.getFrame(1).image,
                            this.slime.getFrame(0).image,
                            this.still.getFrame(0).image,
                        ];
                        this.postSlime = createWith("postSlime", images.length, { images: images });
                        this.postSlime.getFrame(2).duration = restDuration;
                        this.death = create("death", 7);
                        this.death.setDurationOfAllFrames(100);
                        this.death.getFrame(6).duration = 3000;
                        this.carcass = createWith("carcass", 1, { images: [this.death.getFrame(6).image] });
                        this.death.switchToWhenDone = this.carcass;
                        this.hit = createWith("hit", 1, { images: [this.death.getFrame(0).image] });
                        this.attack = createWith("attack", 1, { images: [this.death.getFrame(0).image] });
                        this.attack.getFrame(0).duration = 500;
                    }
                    MasterFrameSequences.create = function () {
                        MasterFrameSequences.singleton = new MasterFrameSequences();
                    };
                    return MasterFrameSequences;
                })(Monsters.MonsterFrameSequences);
                Monsters.MasterFrameSequences = MasterFrameSequences;
            })(Monsters = Beings.Monsters || (Beings.Monsters = {}));
        })(Beings = Entities.Beings || (Entities.Beings = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="MonsterMovementMethod.ts" />
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Beings;
        (function (Beings) {
            var Monsters;
            (function (Monsters) {
                var Movement;
                (function (Movement) {
                    var Duration = Dunjax.Time.Duration;
                    var FallDownAndUpMovementMethod = (function (_super) {
                        __extends(FallDownAndUpMovementMethod, _super);
                        function FallDownAndUpMovementMethod(monster) {
                            _super.call(this, monster);
                            this.betweenFallsDuration = new Duration(0, false);
                            this.fallMonster = monster;
                            this.groundAndCeilingMovementMethod = new Movement.MoveAlongGroundOrCeiling(monster);
                            if (Dunjax.randomInt(0, 1) === 0)
                                monster.isFalling = true;
                            else
                                monster.isFallingUp = true;
                        }
                        FallDownAndUpMovementMethod.prototype.moveMonster = function () {
                            if (this.couldStartFall())
                                this.moveWhenCouldStartFall();
                            else if (this.fallMonster.isFalling)
                                this.fall(false);
                            else if (this.fallMonster.isFallingUp)
                                this.fall(true);
                            return true;
                        };
                        FallDownAndUpMovementMethod.prototype.couldStartFall = function () {
                            var monster = this.fallMonster;
                            return !monster.isFalling && !monster.isFallingUp && !monster.isDying
                                && !monster.isTurning && !monster.isHit && !monster.isAttacking
                                && !monster.isCloseEnoughToAttack();
                        };
                        FallDownAndUpMovementMethod.prototype.moveWhenCouldStartFall = function () {
                            if (this.betweenFallsDuration.isDone)
                                this.startFall();
                            else
                                this.groundAndCeilingMovementMethod.moveMonster();
                        };
                        FallDownAndUpMovementMethod.prototype.startFall = function () {
                            var monster = this.fallMonster;
                            monster.isMoving = false;
                            if (monster.isOnCeiling)
                                monster.isFalling = true;
                            else
                                monster.isFallingUp = true;
                            monster.onFallStarted();
                        };
                        FallDownAndUpMovementMethod.prototype.fall = function (upward) {
                            var amount = this.canFall(upward);
                            if (amount !== 0) {
                                this.monster.y += amount;
                            }
                            else
                                this.endFall(upward);
                        };
                        FallDownAndUpMovementMethod.prototype.canFall = function (upward) {
                            var monster = this.fallMonster;
                            var x = monster.x;
                            var y = upward ? monster.top : monster.bottom;
                            var width = monster.width;
                            var map = monster.map;
                            var check = function (dy) {
                                return map.isFallThroughable(x - width / 4, y + dy)
                                    && map.isFallThroughable(x + width / 4, y + dy);
                            };
                            var dy = Dunjax.getMovementForSpeed(monster.fallSpeed) * (upward ? -1 : 1);
                            if (check(dy))
                                return dy;
                            dy = upward ? -1 : 1;
                            if (check(dy))
                                return dy;
                            return 0;
                        };
                        FallDownAndUpMovementMethod.prototype.endFall = function (fallWasUpward) {
                            var monster = this.fallMonster;
                            monster.isFalling = monster.isFallingUp = false;
                            var duration = this.betweenFallsDuration;
                            duration.length =
                                FallDownAndUpMovementMethod.betweenFallsDurationLength
                                    * Dunjax.randomReal(0.5, 1.5);
                            this.betweenFallsDuration.start();
                            monster.isOnCeiling = fallWasUpward;
                        };
                        FallDownAndUpMovementMethod.betweenFallsDurationLength = 700;
                        return FallDownAndUpMovementMethod;
                    })(Movement.MonsterMovementMethod);
                    Movement.FallDownAndUpMovementMethod = FallDownAndUpMovementMethod;
                })(Movement = Monsters.Movement || (Monsters.Movement = {}));
            })(Monsters = Beings.Monsters || (Beings.Monsters = {}));
        })(Beings = Entities.Beings || (Entities.Beings = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="Monster.ts" />
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Beings;
        (function (Beings) {
            var Monsters;
            (function (Monsters) {
                var FallDownAndUpMovementMethod = Monsters.Movement.FallDownAndUpMovementMethod;
                var Slime = (function (_super) {
                    __extends(Slime, _super);
                    function Slime(x, y) {
                        _super.call(this, x, y);
                        this.isFalling = false;
                        this.isFallingUp = false;
                        this.fallSpeed = 5 * Dunjax.Maps.tileHeight;
                        this.monsterType = Monsters.MonsterType.Slime;
                        this.currentMovementMethod = new FallDownAndUpMovementMethod(this);
                        this.health = 8;
                        this.betweenAttacksDuration.length = 1000;
                        this.attackDamage = 4;
                        this.moveSpeed = 4 * Dunjax.Maps.tileWidth;
                        this.beingFrameSequences = this.monsterFrameSequences = SlimeFrameSequences.singleton;
                        this.beingSounds = this.monsterSounds = new Monsters.MonsterSounds();
                    }
                    Slime.prototype.getFrameSequenceForStateAsMonsterSubtype = function (monsterSequenceChosen) {
                        var sequences = SlimeFrameSequences.singleton;
                        var sequence = monsterSequenceChosen;
                        if (this.isDead || this.isDying || this.isAttacking)
                            ;
                        else if (this.isFallingUp)
                            sequence = sequences.fallUp;
                        else if (this.isFalling)
                            sequence = sequences.fall;
                        else if (this.isAttacking && this.isFalling)
                            sequence = sequences.ceilingAttack;
                        return sequence;
                    };
                    Slime.prototype.alignSpecially = function () {
                        if (!this.map.isPassible(this.x, this.top - 1))
                            this.isOnCeiling = true;
                    };
                    Slime.prototype.isGroundDwelling = function () { return false; };
                    Slime.prototype.onFallStarted = function () {
                        Dunjax.Program.sounds.slimeJump.play();
                    };
                    return Slime;
                })(Monsters.Monster);
                Monsters.Slime = Slime;
                var SlimeFrameSequences = (function (_super) {
                    __extends(SlimeFrameSequences, _super);
                    function SlimeFrameSequences() {
                        var _this = this;
                        _super.call(this);
                        this.imagesPrefix = "slime";
                        var create = function (n, l) { return _this.create.call(_this, n, l); };
                        var createWith = function (n, l, o) {
                            return _this.createWith.call(_this, n, l, o);
                        };
                        this.still = create("still", 1);
                        this.move = createWith("move", 5, { repeatedFrames: [-1, -1, -1, -1, -1, 1] });
                        this.ceilingMove =
                            createWith("ceilingMove", 5, { repeatedFrames: [-1, -1, -1, -1, -1, 1] });
                        this.fall = create("fall", 4);
                        this.fallUp = create("fallUp", 5);
                        this.ceilingStill = create("ceilingStill", 1);
                        this.death = create("death", 3);
                        this.ceilingDeath = create("ceilingDeath", 3);
                        var images = [
                            this.move.getFrame(2).image,
                            this.move.getFrame(3).image,
                            this.move.getFrame(4).image,
                        ];
                        this.attack = createWith("attack", 3, { images: images });
                        images = [
                            this.ceilingMove.getFrame(2).image,
                            this.ceilingMove.getFrame(3).image,
                            this.ceilingMove.getFrame(4).image,
                        ];
                        this.ceilingAttack = createWith("ceilingAttack", 3, { images: images });
                    }
                    SlimeFrameSequences.create = function () {
                        SlimeFrameSequences.singleton = new SlimeFrameSequences();
                    };
                    return SlimeFrameSequences;
                })(Monsters.MonsterFrameSequences);
                Monsters.SlimeFrameSequences = SlimeFrameSequences;
            })(Monsters = Beings.Monsters || (Beings.Monsters = {}));
        })(Beings = Entities.Beings || (Entities.Beings = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Items;
        (function (Items) {
            (function (ItemType) {
                ItemType[ItemType["PulseAmmo"] = 0] = "PulseAmmo";
                ItemType[ItemType["PulseAmmoBig"] = 1] = "PulseAmmoBig";
                ItemType[ItemType["GrapeshotAmmo"] = 2] = "GrapeshotAmmo";
                ItemType[ItemType["GrapeshotGun"] = 3] = "GrapeshotGun";
                ItemType[ItemType["ShieldPowerUp"] = 4] = "ShieldPowerUp";
                ItemType[ItemType["ShieldPowerPack"] = 5] = "ShieldPowerPack";
                ItemType[ItemType["Key"] = 6] = "Key";
            })(Items.ItemType || (Items.ItemType = {}));
            var ItemType = Items.ItemType;
            ;
            function asItem(entity) {
                return entity.itemType !== undefined ? entity : null;
            }
            Items.asItem = asItem;
        })(Items = Entities.Items || (Entities.Items = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Beings;
        (function (Beings) {
            var Player;
            (function (Player) {
                function asPlayer(being) {
                    return being.onReachedExit != undefined ? being : null;
                }
                Player.asPlayer = asPlayer;
            })(Player = Beings.Player || (Beings.Player = {}));
        })(Beings = Entities.Beings || (Entities.Beings = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
var Dunjax;
(function (Dunjax) {
    var Maps;
    (function (Maps) {
        (function (SlideType) {
            SlideType[SlideType["NotASlide"] = 0] = "NotASlide";
            SlideType[SlideType["Left"] = 1] = "Left";
            SlideType[SlideType["Right"] = 2] = "Right";
        })(Maps.SlideType || (Maps.SlideType = {}));
        var SlideType = Maps.SlideType;
        ;
        Maps.tileWidth = 32, Maps.tileHeight = 32;
        function toTileX(x) {
            return Math.floor(x / Maps.tileWidth);
        }
        Maps.toTileX = toTileX;
        function toTileY(y) {
            return Math.floor(y / Maps.tileHeight);
        }
        Maps.toTileY = toTileY;
        function toTileLeft(x) {
            return x - x % Maps.tileWidth;
        }
        Maps.toTileLeft = toTileLeft;
        function toTileTop(y) {
            return y - y % Maps.tileHeight;
        }
        Maps.toTileTop = toTileTop;
        function toTileMidX(x) {
            return x - x % Maps.tileWidth + Maps.tileWidth / 2;
        }
        Maps.toTileMidX = toTileMidX;
        function toTileMidY(y) {
            return y - y % Maps.tileHeight + Maps.tileHeight / 2;
        }
        Maps.toTileMidY = toTileMidY;
    })(Maps = Dunjax.Maps || (Dunjax.Maps = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="../../../maps/ITileType.ts" />
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Beings;
        (function (Beings) {
            var Player;
            (function (Player) {
                var PlayerSpeeds = (function () {
                    function PlayerSpeeds() {
                    }
                    PlayerSpeeds.move = 7 * Dunjax.Maps.tileWidth;
                    PlayerSpeeds.jump = 5 * Dunjax.Maps.tileHeight;
                    PlayerSpeeds.climb = 4 * Dunjax.Maps.tileHeight;
                    PlayerSpeeds.whirlwindedMove = 2 * PlayerSpeeds.move;
                    return PlayerSpeeds;
                })();
                Player.PlayerSpeeds = PlayerSpeeds;
            })(Player = Beings.Player || (Beings.Player = {}));
        })(Beings = Entities.Beings || (Entities.Beings = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Shots;
        (function (Shots) {
            (function (ShotType) {
                ShotType[ShotType["FireballHit"] = 0] = "FireballHit";
                ShotType[ShotType["FireballShot"] = 1] = "FireballShot";
                ShotType[ShotType["FireSpot"] = 2] = "FireSpot";
                ShotType[ShotType["GrapeshotShot"] = 3] = "GrapeshotShot";
                ShotType[ShotType["HalberdShot"] = 4] = "HalberdShot";
                ShotType[ShotType["PulseShot"] = 5] = "PulseShot";
                ShotType[ShotType["SlimeShotHit"] = 6] = "SlimeShotHit";
                ShotType[ShotType["SlimeShot"] = 7] = "SlimeShot";
                ShotType[ShotType["SlimeSpot"] = 8] = "SlimeSpot";
                ShotType[ShotType["WebShot"] = 9] = "WebShot";
            })(Shots.ShotType || (Shots.ShotType = {}));
            var ShotType = Shots.ShotType;
            ;
            function asShot(entity) {
                return entity.shotType ? entity : null;
            }
            Shots.asShot = asShot;
        })(Shots = Entities.Shots || (Entities.Shots = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Beings;
        (function (Beings) {
            var Player;
            (function (Player) {
                var Ammos;
                (function (Ammos) {
                    var Ammo = (function () {
                        function Ammo() {
                            this.amount = 0;
                        }
                        Ammo.prototype.onFired = function (map, firingX, firingY, toSide, toVerticalSide) {
                            this.amount--;
                            this.createShots(map, firingX, firingY, toSide, toVerticalSide);
                        };
                        Ammo.prototype.add = function (amount) {
                            this.amount += amount;
                        };
                        Ammo.prototype.createShots = function (map, firingX, firingY, toSide, toVerticalSide) { };
                        return Ammo;
                    })();
                    Ammos.Ammo = Ammo;
                })(Ammos = Player.Ammos || (Player.Ammos = {}));
            })(Player = Beings.Player || (Beings.Player = {}));
        })(Beings = Entities.Beings || (Entities.Beings = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="Shot.ts" />
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Shots;
        (function (Shots) {
            var TargetBeingType = Shots.TargetBeingType;
            var PulseShot = (function (_super) {
                __extends(PulseShot, _super);
                function PulseShot(x, y, directionX, directionY) {
                    _super.call(this, Shots.ShotType.PulseShot, x, y, directionX, directionY);
                    this.speed = 600;
                    this.impactDamage = 1;
                    this.shotFrameSequences = PulseShotFrameSequences.singleton;
                    this.targetBeingType = TargetBeingType.Monster;
                }
                return PulseShot;
            })(Shots.Shot);
            Shots.PulseShot = PulseShot;
            var PulseShotFrameSequences = (function (_super) {
                __extends(PulseShotFrameSequences, _super);
                function PulseShotFrameSequences() {
                    var _this = this;
                    _super.call(this);
                    var create = function (name, length) {
                        return _this.createWith(name, length, { isRepeating: true });
                    };
                    this.imagesPrefix = "pulseShot";
                    this.level = create("level", 1),
                        this.up = create("up", 1);
                    this.down = create("down", 1);
                }
                PulseShotFrameSequences.create = function () {
                    PulseShotFrameSequences.singleton = new PulseShotFrameSequences();
                };
                return PulseShotFrameSequences;
            })(Shots.ShotFrameSequences);
            Shots.PulseShotFrameSequences = PulseShotFrameSequences;
        })(Shots = Entities.Shots || (Entities.Shots = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="Ammo.ts" />
/// <reference path="../../../shots/PulseShot.ts" />
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Beings;
        (function (Beings) {
            var Player;
            (function (Player) {
                var Ammos;
                (function (Ammos) {
                    var PulseShot = Entities.Shots.PulseShot;
                    var PulseAmmo = (function (_super) {
                        __extends(PulseAmmo, _super);
                        function PulseAmmo() {
                            _super.call(this);
                            this.type = Ammos.AmmoType.Pulse;
                        }
                        PulseAmmo.prototype.createShots = function (map, firingX, firingY, toSide, toVerticalSide) {
                            var dirX = 0, dirY = 0;
                            if (toVerticalSide === Entities.VerticalSide.Top)
                                dirY = -1;
                            else if (toVerticalSide === Entities.VerticalSide.Bottom)
                                dirY = 1;
                            else if (toSide === Entities.Side.Left)
                                dirX = -1;
                            else
                                dirX = 1;
                            var shot = new PulseShot(firingX, firingY, dirX, dirY);
                            map.entities.addEntity(shot);
                            Dunjax.Program.sounds.pulseFire.play();
                        };
                        return PulseAmmo;
                    })(Ammos.Ammo);
                    Ammos.PulseAmmo = PulseAmmo;
                })(Ammos = Player.Ammos || (Player.Ammos = {}));
            })(Player = Beings.Player || (Beings.Player = {}));
        })(Beings = Entities.Beings || (Entities.Beings = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="Shot.ts" />
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Shots;
        (function (Shots) {
            var TargetBeingType = Shots.TargetBeingType;
            var GrapeshotShot = (function (_super) {
                __extends(GrapeshotShot, _super);
                function GrapeshotShot(x, y, startDirX, startDirY) {
                    _super.call(this, Shots.ShotType.GrapeshotShot, x, y, startDirX, startDirY);
                    this.speed = 500;
                    this.impactDamage = 1;
                    this.shotFrameSequences = GrapeshotShotFrameSequences.singleton;
                    this.targetBeingType = TargetBeingType.Monster;
                }
                return GrapeshotShot;
            })(Shots.Shot);
            Shots.GrapeshotShot = GrapeshotShot;
            var GrapeshotShotFrameSequences = (function (_super) {
                __extends(GrapeshotShotFrameSequences, _super);
                function GrapeshotShotFrameSequences() {
                    var _this = this;
                    _super.call(this);
                    var create = function (name, length) {
                        return _this.createWith(name, length, { isRepeating: true });
                    };
                    this.imagesPrefix = "grapeShot";
                    this.level = create("level", 1),
                        this.up = create("up", 1);
                    this.down = create("down", 1);
                    this.upAngle = create("upAngle", 1);
                    this.downAngle = create("downAngle", 1);
                }
                GrapeshotShotFrameSequences.create = function () {
                    GrapeshotShotFrameSequences.singleton = new GrapeshotShotFrameSequences();
                };
                return GrapeshotShotFrameSequences;
            })(Shots.ShotFrameSequences);
            Shots.GrapeshotShotFrameSequences = GrapeshotShotFrameSequences;
        })(Shots = Entities.Shots || (Entities.Shots = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="../../../shots/IShot.ts" />
/// <reference path="../../../shots/GrapeshotShot.ts" />
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Beings;
        (function (Beings) {
            var Player;
            (function (Player) {
                var Ammos;
                (function (Ammos) {
                    var GrapeshotShot = Entities.Shots.GrapeshotShot;
                    var GrapeshotAmmo = (function (_super) {
                        __extends(GrapeshotAmmo, _super);
                        function GrapeshotAmmo() {
                            _super.call(this);
                            this.type = Ammos.AmmoType.Grapeshot;
                        }
                        GrapeshotAmmo.prototype.createShots = function (map, firingX, firingY, toSide, toVerticalSide) {
                            for (var i = 0; i < 3; i++) {
                                var dirX = 0, dirY = 0;
                                if (toVerticalSide === Entities.VerticalSide.Top) {
                                    dirY = -5;
                                    if (i === 1)
                                        dirX = -1;
                                    if (i === 2)
                                        dirX = 1;
                                }
                                else if (toVerticalSide === Entities.VerticalSide.Bottom) {
                                    dirY = 5;
                                    if (i === 1)
                                        dirX = -1;
                                    if (i === 2)
                                        dirX = 1;
                                }
                                else if (toSide === Entities.Side.Left) {
                                    dirX = -5;
                                    if (i === 1)
                                        dirY = -1;
                                    if (i === 2)
                                        dirY = 1;
                                }
                                else {
                                    dirX = 5;
                                    if (i === 1)
                                        dirY = -1;
                                    if (i === 2)
                                        dirY = 1;
                                }
                                var shot = new GrapeshotShot(firingX, firingY, dirX, dirY);
                                map.entities.addEntity(shot);
                            }
                            Dunjax.Program.sounds.grapeFire.play();
                        };
                        return GrapeshotAmmo;
                    })(Ammos.Ammo);
                    Ammos.GrapeshotAmmo = GrapeshotAmmo;
                })(Ammos = Player.Ammos || (Player.Ammos = {}));
            })(Player = Beings.Player || (Beings.Player = {}));
        })(Beings = Entities.Beings || (Entities.Beings = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="Shot.ts" />
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Shots;
        (function (Shots) {
            var pixelsPerTenFeet = Dunjax.Maps.pixelsPerTenFeet;
            var HalberdShot = (function (_super) {
                __extends(HalberdShot, _super);
                function HalberdShot(x, y, startDirX, startDirY) {
                    _super.call(this, Shots.ShotType.HalberdShot, x, y, startDirX, startDirY);
                    this.speed = 500;
                    this.impactDamage = 3;
                    this.shotFrameSequences = HalberdShotFrameSequences.singleton;
                    this.targetBeingType = Shots.TargetBeingType.Monster;
                }
                HalberdShot.prototype.getRange = function () { return pixelsPerTenFeet; };
                return HalberdShot;
            })(Shots.Shot);
            Shots.HalberdShot = HalberdShot;
            var HalberdShotFrameSequences = (function (_super) {
                __extends(HalberdShotFrameSequences, _super);
                function HalberdShotFrameSequences() {
                    _super.call(this);
                    this.imagesPrefix = "halberdShot";
                    this.level = this.create("level", 1);
                }
                HalberdShotFrameSequences.create = function () {
                    HalberdShotFrameSequences.singleton = new HalberdShotFrameSequences();
                };
                return HalberdShotFrameSequences;
            })(Shots.ShotFrameSequences);
            Shots.HalberdShotFrameSequences = HalberdShotFrameSequences;
        })(Shots = Entities.Shots || (Entities.Shots = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="../Being.ts" />
/// <reference path="../../items/IItem.ts" />
/// <reference path="IPlayer.ts" />
/// <reference path="PlayerSpeeds.ts" />
/// <reference path="../../shots/IShot.ts" />
/// <reference path="ammos/PulseAmmo.ts" />
/// <reference path="ammos/GrapeshotAmmo.ts" />
/// <reference path="../../shots/HalberdShot.ts" />
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Beings;
        (function (Beings) {
            var Player;
            (function (Player_1) {
                var TerrainFeature = Dunjax.Maps.TerrainFeature;
                var ItemType = Entities.Items.ItemType;
                var PulseAmmo = Beings.Player.Ammos.PulseAmmo;
                var GrapeshotAmmo = Beings.Player.Ammos.GrapeshotAmmo;
                var HalberdShot = Entities.Shots.HalberdShot;
                var Player = (function (_super) {
                    __extends(Player, _super);
                    function Player() {
                        _super.call(this, 0, 0);
                        this.isFalling = false;
                        this.isJumping = false;
                        this.isSliding = false;
                        this.isClimbing = false;
                        this.isLanding = false;
                        this.isSwinging = false;
                        this.isWhirlwinded = false;
                        this.isWebbed = false;
                        this.moveSpeed = 0;
                        this.climbSpeed = 0;
                        this.isForcedFalling = false;
                        this.whirlwindedSide = Entities.Side.NeitherSide;
                        this.whirlwindImpactDamage = 6;
                        this.pulseAmmo = new PulseAmmo();
                        this.grapeshotAmmo = new GrapeshotAmmo();
                        this.ammoInUse = this.pulseAmmo;
                        this.durations = new Player_1.PlayerDurations();
                        this.webbedDurationLength = 3000;
                        this.maxJumpHeight = 5.5 * Dunjax.Maps.pixelsPerTenFeet;
                        this.keysHeld = 0;
                        this.isFacingLeft = false;
                        this.pulseAmmo.add(300);
                        this.health = 100;
                        this.beingFrameSequences = this.playerFrameSequences =
                            Player_1.PlayerFrameSequences.singleton;
                        var sounds = this.beingSounds = new Beings.BeingSounds();
                        sounds.death = Dunjax.Program.sounds.playerDeath;
                        sounds.hit = Dunjax.Program.sounds.playerHit;
                    }
                    Player.prototype.canMove = function (toLeft) {
                        if (this.isSliding || this.isWebbed || this.isWhirlwinded || this.isSwinging
                            || this.isForcedFalling)
                            return false;
                        return this.isPassibleToSide(toLeft ? Entities.Side.Left : Entities.Side.Right, 1);
                    };
                    Player.prototype.move = function (toLeft) {
                        this.moveToSide(toLeft);
                        if (!this.isTurningToSide(toLeft))
                            this.decideStateAfterMoveAttempt(toLeft);
                        this.isFacingLeft = toLeft;
                    };
                    Player.prototype.isTurningToSide = function (toLeft) {
                        return this.isTurning && toLeft === this.isFacingLeft;
                    };
                    Player.prototype.decideStateAfterMoveAttempt = function (toLeft) {
                        if (toLeft !== this.isFacingLeft) {
                            this.isTurning = true;
                            this.moveSpeed = Player.startMoveSpeed;
                        }
                        else
                            this.isMoving = true;
                    };
                    Player.prototype.moveToSide = function (toLeft) {
                        var body = this.sprite.body;
                        var speed = this.moveSpeed =
                            this.isClimbing
                                ? Player_1.PlayerSpeeds.climb / 2
                                : Math.min(this.moveSpeed += 10, Player_1.PlayerSpeeds.move);
                        if (toLeft)
                            body.moveLeft(speed);
                        else
                            body.moveRight(speed);
                        if (this.durations.betweenMovementSounds.isDone
                            && !this.map.isFallThroughable(this.x, this.bottom + 1))
                            Dunjax.Program.sounds.playerMove.play();
                        this.isLanding = false;
                        this.animation.isNodGivenToAdvance = true;
                    };
                    Player.prototype.checkToOpenDoor = function () {
                        var x = this.isFacingLeft ? this.left - 1 : this.right + 1;
                        if (this.isOpenableDoorAt(x, this.y))
                            this.openDoorAt(x, this.y);
                    };
                    Player.prototype.isOpenableDoorAt = function (x, y) {
                        var feature = this.map.getTerrainFeatureAt(x, y);
                        return feature === TerrainFeature.ClosedDoor ||
                            (feature === TerrainFeature.LockedDoor && this.keysHeld > 0);
                    };
                    Player.prototype.openDoorAt = function (x, y) {
                        if (this.map.getTerrainFeatureAt(x, y) === TerrainFeature.LockedDoor)
                            this.keysHeld--;
                        this.map.setTerrainFeatureAt(x, y, TerrainFeature.OpenDoor);
                        Dunjax.Program.sounds.door.play();
                    };
                    Player.prototype.canJump = function () {
                        if (this.isFalling || this.isJumping || this.isWebbed || this.isOnLadder(false)
                            || this.isSliding)
                            return false;
                        var map = this.map;
                        return !map.isFallThroughable(this.left, this.bottom + 1)
                            || !map.isFallThroughable(this.right, this.bottom + 1);
                    };
                    Player.prototype.jump = function (toSide) {
                        this.isJumping = true;
                        this.jumpDistanceSoFar = 0;
                        Dunjax.Program.sounds.playerJump.play();
                    };
                    Player.prototype.checkForClimbing = function () {
                        if (this.isClimbing)
                            if (this.isOnLadder(false))
                                this.makeXStill();
                            else
                                this.isClimbing = false;
                        if (this.isAboveLadder())
                            this.makeXStill();
                    };
                    Player.prototype.isAboveLadder = function () {
                        return this.isOnLadder(true) && !this.isOnLadder(false);
                    };
                    Player.prototype.canClimbOrDescend = function (climb) {
                        if (this.isWebbed)
                            return false;
                        var y = climb ? this.top - 1 : this.bottom + 1;
                        var result = this.isOnLadder(!climb)
                            && this.map.isPassible(this.left + 4, y, true)
                            && this.map.isPassible(this.right - 4, y, true);
                        return result;
                    };
                    Player.prototype.climbOrDescend = function (climb) {
                        var body = this.sprite.body;
                        var speed = Math.min(this.climbSpeed += 5, Player_1.PlayerSpeeds.climb);
                        if (climb)
                            body.moveUp(speed);
                        else
                            body.moveDown(speed);
                        this.isClimbing = true;
                        this.animation.isNodGivenToAdvance = true;
                        Dunjax.Program.sounds.playerClimb.play();
                    };
                    Player.prototype.isOnLadder = function (orAbove) {
                        var ladder = TerrainFeature.Ladder;
                        var map = this.map;
                        return map.getTerrainFeatureAt(this.x, this.top) === ladder
                            || map.getTerrainFeatureAt(this.x, this.y) === ladder
                            || map.getTerrainFeatureAt(this.x, this.bottom + (orAbove ? 1 : 0)) === ladder;
                    };
                    Player.prototype.canFire = function () {
                        if (this.isSwinging || this.isWebbed)
                            return false;
                        if (!this.durations.betweenFires.isDone)
                            return false;
                        if (this.ammoInUse.amount <= 0)
                            return false;
                        return true;
                    };
                    Player.prototype.fire = function (toSide) {
                        var spot = this.getFireSpot(toSide);
                        this.ammoInUse.onFired(this.map, spot.x, spot.y, this.isFacingLeft ? Entities.Side.Left : Entities.Side.Right, toSide);
                        this.postFireFrameSequence = this.getPostFireFrameSequence(toSide);
                    };
                    Player.prototype.getFireSpot = function (toSide) {
                        var _a = [this.x, this.y], spotX = _a[0], spotY = _a[1];
                        if (toSide === Entities.VerticalSide.Top || toSide === Entities.VerticalSide.Bottom) {
                            spotX += this.isFacingLeft && !this.isOnLadder(false) ? -2 : 4;
                            spotY = (toSide === Entities.VerticalSide.Top) ? this.top - 1 : this.bottom + 1;
                        }
                        else {
                            spotX = this.isFacingLeft ? this.left - 1 : this.right + 1;
                            spotY -= this.isJumping ? 8 : 3;
                        }
                        return { x: spotX, y: spotY };
                    };
                    Player.prototype.getPostFireFrameSequence = function (fireSide) {
                        var firingDown = (fireSide === Entities.VerticalSide.Bottom);
                        var firingUp = (fireSide === Entities.VerticalSide.Top);
                        var sequence = null;
                        var sequences = this.playerFrameSequences;
                        if (this.isJumping && fireSide === Entities.VerticalSide.NeitherVerticalSide) { }
                        else if (this.isSliding) {
                            sequence = firingUp ? sequences.fireUpOnSlide : sequences.fireOnSlide;
                        }
                        else if (this.isOnLadder(false)) {
                            if (firingUp)
                                sequence = sequences.fireUpOnLadder;
                            else if (firingDown)
                                sequence = sequences.fireDownOnLadder;
                            else
                                sequence = sequences.fireOnLadder;
                        }
                        else {
                            if (firingUp)
                                sequence = sequences.fireUp;
                            else if (firingDown)
                                sequence = sequences.fireDown;
                            else
                                sequence = sequences.fire;
                        }
                        return sequence;
                    };
                    Player.prototype.checkForFallAsPlayer = function () {
                        if (this.canFallAsPlayer())
                            this.isFalling = true;
                        else if (this.isFalling && this.cantFallDueToBlocked && !this.isDead
                            && !this.isDying && !this.isWhirlwinded)
                            this.land();
                    };
                    Player.prototype.canFallAsPlayer = function () {
                        this.cantFallDueToBlocked = false;
                        this.isForcedFalling = false;
                        if (this.isClimbing || this.isJumping || this.isSliding || this.isWhirlwinded)
                            return false;
                        if (this.checkForMissedHole())
                            return true;
                        var y = Math.floor(this.bottom) + 1;
                        var map = this.map;
                        if (!map.isFallThroughable(this.left, y)
                            || !map.isFallThroughable(this.right, y)) {
                            this.cantFallDueToBlocked = true;
                            return false;
                        }
                        return true;
                    };
                    Player.prototype.checkForMissedHole = function () {
                        var dx = Math.floor(this.sprite.deltaX);
                        var absDx = Math.abs(dx);
                        if (this.isFalling || absDx <= 1)
                            return false;
                        var y = Math.floor(this.bottom) + 1;
                        var map = this.map;
                        for (var i = 0; i <= absDx; i++) {
                            var step = dx < 0 ? -i : i;
                            var testLeft = Math.floor(this.left) - dx + step;
                            var testRight = Math.floor(this.right) - dx + step;
                            if (map.isFallThroughable(testLeft, y)
                                && !map.isFallThroughable(testLeft - 1, y)
                                && map.isFallThroughable(testRight, y)
                                && !map.isFallThroughable(testRight + 1, y)) {
                                this.x = testLeft + this.width / 2;
                                this.y += 4;
                                this.isForcedFalling = true;
                                return true;
                            }
                        }
                        return false;
                    };
                    Player.prototype.land = function () {
                        this.isFalling = false;
                        this.isLanding = true;
                        Dunjax.Program.sounds.playerLand.play();
                    };
                    Player.prototype.checkForWhirlwindedMove = function () {
                        if (!this.hasWhirlwindedMoveDue())
                            return;
                        var body = this.sprite.body;
                        var speed = Player_1.PlayerSpeeds.whirlwindedMove;
                        if (this.whirlwindedSide === Entities.Side.Left)
                            body.moveLeft(speed);
                        else
                            body.moveRight(speed);
                        var dx = Dunjax.getMovementForSpeed(speed);
                        if (!this.isPassibleToSide(this.whirlwindedSide, dx, false)) {
                            this.endWhirlwinded();
                            Dunjax.Program.sounds.shotImpact.play();
                            this.onStruck(this.whirlwindImpactDamage);
                        }
                    };
                    Player.prototype.hasWhirlwindedMoveDue = function () {
                        return this.isWhirlwinded && this.durations.betweenWhirlwindedMoves.isDone;
                    };
                    Player.prototype.checkForWhirlwindedVerticalMove = function () {
                        if (!this.hasWhirlwindedVerticalMoveDue())
                            return;
                        var body = this.sprite.body;
                        var speed = Player_1.PlayerSpeeds.whirlwindedMove;
                        body.moveUp(speed);
                        if (!this.isPassibleToVerticalSide(Entities.VerticalSide.Top, 0)) {
                            this.whirlwindedVerticalMoveEnded = true;
                            Dunjax.Program.sounds.shotImpact.play();
                            this.onStruck(this.whirlwindImpactDamage);
                        }
                    };
                    Player.prototype.hasWhirlwindedVerticalMoveDue = function () {
                        return this.isWhirlwinded
                            && !this.whirlwindedVerticalMoveEnded
                            && this.durations.betweenWhirlwindedVerticalMoves.isDone;
                    };
                    Player.prototype.checkForSlide = function () {
                        if (this.canSlideToSide(Entities.Side.Left))
                            this.slide(Entities.Side.Left);
                        else if (this.canSlideToSide(Entities.Side.Right))
                            this.slide(Entities.Side.Right);
                        else if (this.isSliding)
                            this.isSliding = false;
                        var sound = Dunjax.Program.sounds.playerSlide;
                        if (!this.isSliding && sound.isPlaying)
                            sound.stop();
                    };
                    Player.prototype.canSlideToSide = function (toSide) {
                        return this.isSlideUnderneath(toSide === Entities.Side.Left);
                    };
                    Player.prototype.isSlideUnderneath = function (toLeft) {
                        var slide = toLeft ? TerrainFeature.SlideLeft : TerrainFeature.SlideRight;
                        return this.map.getTerrainFeatureAt(toLeft ? this.right : this.left, this.bottom + 3) === slide
                            || this.map.getTerrainFeatureAt(toLeft ? this.right : this.left, this.bottom + 1) === slide;
                    };
                    Player.prototype.slide = function (toSide) {
                        var toLeft = (toSide === Entities.Side.Left);
                        this.isFacingLeft = toLeft;
                        this.isSliding = true;
                        if (this.durations.betweenSlidesSounds.isDone)
                            Dunjax.Program.sounds.playerSlide.play();
                        this.isFalling = false;
                        this.isLanding = false;
                    };
                    Player.prototype.canTryToJumpMove = function () {
                        if (!this.isJumping || this.isSliding)
                            return false;
                        return true;
                    };
                    Player.prototype.tryToJumpMove = function (jumpForceStillBeingAdded) {
                        var isBlocked = this.isJumpMoveBlocked();
                        if (!isBlocked)
                            this.sprite.body.moveUp(Player_1.PlayerSpeeds.jump);
                        else
                            this.jumpMoveBlocked(jumpForceStillBeingAdded);
                        this.jumpDistanceSoFar += isBlocked ? 4 : Math.abs(this.sprite.deltaY);
                        if (this.isJumping && this.durations.betweenThrustSounds.isDone)
                            Dunjax.Program.sounds.playerThrust.play();
                    };
                    Player.prototype.hasMaxJumpHeightBeenReached = function (jumpForceStillBeingAdded) {
                        return this.isJumping &&
                            (this.jumpDistanceSoFar >= this.maxJumpHeight || !jumpForceStillBeingAdded);
                    };
                    Player.prototype.isJumpMoveBlocked = function () {
                        var jumpFudgeMargin = Math.abs(this.sprite.deltaX);
                        var map = this.map;
                        var y = this.top - 1;
                        return !map.isPassible(this.left + jumpFudgeMargin, y, true)
                            || !map.isPassible(this.right - jumpFudgeMargin, y, true);
                    };
                    Player.prototype.jumpMoveBlocked = function (jumpForceStillBeingAdded) {
                        if (!jumpForceStillBeingAdded)
                            this.endJump();
                    };
                    Player.prototype.endJump = function () {
                        this.isJumping = false;
                        Dunjax.Program.sounds.playerThrust.stop();
                    };
                    Player.prototype.canSwingMeleeWeapon = function () {
                        if (this.isSwinging || this.isWebbed || this.isWhirlwinded || this.isClimbing)
                            return false;
                        return this.durations.betweenSwings.isDone;
                    };
                    Player.prototype.swingMeleeWeapon = function () {
                        this.isSwinging = true;
                    };
                    Player.prototype.canSwitchGuns = function () {
                        return this.durations.betweenGunSwitches.isDone;
                    };
                    Player.prototype.switchGuns = function () {
                        var oldAmmo = this.ammoInUse;
                        if (this.ammoInUse === this.pulseAmmo && this.hasGrapeshotGun)
                            this.ammoInUse = this.grapeshotAmmo;
                        else
                            this.ammoInUse = this.pulseAmmo;
                        if (this.ammoInUse !== oldAmmo)
                            Dunjax.Program.sounds.playerSwitchGuns.play();
                    };
                    Player.prototype.onWebbed = function () {
                        if (this.isWebbed)
                            return;
                        this.isWebbed = true;
                        Dunjax.Program.sounds.playerWebbed.play();
                    };
                    Player.prototype.checkForTerrainFeature = function () {
                        var feature = this.map.getTerrainFeatureAt(this.x, this.y);
                        if (this.isJumping
                            && feature === TerrainFeature.Stalagmite
                            && this.durations.betweenStalagtiteHits.isDone) {
                            this.onStruck(4);
                        }
                        if (feature === TerrainFeature.ExitCave) {
                            this.onReachedExit();
                            return;
                        }
                        if (feature === TerrainFeature.SlimePool
                            && this.durations.betweenSlimePoolHits.isDone) {
                            this.onStruck(16);
                        }
                        if (feature === TerrainFeature.Spikes
                            && this.durations.betweenSpikesHits.isDone) {
                            this.onStruck(12);
                        }
                    };
                    Player.prototype.beforeSurvivedStrike = function () {
                        this.endJump();
                    };
                    Player.prototype.checkForItem = function () {
                        var item = this.map.entities.getItemInBounds(this);
                        if (item == null)
                            return;
                        this.acceptItemEffects(item);
                        item.onConsumed();
                    };
                    Player.prototype.acceptItemEffects = function (item) {
                        var type = item.itemType;
                        if (type === ItemType.PulseAmmo)
                            this.pulseAmmo.add(30);
                        else if (type === ItemType.PulseAmmoBig) {
                            this.pulseAmmo.add(100);
                            while (this.ammoInUse !== this.pulseAmmo)
                                this.switchGuns();
                        }
                        else if (type === ItemType.GrapeshotAmmo)
                            this.grapeshotAmmo.add(30);
                        else if (type === ItemType.GrapeshotGun) {
                            this.grapeshotAmmo.add(100);
                            this.hasGrapeshotGun = true;
                            while (this.ammoInUse !== this.grapeshotAmmo)
                                this.switchGuns();
                        }
                        else if (type === ItemType.ShieldPowerUp)
                            this.health += 50;
                        else if (type === ItemType.ShieldPowerPack)
                            this.health += 200;
                        else if (type === ItemType.Key)
                            this.keysHeld++;
                    };
                    Player.prototype.actAfterAnimationCheckedAsBeingSubtype = function () {
                        this.actAsPlayer();
                    };
                    Player.prototype.actAsPlayer = function () {
                        if (this.isDead || this.isDying) {
                            this.actWhenDyingOrDead();
                            return;
                        }
                        this.endNonterminatedStates();
                        this.checkForSlide();
                        this.checkForFallAsPlayer();
                        var userInput = Dunjax.Program.userInput;
                        var jumpSignaled = userInput.isSignaled(Dunjax.Command.Jump);
                        if (this.hasMaxJumpHeightBeenReached(jumpSignaled))
                            this.endJump();
                        if (this.canTryToJumpMove())
                            this.tryToJumpMove(jumpSignaled);
                        this.checkForWhirlwindedMove();
                        this.checkForWhirlwindedVerticalMove();
                        if (this.shouldNoLongerBeWhirlwinded())
                            this.endWhirlwinded();
                        this.checkForClimbing();
                        var upSignalled = userInput.isSignaled(Dunjax.Command.Up), downSignalled = userInput.isSignaled(Dunjax.Command.Down);
                        if (upSignalled || downSignalled) {
                            if (this.canClimbOrDescend(upSignalled))
                                this.climbOrDescend(upSignalled);
                            else if (this.isClimbing)
                                this.makeStill();
                        }
                        else {
                            if (this.isClimbing)
                                this.makeStill();
                            this.climbSpeed = Player.startClimbSpeed;
                        }
                        if (userInput.isSignaled(Dunjax.Command.SwitchGuns)) {
                            if (this.canSwitchGuns())
                                this.switchGuns();
                        }
                        if (userInput.isSignaled(Dunjax.Command.Swing)) {
                            if (this.canSwingMeleeWeapon())
                                this.swingMeleeWeapon();
                        }
                        var leftSignaled = userInput.isSignaled(Dunjax.Command.Left), rightSignaled = userInput.isSignaled(Dunjax.Command.Right);
                        if (jumpSignaled) {
                            var side = leftSignaled ? Entities.Side.Left :
                                (rightSignaled ? Entities.Side.Right : Entities.Side.NeitherSide);
                            if (this.canJump())
                                this.jump(side);
                        }
                        if (userInput.isSignaled(Dunjax.Command.Fire))
                            if (this.canFire())
                                this.fire(upSignalled
                                    ? Entities.VerticalSide.Top
                                    : (downSignalled
                                        ? Entities.VerticalSide.Bottom : Entities.VerticalSide.NeitherVerticalSide));
                            else
                                this.checkForOutOfAmmoSound();
                        this.checkForItem();
                        this.checkForTerrainFeature();
                        this.checkForSpecialEntity();
                        if (leftSignaled || rightSignaled) {
                            if (this.canMove(leftSignaled))
                                this.move(leftSignaled);
                            else {
                                this.isMoving = false;
                                if (this.isFacingLeft !== leftSignaled) {
                                    this.isTurning = true;
                                    this.isFacingLeft = leftSignaled;
                                }
                            }
                            this.checkToOpenDoor();
                        }
                        else {
                            this.isMoving = false;
                            this.moveSpeed = Player.startMoveSpeed;
                        }
                        this.determineGravity();
                        this.determineFriction();
                        this.determineBounce();
                    };
                    Player.prototype.endNonterminatedStates = function () {
                        var sequence = this.animation.frameSequence;
                        var sequences = this.playerFrameSequences;
                        if (this.isLanding && sequence !== sequences.land)
                            this.isLanding = false;
                        if (this.isSwinging && (sequence !== sequences.swing || this.animation.isDone))
                            this.isSwinging = false;
                    };
                    Player.prototype.actWhenDyingOrDead = function () {
                        this.checkForSlide();
                        this.determineGravity();
                        this.determineFriction();
                        this.determineBounce();
                    };
                    Player.prototype.makeStill = function () {
                        this.sprite.body.setZeroVelocity();
                    };
                    Player.prototype.makeXStill = function () {
                        this.sprite.body.shape.oldpos.x = this.sprite.body.shape.pos.x;
                    };
                    Player.prototype.onWhirlwinded = function (toSide) {
                        if (this.isWhirlwinded)
                            return;
                        this.whirlwindedSide = toSide;
                        this.isWhirlwinded = true;
                        this.whirlwindedVerticalMoveEnded = false;
                        this.durations.whirlwindEffect.start();
                        Dunjax.Program.sounds.playerWhirlwinded.play();
                    };
                    Player.prototype.shouldNoLongerBeWhirlwinded = function () {
                        return this.isWhirlwinded && this.durations.whirlwindEffect.isDone;
                    };
                    Player.prototype.endWhirlwinded = function () {
                        this.whirlwindedSide = Entities.Side.NeitherSide;
                        this.isWhirlwinded = false;
                    };
                    Player.prototype.onBeforeAnimationDoneAsBeing = function (sequence) {
                        var sequences = this.playerFrameSequences;
                        if (sequence === sequences.swing)
                            this.onMeleeWeaponSwung();
                        else if (sequence === sequences.land)
                            this.isLanding = false;
                        else if (sequence === sequences.webbed) {
                            this.isWebbed = false;
                            this.isMoving = true;
                        }
                        if (sequence === sequences.death)
                            this.onHasDied();
                        if (sequence === this.postFireFrameSequence)
                            this.postFireFrameSequence = null;
                    };
                    Player.prototype.onMeleeWeaponSwung = function () {
                        var dir = this.getHalberdShotDirection();
                        var loc = this.getHalberdShotLocation();
                        var shot = new HalberdShot(loc.x, loc.y, dir.x, dir.y);
                        this.map.entities.addEntity(shot);
                        Dunjax.Program.sounds.halberdFire.play();
                        this.isSwinging = false;
                    };
                    Player.prototype.getHalberdShotDirection = function () {
                        return { x: this.isFacingLeft ? -1 : 1, y: 0 };
                    };
                    Player.prototype.getHalberdShotLocation = function () {
                        return { x: this.isFacingLeft ? this.left : this.right, y: this.y };
                    };
                    Player.prototype.getFrameSequenceForStateAsBeingSubtype = function (beingSequenceChosen) {
                        var sequences = this.playerFrameSequences;
                        var sequence = beingSequenceChosen;
                        var postFireSequence = this.postFireFrameSequence;
                        if (this.isDead || this.isDying)
                            ;
                        else if (this.isWebbed)
                            sequence = sequences.webbed;
                        else if (this.isWhirlwinded)
                            sequence = sequences.whirlwinded;
                        else if (this.isSwinging)
                            sequence = sequences.swing;
                        else if (this.isHit)
                            ;
                        else if (this.isClimbing && postFireSequence == null)
                            sequence = sequences.climb;
                        else if (this.isSliding)
                            sequence = sequences.slide;
                        else if (this.isJumping)
                            sequence = sequences.jump;
                        else if (this.isLanding && postFireSequence == null)
                            sequence = sequences.land;
                        else if (this.isMoving)
                            sequence = sequences.move;
                        else if (postFireSequence != null)
                            sequence = postFireSequence;
                        if (postFireSequence != null && sequence !== postFireSequence)
                            this.postFireFrameSequence = null;
                        return sequence;
                    };
                    Player.prototype.beforeDying = function () {
                        Dunjax.Program.sounds.playerThrust.stop();
                        Dunjax.Program.sounds.playerSlide.stop();
                    };
                    Player.prototype.checkForOutOfAmmoSound = function () {
                        if (this.ammoInUse.amount <= 0)
                            Dunjax.Program.sounds.outOfAmmo.play();
                    };
                    Object.defineProperty(Player.prototype, "armorLeft", {
                        get: function () { return this.health; },
                        enumerable: true,
                        configurable: true
                    });
                    Player.prototype.checkForSpecialEntity = function () {
                        var entity = this.map.entities.getAnimationEntityInBounds(this);
                        if (entity == null)
                            return;
                        if (entity.animationEntityType === Entities.AnimationEntityType.FinalRoomTrigger) {
                            this.onReachedFinalRoomTrigger();
                            this.map.entities.removeEntity(entity);
                        }
                    };
                    Player.prototype.determineGravity = function () {
                        var gravity = 1;
                        if (this.isSliding || this.isDying || this.isDead)
                            ;
                        else if (this.isJumping || this.isClimbing)
                            gravity = 0;
                        var body = this.sprite.body;
                        body.gravityScale = gravity;
                    };
                    Player.prototype.determineFriction = function () {
                        var friction = 0.2;
                        if (this.isClimbing || this.isDying || this.isDead)
                            friction = 1;
                        else if (this.isSliding || this.isFalling)
                            friction = 0;
                        var body = this.sprite.body;
                        body.friction = friction;
                    };
                    Player.prototype.determineBounce = function () {
                        var bounce = 0.2;
                        if (this.isClimbing || this.isOnLadder(true) || this.isSliding || this.isJumping
                            || this.isDying || this.isDead)
                            bounce = 0;
                        var body = this.sprite.body;
                        body.bounce = bounce;
                    };
                    Player.prototype.canFall = function () {
                        return 0;
                    };
                    Player.startMoveSpeed = 80;
                    Player.startClimbSpeed = 0;
                    return Player;
                })(Beings.Being);
                Player_1.Player = Player;
            })(Player = Beings.Player || (Beings.Player = {}));
        })(Beings = Entities.Beings || (Entities.Beings = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="Shot.ts" />
/// <reference path="../beings/player/Player.ts" />
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Shots;
        (function (Shots) {
            var Player = Entities.Beings.Player;
            var WebShot = (function (_super) {
                __extends(WebShot, _super);
                function WebShot(x, y, startDirX, startDirY) {
                    _super.call(this, Shots.ShotType.PulseShot, x, y, startDirX, startDirY);
                    this.speed = 333;
                    this.impactDamage = 0;
                    this.shotFrameSequences = WebShotFrameSequences.singleton;
                    this.targetBeingType = Shots.TargetBeingType.Player;
                    this.impactSound = Dunjax.Program.sounds.webImpact;
                }
                WebShot.prototype.preRemovedFromPlayDueToImpact = function (beingHit) {
                    if (beingHit == null)
                        return;
                    var player = Player.asPlayer(beingHit);
                    if (player != null)
                        player.onWebbed();
                };
                return WebShot;
            })(Shots.Shot);
            Shots.WebShot = WebShot;
            var WebShotFrameSequences = (function (_super) {
                __extends(WebShotFrameSequences, _super);
                function WebShotFrameSequences() {
                    var _this = this;
                    _super.call(this);
                    var create = function (name, length) {
                        return _this.createWith(name, length, { isRepeating: true });
                    };
                    this.imagesPrefix = "webShot";
                    this.level = this.downAngle = this.down = create("level", 1);
                    this.up = this.upAngle = create("upAngle", 1);
                }
                WebShotFrameSequences.create = function () {
                    WebShotFrameSequences.singleton = new WebShotFrameSequences();
                };
                return WebShotFrameSequences;
            })(Shots.ShotFrameSequences);
            Shots.WebShotFrameSequences = WebShotFrameSequences;
        })(Shots = Entities.Shots || (Entities.Shots = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="Monster.ts" />
/// <reference path="../../shots/WebShot.ts" />
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Beings;
        (function (Beings) {
            var Monsters;
            (function (Monsters) {
                var MoveAlongGroundOrCeiling = Monsters.Movement.MoveAlongGroundOrCeiling;
                var Duration = Dunjax.Time.Duration;
                var WebShot = Entities.Shots.WebShot;
                var Spider = (function (_super) {
                    __extends(Spider, _super);
                    function Spider(x, y) {
                        _super.call(this, x, y);
                        this.betweenFiresDuration = new Duration(Spider.betweenFiresDurationLength);
                        this.monsterType = Monsters.MonsterType.Spider;
                        this.currentMovementMethod = new MoveAlongGroundOrCeiling(this);
                        this.health = 20;
                        this.attackDamage = 8;
                        this.moveSpeed = 167;
                        this.isSleeping = true;
                        this.beingFrameSequences = this.monsterFrameSequences =
                            SpiderFrameSequences.singleton;
                        this.beingSounds = this.monsterSounds = new Monsters.MonsterSounds();
                    }
                    Spider.prototype.actBeforeActingAsMonster = function () {
                        if (this.isSleeping && this.shouldWake())
                            this.wake();
                        if (this.canFire())
                            this.fire();
                    };
                    Spider.prototype.shouldWake = function () {
                        var player = this.map.entities.player;
                        return Math.abs(this.x - player.x) < 3 * Dunjax.Maps.pixelsPerTenFeet
                            && Math.abs(this.y - player.y) < 3 * Dunjax.Maps.pixelsPerTenFeet
                            && this.map.isLocationVisibleFrom(player.x, player.y, this.x, this.y);
                    };
                    Spider.prototype.wake = function () {
                        this.isSleeping = false;
                        this.isWaking = true;
                        this.alignWithSurroundings();
                        Dunjax.Program.sounds.spiderWake.play();
                        this.betweenFiresDuration.start();
                    };
                    Spider.prototype.canFire = function () {
                        var player = this.map.entities.player;
                        if (this.isDead || this.isSleeping || this.isWaking
                            || !this.betweenFiresDuration.isDone || player.isDead)
                            return false;
                        var front = this.getFrontLocation();
                        return this.map.isLocationVisibleFrom(player.x, player.y, front.x, front.y);
                    };
                    Spider.prototype.getFrontLocation = function () {
                        return {
                            x: this.isFacingLeft ? this.left + 4 : this.right - 4,
                            y: this.y
                        };
                    };
                    Spider.prototype.onBeforeAnimationDoneAsMonster = function (sequence) {
                        if (sequence === this.monsterFrameSequences.wake)
                            this.isWaking = false;
                    };
                    Spider.prototype.isStrikableAtContainedLocation = function (x, y) {
                        return !this.isSleeping && !this.isWaking;
                    };
                    Spider.prototype.fire = function () {
                        var front = this.getFrontLocation();
                        var player = this.map.entities.player;
                        var shot = new WebShot(front.x, front.y, player.x - front.x, player.y - front.y);
                        this.map.entities.addEntity(shot);
                        Dunjax.Program.sounds.webFire.play();
                        this.betweenFiresDuration.length =
                            Spider.betweenFiresDurationLength / 2 +
                                Dunjax.randomInt(0, Spider.betweenFiresDurationLength);
                        this.betweenFiresDuration.start();
                    };
                    Spider.prototype.shouldAlignWithGroundOrCeiling = function () {
                        return !this.isSleeping;
                    };
                    Spider.prototype.alignSpecially = function () {
                        this.x -= this.left % Dunjax.Maps.pixelsPerTenFeet;
                        this.y -= this.top % Dunjax.Maps.pixelsPerTenFeet;
                    };
                    Spider.betweenFiresDurationLength = 1500;
                    return Spider;
                })(Monsters.Monster);
                Monsters.Spider = Spider;
                var SpiderFrameSequences = (function (_super) {
                    __extends(SpiderFrameSequences, _super);
                    function SpiderFrameSequences() {
                        var _this = this;
                        _super.call(this);
                        this.imagesPrefix = "spider";
                        var create = function (n, l) { return _this.create.call(_this, n, l); };
                        var createWith = function (n, l, o) {
                            return _this.createWith.call(_this, n, l, o);
                        };
                        var goToStill = function (s) { return s.switchToWhenDone = _this.still; };
                        this.sleep = createWith("sleep", 1, { images: [Dunjax.Program.emptyImage] });
                        this.wake = create("wake", 7);
                        this.still = create("still", 1);
                        this.move = create("move", 6);
                        this.turn = create("turn", 1);
                        goToStill(this.turn);
                        this.carcass = create("carcass", 1);
                        this.death = create("death", 3);
                        this.death.switchToWhenDone = this.carcass;
                        var images = [
                            this.move.getFrame(3).image,
                            this.move.getFrame(4).image,
                            this.move.getFrame(5).image
                        ];
                        this.attack = createWith("attack", 3, { images: images });
                        goToStill(this.attack);
                    }
                    SpiderFrameSequences.create = function () {
                        SpiderFrameSequences.singleton = new SpiderFrameSequences();
                    };
                    return SpiderFrameSequences;
                })(Monsters.MonsterFrameSequences);
                Monsters.SpiderFrameSequences = SpiderFrameSequences;
            })(Monsters = Beings.Monsters || (Beings.Monsters = {}));
        })(Beings = Entities.Beings || (Entities.Beings = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="Monster.ts" />
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Beings;
        (function (Beings) {
            var Monsters;
            (function (Monsters) {
                var MoveAlongGroundOrCeiling = Monsters.Movement.MoveAlongGroundOrCeiling;
                var Whirlwind = (function (_super) {
                    __extends(Whirlwind, _super);
                    function Whirlwind(x, y) {
                        _super.call(this, x, y);
                        this.monsterType = Monsters.MonsterType.Whirlwind;
                        this.currentMovementMethod = new MoveAlongGroundOrCeiling(this);
                        this.health = 10;
                        this.betweenAttacksDuration.length = 600;
                        this.attackDamage = 0;
                        this.moveSpeed = 250;
                        this.beingFrameSequences = this.monsterFrameSequences =
                            WhirlwindFrameSequences.singleton;
                        this.beingSounds = this.monsterSounds = new Monsters.MonsterSounds();
                        this.beingSounds.death = null;
                    }
                    Whirlwind.prototype.onPlayerHit = function (player) {
                        player.onWhirlwinded(this.isFacingLeft ? Entities.Side.Left : Entities.Side.Right);
                    };
                    return Whirlwind;
                })(Monsters.Monster);
                Monsters.Whirlwind = Whirlwind;
                var WhirlwindFrameSequences = (function (_super) {
                    __extends(WhirlwindFrameSequences, _super);
                    function WhirlwindFrameSequences() {
                        var _this = this;
                        _super.call(this);
                        this.imagesPrefix = "whirlwind";
                        var createWith = function (n, l, o) {
                            return _this.createWith.call(_this, n, l, o);
                        };
                        var goToStill = function (s) { return s.switchToWhenDone = _this.still; };
                        this.still = this.move = createWith("move", 8, { isRepeating: true });
                        this.attack = createWith("attack", 1, { images: [this.still.getFrame(0).image] });
                        goToStill(this.attack);
                        var deathImages = [];
                        var imagesCount = this.move.framesCount * 2;
                        for (var i = 0; i < imagesCount; i++)
                            deathImages[i] =
                                (i % 2 === 1)
                                    ? this.move.getFrame(Math.floor(i / 2)).image
                                    : Dunjax.Program.emptyImage;
                        this.death = createWith("death", deathImages.length, { images: deathImages });
                        this.death.setDurationOfAllFrames(140);
                    }
                    WhirlwindFrameSequences.create = function () {
                        WhirlwindFrameSequences.singleton = new WhirlwindFrameSequences();
                    };
                    return WhirlwindFrameSequences;
                })(Monsters.MonsterFrameSequences);
                Monsters.WhirlwindFrameSequences = WhirlwindFrameSequences;
            })(Monsters = Beings.Monsters || (Beings.Monsters = {}));
        })(Beings = Entities.Beings || (Entities.Beings = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="Monster.ts" />
/// <reference path="../../../maps/ITileType.ts" />
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Beings;
        (function (Beings) {
            var Monsters;
            (function (Monsters) {
                var Stalagmite = (function (_super) {
                    __extends(Stalagmite, _super);
                    function Stalagmite(x, y) {
                        _super.call(this, x, y);
                        this.monsterType = Monsters.MonsterType.Stalagmite;
                        this.currentMovementMethod = this;
                        this.health = 100;
                        this.moveSpeed = 10 * Dunjax.Maps.tileHeight;
                        this.betweenAttacksDuration.length = 1000 / this.moveSpeed;
                        this.isSleeping = true;
                        this.isFalling = false;
                        this.beingFrameSequences = this.monsterFrameSequences =
                            StalagmiteFrameSequences.singleton;
                        this.beingSounds = this.monsterSounds = new Monsters.MonsterSounds();
                    }
                    Stalagmite.prototype.getDefaultImage = function () {
                        return StalagmiteFrameSequences.singleton.sleep.getFrame(0).image;
                    };
                    Stalagmite.prototype.actBeforeActingAsMonster = function () {
                        if (this.isSleeping && this.shouldWake())
                            this.wake();
                    };
                    Stalagmite.prototype.shouldWake = function () {
                        var player = this.map.entities.player;
                        return player.y > this.y
                            && Math.abs(this.x - player.x) / (player.y - this.y) < 0.5
                            && this.map.isLocationVisibleFrom(player.x, player.y, this.x, this.y);
                    };
                    Stalagmite.prototype.wake = function () {
                        this.isSleeping = false;
                        this.isWaking = true;
                        Dunjax.Program.sounds.stalagmiteWake.play();
                    };
                    Stalagmite.prototype.canMoveInCurrentState = function () {
                        return this.isFalling;
                    };
                    Stalagmite.prototype.moveMonster = function () {
                        var map = this.map;
                        var dy = Dunjax.getMovementForSpeed(this.moveSpeed);
                        if (map.isPassible(this.left + 6, this.bottom + dy) &&
                            map.isPassible(this.right - 6, this.bottom + dy)) {
                            this.y += dy;
                        }
                        else {
                            this.isFalling = false;
                            this.isHittingGround = true;
                        }
                        var player = map.entities.player;
                        if (Dunjax.Geometry.rectContains(player, this.left + 8, this.bottom)
                            || Dunjax.Geometry.rectContains(player, this.right - 8, this.bottom))
                            player.onStruck(Math.floor(dy));
                        return true;
                    };
                    Stalagmite.prototype.onBeforeAnimationDoneAsMonster = function (sequence) {
                        if (this.isWaking) {
                            this.isWaking = false;
                            this.isFalling = true;
                        }
                        else if (this.isHittingGround) {
                            this.map.entities.removeEntity(this);
                        }
                    };
                    Stalagmite.prototype.isCloseEnoughToAttack = function () {
                        return false;
                    };
                    Stalagmite.prototype.isCeilingDwelling = function () { return true; };
                    Stalagmite.prototype.isGroundDwelling = function () { return false; };
                    Stalagmite.prototype.getFrameSequenceForStateAsMonsterSubtype = function (monsterSequenceChosen) {
                        var sequences = StalagmiteFrameSequences.singleton;
                        var sequence = monsterSequenceChosen;
                        if (this.isDead)
                            ;
                        else if (this.isHittingGround)
                            sequence = sequences.hitGround;
                        else if (this.isFalling)
                            sequence = sequences.fall;
                        return sequence;
                    };
                    return Stalagmite;
                })(Monsters.Monster);
                Monsters.Stalagmite = Stalagmite;
                var StalagmiteFrameSequences = (function (_super) {
                    __extends(StalagmiteFrameSequences, _super);
                    function StalagmiteFrameSequences() {
                        var _this = this;
                        _super.call(this);
                        this.imagesPrefix = "stalagmite";
                        var create = function (n, l) { return _this.create.call(_this, n, l); };
                        this.sleep = create("sleep", 1);
                        this.wake = create("wake", 2);
                        this.fall = create("fall", 1);
                        this.hitGround = create("hitGround", 4);
                    }
                    StalagmiteFrameSequences.create = function () {
                        StalagmiteFrameSequences.singleton = new StalagmiteFrameSequences();
                    };
                    return StalagmiteFrameSequences;
                })(Monsters.MonsterFrameSequences);
                Monsters.StalagmiteFrameSequences = StalagmiteFrameSequences;
            })(Monsters = Beings.Monsters || (Beings.Monsters = {}));
        })(Beings = Entities.Beings || (Entities.Beings = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="Monster.ts" />
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Beings;
        (function (Beings) {
            var Monsters;
            (function (Monsters) {
                var MoveAlongGroundOrCeiling = Monsters.Movement.MoveAlongGroundOrCeiling;
                var YellowGargoyle = (function (_super) {
                    __extends(YellowGargoyle, _super);
                    function YellowGargoyle(x, y) {
                        _super.call(this, x, y);
                        this.monsterType = Monsters.MonsterType.YellowGargoyle;
                        this.currentMovementMethod = new MoveAlongGroundOrCeiling(this);
                        this.health = 2;
                        this.attackDamage = 3;
                        this.moveSpeed = 140;
                        this.beingFrameSequences = this.monsterFrameSequences =
                            YellowGargoyleFrameSequences.singleton;
                        this.beingSounds = this.monsterSounds = new Monsters.MonsterSounds();
                    }
                    return YellowGargoyle;
                })(Monsters.Monster);
                Monsters.YellowGargoyle = YellowGargoyle;
                var YellowGargoyleFrameSequences = (function (_super) {
                    __extends(YellowGargoyleFrameSequences, _super);
                    function YellowGargoyleFrameSequences() {
                        var _this = this;
                        _super.call(this);
                        this.imagesPrefix = "yellowGargoyle";
                        var create = function (n, l) { return _this.create.call(_this, n, l); };
                        var createWith = function (n, l, o) {
                            return _this.createWith.call(_this, n, l, o);
                        };
                        var goToStill = function (s) { return s.switchToWhenDone = _this.still; };
                        this.still = create("still", 1);
                        this.move = createWith("move", 6, { isRepeating: true });
                        this.attack = createWith("attack", 4, { repeatedFrames: [-1, -1, -1, 2] });
                        goToStill(this.attack);
                        this.turn = create("turn", 2);
                        goToStill(this.turn);
                        this.carcass = create("carcass", 1);
                        this.death = create("death", 6);
                        this.death.switchToWhenDone = this.carcass;
                        this.spatter = create("spatter", 5);
                    }
                    YellowGargoyleFrameSequences.create = function () {
                        YellowGargoyleFrameSequences.singleton = new YellowGargoyleFrameSequences();
                    };
                    return YellowGargoyleFrameSequences;
                })(Monsters.MonsterFrameSequences);
                Monsters.YellowGargoyleFrameSequences = YellowGargoyleFrameSequences;
            })(Monsters = Beings.Monsters || (Beings.Monsters = {}));
        })(Beings = Entities.Beings || (Entities.Beings = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="../../../time/Duration.ts" />
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Beings;
        (function (Beings) {
            var Player;
            (function (Player) {
                var Duration = Dunjax.Time.Duration;
                var PlayerDurations = (function () {
                    function PlayerDurations() {
                        this.betweenFires = new Duration(140);
                        this.betweenSwings = new Duration(250);
                        this.betweenGunSwitches = new Duration(650);
                        this.betweenStalagtiteHits = new Duration(300);
                        this.betweenSlimePoolHits = new Duration(700);
                        this.betweenSpikesHits = new Duration(1000);
                        this.betweenWhirlwindedMoves = new Duration(3);
                        this.betweenWhirlwindedVerticalMoves = new Duration(2);
                        this.whirlwindEffect = new Duration(1500, false);
                        this.betweenMovementSounds = new Duration(200);
                        this.betweenThrustSounds = new Duration(400);
                        this.betweenSlidesSounds = new Duration(200);
                    }
                    return PlayerDurations;
                })();
                Player.PlayerDurations = PlayerDurations;
            })(Player = Beings.Player || (Beings.Player = {}));
        })(Beings = Entities.Beings || (Entities.Beings = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="../Being.ts" />
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Beings;
        (function (Beings) {
            var Player;
            (function (Player) {
                var PlayerFrameSequences = (function (_super) {
                    __extends(PlayerFrameSequences, _super);
                    function PlayerFrameSequences() {
                        var _this = this;
                        _super.call(this);
                        this.imagesPath = "entities/player/";
                        this.imagesPrefix = "player";
                        var create = function (n, l) { return _this.create.call(_this, n, l); };
                        var createWith = function (n, l, o) {
                            return _this.createWith.call(_this, n, l, o);
                        };
                        var goToStill = function (s) { return s.switchToWhenDone = _this.still; };
                        var lengthen = function (s) {
                            s.getFrame(0).duration = 500;
                            goToStill(s);
                        };
                        this.move = createWith("move", 4, { isRepeating: true });
                        this.move.requiresNodToAdvance = true;
                        this.still =
                            createWith("still", 1, { images: [this.move.getFrame(3).image] });
                        this.turn = create("turn", 2);
                        this.death = create("death", 7);
                        this.hit = create("hit", 1);
                        goToStill(this.hit);
                        this.carcass = createWith("carcass", 1, { isRepeating: true });
                        this.jump = create("jump", 3);
                        this.climb =
                            createWith("climb", 3, { isRepeating: true, repeatedFrames: [-1, -1, -1, 1] });
                        this.climb.requiresNodToAdvance = true;
                        this.slide = create("slide", 1);
                        this.land = create("land", 2);
                        this.land.getFrame(this.land.framesCount - 1).duration = 1000;
                        goToStill(this.land);
                        this.swing = create("swing", 4);
                        this.swing.setDurationOfAllFrames(40);
                        goToStill(this.swing);
                        this.fire = createWith("fire", 1, { images: [this.move.getFrame(3).image] });
                        lengthen(this.fire);
                        this.fireUp = create("fireUp", 1);
                        lengthen(this.fireUp);
                        this.fireDown = create("fireDown", 1);
                        lengthen(this.fireDown);
                        this.fireOnLadder =
                            createWith("fireOnLadder", 3, { repeatedFrames: [-1, -1, -1, 1] });
                        lengthen(this.fireOnLadder);
                        this.fireUpOnLadder =
                            createWith("fireUpOnLadder", 3, { repeatedFrames: [-1, -1, -1, 1] });
                        lengthen(this.fireUpOnLadder);
                        this.fireDownOnLadder =
                            createWith("fireDownOnLadder", 3, { repeatedFrames: [-1, -1, -1, 1] });
                        lengthen(this.fireDownOnLadder);
                        this.fireOnSlide = this.fire;
                        this.fireUpOnSlide = create("fireUpOnSlide", 1);
                        lengthen(this.fireUpOnSlide);
                        this.whirlwinded =
                            createWith("whirlwinded", 1, { images: [this.move.getFrame(0).image] });
                        this.webbed = create("webbed", 2);
                        var lastFrameIndex = this.webbed.framesCount - 1;
                        this.webbed.getFrame(lastFrameIndex).duration = 3000;
                    }
                    PlayerFrameSequences.create = function () {
                        PlayerFrameSequences.singleton = new PlayerFrameSequences();
                    };
                    return PlayerFrameSequences;
                })(Beings.BeingFrameSequences);
                Player.PlayerFrameSequences = PlayerFrameSequences;
            })(Player = Beings.Player || (Beings.Player = {}));
        })(Beings = Entities.Beings || (Entities.Beings = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="IItem.ts" />
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Items;
        (function (Items) {
            var ItemFrameSequences = (function (_super) {
                __extends(ItemFrameSequences, _super);
                function ItemFrameSequences() {
                    var _this = this;
                    _super.call(this);
                    this.sequences = [];
                    this.imagesPath = "entities/items/";
                    var create = function (name, length) {
                        return _this.createWith(name, length, { isRepeating: true });
                    };
                    var sequences = this.sequences;
                    sequences[Items.ItemType.PulseAmmo] = create("pulseAmmo", 4);
                    sequences[Items.ItemType.PulseAmmoBig] = create("pulseAmmoBig", 4);
                    sequences[Items.ItemType.GrapeshotAmmo] = create("grapeshotAmmo", 3);
                    sequences[Items.ItemType.GrapeshotGun] = create("grapeshotGun", 4);
                    sequences[Items.ItemType.ShieldPowerUp] = create("shieldPowerUp", 3);
                    sequences[Items.ItemType.ShieldPowerPack] = create("shieldPowerPack", 3);
                    sequences[Items.ItemType.Key] = create("key", 1);
                }
                ItemFrameSequences.create = function () {
                    ItemFrameSequences.singleton = new ItemFrameSequences();
                };
                return ItemFrameSequences;
            })(Entities.FrameSequences);
            Items.ItemFrameSequences = ItemFrameSequences;
        })(Items = Entities.Items || (Entities.Items = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="ItemFrameSequences.ts" />
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Items;
        (function (Items) {
            var Item = (function (_super) {
                __extends(Item, _super);
                function Item(itemType, x, y) {
                    _super.call(this, Entities.AnimationEntityType.Item, Items.ItemFrameSequences.singleton.sequences[itemType], x, y);
                    this.itemType = itemType;
                    this.isPassible = true;
                }
                Item.prototype.onConsumed = function () {
                    this.removeFromPlay();
                    Dunjax.Program.sounds.itemTaken.play();
                };
                Item.prototype.shouldAlignWithGroundOrCeiling = function () {
                    return this.itemType !== Items.ItemType.Key;
                };
                return Item;
            })(Entities.AnimationEntity);
            Items.Item = Item;
        })(Items = Entities.Items || (Entities.Items = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="Shot.ts" />
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Shots;
        (function (Shots) {
            var Duration = Dunjax.Time.Duration;
            var FireballHit = (function (_super) {
                __extends(FireballHit, _super);
                function FireballHit(x, y) {
                    _super.call(this, Shots.ShotType.FireballHit, x, y, 0, 0);
                    this.numFireSpotPairsSet = 0;
                    this.betweenFireSpotPairsDuration = new Duration(100, true);
                    this.isAMover = false;
                    this.isImpacter = false;
                    this.betweenFireSpotPairsDuration.start();
                    this.shotFrameSequences = new Shots.ShotFrameSequences();
                    this.shotFrameSequences.stationary = Dunjax.Animations.invisibleFrameSequence;
                }
                FireballHit.prototype.actBeforeMove = function () {
                    if (!this.betweenFireSpotPairsDuration.isDone)
                        return;
                    var spotWidth = 32, spotHeight = 32;
                    var y = this.map.getYJustAboveGround(this.x, this.y) - spotHeight / 2;
                    var dx = (this.numFireSpotPairsSet + 1) * spotWidth;
                    var spot = new Shots.FireSpot(this.x - dx, y);
                    this.map.entities.addEntity(spot);
                    spot = new Shots.FireSpot(this.x + dx, y);
                    this.map.entities.addEntity(spot);
                    if (++this.numFireSpotPairsSet === FireballHit.numFireSpotPairsToSet)
                        this.removeFromPlay();
                };
                FireballHit.numFireSpotPairsToSet = 5;
                return FireballHit;
            })(Shots.Shot);
            Shots.FireballHit = FireballHit;
        })(Shots = Entities.Shots || (Entities.Shots = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="Shot.ts" />
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Shots;
        (function (Shots) {
            var FireSpot = (function (_super) {
                __extends(FireSpot, _super);
                function FireSpot(x, y) {
                    _super.call(this, Shots.ShotType.FireSpot, x, y, 0, 0);
                    this.existenceDuration.length = 1000;
                    this.isAMover = false;
                    this.impactDamage = 10;
                    this.shotFrameSequences = FireSpotFrameSequences.singleton;
                    this.targetBeingType = Shots.TargetBeingType.Player;
                }
                return FireSpot;
            })(Shots.Shot);
            Shots.FireSpot = FireSpot;
            var FireSpotFrameSequences = (function (_super) {
                __extends(FireSpotFrameSequences, _super);
                function FireSpotFrameSequences() {
                    var _this = this;
                    _super.call(this);
                    var create = function (name, length) {
                        return _this.createWith(name, length, { isRepeating: true });
                    };
                    this.imagesPrefix = "fireSpot";
                    this.stationary = create("", 11);
                    this.stationary.setDurationOfAllFrames(60);
                }
                FireSpotFrameSequences.create = function () {
                    FireSpotFrameSequences.singleton = new FireSpotFrameSequences();
                };
                return FireSpotFrameSequences;
            })(Shots.ShotFrameSequences);
            Shots.FireSpotFrameSequences = FireSpotFrameSequences;
        })(Shots = Entities.Shots || (Entities.Shots = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="Shot.ts" />
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Shots;
        (function (Shots) {
            var Duration = Dunjax.Time.Duration;
            var Slime = Entities.Beings.Monsters.Slime;
            var SlimeShotHit = (function (_super) {
                __extends(SlimeShotHit, _super);
                function SlimeShotHit(x, y) {
                    _super.call(this, Shots.ShotType.SlimeShotHit, x, y, 0, 0);
                    this.numSlimeSpotPairsSet = 0;
                    this.betweenSlimeSpotPairsDuration = new Duration(140, true);
                    this.isAMover = false;
                    this.isImpacter = false;
                    this.betweenSlimeSpotPairsDuration.start();
                    this.shotFrameSequences = new Shots.ShotFrameSequences();
                    this.shotFrameSequences.stationary = Dunjax.Animations.invisibleFrameSequence;
                }
                SlimeShotHit.prototype.actBeforeMove = function () {
                    if (!this.betweenSlimeSpotPairsDuration.isDone)
                        return;
                    var spotWidth = 32;
                    var y = this.map.getYJustBelowCeiling(this.x, this.y);
                    var dx = (this.numSlimeSpotPairsSet + 1) * spotWidth;
                    var spot = new Shots.SlimeSpot(this.x - dx, y);
                    this.map.entities.addEntity(spot);
                    spot = new Shots.SlimeSpot(this.x + dx, y);
                    this.map.entities.addEntity(spot);
                    if (++this.numSlimeSpotPairsSet === SlimeShotHit.numSlimeSpotPairsToSet) {
                        this.createSlime(spotWidth);
                        this.removeFromPlay();
                    }
                };
                SlimeShotHit.prototype.createSlime = function (spotWidth) {
                    var x = this.left
                        + Dunjax.randomInt(0, SlimeShotHit.numSlimeSpotPairsToSet * spotWidth)
                            * (Dunjax.randomInt(0, 1) ? -1 : 1);
                    var y = this.map.getYJustBelowCeiling(this.x, this.y) + this.height / 2 - 1;
                    var monster = new Slime(x, y);
                    this.map.entities.addEntity(monster);
                };
                SlimeShotHit.numSlimeSpotPairsToSet = 5;
                return SlimeShotHit;
            })(Shots.Shot);
            Shots.SlimeShotHit = SlimeShotHit;
        })(Shots = Entities.Shots || (Entities.Shots = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="Shot.ts" />
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Shots;
        (function (Shots) {
            var SlimeSpot = (function (_super) {
                __extends(SlimeSpot, _super);
                function SlimeSpot(x, y) {
                    _super.call(this, Shots.ShotType.SlimeSpot, x, y, 0, 0);
                    this.existenceDuration.length = 1000;
                    this.isAMover = false;
                    this.impactDamage = 5;
                    this.shotFrameSequences = SlimeSpotFrameSequences.singleton;
                    this.targetBeingType = Shots.TargetBeingType.Player;
                }
                return SlimeSpot;
            })(Shots.Shot);
            Shots.SlimeSpot = SlimeSpot;
            var SlimeSpotFrameSequences = (function (_super) {
                __extends(SlimeSpotFrameSequences, _super);
                function SlimeSpotFrameSequences() {
                    var _this = this;
                    _super.call(this);
                    var create = function (name, length) {
                        return _this.createWith(name, length, { isRepeating: true });
                    };
                    this.imagesPrefix = "slimeSpot";
                    this.stationary = create("", 2);
                    this.stationary.setDurationOfAllFrames(60);
                }
                SlimeSpotFrameSequences.create = function () {
                    SlimeSpotFrameSequences.singleton = new SlimeSpotFrameSequences();
                };
                return SlimeSpotFrameSequences;
            })(Shots.ShotFrameSequences);
            Shots.SlimeSpotFrameSequences = SlimeSpotFrameSequences;
        })(Shots = Entities.Shots || (Entities.Shots = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Spatter = (function (_super) {
            __extends(Spatter, _super);
            function Spatter(sequence, x, y) {
                _super.call(this, Entities.AnimationEntityType.Spatter, sequence, x, y);
                this.shouldCheckForFall = true;
                this.currentFallStartTime = Dunjax.Program.clock.time;
            }
            Spatter.prototype.onFallBlocked = function () {
                if (this.animation.isDone)
                    this.isPermanentlyInactive = true;
            };
            Spatter.prototype.onAnimationDone = function () {
                if (!this.shouldCheckForFall)
                    this.isPermanentlyInactive = true;
            };
            return Spatter;
        })(Entities.AnimationEntity);
        Entities.Spatter = Spatter;
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
var Dunjax;
(function (Dunjax) {
    var Maps;
    (function (Maps) {
        (function (TileTypeId) {
            TileTypeId[TileTypeId["WallRock1"] = 158] = "WallRock1";
            TileTypeId[TileTypeId["WallRock2"] = 197] = "WallRock2";
            TileTypeId[TileTypeId["StartCave"] = 194] = "StartCave";
            TileTypeId[TileTypeId["ClosedDoorBrick"] = 84] = "ClosedDoorBrick";
            TileTypeId[TileTypeId["LockedDoorBrick"] = 129] = "LockedDoorBrick";
            TileTypeId[TileTypeId["WallTerraceDown"] = 198] = "WallTerraceDown";
            TileTypeId[TileTypeId["WallTerraceDownLeft"] = 160] = "WallTerraceDownLeft";
            TileTypeId[TileTypeId["WallTerraceDownRight"] = 120] = "WallTerraceDownRight";
            TileTypeId[TileTypeId["WallTerrace"] = 159] = "WallTerrace";
            TileTypeId[TileTypeId["ClosedDoorCave"] = 124] = "ClosedDoorCave";
            TileTypeId[TileTypeId["LockedDoorCave"] = 12] = "LockedDoorCave";
            TileTypeId[TileTypeId["RockBrick1"] = 103] = "RockBrick1";
            TileTypeId[TileTypeId["RockBrick2"] = 104] = "RockBrick2";
            TileTypeId[TileTypeId["TrashBrick1"] = 195] = "TrashBrick1";
            TileTypeId[TileTypeId["TrashBrick2"] = 157] = "TrashBrick2";
            TileTypeId[TileTypeId["TrashBrick3"] = 196] = "TrashBrick3";
            TileTypeId[TileTypeId["TrashBrick4"] = 40] = "TrashBrick4";
            TileTypeId[TileTypeId["RockCave1"] = 143] = "RockCave1";
            TileTypeId[TileTypeId["RockCave2"] = 182] = "RockCave2";
            TileTypeId[TileTypeId["TrashCave1"] = 79] = "TrashCave1";
            TileTypeId[TileTypeId["TrashCave2"] = 118] = "TrashCave2";
            TileTypeId[TileTypeId["TrashCave3"] = 80] = "TrashCave3";
            TileTypeId[TileTypeId["TrashCave4"] = 119] = "TrashCave4";
            TileTypeId[TileTypeId["Ledge1Left"] = 9] = "Ledge1Left";
            TileTypeId[TileTypeId["Ledge1a"] = 166] = "Ledge1a";
            TileTypeId[TileTypeId["Ledge1b"] = 167] = "Ledge1b";
            TileTypeId[TileTypeId["Ledge1c"] = 168] = "Ledge1c";
            TileTypeId[TileTypeId["Ledge1Right"] = 10] = "Ledge1Right";
            TileTypeId[TileTypeId["ProjectingLedge"] = 142] = "ProjectingLedge";
            TileTypeId[TileTypeId["SlideTerraceBrickLeft"] = 67] = "SlideTerraceBrickLeft";
            TileTypeId[TileTypeId["SlideTerraceBrickRight"] = 28] = "SlideTerraceBrickRight";
            TileTypeId[TileTypeId["SlideTerraceBrickUpLeft"] = 68] = "SlideTerraceBrickUpLeft";
            TileTypeId[TileTypeId["SlideTerraceBrickUpRight"] = 146] = "SlideTerraceBrickUpRight";
            TileTypeId[TileTypeId["SlideTerraceRockLeft"] = 107] = "SlideTerraceRockLeft";
            TileTypeId[TileTypeId["SlideTerraceRockRight"] = 29] = "SlideTerraceRockRight";
            TileTypeId[TileTypeId["SlideTerraceRockUpLeft"] = 185] = "SlideTerraceRockUpLeft";
            TileTypeId[TileTypeId["SlideTerraceRockUpRight"] = 147] = "SlideTerraceRockUpRight";
            TileTypeId[TileTypeId["SlideRockCaveLeft"] = 144] = "SlideRockCaveLeft";
            TileTypeId[TileTypeId["SlideRockCaveRight"] = 145] = "SlideRockCaveRight";
            TileTypeId[TileTypeId["SlideRockCaveUpLeft"] = 184] = "SlideRockCaveUpLeft";
            TileTypeId[TileTypeId["SlideRockCaveUpRight"] = 106] = "SlideRockCaveUpRight";
            TileTypeId[TileTypeId["SlideRockBrickLeft"] = 27] = "SlideRockBrickLeft";
            TileTypeId[TileTypeId["SlideRockBrickRight"] = 66] = "SlideRockBrickRight";
            TileTypeId[TileTypeId["SlideRockBrickUpLeft"] = 183] = "SlideRockBrickUpLeft";
            TileTypeId[TileTypeId["SlideRockBrickUpRight"] = 105] = "SlideRockBrickUpRight";
            TileTypeId[TileTypeId["Darkness"] = 5] = "Darkness";
            TileTypeId[TileTypeId["SecretDoorRock"] = 65] = "SecretDoorRock";
            TileTypeId[TileTypeId["TerraceRockUp"] = 26] = "TerraceRockUp";
            TileTypeId[TileTypeId["SecretDoorTerraceWall"] = 38] = "SecretDoorTerraceWall";
            TileTypeId[TileTypeId["LadderBrick"] = 164] = "LadderBrick";
            TileTypeId[TileTypeId["LadderCave"] = 165] = "LadderCave";
            TileTypeId[TileTypeId["ExitCaveDownLeft"] = 6] = "ExitCaveDownLeft";
            TileTypeId[TileTypeId["ExitCaveDownRight"] = 45] = "ExitCaveDownRight";
            TileTypeId[TileTypeId["OpenDoorBrick"] = 98] = "OpenDoorBrick";
            TileTypeId[TileTypeId["OpenDoorCave"] = 137] = "OpenDoorCave";
            TileTypeId[TileTypeId["StartCaveDownRight"] = 194] = "StartCaveDownRight";
            TileTypeId[TileTypeId["SpikesBrick"] = 151] = "SpikesBrick";
            TileTypeId[TileTypeId["SpikesCave"] = 190] = "SpikesCave";
            TileTypeId[TileTypeId["StalagmiteCave1"] = 113] = "StalagmiteCave1";
            TileTypeId[TileTypeId["StalagmiteCave2"] = 153] = "StalagmiteCave2";
            TileTypeId[TileTypeId["StalagmiteCave3"] = 36] = "StalagmiteCave3";
            TileTypeId[TileTypeId["StalagmiteCave4"] = 75] = "StalagmiteCave4";
            TileTypeId[TileTypeId["StalagmiteCave5"] = 37] = "StalagmiteCave5";
            TileTypeId[TileTypeId["StalagmiteCave6"] = 76] = "StalagmiteCave6";
            TileTypeId[TileTypeId["StalagmiteCave7"] = 115] = "StalagmiteCave7";
            TileTypeId[TileTypeId["StalagmiteCave8"] = 154] = "StalagmiteCave8";
            TileTypeId[TileTypeId["StalagmiteCave9"] = 193] = "StalagmiteCave9";
            TileTypeId[TileTypeId["StalagmiteCave10"] = 35] = "StalagmiteCave10";
            TileTypeId[TileTypeId["StalagmiteCave11"] = 114] = "StalagmiteCave11";
            TileTypeId[TileTypeId["StalagmiteBrick1"] = 191] = "StalagmiteBrick1";
            TileTypeId[TileTypeId["StalagmiteBrick2"] = 152] = "StalagmiteBrick2";
            TileTypeId[TileTypeId["StalagmiteBrick3"] = 192] = "StalagmiteBrick3";
            TileTypeId[TileTypeId["StalagmiteBrick4"] = 33] = "StalagmiteBrick4";
            TileTypeId[TileTypeId["StalagmiteBrick5"] = 34] = "StalagmiteBrick5";
            TileTypeId[TileTypeId["StalagmiteBrick6"] = 73] = "StalagmiteBrick6";
            TileTypeId[TileTypeId["StalagmiteBrick7"] = 74] = "StalagmiteBrick7";
            TileTypeId[TileTypeId["SlimePoolBrick1"] = 30] = "SlimePoolBrick1";
            TileTypeId[TileTypeId["SlimePoolBrick2"] = 69] = "SlimePoolBrick2";
            TileTypeId[TileTypeId["SlimePoolBrick3"] = 186] = "SlimePoolBrick3";
            TileTypeId[TileTypeId["SlimePoolCave1"] = 108] = "SlimePoolCave1";
            TileTypeId[TileTypeId["SlimePoolCave2"] = 148] = "SlimePoolCave2";
            TileTypeId[TileTypeId["SlimePoolCave3"] = 109] = "SlimePoolCave3";
            TileTypeId[TileTypeId["TorchBrick1"] = 78] = "TorchBrick1";
            TileTypeId[TileTypeId["TorchBrick2"] = 117] = "TorchBrick2";
            TileTypeId[TileTypeId["TorchBrick3"] = 39] = "TorchBrick3";
            TileTypeId[TileTypeId["TorchBrick4"] = 156] = "TorchBrick4";
            TileTypeId[TileTypeId["SmallFire1"] = 187] = "SmallFire1";
            TileTypeId[TileTypeId["SmallFire2"] = 70] = "SmallFire2";
            TileTypeId[TileTypeId["SmallFire3"] = 31] = "SmallFire3";
            TileTypeId[TileTypeId["SmallFire4"] = 71] = "SmallFire4";
            TileTypeId[TileTypeId["Fungi1a"] = 85] = "Fungi1a";
            TileTypeId[TileTypeId["Fungi1b"] = 86] = "Fungi1b";
            TileTypeId[TileTypeId["Fungi1c"] = 125] = "Fungi1c";
            TileTypeId[TileTypeId["Fungi1d"] = 47] = "Fungi1d";
            TileTypeId[TileTypeId["Fungi1e"] = 8] = "Fungi1e";
            TileTypeId[TileTypeId["Fungi1f"] = 48] = "Fungi1f";
            TileTypeId[TileTypeId["Fungi2a"] = 87] = "Fungi2a";
            TileTypeId[TileTypeId["Fungi2b"] = 126] = "Fungi2b";
            TileTypeId[TileTypeId["Fungi2c"] = 127] = "Fungi2c";
            TileTypeId[TileTypeId["Fungi2d"] = 88] = "Fungi2d";
            TileTypeId[TileTypeId["Fungi2e"] = 128] = "Fungi2e";
            TileTypeId[TileTypeId["Fungi2f"] = 161] = "Fungi2f";
        })(Maps.TileTypeId || (Maps.TileTypeId = {}));
        var TileTypeId = Maps.TileTypeId;
    })(Maps = Dunjax.Maps || (Dunjax.Maps = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="ITileType.ts" />
/// <reference path="IMap.ts" />
var Dunjax;
(function (Dunjax) {
    var Maps;
    (function (Maps) {
        var TileType = (function () {
            function TileType(index, terrainFeature, terrainFeatureRegion) {
                if (terrainFeature === void 0) { terrainFeature = Maps.TerrainFeature.None; }
                if (terrainFeatureRegion === void 0) { terrainFeatureRegion = Region.all; }
                this.passibleRegion = Region.all;
                this.slideType = Maps.SlideType.NotASlide;
                this.animationDuration = 200;
                this.index = index;
                this.terrainFeature = terrainFeature;
                this.terrainFeatureRegion = terrainFeatureRegion;
            }
            TileType.prototype.isFallThroughable = function (x, y) {
                var region = this.fallThroughableRegion;
                return region != null ? region.contains(x, y) : this.isPassible(x, y);
            };
            TileType.prototype.isPassible = function (x, y) {
                var region = this.passibleRegion;
                if (region === Region.all)
                    return true;
                if (region === Region.none)
                    return false;
                return region.contains(x, y);
            };
            TileType.prototype.getTerrainFeatureAt = function (x, y) {
                return this.terrainFeatureRegion.contains(x, y)
                    ? this.terrainFeature : Maps.TerrainFeature.None;
            };
            return TileType;
        })();
        Maps.TileType = TileType;
        var Region = (function () {
            function Region(contains) {
                this.contains = contains;
            }
            Region.initialize = function () {
                this.all = new Region(function () { return true; });
                this.none = new Region(function () { return false; });
                this.up = new Region(function (x, y) { return (y < Maps.tileHeight / 2); });
                this.down = new Region(function (x, y) { return (y >= Maps.tileHeight / 2); });
                this.upLeftInclusive = new Region(function (x, y) { return (y <= Maps.tileWidth - 1 - x); });
                this.upRightInclusive = new Region(function (x, y) { return (y <= x); });
                this.downLeftInclusive = new Region(function (x, y) { return (y >= x); });
                this.downRightInclusive = new Region(function (x, y) { return (y >= Maps.tileWidth - 1 - x); });
                this.upLeftExclusive = new Region(function (x, y) { return (y < Maps.tileWidth - 1 - x); });
                this.upRightExclusive = new Region(function (x, y) { return (y < x); });
                this.downLeftExclusive = new Region(function (x, y) { return (y > x); });
                this.downRightExclusive = new Region(function (x, y) { return (y > Maps.tileWidth - 1 - x); });
            };
            return Region;
        })();
        Maps.Region = Region;
        Region.initialize();
    })(Maps = Dunjax.Maps || (Dunjax.Maps = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="TileTypeId.ts" />
/// <reference path="TileType.ts" />
var Dunjax;
(function (Dunjax) {
    var Maps;
    (function (Maps) {
        Maps.numTileTypes = 240;
        Maps.tileTypes = [];
        Maps.impassibleTiles = [
            Maps.TileTypeId.WallRock1,
            Maps.TileTypeId.WallRock2,
            Maps.TileTypeId.ClosedDoorBrick,
            Maps.TileTypeId.LockedDoorBrick,
            Maps.TileTypeId.WallTerrace,
            Maps.TileTypeId.WallTerraceDown,
            Maps.TileTypeId.WallTerraceDownLeft,
            Maps.TileTypeId.WallTerraceDownRight,
            Maps.TileTypeId.ClosedDoorCave,
            Maps.TileTypeId.LockedDoorCave
        ];
        Maps.downHalfImpassibleTiles = [
            Maps.TileTypeId.RockBrick1, Maps.TileTypeId.RockBrick2,
            Maps.TileTypeId.TrashBrick1, Maps.TileTypeId.TrashBrick2, Maps.TileTypeId.TrashBrick3,
            Maps.TileTypeId.TrashBrick4,
            Maps.TileTypeId.RockCave1, Maps.TileTypeId.RockCave2,
            Maps.TileTypeId.TrashCave1, Maps.TileTypeId.TrashCave2, Maps.TileTypeId.TrashCave3,
            Maps.TileTypeId.TrashCave4,
            Maps.TileTypeId.Ledge1Left, Maps.TileTypeId.Ledge1a, Maps.TileTypeId.Ledge1b,
            Maps.TileTypeId.Ledge1c, Maps.TileTypeId.Ledge1Right,
        ];
        Maps.upHalfImpassibleTiles = [
            Maps.TileTypeId.ProjectingLedge,
        ];
        Maps.leftDownSlopingTiles = [
            Maps.TileTypeId.SlideTerraceBrickLeft,
            Maps.TileTypeId.SlideTerraceRockLeft,
            Maps.TileTypeId.SlideRockCaveLeft,
            Maps.TileTypeId.SlideRockBrickLeft,
        ];
        Maps.rightDownSlopingTiles = [
            Maps.TileTypeId.SlideTerraceBrickRight,
            Maps.TileTypeId.SlideTerraceRockRight,
            Maps.TileTypeId.SlideRockCaveRight,
            Maps.TileTypeId.SlideRockBrickRight,
        ];
        Maps.leftUpSlopingTiles = [
            Maps.TileTypeId.SlideTerraceBrickUpLeft,
            Maps.TileTypeId.SlideTerraceRockUpLeft,
            Maps.TileTypeId.SlideRockCaveUpLeft,
            Maps.TileTypeId.SlideRockBrickUpLeft,
        ];
        Maps.rightUpSlopingTiles = [
            Maps.TileTypeId.SlideTerraceBrickUpRight,
            Maps.TileTypeId.SlideTerraceRockUpRight,
            Maps.TileTypeId.SlideRockCaveUpRight,
            Maps.TileTypeId.SlideRockBrickUpRight,
        ];
        Maps.sightBlockingTiles = [
            Maps.TileTypeId.Darkness,
            Maps.TileTypeId.WallRock1,
            Maps.TileTypeId.WallRock2,
            Maps.TileTypeId.SecretDoorRock,
            Maps.TileTypeId.ClosedDoorBrick,
            Maps.TileTypeId.LockedDoorBrick,
            Maps.TileTypeId.TerraceRockUp,
            Maps.TileTypeId.SecretDoorTerraceWall,
            Maps.TileTypeId.WallTerraceDown,
            Maps.TileTypeId.WallTerraceDownLeft,
            Maps.TileTypeId.WallTerraceDownRight,
            Maps.TileTypeId.WallTerrace,
            Maps.TileTypeId.ClosedDoorCave,
            Maps.TileTypeId.LockedDoorCave,
        ];
        Maps.sightBlockingTilesMap = Object.create(null);
        function initializeTileTypes() {
            for (var i = 0; i < Maps.numTileTypes; i++)
                Maps.tileTypes[i] = new Maps.TileType(i);
            var replace = function (id, feature) {
                return Maps.tileTypes[id] = new Maps.TileType(id, feature);
            };
            replace(Maps.TileTypeId.LadderBrick, Maps.TerrainFeature.Ladder);
            replace(Maps.TileTypeId.ClosedDoorBrick, Maps.TerrainFeature.ClosedDoor);
            replace(Maps.TileTypeId.LockedDoorBrick, Maps.TerrainFeature.LockedDoor);
            replace(Maps.TileTypeId.ClosedDoorCave, Maps.TerrainFeature.ClosedDoor);
            replace(Maps.TileTypeId.LockedDoorCave, Maps.TerrainFeature.LockedDoor);
            [
                Maps.TileTypeId.StalagmiteCave1,
                Maps.TileTypeId.StalagmiteCave2,
                Maps.TileTypeId.StalagmiteCave3,
                Maps.TileTypeId.StalagmiteCave4,
                Maps.TileTypeId.StalagmiteCave5,
                Maps.TileTypeId.StalagmiteCave6,
                Maps.TileTypeId.StalagmiteCave7,
                Maps.TileTypeId.StalagmiteCave8,
                Maps.TileTypeId.StalagmiteCave9,
                Maps.TileTypeId.StalagmiteCave10,
                Maps.TileTypeId.StalagmiteCave11,
                Maps.TileTypeId.StalagmiteBrick1,
                Maps.TileTypeId.StalagmiteBrick2,
                Maps.TileTypeId.StalagmiteBrick3,
                Maps.TileTypeId.StalagmiteBrick4,
                Maps.TileTypeId.StalagmiteBrick5,
                Maps.TileTypeId.StalagmiteBrick6,
                Maps.TileTypeId.StalagmiteBrick7,
            ].forEach(function (id) { return replace(id, Maps.TerrainFeature.Stalagmite); });
            replace(Maps.TileTypeId.ExitCaveDownLeft, Maps.TerrainFeature.ExitCave);
            replace(Maps.TileTypeId.ExitCaveDownRight, Maps.TerrainFeature.ExitCave);
            replace(Maps.TileTypeId.LadderCave, Maps.TerrainFeature.Ladder);
            replace(Maps.TileTypeId.OpenDoorBrick, Maps.TerrainFeature.OpenDoor);
            replace(Maps.TileTypeId.OpenDoorCave, Maps.TerrainFeature.OpenDoor);
            replace(Maps.TileTypeId.StartCaveDownRight, Maps.TerrainFeature.StartCave);
            replace(Maps.TileTypeId.SpikesBrick, Maps.TerrainFeature.Spikes);
            replace(Maps.TileTypeId.SpikesCave, Maps.TerrainFeature.Spikes);
            [
                Maps.TileTypeId.SlimePoolBrick1,
                Maps.TileTypeId.SlimePoolBrick2,
                Maps.TileTypeId.SlimePoolBrick3,
                Maps.TileTypeId.SlimePoolCave1,
                Maps.TileTypeId.SlimePoolCave2,
                Maps.TileTypeId.SlimePoolCave3,
            ].forEach(function (id) { return replace(id, Maps.TerrainFeature.SlimePool); });
            var setAnimationSequence = function (forTileTypes, idSequence) {
                var sequence = [];
                idSequence.forEach(function (id) { return sequence.push(Maps.tileTypes[id]); });
                forTileTypes.forEach(function (type) { return Maps.tileTypes[type].animationSequence = sequence; });
            };
            var torchBricks = [
                Maps.TileTypeId.TorchBrick1,
                Maps.TileTypeId.TorchBrick2,
                Maps.TileTypeId.TorchBrick3,
                Maps.TileTypeId.TorchBrick4,
            ];
            setAnimationSequence(torchBricks, torchBricks);
            var smallFires = [
                Maps.TileTypeId.SmallFire1,
                Maps.TileTypeId.SmallFire2,
                Maps.TileTypeId.SmallFire3,
                Maps.TileTypeId.SmallFire4,
            ];
            setAnimationSequence(smallFires, smallFires);
            var fungis1 = [
                Maps.TileTypeId.Fungi1a,
                Maps.TileTypeId.Fungi1b,
                Maps.TileTypeId.Fungi1c,
                Maps.TileTypeId.Fungi1d,
                Maps.TileTypeId.Fungi1e,
                Maps.TileTypeId.Fungi1f,
            ];
            setAnimationSequence(fungis1, fungis1);
            var fungis2 = [
                Maps.TileTypeId.Fungi2a,
                Maps.TileTypeId.Fungi2b,
                Maps.TileTypeId.Fungi2c,
                Maps.TileTypeId.Fungi2d,
                Maps.TileTypeId.Fungi2e,
                Maps.TileTypeId.Fungi2f,
            ];
            setAnimationSequence(fungis2, fungis2);
            var trashBricks = [
                Maps.TileTypeId.TrashBrick1,
                Maps.TileTypeId.TrashBrick2,
                Maps.TileTypeId.TrashBrick3,
                Maps.TileTypeId.TrashBrick4,
            ];
            setAnimationSequence(trashBricks, trashBricks);
            var trashCaves = [
                Maps.TileTypeId.TrashCave1,
                Maps.TileTypeId.TrashCave2,
                Maps.TileTypeId.TrashCave3,
                Maps.TileTypeId.TrashCave4,
            ];
            setAnimationSequence(trashCaves, trashCaves);
            var slimePoolBricks = [
                Maps.TileTypeId.SlimePoolBrick1,
                Maps.TileTypeId.SlimePoolBrick2,
                Maps.TileTypeId.SlimePoolBrick3,
            ];
            var slimePoolBrickSequence = [
                Maps.TileTypeId.SlimePoolBrick1,
                Maps.TileTypeId.SlimePoolBrick2,
                Maps.TileTypeId.SlimePoolBrick1,
                Maps.TileTypeId.SlimePoolBrick3,
            ];
            setAnimationSequence(slimePoolBricks, slimePoolBrickSequence);
            var slimePoolCaves = [
                Maps.TileTypeId.SlimePoolCave1,
                Maps.TileTypeId.SlimePoolCave2,
                Maps.TileTypeId.SlimePoolCave3,
            ];
            var slimePoolCaveSequence = [
                Maps.TileTypeId.SlimePoolCave1,
                Maps.TileTypeId.SlimePoolCave2,
                Maps.TileTypeId.SlimePoolCave1,
                Maps.TileTypeId.SlimePoolCave3,
            ];
            setAnimationSequence(slimePoolCaves, slimePoolCaveSequence);
            var replace1 = function (id, feature, region) {
                return Maps.tileTypes[id] = new Maps.TileType(id, feature, region);
            };
            replace1(Maps.TileTypeId.SlideTerraceBrickLeft, Maps.TerrainFeature.SlideLeft, Maps.Region.downRightInclusive);
            replace1(Maps.TileTypeId.SlideTerraceBrickRight, Maps.TerrainFeature.SlideRight, Maps.Region.downLeftInclusive);
            replace1(Maps.TileTypeId.SlideTerraceRockLeft, Maps.TerrainFeature.SlideLeft, Maps.Region.downRightInclusive);
            replace1(Maps.TileTypeId.SlideTerraceRockRight, Maps.TerrainFeature.SlideRight, Maps.Region.downLeftInclusive);
            replace1(Maps.TileTypeId.SlideRockCaveLeft, Maps.TerrainFeature.SlideLeft, Maps.Region.downRightInclusive);
            replace1(Maps.TileTypeId.SlideRockCaveRight, Maps.TerrainFeature.SlideRight, Maps.Region.downLeftInclusive);
            replace1(Maps.TileTypeId.SlideRockBrickLeft, Maps.TerrainFeature.SlideLeft, Maps.Region.downRightInclusive);
            replace1(Maps.TileTypeId.SlideRockBrickRight, Maps.TerrainFeature.SlideRight, Maps.Region.downLeftInclusive);
            var setPassibleRegion = function (types, region) {
                types.forEach(function (type) { return Maps.tileTypes[type].passibleRegion = region; });
            };
            setPassibleRegion(Maps.impassibleTiles, Maps.Region.none);
            setPassibleRegion(Maps.downHalfImpassibleTiles, Maps.Region.up);
            setPassibleRegion(Maps.upHalfImpassibleTiles, Maps.Region.down);
            setPassibleRegion(Maps.leftDownSlopingTiles, Maps.Region.upLeftExclusive);
            setPassibleRegion(Maps.rightDownSlopingTiles, Maps.Region.upRightExclusive);
            setPassibleRegion(Maps.leftUpSlopingTiles, Maps.Region.downLeftExclusive);
            setPassibleRegion(Maps.rightUpSlopingTiles, Maps.Region.downRightExclusive);
            [
                Maps.TileTypeId.LadderBrick,
                Maps.TileTypeId.LadderCave,
            ].forEach(function (type) { return Maps.tileTypes[type].fallThroughableRegion = Maps.Region.none; });
            Maps.sightBlockingTiles.forEach(function (tileId) {
                Maps.sightBlockingTilesMap[tileId.toString()] = tileId;
                Maps.tileTypes[tileId].isSightBlocking = true;
            });
            var setAnimationDuration = function (types, duration) {
                types.forEach(function (type) { return Maps.tileTypes[type].animationDuration = duration; });
            };
            var slimePoolDuration = 600;
            setAnimationDuration([
                Maps.TileTypeId.SlimePoolBrick1,
                Maps.TileTypeId.SlimePoolBrick2,
                Maps.TileTypeId.SlimePoolBrick3,
                Maps.TileTypeId.SlimePoolCave1,
                Maps.TileTypeId.SlimePoolCave2,
                Maps.TileTypeId.SlimePoolCave3,
            ], slimePoolDuration);
        }
        initializeTileTypes();
    })(Maps = Dunjax.Maps || (Dunjax.Maps = {}));
})(Dunjax || (Dunjax = {}));
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Beings;
        (function (Beings) {
            var Monsters;
            (function (Monsters) {
                function asMonster(being) {
                    return being.isCloseEnoughToAttack ? being : null;
                }
                Monsters.asMonster = asMonster;
                (function (MonsterType) {
                    MonsterType[MonsterType["BlueGargoyle"] = 0] = "BlueGargoyle";
                    MonsterType[MonsterType["GreenGargoyle"] = 1] = "GreenGargoyle";
                    MonsterType[MonsterType["Master"] = 2] = "Master";
                    MonsterType[MonsterType["Slime"] = 3] = "Slime";
                    MonsterType[MonsterType["Spider"] = 4] = "Spider";
                    MonsterType[MonsterType["Stalagmite"] = 5] = "Stalagmite";
                    MonsterType[MonsterType["Whirlwind"] = 6] = "Whirlwind";
                    MonsterType[MonsterType["YellowGargoyle"] = 7] = "YellowGargoyle";
                })(Monsters.MonsterType || (Monsters.MonsterType = {}));
                var MonsterType = Monsters.MonsterType;
                ;
            })(Monsters = Beings.Monsters || (Beings.Monsters = {}));
        })(Beings = Entities.Beings || (Entities.Beings = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="../entities/beings/monsters/IMonster.ts" />
var Dunjax;
(function (Dunjax) {
    var Maps;
    (function (Maps) {
        var Monsters = Dunjax.Entities.Beings.Monsters;
        var Beings = Dunjax.Entities.Beings;
        var Player = Dunjax.Entities.Beings.Player;
        var Shots = Dunjax.Entities.Shots;
        var Items = Dunjax.Entities.Items;
        var Master = Dunjax.Entities.Beings.Monsters.Master;
        var MapEntities = (function () {
            function MapEntities(map) {
                this.entities = [];
                this.monsters = [];
                this.monstersInOrNearViewport = [];
                this.shots = [];
                this.items = [];
                this.itemsInOrNearViewport = [];
                this.animationEntities = [];
                this.animationEntitiesInOrNearViewport = [];
                this.map = map;
            }
            MapEntities.prototype.getMonsterAt = function (x, y) {
                return this.getMapEntityAt(x, y, this.monstersInOrNearViewport);
            };
            MapEntities.prototype.getMapEntityAt = function (x, y, entities) {
                for (var i = 0; i < entities.length; i++) {
                    var entity = entities[i];
                    if (Dunjax.Geometry.rectContains(entity, x, y))
                        return entity;
                }
                return null;
            };
            MapEntities.prototype.getBeingInBounds = function (x, y, width, height) {
                if (Dunjax.Geometry.intersectsWith2(this.player, x, y, width, height))
                    return this.player;
                return this.getMapEntityIn2(x, y, width, height, this.monstersInOrNearViewport);
            };
            MapEntities.prototype.getAnimationEntityInBounds = function (bounds) {
                return this.getMapEntityIn(bounds, this.animationEntitiesInOrNearViewport);
            };
            MapEntities.prototype.getItemInBounds = function (bounds) {
                return this.getMapEntityIn(bounds, this.itemsInOrNearViewport);
            };
            MapEntities.prototype.getMapEntityIn = function (rect, entities) {
                for (var i = 0; i < entities.length; i++) {
                    var entity = entities[i];
                    if (Dunjax.Geometry.intersectsWith(entity, rect))
                        return entity;
                }
                return null;
            };
            MapEntities.prototype.getMapEntityIn2 = function (x, y, width, height, entities) {
                for (var i = 0; i < entities.length; i++) {
                    var entity = entities[i];
                    var r = entity;
                    if (Dunjax.Geometry.intersects(r.left, r.top, r.width, r.height, x, y, width, height))
                        return entity;
                }
                return null;
            };
            MapEntities.prototype.addEntity = function (entity) {
                entity.map = this.map;
                this.entities.push(entity);
                var being = Beings.asBeing(entity);
                if (being != null) {
                    var monster = Monsters.asMonster(being);
                    if (monster != null) {
                        this.monsters.push(monster);
                        if (monster instanceof Master)
                            this.master = monster;
                    }
                    var player = Player.asPlayer(being);
                    if (player != null)
                        this.player = player;
                }
                var shot = Shots.asShot(entity);
                if (shot != null)
                    this.shots.push(shot);
                var anim = Dunjax.Entities.asAnimationEntity(entity);
                if (anim != null) {
                    this.animationEntities.push(anim);
                    var item = Items.asItem(anim);
                    if (item != null)
                        this.items.push(item);
                }
            };
            MapEntities.prototype.removeEntity = function (entity) {
                entity.map = null;
                this.entities.remove(entity);
                var being = Beings.asBeing(entity);
                if (being != null) {
                    var monster = Monsters.asMonster(being);
                    if (monster != null)
                        this.monsters.remove(monster);
                    var player = Player.asPlayer(being);
                    if (player != null)
                        this.player = null;
                }
                var shot = Shots.asShot(entity);
                if (shot != null)
                    this.shots.remove(shot);
                var anim = Dunjax.Entities.asAnimationEntity(entity);
                if (anim != null) {
                    this.animationEntities.remove(anim);
                    var item = Items.asItem(anim);
                    if (item != null)
                        this.items.remove(item);
                }
            };
            MapEntities.prototype.act = function () {
                this.entities.forEach(function (e) { return e.determineIsInOrNearViewport(); });
                this.monstersInOrNearViewport =
                    this.monsters.filter(function (m) { return m.isInOrNearViewport && !m.isDead; });
                this.itemsInOrNearViewport = this.items.filter(function (i) { return i.isInOrNearViewport; });
                this.animationEntitiesInOrNearViewport =
                    this.animationEntities.filter(function (a) { return a.isInOrNearViewport; });
                this.entities.forEach(function (e) { if (e.isInOrNearViewport)
                    e.act.call(e); });
            };
            return MapEntities;
        })();
        Maps.MapEntities = MapEntities;
    })(Maps = Dunjax.Maps || (Dunjax.Maps = {}));
})(Dunjax || (Dunjax = {}));
var Dunjax;
(function (Dunjax) {
    var Maps;
    (function (Maps) {
        var Duration = Dunjax.Time.Duration;
        var AnimatedTileData = (function () {
            function AnimatedTileData(map, x, y, tileType) {
                this.index = 0;
                this.map = map;
                this.x = x;
                this.y = y;
                this.durationUntilNext = new Duration(tileType.animationDuration);
                this.sequence = tileType.animationSequence;
                this.index = this.sequence.indexOf(tileType);
            }
            AnimatedTileData.prototype.checkForAdvance = function () {
                if (this.durationUntilNext.isDone)
                    return this.advance();
                return false;
            };
            AnimatedTileData.prototype.advance = function () {
                this.index++;
                if (this.index >= this.sequence.length)
                    this.index = 0;
                var next = this.sequence[this.index];
                this.durationUntilNext.length = next.animationDuration;
                var visible = false;
                var x = this.x, y = this.y;
                if (Dunjax.isInViewport(x, y) && !this.map.isTileDarkenedAtPixel(x, y)) {
                    this.map.setTileTypeAtPixel(x, y, next);
                    visible = true;
                }
                return visible;
            };
            return AnimatedTileData;
        })();
        Maps.AnimatedTileData = AnimatedTileData;
    })(Maps = Dunjax.Maps || (Dunjax.Maps = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="MapEntities.ts" />
/// <reference path="AnimatedTileData.ts" />
var Dunjax;
(function (Dunjax) {
    var Maps;
    (function (Maps) {
        var Duration = Dunjax.Time.Duration;
        var Map = (function () {
            function Map(tilemap) {
                this.entities = new Maps.MapEntities(this);
                this.animatedTileDatas = [];
                this.animateTilesDuration = new Duration(200);
                this.squareRootCache = [];
                this.visibilityCache = {};
                this.tilemap = tilemap;
            }
            Map.prototype.getTileTypeAtPixel = function (x, y) {
                if (!this.containsLocation(x, y))
                    return Maps.tileTypes[Maps.TileTypeId.Darkness];
                var tile = this.tilemap.getTileWorldXY(x, y);
                var darkenedIndex = tile.darkenedIndex;
                var index = darkenedIndex != null ? darkenedIndex : tile.index;
                return Maps.tileTypes[index];
            };
            Map.prototype.getTileType = function (tileX, tileY) {
                return Maps.tileTypes[this.tilemap.getTile(tileX, tileY).index];
            };
            Map.prototype.setTileTypeAtPixel = function (x, y, tileType) {
                this.tilemap.layers[0]
                    .data[Maps.toTileY(y)][Maps.toTileX(x)].index = tileType.index;
            };
            Map.prototype.setTileType = function (tileX, tileY, tileType) {
                this.tilemap.layers[0].data[tileY][tileX].index = tileType.index;
            };
            Map.prototype.isTilePassible = function (x, y) {
                var tileType = this.getTileTypeAtPixel(x, y);
                return tileType.isPassible(x % Maps.tileWidth, y % Maps.tileHeight);
            };
            Map.prototype.isPassible = function (x, y, considerBeings) {
                if (considerBeings === void 0) { considerBeings = false; }
                if (!this.isTilePassible(x, y))
                    return false;
                if (!considerBeings)
                    return true;
                var monster;
                var entities = this.entities;
                var impassible = ((monster = entities.getMonsterAt(x, y)) != null && !monster.isPassible)
                    || (entities.player != null && Dunjax.Geometry.rectContains(entities.player, x, y));
                return !impassible;
            };
            Map.prototype.isVerticalLinePassible = function (x, y, height, considerBeings) {
                if (considerBeings === void 0) { considerBeings = true; }
                var halfHeight = height / 2;
                var top = y - halfHeight, bottom = y + halfHeight - 1;
                if (!this.isTilePassible(x, y)
                    || !this.isTilePassible(x, top)
                    || !this.isTilePassible(x, bottom))
                    return false;
                if (considerBeings && this.entities.getBeingInBounds(x, top, 1, height) != null)
                    return false;
                return true;
            };
            Map.prototype.isHorizontalLinePassible = function (x, y, width, considerBeings) {
                if (considerBeings === void 0) { considerBeings = true; }
                var halfWidth = width / 2;
                var left = x - halfWidth, right = x + halfWidth - 1;
                if (!this.isTilePassible(x, y)
                    || !this.isTilePassible(left, y)
                    || !this.isTilePassible(right, y))
                    return false;
                if (considerBeings && this.entities.getBeingInBounds(left, y, width, 1) != null)
                    return false;
                return true;
            };
            Map.prototype.containsLocation = function (x, y) {
                return x >= 0 && y >= 0
                    && x < this.tilemap.widthInPixels
                    && y < this.tilemap.heightInPixels;
            };
            Map.prototype.isFallThroughable = function (x, y) {
                return this.getTileTypeAtPixel(x, y)
                    .isFallThroughable(x % Maps.tileWidth, y % Maps.tileHeight);
            };
            Map.prototype.getVisibilityHash = function (fromX, fromY, toX, toY) {
                return (fromX << 12 | fromY).toString(16) + (toX << 12 | toY).toString(16);
            };
            Map.prototype.isLocationVisibleFrom = function (x, y, fromX, fromY) {
                if (!this.containsLocation(x, y))
                    return false;
                var fromTileY = Maps.toTileY(fromY);
                var fromTileX = Maps.toTileX(fromX);
                var toTileY = Maps.toTileY(y);
                var toTileX = Maps.toTileX(x);
                if (Math.abs(toTileX - fromTileX) <= 1
                    && Math.abs(toTileY - fromTileY) <= 1)
                    return true;
                var hash = this.getVisibilityHash(fromX, fromY, x, y);
                var value = this.visibilityCache[hash];
                if (value != undefined)
                    return value;
                var dy = y - fromY;
                var dx = x - fromX;
                var squaredDistance = dx * dx + dy * dy;
                var distance = this.squareRootCache[squaredDistance];
                if (distance == undefined)
                    distance = this.squareRootCache[squaredDistance] = Math.sqrt(squaredDistance);
                var directionX = dx / distance;
                var directionY = dy / distance;
                var pixelsPerStep = Maps.tileWidth / 2;
                directionX *= pixelsPerStep;
                directionY *= pixelsPerStep;
                var visible = false;
                var testY = fromY;
                var testX = fromX;
                var step = 0;
                var limit = 20 * Maps.tileWidth / pixelsPerStep;
                while (step++ <= limit) {
                    testX += directionX;
                    testY += directionY;
                    var tileX = Math.floor(testX / Maps.tileWidth);
                    var tileY = Math.floor(testY / Maps.tileHeight);
                    if (tileX === toTileX && tileY === toTileY) {
                        visible = true;
                        break;
                    }
                    var tile = this.tilemap.getTile(tileX, tileY);
                    if (tile == null || tile.index in Maps.sightBlockingTilesMap)
                        break;
                }
                this.visibilityCache[hash] = visible;
                return visible;
            };
            Map.prototype.getLocationOfTerrainFeature = function (feature) {
                var tileTypeId;
                if (feature === Maps.TerrainFeature.StartCave)
                    tileTypeId = Maps.TileTypeId.StartCaveDownRight;
                else if (feature === Maps.TerrainFeature.ExitCave)
                    tileTypeId = Maps.TileTypeId.ExitCaveDownLeft;
                else
                    return null;
                for (var i = 0; i < this.tilemap.width; i++) {
                    for (var j = 0; j < this.tilemap.height; j++) {
                        if (this.tilemap.getTile(i, j).index === tileTypeId) {
                            return this.asMapLocation(i, j);
                        }
                    }
                }
                return null;
            };
            Map.prototype.asMapLocation = function (x, y) {
                return {
                    x: x * Maps.tileWidth + Maps.tileWidth / 2,
                    y: y * Maps.tileHeight + Maps.tileHeight / 2
                };
            };
            Map.prototype.getTerrainFeatureAt = function (x, y) {
                return this.getTileTypeAtPixel(x, y)
                    .getTerrainFeatureAt(x % Maps.tileWidth, y % Maps.tileHeight);
            };
            Map.prototype.setTerrainFeatureAt = function (x, y, feature) {
                if (feature === Maps.TerrainFeature.OpenDoor) {
                    var tileY = Maps.toTileY(y);
                    var tileX = Maps.toTileX(x);
                    var doorType = this.getTileType(tileX, tileY);
                    var openDoorType = this.getOpenDoorTileTypeToReplaceClosedDoor(doorType);
                    this.setTileType(tileX, tileY, openDoorType);
                    this.visibilityCache = {};
                    this.onLineOfSightChangeOccurred();
                    var bodies = this.tilemap.layers[0].bodies;
                    for (var i = 0; i < bodies.length; i++) {
                        var body = bodies[i];
                        if (Maps.toTileX(body.x) === tileX && Maps.toTileY(body.y) === tileY) {
                            bodies.splice(i, 1);
                            break;
                        }
                    }
                }
                else
                    throw new Error("Unsupported terrain feature");
            };
            Map.prototype.getOpenDoorTileTypeToReplaceClosedDoor = function (closedDoor) {
                var result = null;
                if (closedDoor.index === Maps.TileTypeId.ClosedDoorBrick
                    || closedDoor.index === Maps.TileTypeId.LockedDoorBrick)
                    result = Maps.tileTypes[Maps.TileTypeId.OpenDoorBrick];
                else if (closedDoor.index === Maps.TileTypeId.ClosedDoorCave
                    || closedDoor.index === Maps.TileTypeId.LockedDoorCave)
                    result = Maps.tileTypes[Maps.TileTypeId.OpenDoorCave];
                return result;
            };
            Map.prototype.act = function () {
                if (this.animateTilesDuration.isDone)
                    this.animateTiles();
            };
            Map.prototype.createTileAnimations = function () {
                for (var i = 0; i < this.tilemap.width; i++) {
                    for (var j = 0; j < this.tilemap.height; j++) {
                        var tile = this.tilemap.getTile(i, j);
                        var tileType = Maps.tileTypes[tile.index];
                        if (tileType.animationSequence != null)
                            this.createAnimationDataStorageForTile(i, j, tileType);
                    }
                }
            };
            Map.prototype.createAnimationDataStorageForTile = function (x, y, tileType) {
                var loc = this.asMapLocation(x, y);
                var data = new Maps.AnimatedTileData(this, loc.x, loc.y, tileType);
                this.animatedTileDatas.push(data);
            };
            Map.prototype.animateTiles = function () {
                var datasCount = this.animatedTileDatas.length;
                var isAnyVisibleAdvances = false;
                for (var i = 0; i < datasCount; i++) {
                    if (this.animatedTileDatas[i].checkForAdvance())
                        isAnyVisibleAdvances = true;
                }
                if (isAnyVisibleAdvances)
                    this.tilemap.layers[0].dirty = true;
            };
            Map.prototype.getTileCenterLocation = function (x, y) {
                return {
                    x: x - x % Maps.tileWidth + Maps.tileWidth / 2,
                    y: y - y % Maps.tileHeight + Maps.tileHeight / 2
                };
            };
            Map.prototype.getYJustAboveGround = function (x, y) {
                var limit = y - 4 * Maps.tileHeight;
                while (y > limit) {
                    if (this.isPassible(x, y) && !this.isPassible(x, y + 1))
                        return y;
                    y--;
                }
                throw new Error("Limit reached.");
            };
            Map.prototype.getYJustBelowCeiling = function (x, y) {
                var limit = y + 4 * Maps.tileHeight;
                while (y < limit) {
                    if (this.isPassible(x, y) && !this.isPassible(x, y - 1))
                        return y;
                    y++;
                }
                throw new Error("Limit reached.");
            };
            Map.prototype.darkenTileAtPixel = function (x, y) {
                var tile = this.tilemap.layers[0].data[Maps.toTileY(y)][Maps.toTileX(x)];
                tile.darkenedIndex = tile.index;
                tile.index = Maps.TileTypeId.Darkness;
            };
            Map.prototype.undarkenTileAtPixel = function (x, y) {
                var tile = this.tilemap.layers[0].data[Maps.toTileY(y)][Maps.toTileX(x)];
                tile.index = tile.darkenedIndex;
                tile.darkenedIndex = null;
            };
            Map.prototype.isTileDarkenedAtPixel = function (x, y) {
                var tile = this.tilemap.layers[0].data[Maps.toTileY(y)][Maps.toTileX(x)];
                return tile.darkenedIndex != null;
            };
            return Map;
        })();
        Maps.Map = Map;
    })(Maps = Dunjax.Maps || (Dunjax.Maps = {}));
})(Dunjax || (Dunjax = {}));
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        (function (AnimationEntityType) {
            AnimationEntityType[AnimationEntityType["Item"] = 0] = "Item";
            AnimationEntityType[AnimationEntityType["Spatter"] = 1] = "Spatter";
            AnimationEntityType[AnimationEntityType["FinalRoomTrigger"] = 2] = "FinalRoomTrigger";
        })(Entities.AnimationEntityType || (Entities.AnimationEntityType = {}));
        var AnimationEntityType = Entities.AnimationEntityType;
        ;
        function asAnimationEntity(entity) {
            return entity.animationEntityType !== undefined
                ? entity : null;
        }
        Entities.asAnimationEntity = asAnimationEntity;
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="../maps/TileTypes.ts" />
/// <reference path="../maps/Map.ts" />
/// <reference path="../entities/beings/player/Player.ts" />
/// <reference path="../entities/beings/monsters/YellowGargoyle.ts" />
/// <reference path="../entities/IAnimationEntity.ts" />
var Dunjax;
(function (Dunjax) {
    var GameStates;
    (function (GameStates) {
        var Sound = Dunjax.Sounds.Sound;
        var LevelState = (function (_super) {
            __extends(LevelState, _super);
            function LevelState(levelNumber, dunjaxGame) {
                _super.call(this);
                this.isStoryDismissed = false;
                this.levelNumber = levelNumber;
                this.dunjaxGame = dunjaxGame;
            }
            LevelState.prototype.preload = function () {
                var displayer = this.storyDisplayer = new GameStates.StoryDisplayer(this.levelNumber);
                displayer.nextLine();
                this.load.atlas('dpad', 'images/dpad.png', 'images/dpad.json');
                var game = Dunjax.Program.game;
                game.load.tilemap('map', "maps/" + this.levelNumber + ".json", null, Phaser.Tilemap.TILED_JSON);
                if (Sound.soundsEnabled) {
                    game.load.audio("song" + this.levelNumber, "songs/dunjax" + this.levelNumber + ".mp3");
                    if (this.levelNumber === 2)
                        game.load.audio("bossSong", "songs/dunjaxBoss.mp3");
                }
            };
            LevelState.prototype.create = function () {
                var _this = this;
                var game = Dunjax.Program.game;
                var tilemap = this.tilemap = game.add.tilemap('map');
                tilemap.addTilesetImage('tiles');
                var layer = this.layer = tilemap.createLayer('tiles');
                layer.resizeWorld();
                game.time.advancedTiming = true;
                var physics = game.physics;
                physics.startSystem(Phaser.Physics.NINJA);
                this.ninjaTiles = GameStates.setNinjaTileTypes(tilemap, layer);
                this.dunjaxGame.onLevelLoaded(tilemap);
                var dunjaxMap = this.dunjaxGame.map;
                var master = dunjaxMap.entities.master;
                if (master != null)
                    master.onKilled = function () { return _this.onMasterKilled.call(_this); };
                this.sightBlocker = new GameStates.SightBlocker(dunjaxMap);
                dunjaxMap.onLineOfSightChangeOccurred =
                    function () { return _this.sightBlocker.isSightBlockRefreshNeeded = true; };
                var player = this.dunjaxGame.player;
                var playerSprite = player.frameworkObject;
                physics.ninja.enableAABB(playerSprite);
                this.dunjaxGame.onLevelCompleted = function () { return _this.onLevelCompleted.call(_this); };
                player.onReachedFinalRoomTrigger =
                    function () { return _this.onPlayerReachedFinalRoomTrigger.call(_this); };
                game.camera.follow(playerSprite);
                game.camera.bounds = null;
                if (Dunjax.Program.userInput == null)
                    Dunjax.Program.userInput = new Dunjax.UserInput();
                this.setupDpadAndButtons();
                this.statsDisplay = new GameStates.StatsDisplay();
                this.statsDisplay.addStats();
                Dunjax.Program.sounds.addSounds();
                this.dismissStoryKey = this.game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
                if (Sound.soundsEnabled) {
                    this.levelSong = game.add.audio("song" + this.levelNumber);
                    if (this.levelNumber === 2)
                        this.bossSong = game.add.audio("bossSong");
                }
                game.paused = true;
                game.world.visible = false;
            };
            LevelState.prototype.setupDpadAndButtons = function () {
                if (Dunjax.Program.virtualPadManager == null)
                    Dunjax.Program.virtualPadManager = Dunjax.Program.game.plugins.add(Phaser.VirtualJoystick);
                var padManager = Dunjax.Program.virtualPadManager;
                var dPad = this.dPad = padManager.addDPad(0, 0, 200, 'dpad');
                dPad.alignBottomLeft(0);
                var swingButton = this.swingButton =
                    padManager.addButton(500, 520, 'dpad', 'button1-up', 'button1-down');
                var fireButton = this.fireButton =
                    padManager.addButton(615, 485, 'dpad', 'button2-up', 'button2-down');
                var jumpButton = this.jumpButton =
                    padManager.addButton(730, 450, 'dpad', 'button3-up', 'button3-down');
                var switchGunsButton = this.switchGunsButton =
                    padManager.addButton(300, 520, 'dpad', 'button1-up', 'button1-down');
                dPad.visible = swingButton.visible = fireButton.visible = jumpButton.visible =
                    switchGunsButton.visible = false;
                var input = Dunjax.Program.userInput;
                input.dPad = dPad;
                input.buttonARight = swingButton;
                input.buttonB = fireButton;
                input.buttonC = jumpButton;
                input.buttonALeft = switchGunsButton;
            };
            LevelState.prototype.pauseUpdate = function () {
                if (!this.isStoryDismissed) {
                    if (this.dismissStoryKey.isDown || this.game.input.pointer1.isDown) {
                        this.isStoryDismissed = true;
                        this.storyDisplayer.onDismissed();
                        var game = Dunjax.Program.game;
                        game.paused = false;
                        Dunjax.Program.clock.start();
                        if (Sound.soundsEnabled)
                            this.levelSong.play(null, 0, 0.5, true);
                    }
                }
            };
            LevelState.prototype.update = function () {
                var game = this.dunjaxGame;
                game.doTurn();
                var player = game.player;
                var playerSprite = player.frameworkObject;
                var body = playerSprite.body;
                this.ninjaTiles.forEach(function (t) { return body.aabb.collideAABBVsTile(t.tile); });
                this.sightBlocker.restoreSightBlockedEntities();
                var tilemap = this.tilemap;
                this.sightBlocker.checkToAddSightBlocking(function () { return tilemap.layers[0].dirty = true; });
                this.sightBlocker.hideSightBlockedEntities();
                this.statsDisplay.updateStats(player);
                if (!this.dPad.visible && this.game.input.pointer1.isDown)
                    this.dPad.visible = this.swingButton.visible = this.fireButton.visible =
                        this.jumpButton.visible = true;
                if (!this.switchGunsButton.visible && this.dPad.visible)
                    this.switchGunsButton.visible = player.hasGrapeshotGun;
                var world = Dunjax.Program.game.world;
                if (!world.visible)
                    world.visible = true;
            };
            LevelState.prototype.onGameOver = function () {
                this.removeDpadAndButtons();
                if (Sound.soundsEnabled) {
                    if (this.levelSong.isPlaying)
                        this.levelSong.stop();
                    if (this.bossSong && this.bossSong.isPlaying)
                        this.bossSong.stop();
                }
            };
            LevelState.prototype.onPlayerReachedFinalRoomTrigger = function () {
                if (Sound.soundsEnabled) {
                    this.levelSong.stop();
                    this.bossSong.play(null, 0, 0.5, true);
                }
            };
            LevelState.prototype.onLevelCompleted = function () {
                this.removeDpadAndButtons();
                if (Sound.soundsEnabled) {
                    this.levelSong.stop();
                }
                var game = this.game;
                game.paused = true;
                setTimeout(function () { return game.paused = false; }, 3000);
            };
            LevelState.prototype.onMasterKilled = function () {
                this.removeDpadAndButtons();
                var game = this.game;
                game.paused = true;
                new GameStates.StoryDisplayer(-1).nextLine();
            };
            LevelState.prototype.removeDpadAndButtons = function () {
                Dunjax.Program.virtualPadManager.destroy();
            };
            return LevelState;
        })(Phaser.State);
        GameStates.LevelState = LevelState;
    })(GameStates = Dunjax.GameStates || (Dunjax.GameStates = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="gamestates/LevelState.ts" />
var Dunjax;
(function (Dunjax) {
    var Duration = Dunjax.Time.Duration;
    var Player = Dunjax.Entities.Beings.Player.Player;
    var TerrainFeature = Dunjax.Maps.TerrainFeature;
    var pixelsPerTenFeet = Dunjax.Maps.pixelsPerTenFeet;
    var Map = Dunjax.Maps.Map;
    var LevelState = Dunjax.GameStates.LevelState;
    var Game = (function () {
        function Game() {
            this.postGameOverRunTime = new Duration(4000, false);
            this.startingLevelNumber = 1;
            this.currentLevelNumber = this.startingLevelNumber;
        }
        Game.prototype.startCurrentLevel = function () {
            var game = Dunjax.Program.game;
            var levelNumber = this.currentLevelNumber;
            var levelState = this.levelState = new LevelState(levelNumber, this);
            var key = levelState.key = "Level${levelNumber}";
            var stateManager = game.state;
            stateManager.add(key, levelState);
            stateManager.start(key);
        };
        Game.prototype.onLevelLoaded = function (tilemap) {
            var _this = this;
            var map = this.map = new Map(tilemap);
            map.createTileAnimations();
            Dunjax.GameStates.createEntities(tilemap, map);
            var player = this.player;
            if (player == null) {
                player = this.player = new Player();
                player.onHasDied = function () { return _this.onPlayerDead.call(_this); };
                player.onReachedExit = function () { return _this.onPlayerReachedExit.call(_this); };
                player.determineImage();
            }
            else
                player.frameworkObject = Dunjax.Program.game.add.sprite(0, 0);
            this.putPlayerAtStartCave(this.player);
        };
        Game.prototype.putPlayerAtStartCave = function (player) {
            var map = this.map;
            var loc = map.getLocationOfTerrainFeature(TerrainFeature.StartCave);
            _a = [loc.x - pixelsPerTenFeet / 2, loc.y], player.x = _a[0], player.y = _a[1];
            map.entities.addEntity(player);
            player.alignWithSurroundings();
            player.isFacingLeft = false;
            var _a;
        };
        Game.prototype.isGameDone = function () {
            return this.isOver && this.postGameOverRunTime.isDone;
        };
        Game.prototype.doTurn = function () {
            if (this.isGameDone()) {
                this.exitGame();
                return;
            }
            this.map.entities.act();
            this.map.act();
        };
        Game.prototype.exitGame = function () {
            if (this.map.entities.player.isDead) {
                this.levelState.onGameOver();
                this.onGameOver();
            }
        };
        Game.prototype.advanceToNextMap = function () {
            var map = this.map;
            var player = map.entities.player;
            map.entities.removeEntity(player);
            this.currentLevelNumber++;
            this.startCurrentLevel();
        };
        Game.prototype.onPlayerDead = function () {
            this.isOver = true;
            this.postGameOverRunTime.start();
        };
        Game.prototype.onPlayerReachedExit = function () {
            var _this = this;
            new Audio("sounds/levelEndReached.mp3").play();
            this.onLevelCompleted();
            setTimeout(function () { return _this.advanceToNextMap(); }, 3000);
        };
        return Game;
    })();
    Dunjax.Game = Game;
})(Dunjax || (Dunjax = {}));
var Dunjax;
(function (Dunjax) {
    var GameStates;
    (function (GameStates) {
        var Item = Dunjax.Entities.Items.Item;
        var YellowGargoyle = Dunjax.Entities.Beings.Monsters.YellowGargoyle;
        var GreenGargoyle = Dunjax.Entities.Beings.Monsters.GreenGargoyle;
        var Stalagmite = Dunjax.Entities.Beings.Monsters.Stalagmite;
        var Whirlwind = Dunjax.Entities.Beings.Monsters.Whirlwind;
        var Spider = Dunjax.Entities.Beings.Monsters.Spider;
        var BlueGargoyle = Dunjax.Entities.Beings.Monsters.BlueGargoyle;
        var Slime = Dunjax.Entities.Beings.Monsters.Slime;
        var Master = Dunjax.Entities.Beings.Monsters.Master;
        var ItemType = Dunjax.Entities.Items.ItemType;
        var AnimationEntity = Dunjax.Entities.AnimationEntity;
        var AnimationEntityType = Dunjax.Entities.AnimationEntityType;
        var BlueGargoyleFrameSequences = Dunjax.Entities.Beings.Monsters.BlueGargoyleFrameSequences;
        var GreenGargoyleFrameSequences = Dunjax.Entities.Beings.Monsters.GreenGargoyleFrameSequences;
        var MasterFrameSequences = Dunjax.Entities.Beings.Monsters.MasterFrameSequences;
        var SlimeFrameSequences = Dunjax.Entities.Beings.Monsters.SlimeFrameSequences;
        var SpiderFrameSequences = Dunjax.Entities.Beings.Monsters.SpiderFrameSequences;
        var StalagmiteFrameSequences = Dunjax.Entities.Beings.Monsters.StalagmiteFrameSequences;
        var WhirlwindFrameSequences = Dunjax.Entities.Beings.Monsters.WhirlwindFrameSequences;
        var YellowGargoyleFrameSequences = Dunjax.Entities.Beings.Monsters.YellowGargoyleFrameSequences;
        var PlayerFrameSequences = Dunjax.Entities.Beings.Player.PlayerFrameSequences;
        var ItemFrameSequences = Dunjax.Entities.Items.ItemFrameSequences;
        var PulseShotFrameSequences = Dunjax.Entities.Shots.PulseShotFrameSequences;
        var FireballShotFrameSequences = Dunjax.Entities.Shots.FireballShotFrameSequences;
        var GrapeshotShotFrameSequences = Dunjax.Entities.Shots.GrapeshotShotFrameSequences;
        var HalberdShotFrameSequences = Dunjax.Entities.Shots.HalberdShotFrameSequences;
        var WebShotFrameSequences = Dunjax.Entities.Shots.WebShotFrameSequences;
        var SlimeShotFrameSequences = Dunjax.Entities.Shots.SlimeShotFrameSequences;
        var FireSpotFrameSequences = Dunjax.Entities.Shots.FireSpotFrameSequences;
        var SlimeSpotFrameSequences = Dunjax.Entities.Shots.SlimeSpotFrameSequences;
        function createFrameSequences() {
            Dunjax.Animations.createMiscFrameSequences();
            var classes = [
                ItemFrameSequences, PlayerFrameSequences,
                PulseShotFrameSequences, GrapeshotShotFrameSequences,
                HalberdShotFrameSequences, WebShotFrameSequences,
                FireballShotFrameSequences, SlimeShotFrameSequences,
                FireSpotFrameSequences, SlimeSpotFrameSequences,
                BlueGargoyleFrameSequences, GreenGargoyleFrameSequences, MasterFrameSequences,
                SlimeFrameSequences, SpiderFrameSequences, StalagmiteFrameSequences,
                WhirlwindFrameSequences, YellowGargoyleFrameSequences
            ];
            classes.forEach(function (c) { return c.create(); });
        }
        GameStates.createFrameSequences = createFrameSequences;
        function createEntities(tilemap, map) {
            var allEntityDatas = tilemap.objects['entities'];
            var entityDatas = allEntityDatas.filter(function (e) { return e.type === 'item' || e.type === 'monster' || e.type === 'trigger'; });
            for (var i = 0; i < entityDatas.length; i++) {
                var data = entityDatas[i];
                var isItem = data.type === 'item';
                var x = data.x + data.width / 2;
                var y = data.y + data.height / 2;
                var entity = void 0;
                if (isItem)
                    entity = new Item(ItemType[data.name], x, y);
                else if (data.name === 'YellowGargoyle')
                    entity = new YellowGargoyle(x, y);
                else if (data.name === 'GreenGargoyle')
                    entity = new GreenGargoyle(x, y);
                else if (data.name === 'Stalagmite')
                    entity = new Stalagmite(x, y);
                else if (data.name === 'Whirlwind')
                    entity = new Whirlwind(x, y);
                else if (data.name === 'Spider')
                    entity = new Spider(x, y);
                else if (data.name === 'BlueGargoyle')
                    entity = new BlueGargoyle(x, y);
                else if (data.name === 'Slime')
                    entity = new Slime(x, y);
                else if (data.name === 'Master')
                    entity = new Master(x, y);
                else if (data.name === 'FinalRoomTrigger')
                    entity = new AnimationEntity(AnimationEntityType.FinalRoomTrigger, null, x, y);
                else
                    continue;
                entity.id = data.id;
                entity.determineImage();
                map.entities.addEntity(entity);
                entity.alignWithSurroundings();
            }
        }
        GameStates.createEntities = createEntities;
        function setNinjaTileTypes(tilemap, layer) {
            var phaserGame = Dunjax.Program.game;
            var physics = phaserGame.physics;
            var typeMap = {};
            var impassibleId = 1;
            Dunjax.Maps.impassibleTiles.forEach(function (t) { return typeMap[t.toString()] = impassibleId; });
            var downHalfImpassibleId = 30;
            Dunjax.Maps.downHalfImpassibleTiles.forEach(function (t) { return typeMap[t.toString()] = downHalfImpassibleId; });
            var upHalfImpassibleId = 32;
            Dunjax.Maps.upHalfImpassibleTiles.forEach(function (t) { return typeMap[t.toString()] = upHalfImpassibleId; });
            var leftDownSlopingId = 3;
            Dunjax.Maps.leftDownSlopingTiles.forEach(function (t) { return typeMap[t.toString()] = leftDownSlopingId; });
            var rightDownSlopingId = 2;
            Dunjax.Maps.rightDownSlopingTiles.forEach(function (t) { return typeMap[t.toString()] = rightDownSlopingId; });
            var leftUpSlopingId = 4;
            Dunjax.Maps.leftUpSlopingTiles.forEach(function (t) { return typeMap[t.toString()] = leftUpSlopingId; });
            var rightUpSlopingId = 5;
            Dunjax.Maps.rightUpSlopingTiles.forEach(function (t) { return typeMap[t.toString()] = rightUpSlopingId; });
            return physics.ninja.convertTilemap(tilemap, layer, typeMap);
        }
        GameStates.setNinjaTileTypes = setNinjaTileTypes;
    })(GameStates = Dunjax.GameStates || (Dunjax.GameStates = {}));
})(Dunjax || (Dunjax = {}));
var Dunjax;
(function (Dunjax) {
    var GameStates;
    (function (GameStates) {
        var tileWidth = Dunjax.Maps.tileWidth;
        var tileHeight = Dunjax.Maps.tileHeight;
        var SightBlocker = (function () {
            function SightBlocker(map) {
                this.darkenedTiles = [];
                this.darkenedTilePool = [];
                this.map = map;
            }
            SightBlocker.prototype.checkToAddSightBlocking = function (onSightBlockingAdded) {
                var player = this.map.entities.player;
                if (player == null)
                    return;
                var playerTileX = Dunjax.Maps.toTileX(player.x);
                var playerTileY = Dunjax.Maps.toTileY(player.y);
                if (this.isSightBlockRefreshNeeded
                    || playerTileX !== this.oldPlayerTileX
                    || playerTileY !== this.oldPlayerTileY) {
                    this.oldPlayerTileX = playerTileX;
                    this.oldPlayerTileY = playerTileY;
                    this.restoreDarkenedTiles();
                    this.addSightBlocking();
                    onSightBlockingAdded();
                    this.isSightBlockRefreshNeeded = false;
                }
            };
            SightBlocker.prototype.addSightBlocking = function () {
                var phaserGame = Dunjax.Program.game;
                var camera = phaserGame.camera;
                var player = this.map.entities.player;
                var _a = [Dunjax.Maps.toTileMidX(player.x), Dunjax.Maps.toTileMidY(player.y)], fromX = _a[0], fromY = _a[1];
                var cameraTileX = Dunjax.Maps.toTileLeft(camera.x) - tileWidth, cameraTileY = Dunjax.Maps.toTileTop(camera.y) - tileHeight;
                var x1, y1, x2, y2;
                var xLimit = camera.x + camera.width + tileWidth;
                var yLimit = camera.y + camera.height + tileHeight;
                for (var x = cameraTileX; x <= xLimit; x += tileWidth) {
                    for (var y = cameraTileY; y <= yLimit; y += tileHeight, x2 = undefined) {
                        if (!this.map.containsLocation(x, y))
                            continue;
                        var right = x + tileWidth - 1;
                        var bottom = y + tileHeight - 1;
                        var midX = x + tileWidth / 2;
                        var midY = y + tileHeight / 2;
                        var isToLeft = right < player.left;
                        var isToRight = x > player.right - 1;
                        var isAbove = bottom < player.top;
                        var isBelow = y > player.bottom - 1;
                        if (isToLeft && isAbove) {
                            _b = [right, midY], x1 = _b[0], y1 = _b[1];
                            _c = [midX, bottom], x2 = _c[0], y2 = _c[1];
                        }
                        else if (isToRight && isAbove) {
                            _d = [x, midY], x1 = _d[0], y1 = _d[1];
                            _e = [midX, bottom], x2 = _e[0], y2 = _e[1];
                        }
                        else if (isToLeft && isBelow) {
                            _f = [right, midY], x1 = _f[0], y1 = _f[1];
                            _g = [midX, y], x2 = _g[0], y2 = _g[1];
                        }
                        else if (isToRight && isBelow) {
                            _h = [x, midY], x1 = _h[0], y1 = _h[1];
                            _j = [midX, y], x2 = _j[0], y2 = _j[1];
                        }
                        else if (isToLeft)
                            _k = [right, midY], x1 = _k[0], y1 = _k[1];
                        else if (isToRight)
                            _l = [x, midY], x1 = _l[0], y1 = _l[1];
                        else if (isAbove)
                            _m = [midX, bottom], x1 = _m[0], y1 = _m[1];
                        else if (isBelow)
                            _o = [midX, y], x1 = _o[0], y1 = _o[1];
                        else
                            continue;
                        if (!this.map.isLocationVisibleFrom(x1, y1, fromX, fromY)
                            && (x2 === undefined
                                || !this.map.isLocationVisibleFrom(x2, y2, fromX, fromY))) {
                            var darkened = this.darkenedTilePool.pop();
                            if (darkened != null) {
                                darkened.x = x;
                                darkened.y = y;
                            }
                            else
                                darkened = { x: x, y: y };
                            this.darkenedTiles.push(darkened);
                            this.map.darkenTileAtPixel(x, y);
                        }
                    }
                }
                var _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o;
            };
            SightBlocker.prototype.restoreDarkenedTiles = function () {
                var darkenedTiles = this.darkenedTiles;
                while (darkenedTiles.length > 0) {
                    var darkened = darkenedTiles.pop();
                    this.map.undarkenTileAtPixel(darkened.x, darkened.y);
                    this.darkenedTilePool.push(darkened);
                }
            };
            SightBlocker.prototype.hideSightBlockedEntities = function () {
                var _this = this;
                var entities = this.map.entities.entities;
                var camera = Dunjax.Program.game.camera;
                var view = camera.view;
                for (var i = 0; i < entities.length; i++) {
                    var entity = entities[i];
                    var r = entity;
                    if (!Dunjax.Geometry.intersects(r.left, r.top, r.width, r.height, camera.x, camera.y, camera.width, camera.height))
                        continue;
                    var isOutOfSight = function (x, y) {
                        return !view.contains(x, y) || _this.map.isTileDarkenedAtPixel(x, y);
                    };
                    if (isOutOfSight(entity.left, entity.top)
                        && isOutOfSight(entity.right, entity.top)
                        && isOutOfSight(entity.left, entity.bottom)
                        && isOutOfSight(entity.right, entity.bottom))
                        entity.shouldBeDrawn = false;
                }
            };
            SightBlocker.prototype.restoreSightBlockedEntities = function () {
                var entities = this.map.entities.entities;
                entities.forEach(function (e) { return e.shouldBeDrawn = true; });
            };
            return SightBlocker;
        })();
        GameStates.SightBlocker = SightBlocker;
    })(GameStates = Dunjax.GameStates || (Dunjax.GameStates = {}));
})(Dunjax || (Dunjax = {}));
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Beings;
        (function (Beings) {
            var Player;
            (function (Player) {
                var Ammos;
                (function (Ammos) {
                    (function (AmmoType) {
                        AmmoType[AmmoType["Pulse"] = 0] = "Pulse";
                        AmmoType[AmmoType["Grapeshot"] = 1] = "Grapeshot";
                    })(Ammos.AmmoType || (Ammos.AmmoType = {}));
                    var AmmoType = Ammos.AmmoType;
                })(Ammos = Player.Ammos || (Player.Ammos = {}));
            })(Player = Beings.Player || (Beings.Player = {}));
        })(Beings = Entities.Beings || (Entities.Beings = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
/// <reference path="../entities/beings/player/ammos/IAmmo.ts" />
var Dunjax;
(function (Dunjax) {
    var GameStates;
    (function (GameStates) {
        var AmmoType = Dunjax.Entities.Beings.Player.Ammos.AmmoType;
        var StatsDisplay = (function () {
            function StatsDisplay() {
                this.lastArmorValue = 0;
                this.lastKeysValue = -1;
                this.lastPulseAmmoValue = -1;
                this.lastGrapeshotAmmoValue = -1;
            }
            StatsDisplay.prototype.addStats = function () {
                var game = Dunjax.Program.game;
                var image = game.add.image(0, 0, 'armorStat');
                image.fixedToCamera = true;
                var statsHeight = 30;
                var statsY = game.height - statsHeight;
                image.cameraOffset.setTo(0, statsY);
                var style = { font: "bold 12pt Arial", fill: "#ffffff", align: "left" };
                var text = this.armorText = game.add.text(0, 0, '', style);
                text.fixedToCamera = true;
                text.cameraOffset.setTo(image.width + 5, statsY);
                image = game.add.image(0, 0, 'keysStat');
                image.fixedToCamera = true;
                var keysX = game.width / 4;
                image.cameraOffset.setTo(keysX, statsY + 6);
                text = this.keysText = game.add.text(0, 0, '', style);
                text.fixedToCamera = true;
                text.cameraOffset.setTo(keysX + image.width + 5, statsY);
                image = this.pulseAmmoImage = game.add.image(0, 0, 'pulseAmmoStat');
                image.fixedToCamera = true;
                var ammoX = game.width / 2;
                image.cameraOffset.setTo(ammoX, statsY);
                text = this.pulseAmmoText = game.add.text(0, 0, '', style);
                text.fixedToCamera = true;
                text.cameraOffset.setTo(ammoX + image.width + 5, statsY);
                image = this.grapeshotAmmoImage = game.add.image(0, 0, 'grapeshotAmmoStat');
                image.fixedToCamera = true;
                image.cameraOffset.setTo(ammoX, statsY);
                image.visible = false;
                text = this.grapeshotAmmoText = game.add.text(0, 0, '', style);
                text.fixedToCamera = true;
                text.cameraOffset.setTo(ammoX + image.width + 5, statsY);
                text.visible = false;
            };
            StatsDisplay.prototype.updateStats = function (player) {
                if (this.lastArmorValue !== player.armorLeft) {
                    this.armorText.setText("" + player.armorLeft);
                    this.lastArmorValue = player.armorLeft;
                }
                if (this.lastKeysValue !== player.keysHeld) {
                    this.keysText.setText("" + player.keysHeld);
                    this.lastKeysValue = player.keysHeld;
                }
                var ammo = player.ammoInUse;
                var isPulse = ammo.type === AmmoType.Pulse;
                if (this.lastAmmoUsed !== ammo) {
                    this.pulseAmmoImage.visible = isPulse;
                    this.pulseAmmoText.visible = isPulse;
                    this.grapeshotAmmoImage.visible = !isPulse;
                    this.grapeshotAmmoText.visible = !isPulse;
                    this.lastAmmoUsed = ammo;
                    this.lastPulseAmmoValue = this.lastGrapeshotAmmoValue = -1;
                }
                if (isPulse && this.lastPulseAmmoValue !== ammo.amount) {
                    this.pulseAmmoText.setText("" + ammo.amount);
                    this.lastPulseAmmoValue = ammo.amount;
                }
                if (!isPulse && this.lastGrapeshotAmmoValue !== ammo.amount) {
                    this.grapeshotAmmoText.setText("" + ammo.amount);
                    this.lastGrapeshotAmmoValue = ammo.amount;
                }
            };
            return StatsDisplay;
        })();
        GameStates.StatsDisplay = StatsDisplay;
    })(GameStates = Dunjax.GameStates || (Dunjax.GameStates = {}));
})(Dunjax || (Dunjax = {}));
var Dunjax;
(function (Dunjax) {
    var GameStates;
    (function (GameStates) {
        var StoryDisplayer = (function () {
            function StoryDisplayer(levelNumber) {
                this.lineIndex = 0;
                this.lineDelay = 400;
                this.wordIndex = 0;
                this.wordDelay = 120;
                this.level1Story = [
                    "Level 1:  After the Thieves",
                    "",
                    "Your spaceship having crash-landed on an unexplored",
                    "planet, you awake from unconsciousness to find your",
                    "vessel ransacked by beings who have since left the",
                    "area. Missing is a critical element of the ship's",
                    "power core, without which you cannot achieve lift-off.",
                    "Donning your power armor, you follow a large group",
                    "of unidentifiable footprints to a cave entrance set",
                    "into the base of a nearby mountainside.",
                    "",
                    "With limited supplies you cannot survive on this",
                    "planet for long. You must reclaim your power core",
                    "at any cost.  Into the cave you go!",
                ];
                this.level2Story = [
                    "Level 2:  What Dark Thing Lurks Here?",
                    "",
                    "It would appear the task before you is much more",
                    "difficult than you had first guessed. Who or what",
                    "built this massive network of caves?  Who created",
                    "these horrid animations that seek your life at",
                    "every turn? And more importantly, will your armor",
                    "and weapons hold out against them until you can",
                    "find your power core and escape?",
                    "",
                    "The exit from the first cave leads you down a long",
                    "passageway which opens into yet another vast",
                    "underground expanse...",
                ];
                this.endingStory = [
                    "Their master defeated, the remaining monsters",
                    "surrendered your ship's missing part to you,",
                    "intent on you leaving their caves.",
                    "",
                    "With your ship repaired, you were able to lift",
                    "off from the planet and resume your journey.",
                    "",
                    "Congratulations!",
                    "",
                    "We hope you enjoyed this game.",
                    "",
                    "Thank you for playing.",
                ];
                this.continuePrompt = [
                    "",
                    "Press [Enter] (or touch device screen) to continue"
                ];
                var levelStory = this.level1Story;
                if (levelNumber === 2)
                    levelStory = this.level2Story;
                else if (levelNumber < 0)
                    levelStory = this.endingStory;
                var game = Dunjax.Program.game;
                var story = this.story = [];
                levelStory.forEach(function (l) { return story.push(l); });
                if (levelNumber >= 0)
                    this.continuePrompt.forEach(function (l) { return story.push(l); });
                var group = new Phaser.Group(game, game.stage);
                var font = levelNumber >= 0 ? "15px Arial" : "bold 20px Arial";
                var style = { font: font, fill: "#19de65", stroke: null, strokeThickness: 0 };
                if (levelNumber < 0) {
                    style.strokeThickness = 5;
                    style.stroke = 'rgba(0, 0, 0, 1)';
                }
                this.text = game.add.text(32, 32, "", style, group);
                if (levelNumber < 0)
                    this.text.setShadow(3, 3, 'rgba(0, 0, 0, 1)', 5);
            }
            StoryDisplayer.prototype.nextLine = function () {
                var _this = this;
                if (this.lineIndex === this.story.length)
                    return;
                if (this.lineIndex === 0)
                    this.text.text = "";
                this.words = this.story[this.lineIndex++].split(' ');
                this.wordIndex = 0;
                setTimeout(function () { return _this.nextWord.call(_this); }, this.wordDelay);
            };
            StoryDisplayer.prototype.nextWord = function () {
                var _this = this;
                this.text.text = this.text.text.concat(this.words[this.wordIndex++] + " ");
                if (this.wordIndex === this.words.length) {
                    this.text.text += "\n";
                    setTimeout(function () { return _this.nextLine.call(_this); }, this.lineDelay);
                }
                else
                    setTimeout(function () { return _this.nextWord.call(_this); }, this.wordDelay);
            };
            StoryDisplayer.prototype.onDismissed = function () {
                this.text.destroy();
            };
            return StoryDisplayer;
        })();
        GameStates.StoryDisplayer = StoryDisplayer;
    })(GameStates = Dunjax.GameStates || (Dunjax.GameStates = {}));
})(Dunjax || (Dunjax = {}));
var Dunjax;
(function (Dunjax) {
    var Geometry;
    (function (Geometry) {
        function getSlope(x1, y1, x2, y2) {
            if (x1 === x2 && y1 === y2)
                return 0;
            if (x1 === x2)
                return y2 > y1 ? 1000 : -1000;
            return (y2 - y1) / (x2 - x1);
        }
        Geometry.getSlope = getSlope;
        function getXSlope(x1, y1, x2, y2) {
            if (x1 === x2 && y1 === y2)
                return 0;
            if (y1 === y2)
                return x2 > x1 ? 1000 : -1000;
            return (x2 - x1) / (y2 - y1);
        }
        Geometry.getXSlope = getXSlope;
        function getDistance(x1, y1, x2, y2) {
            var b = x2 - x1;
            var a = y2 - y1;
            return Math.sqrt(a * a + b * b);
        }
        Geometry.getDistance = getDistance;
        function isDistanceAtMost(x1, y1, x2, y2, d) {
            var b = x2 - x1;
            var a = y2 - y1;
            return a * a + b * b < d * d;
        }
        Geometry.isDistanceAtMost = isDistanceAtMost;
        function getDifference(x1, y1, x2, y2) {
            return { x: x2 - x1, y: y2 - y1 };
        }
        Geometry.getDifference = getDifference;
        function getUnitVector(x1, y1, x2, y2) {
            if (x1 === x2 && y1 === y2)
                return { x: 0, y: 0 };
            var distance = getDistance(x1, y1, x2, y2);
            return { x: (x2 - x1) / distance, y: (y2 - y1) / distance };
        }
        Geometry.getUnitVector = getUnitVector;
        function isLocationInDirection(x1, y1, x2, y2, dirX, dirY) {
            var range = Math.PI / 3;
            var unit = getUnitVector(x1, y1, x2, y2);
            return getAngleBetween(dirX, dirY, unit.x, unit.y) <= range;
        }
        Geometry.isLocationInDirection = isLocationInDirection;
        function getAngleBetween(x1, y1, x2, y2) {
            var magnitude2 = getMagnitude(x2, y2);
            var magnitude1 = getMagnitude(x1, y1);
            if (magnitude1 === 0 || magnitude2 === 0)
                return 0;
            var cosine = getDotProduct(x1, y1, x2, y2) / (magnitude1 * magnitude2);
            if (cosine > 1)
                cosine = 1;
            if (cosine < -1)
                cosine = -1;
            return Math.acos(cosine);
        }
        Geometry.getAngleBetween = getAngleBetween;
        function getMagnitude(x, y) {
            return Math.sqrt(x * x + y * y);
        }
        Geometry.getMagnitude = getMagnitude;
        function getDotProduct(x1, y1, x2, y2) {
            return x1 * x2 + y1 * y2;
        }
        Geometry.getDotProduct = getDotProduct;
        function setBounds(r, x, y, width, height) {
            r.x = x;
            r.y = y;
            r.width = width;
            r.height = height;
        }
        Geometry.setBounds = setBounds;
        function setBoundsTo(r1, r2) {
            r1.x = r2.x;
            r1.y = r2.y;
            r1.width = r2.width;
            r1.height = r2.height;
        }
        Geometry.setBoundsTo = setBoundsTo;
        function contains(rectX, rectY, rectWidth, rectHeight, x, y) {
            return x >= rectX
                && y >= rectY
                && x < rectX + rectWidth - .0001
                && y < rectY + rectHeight - .0001;
        }
        Geometry.contains = contains;
        function rectContains(r, x, y) {
            return x >= r.left
                && y >= r.top
                && x < r.right
                && y < r.bottom;
        }
        Geometry.rectContains = rectContains;
        function intersects(x1, y1, width1, height1, x2, y2, width2, height2) {
            var tw = width1;
            var th = height1;
            var rw = width2;
            var rh = height2;
            if (rw <= 0 || rh <= 0 || tw <= 0 || th <= 0)
                return false;
            var tx = x1;
            var ty = y1;
            var rx = x2;
            var ry = y2;
            rw += rx;
            rh += ry;
            tw += tx;
            th += ty;
            return ((rw < rx || rw > tx) &&
                (rh < ry || rh > ty) &&
                (tw < tx || tw > rx) &&
                (th < ty || th > ry));
        }
        Geometry.intersects = intersects;
        function intersectsWith(r1, r2) {
            return intersects(r1.left, r1.top, r1.width, r1.height, r2.left, r2.top, r2.width, r2.height);
        }
        Geometry.intersectsWith = intersectsWith;
        function intersectsWith2(r, left, top, width, height) {
            return intersects(r.left, r.top, r.width, r.height, left, top, width, height);
        }
        Geometry.intersectsWith2 = intersectsWith2;
    })(Geometry = Dunjax.Geometry || (Dunjax.Geometry = {}));
})(Dunjax || (Dunjax = {}));
var Dunjax;
(function (Dunjax) {
    (function (Command) {
        Command[Command["Left"] = 0] = "Left";
        Command[Command["Right"] = 1] = "Right";
        Command[Command["Up"] = 2] = "Up";
        Command[Command["Down"] = 3] = "Down";
        Command[Command["Swing"] = 4] = "Swing";
        Command[Command["SwitchGuns"] = 5] = "SwitchGuns";
        Command[Command["Fire"] = 6] = "Fire";
        Command[Command["Jump"] = 7] = "Jump";
    })(Dunjax.Command || (Dunjax.Command = {}));
    var Command = Dunjax.Command;
    ;
})(Dunjax || (Dunjax = {}));
var Dunjax;
(function (Dunjax) {
    var UserInput = (function () {
        function UserInput() {
            var keyboard = Dunjax.Program.game.input.keyboard;
            var cursors = keyboard.createCursorKeys();
            this.commandKeys = [
                cursors.left,
                cursors.right,
                cursors.up,
                cursors.down,
                keyboard.addKey(Phaser.Keyboard.B),
                keyboard.addKey(Phaser.Keyboard.F),
                keyboard.addKey(Phaser.Keyboard.C),
                keyboard.addKey(Phaser.Keyboard.V),
            ];
        }
        UserInput.prototype.isSignaled = function (command) {
            var dPad = this.dPad;
            if (dPad.isDown) {
                if (command === Dunjax.Command.Left && dPad.direction === Phaser.LEFT)
                    return true;
                if (command === Dunjax.Command.Right && dPad.direction === Phaser.RIGHT)
                    return true;
                if (command === Dunjax.Command.Up && dPad.direction === Phaser.UP)
                    return true;
                if (command === Dunjax.Command.Down && dPad.direction === Phaser.DOWN)
                    return true;
            }
            if (command === Dunjax.Command.Jump && this.buttonC.isDown)
                return true;
            if (command === Dunjax.Command.Fire && this.buttonB.isDown)
                return true;
            if (command === Dunjax.Command.Swing && this.buttonARight.isDown)
                return true;
            if (command === Dunjax.Command.SwitchGuns && this.buttonALeft.isDown)
                return true;
            return this.commandKeys[command].isDown;
        };
        return UserInput;
    })();
    Dunjax.UserInput = UserInput;
})(Dunjax || (Dunjax = {}));
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Beings;
        (function (Beings) {
            function asBeing(entity) {
                return entity.isDead !== undefined ? entity : null;
            }
            Beings.asBeing = asBeing;
        })(Beings = Entities.Beings || (Entities.Beings = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
Array.prototype.remove = function (which) {
    return this.splice(this.indexOf(which), 1)[0];
};
Array.prototype.contains = function (object) {
    return this.indexOf(object) >= 0;
};
String.prototype.makeFirstLetterUppercase = function () {
    var s = this;
    if (s.length === 0)
        return s;
    return s.substring(0, 1).toUpperCase() + s.substring(1);
};
var Dunjax;
(function (Dunjax) {
    function randomInt(min, max) {
        return Dunjax.Program.random.integerInRange(min, max);
    }
    Dunjax.randomInt = randomInt;
    function randomReal(min, max) {
        return Dunjax.Program.random.realInRange(min, max);
    }
    Dunjax.randomReal = randomReal;
    function isInViewport(x, y) {
        var camera = Dunjax.Program.game.camera;
        return Dunjax.Geometry.contains(camera.x, camera.y, camera.width, camera.height, x, y);
    }
    Dunjax.isInViewport = isInViewport;
    function getMovementForSpeed(speed) {
        return Math.abs(-speed * Dunjax.Program.game.time.physicsElapsed);
    }
    Dunjax.getMovementForSpeed = getMovementForSpeed;
})(Dunjax || (Dunjax = {}));
//# sourceMappingURL=dunjax.js.map