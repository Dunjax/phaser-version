/// <reference path="maps/TileTypes.ts" />
/// <reference path="maps/Map.ts" />
/// <reference path="entities/beings/player/Player.ts" />
var Dunjax;
(function (Dunjax) {
    var Duration = Dunjax.Time.Duration;
    var Player = Dunjax.Entities.Beings.Player.Player;
    var TerrainFeature = Dunjax.Maps.TerrainFeature;
    var pixelsPerTenFeet = Dunjax.Maps.pixelsPerTenFeet;
    var Map = Dunjax.Maps.Map;
    var Game = (function () {
        function Game(startingLevelNumber) {
            this.postGameOverRunTime = new Duration(4000, false);
            this.lastTimeEntityActionsProcessed = -1;
            this.currentLevelNumber = startingLevelNumber;
        }
        Game.prototype.start = function () {
            var phaserGame = Dunjax.Program.phaserGame;
            phaserGame.state.add("Level1", new Dunjax.LevelState(1, this));
            phaserGame.state.start("Level1");
        };
        Game.prototype.onLevelLoaded = function (state) {
            var map = this.map = state.map = new Map(state.tilemap);
            var player = this.player;
            if (player == null) {
                player = this.player = new Player();
                player.onHasDied = this.onPlayerDead;
                player.onReachedExit = this.onPlayerReachedExit;
                player.determineSize();
            }
            this.putPlayerAtStartCave(this.player);
            if (this.currentLevelNumber === 1) {
                _a = [23 * 32, 53 * 32], player.x = _a[0], player.y = _a[1];
            }
            var master = map.entities.master;
            if (master != null)
                ;
            Dunjax.Time.clock.start();
            this.lastTimeEntityActionsProcessed = -1;
            var _a;
        };
        Game.prototype.putPlayerAtStartCave = function (player) {
            var map = this.map;
            var loc = map.getLocationOfTerrainFeature(TerrainFeature.StartCave);
            _a = [loc.x - pixelsPerTenFeet / 2, loc.y], player.x = _a[0], player.y = _a[1];
            map.entities.addEntity(player);
            player.alignWithSurroundings();
            var _a;
        };
        Game.prototype.isGameDone = function () {
            return this.isOver && this.postGameOverRunTime.isDone;
        };
        Game.prototype.doTurn = function () {
            if (this.isGameDone()) {
                this.exitGame();
                return;
            }
            this.map.entities.entities.forEach(function (e) {
                if (e.sprite.inCamera)
                    e.act.call(e);
            });
            this.map.act();
            if (this.shouldAdvanceToNextMap)
                this.advanceToNextMap();
        };
        Game.prototype.exitGame = function () {
            if (this.map.entities.player.isDead)
                this.onGameOver();
            else if (this.isMasterDead) {
                this.onGameCompleted();
                Dunjax.Program.sounds.playerThrust.stop();
            }
        };
        Game.prototype.advanceToNextMap = function () {
            this.shouldAdvanceToNextMap = false;
            var map = this.map;
            var player = map.entities.player;
            map.entities.removeEntity(player);
            Dunjax.Program.phaserGame.state.start("Level" + ++this.currentLevelNumber);
            this.onNextLevelReady();
        };
        Game.prototype.onPlayerDead = function () {
            this.isOver = true;
            this.postGameOverRunTime.start();
        };
        Game.prototype.onPlayerReachedExit = function () {
            var _this = this;
            if (this.shouldAdvanceToNextMap)
                return;
            this.onLevelCompleted();
            Dunjax.Program.sounds.levelEndReached.play();
            setTimeout(function () { _this.shouldAdvanceToNextMap = true; }, 3000);
        };
        Game.prototype.pause = function () {
        };
        Game.prototype.resume = function () {
        };
        Game.prototype.onMasterKilled = function () {
            this.isMasterDead = true;
            this.isOver = true;
            this.postGameOverRunTime.start();
        };
        return Game;
    })();
    Dunjax.Game = Game;
})(Dunjax || (Dunjax = {}));
