﻿/// <reference path="gamestates/LevelState.ts" />

module Dunjax
{
    import IMap = Maps.IMap;
    import IDuration = Time.IDuration;
    import Duration = Time.Duration;
    import Player = Entities.Beings.Player.Player;
    import TerrainFeature = Maps.TerrainFeature;
    import IPlayer = Entities.Beings.Player.IPlayer;
    import pixelsPerTenFeet = Maps.pixelsPerTenFeet;
    import Map = Maps.Map;
    import LevelState = GameStates.LevelState;

    /* Implementation */
    export class Game implements IGame
    {
        /* Implementation */
        player: IPlayer;

        /**
         * Whether this game it is over (meaning, it has finished before the player 
         * was able to reach the ending).
         */
        private isOver: boolean;
        
        /**
         * How long this game should continue running after it's over. 
         */
        private postGameOverRunTime: IDuration = new Duration(4000, false);

        /* Implementation */
        map: IMap;
        
        /**
         * The Phaser game state for the current level being played in this game.
         */
        private levelState: LevelState;

        /**
         * For testing purposes, which level the game should start on.  
         */
        private startingLevelNumber = 1;

        /**
         * The one-based index of the current level being played in this game.
         */
        private currentLevelNumber = this.startingLevelNumber;

        /* Implementation */
        startCurrentLevel()
        {
            // create a Phaser game-state for the current level
            const game = Program.game;
            const levelNumber = this.currentLevelNumber;
            const levelState = this.levelState = new LevelState(levelNumber, this);

            // add the new game-state to the Phaser game
            const key = levelState.key = "Level${levelNumber}";
            const stateManager = game.state;
            stateManager.add(key, levelState);

            // start the current level running in the Phaser game
            stateManager.start(key);
        }

        /* Implementation */
        onLevelLoaded(tilemap: Phaser.Tilemap)
        {
            // load the map
            const map = this.map = new Map(tilemap); 
            
            map.createTileAnimations();

            // create the entities within the map
            GameStates.createEntities(tilemap, map);

            // if this game's player has not yet been created
            let player = this.player;
            if (player == null) {
                // create the player, and listen for its events
                player = this.player = new Player();
                player.onHasDied = () => this.onPlayerDead.call(this);
                player.onReachedExit = () => this.onPlayerReachedExit.call(this);
                player.determineImage();
            }

            // otherwise, attach a new Phaser sprite object to the player, as Phaser
            // would have destroyed the old one during the level change
            else player.frameworkObject = Program.game.add.sprite(0, 0);

            // set the player into the map at its start cave 
            this.putPlayerAtStartCave(this.player);

            // code for testing various areas of the map
            //if (this.currentLevelNumber === this.startingLevelNumber)
            //    [player.x, player.y] =
                    //[38*32, 13*32]; // out in open after beginning
                    //[13 * 32, 21 * 32]; // by long fall
                    //[64 * 32, 104 * 32]; // at bottom of longest slide
                    //[27*32, 65*32]; // double ladder with door openings
                    //[997, 2032]; // slide sequence
                    //[1511, 2416]; // multi-slide sequence
                    //[1455, 697]; // on ladder near door
                    //[16*32, 47*32]; // by small platforms
                    //[23*32, 53*32]; // by graphshot gun
                    //384,3144)); // small slides
                    //[2639, 1728]; // spiders
                    //[2650, 3168]; // slimes
                    //[2894, 544]; // master
                    //[439, 1840]; // stalagmites
                    //1152, 1920)); // grape
                    //[2484, 890]; // whirlwinds
                    //[39*32, 39*32]; // yellow gargoyles
                    //[1168, 1072]; // yellow gargoyles on ladders
                    //[1179, 2128]; // blue gargoyles
                    //[106*32, 105*32]; // blue gargoyle
                    //[78*32, 81*32]; // long ladders
                    //[89* 32, 26 * 32]; // by final room trigger
                    //[50 * 32, 31 * 32]; // by ladder beside wall
                    //[106 * 32, 57 * 32]; // by level exit
        }
        
        /**
         * Adds the player to the new current map at the location of its start cave.
         */
        private putPlayerAtStartCave(player: IPlayer)
        {
            const map = this.map;
            const loc = map.getLocationOfTerrainFeature(TerrainFeature.StartCave);
            [player.x, player.y] = [loc.x - pixelsPerTenFeet / 2, loc.y];
            map.entities.addEntity(player);
            player.alignWithSurroundings();
            player.isFacingLeft = false;
        }
        
        /**
         * Returns whether this game is done running.
         */
        private isGameDone(): boolean
        {
            return this.isOver && this.postGameOverRunTime.isDone;
        }
        
        /* Implementation */
        doTurn()
        {
            // if this game is over
            if (this.isGameDone()) {
                // exit the game, rather than perform a turn
                this.exitGame();
                return;
            }

            // have the map's entities perform their actions
            this.map.entities.act();

            // have the map perform its actions
            this.map.act();
        }

        /**
         * Fires events which will cause this game to be discarded.
         */
        private exitGame()
        {
            if (this.map.entities.player.isDead) {
                this.levelState.onGameOver();
                this.onGameOver();
            }
        }
        
        /**
         * Has this game switch from running within its current map to running within
         * the next one.
         */
        private advanceToNextMap()
        {
            // remove the player from the previous map
            const map = this.map;
            const player = map.entities.player;
            map.entities.removeEntity(player);

            // start the next level
            this.currentLevelNumber++;
            this.startCurrentLevel();
        }

        /* Handler */
        private onPlayerDead()
        {
            // this game is now over
            this.isOver = true;
            
            // start the final duration during which this game will run, now
            // that it is over
            this.postGameOverRunTime.start();
        }

        /* Handler */
        private onPlayerReachedExit()
        {
            // play the level-end-reached sound, using the HTML audio API since the 
            // game pause about to occur means Phaser won't play it
            new Audio(`sounds/levelEndReached.mp3`).play();

            this.onLevelCompleted();

            // wait a bit before advancing to the next map
            setTimeout(() => this.advanceToNextMap(), 3000);
        }
        
        /* Implementation */
        onLevelCompleted: () => void;

        /* Implementation */
        onGameOver: () => void;
    }
} 