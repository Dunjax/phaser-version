﻿module Dunjax
{
    import IMap = Maps.IMap;
    import IPlayer = Entities.Beings.Player.IPlayer;

    /** 
     * A playing of this game program, where the player tries to progress from one
     * map to the next until either he is killed, or wins.
     */
    export interface IGame
    {
        /**
         * The player object which is progressing through this game instance.
         */
        player: IPlayer;

        /**
         * The current map being played in this game.
         */
        map: IMap;

        /**
         * Has this game start its current level for playing.
         */
        startCurrentLevel(): void;

        /**
         * Allows this game to create its abstractions corresponding to the given 
         * Phaser map and the entities it specifies.
         */
        onLevelLoaded(tilemap: Phaser.Tilemap): void; 
        
        /**
         * Has this game process its next turn.
         */
        doTurn(): void;

        /**
         * Fires when the game is over for the player.
         */
        onGameOver: () => void;

        /**
         * Fires when the player has reached the end of the current level 
         * in this game.
         */
        onLevelCompleted: () => void;
    }
} 