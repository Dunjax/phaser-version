var Dunjax;
(function (Dunjax) {
    (function (Command) {
        Command[Command["Left"] = 0] = "Left";
        Command[Command["Right"] = 1] = "Right";
        Command[Command["Up"] = 2] = "Up";
        Command[Command["Down"] = 3] = "Down";
        Command[Command["Swing"] = 4] = "Swing";
        Command[Command["SwitchGuns"] = 5] = "SwitchGuns";
        Command[Command["Fire"] = 6] = "Fire";
        Command[Command["Jump"] = 7] = "Jump";
    })(Dunjax.Command || (Dunjax.Command = {}));
    var Command = Dunjax.Command;
    ;
    Dunjax.userInput;
})(Dunjax || (Dunjax = {}));
