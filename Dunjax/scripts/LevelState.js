/// <reference path="maps/TileTypes.ts" />
/// <reference path="maps/Map.ts" />
/// <reference path="entities/beings/player/Player.ts" />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Dunjax;
(function (Dunjax) {
    var impassibleTiles = Dunjax.Maps.impassibleTiles;
    var downHalfImpassibleTiles = Dunjax.Maps.downHalfImpassibleTiles;
    var upHalfImpassibleTiles = Dunjax.Maps.upHalfImpassibleTiles;
    var leftDownSlopingTiles = Dunjax.Maps.leftDownSlopingTiles;
    var rightDownSlopingTiles = Dunjax.Maps.rightDownSlopingTiles;
    var leftUpSlopingTiles = Dunjax.Maps.leftUpSlopingTiles;
    var rightUpSlopingTiles = Dunjax.Maps.rightUpSlopingTiles;
    var PlayerClass = Dunjax.Entities.Beings.Player;
    var PlayerFrameSequences = Dunjax.Entities.Beings.Player.PlayerFrameSequences;
    var tileWidth = Dunjax.Maps.tileWidth;
    var tileHeight = Dunjax.Maps.tileHeight;
    var PulseAmmo = Dunjax.Entities.Beings.Player.Ammos.PulseAmmo;
    var Item = Dunjax.Entities.Items.Item;
    var ItemFrameSequences = Dunjax.Entities.Items.ItemFrameSequences;
    var ItemType = Dunjax.Entities.Items.ItemType;
    var PulseShot = Dunjax.Entities.Shots.PulseShot;
    var PulseShotFrameSequences = Dunjax.Entities.Shots.PulseShotFrameSequences;
    var HalberdShot = Dunjax.Entities.Shots.HalberdShot;
    var HalberdShotFrameSequences = Dunjax.Entities.Shots.HalberdShotFrameSequences;
    var GrapeshotShot = Dunjax.Entities.Shots.GrapeshotShot;
    var GrapeshotShotFrameSequences = Dunjax.Entities.Shots.GrapeshotShotFrameSequences;
    var AllSounds = Dunjax.Sounds.AllSounds;
    var LevelState = (function (_super) {
        __extends(LevelState, _super);
        function LevelState(levelNumber, dunjaxGame) {
            _super.call(this);
            this.darkenedTiles = [];
            this.darkenedTilePool = [];
            this.lastArmorValue = 0;
            this.lastKeysValue = -1;
            this.lastPulseAmmoValue = -1;
            this.lastGrapeshotAmmoValue = -1;
            this.levelNumber = levelNumber;
            this.dunjaxGame = dunjaxGame;
        }
        LevelState.prototype.preload = function () {
            var phaserGame = Dunjax.Program.phaserGame;
            phaserGame.load.tilemap('map', "maps/" + this.levelNumber + ".json", null, Phaser.Tilemap.TILED_JSON);
            phaserGame.load.image('tiles', 'maps/tiles.png');
            phaserGame.load.image('armorStat', 'images/armorStat.png');
            phaserGame.load.image('keysStat', 'images/entities/items/key1.png');
            phaserGame.load.image('pulseAmmoStat', 'images/entities/items/pulseAmmoBig1.png');
            phaserGame.load.image('grapeshotAmmoStat', 'images/entities/items/grapeshotGun1.png');
            this.loadFrameSequences();
            if (Dunjax.Program.sounds == null)
                Dunjax.Program.sounds = new AllSounds();
        };
        LevelState.prototype.loadFrameSequences = function () {
            if (PlayerClass.playerFrameSequences == null)
                PlayerClass.playerFrameSequences = new PlayerFrameSequences();
            if (Item.frameSequences == null)
                Item.frameSequences = new ItemFrameSequences();
            if (PulseShot.frameSequences == null)
                PulseShot.frameSequences = new PulseShotFrameSequences();
            if (GrapeshotShot.frameSequences == null)
                GrapeshotShot.frameSequences = new GrapeshotShotFrameSequences();
            if (HalberdShot.frameSequences == null)
                HalberdShot.frameSequences = new HalberdShotFrameSequences();
        };
        LevelState.prototype.create = function () {
            var _this = this;
            var phaserGame = Dunjax.Program.phaserGame;
            var tilemap = this.tilemap = phaserGame.add.tilemap('map');
            tilemap.addTilesetImage('tiles');
            var layer = this.layer = tilemap.createLayer('tiles');
            layer.resizeWorld();
            var physics = phaserGame.physics;
            physics.startSystem(Phaser.Physics.NINJA);
            this.setNinjaTileTypes();
            this.dunjaxGame.onLevelLoaded(this);
            this.createItems();
            this.map.createTileAnimations();
            this.map.lineOfSightChangeOccurred =
                function () { _this.isSightBlockRefreshNeeded = true; };
            var player = this.dunjaxGame.player;
            physics.ninja.enableAABB(player.sprite);
            phaserGame.camera.follow(player.sprite);
            phaserGame.camera.bounds = null;
            phaserGame.time.advancedTiming = true;
            if (Dunjax.userInput == null)
                Dunjax.userInput = new Dunjax.UserInput();
            this.addStats();
            Dunjax.Program.sounds.addSounds();
        };
        LevelState.prototype.setNinjaTileTypes = function () {
            var phaserGame = Dunjax.Program.phaserGame;
            var physics = phaserGame.physics;
            var typeMap = {};
            var impassibleId = 1;
            impassibleTiles.forEach(function (t) { return typeMap[t.toString()] = impassibleId; });
            var downHalfImpassibleId = 30;
            downHalfImpassibleTiles.forEach(function (t) { return typeMap[t.toString()] = downHalfImpassibleId; });
            var upHalfImpassibleId = 32;
            upHalfImpassibleTiles.forEach(function (t) { return typeMap[t.toString()] = upHalfImpassibleId; });
            var leftDownSlopingId = 3;
            leftDownSlopingTiles.forEach(function (t) { return typeMap[t.toString()] = leftDownSlopingId; });
            var rightDownSlopingId = 2;
            rightDownSlopingTiles.forEach(function (t) { return typeMap[t.toString()] = rightDownSlopingId; });
            var leftUpSlopingId = 4;
            leftUpSlopingTiles.forEach(function (t) { return typeMap[t.toString()] = leftUpSlopingId; });
            var rightUpSlopingId = 5;
            rightUpSlopingTiles.forEach(function (t) { return typeMap[t.toString()] = rightUpSlopingId; });
            this.ninjaTiles =
                physics.ninja.convertTilemap(this.tilemap, this.layer, typeMap);
        };
        LevelState.prototype.update = function () {
            var game = this.dunjaxGame;
            game.doTurn();
            var player = game.player;
            var body = player.sprite.body;
            var tiles = this.ninjaTiles;
            for (var i = 0; i < tiles.length; i++)
                body.aabb.collideAABBVsTile(tiles[i].tile);
            this.restoreSightBlockedEntities();
            this.checkToAddSightBlocking();
            this.hideSightBlockedEntities();
            this.updateStats();
        };
        LevelState.prototype.checkToAddSightBlocking = function () {
            var player = this.dunjaxGame.player;
            if (player == null)
                return;
            var playerTileX = Dunjax.Maps.toTileX(player.x);
            var playerTileY = Dunjax.Maps.toTileY(player.y);
            if (this.isSightBlockRefreshNeeded
                || playerTileX !== this.oldPlayerTileX
                || playerTileY !== this.oldPlayerTileY) {
                this.oldPlayerTileX = playerTileX;
                this.oldPlayerTileY = playerTileY;
                this.restoreDarkenedTiles();
                this.addSightBlocking();
                this.tilemap.layers[0].dirty = true;
                this.isSightBlockRefreshNeeded = false;
            }
        };
        LevelState.prototype.addSightBlocking = function () {
            var phaserGame = Dunjax.Program.phaserGame;
            var camera = phaserGame.camera;
            var map = this.map;
            var player = this.dunjaxGame.player;
            var _a = [Dunjax.Maps.toTileMidX(player.x), Dunjax.Maps.toTileMidY(player.y)], fromX = _a[0], fromY = _a[1];
            var cameraTileX = Dunjax.Maps.toTileLeft(camera.x) - tileWidth, cameraTileY = Dunjax.Maps.toTileTop(camera.y) - tileHeight;
            var x1, y1, x2, y2;
            var xLimit = camera.x + camera.width + tileWidth;
            var yLimit = camera.y + camera.height + tileHeight;
            for (var x = cameraTileX; x <= xLimit; x += tileWidth) {
                for (var y = cameraTileY; y <= yLimit; y += tileHeight, x2 = undefined) {
                    if (!this.map.containsLocation(x, y))
                        continue;
                    var right = x + tileWidth - 1;
                    var bottom = y + tileHeight - 1;
                    var midX = x + tileWidth / 2;
                    var midY = y + tileHeight / 2;
                    var isToLeft = right < player.left;
                    var isToRight = x > player.right - 1;
                    var isAbove = bottom < player.top;
                    var isBelow = y > player.bottom - 1;
                    if (isToLeft && isAbove) {
                        _b = [right, midY], x1 = _b[0], y1 = _b[1];
                        _c = [midX, bottom], x2 = _c[0], y2 = _c[1];
                    }
                    else if (isToRight && isAbove) {
                        _d = [x, midY], x1 = _d[0], y1 = _d[1];
                        _e = [midX, bottom], x2 = _e[0], y2 = _e[1];
                    }
                    else if (isToLeft && isBelow) {
                        _f = [right, midY], x1 = _f[0], y1 = _f[1];
                        _g = [midX, y], x2 = _g[0], y2 = _g[1];
                    }
                    else if (isToRight && isBelow) {
                        _h = [x, midY], x1 = _h[0], y1 = _h[1];
                        _j = [midX, y], x2 = _j[0], y2 = _j[1];
                    }
                    else if (isToLeft)
                        _k = [right, midY], x1 = _k[0], y1 = _k[1];
                    else if (isToRight)
                        _l = [x, midY], x1 = _l[0], y1 = _l[1];
                    else if (isAbove)
                        _m = [midX, bottom], x1 = _m[0], y1 = _m[1];
                    else if (isBelow)
                        _o = [midX, y], x1 = _o[0], y1 = _o[1];
                    else
                        continue;
                    if (!map.isLocationVisibleFrom(x1, y1, fromX, fromY)
                        && (x2 === undefined
                            || !map.isLocationVisibleFrom(x2, y2, fromX, fromY))) {
                        var darkened = this.darkenedTilePool.pop();
                        if (darkened != null) {
                            darkened.x = x;
                            darkened.y = y;
                        }
                        else
                            darkened = { x: x, y: y };
                        this.darkenedTiles.push(darkened);
                        map.darkenTileAtPixel(x, y);
                    }
                }
            }
            var _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o;
        };
        LevelState.prototype.restoreDarkenedTiles = function () {
            var darkenedTiles = this.darkenedTiles;
            while (darkenedTiles.length > 0) {
                var darkened = darkenedTiles.pop();
                this.map.undarkenTileAtPixel(darkened.x, darkened.y);
                this.darkenedTilePool.push(darkened);
            }
        };
        LevelState.prototype.addStats = function () {
            var phaserGame = Dunjax.Program.phaserGame;
            var image = phaserGame.add.image(0, 0, 'armorStat');
            image.fixedToCamera = true;
            var statsHeight = 30;
            var statsY = phaserGame.height - statsHeight;
            image.cameraOffset.setTo(0, statsY);
            var style = { font: "bold 12pt Arial", fill: "#ffffff", align: "left" };
            var text = this.armorText = phaserGame.add.text(0, 0, '', style);
            text.fixedToCamera = true;
            text.cameraOffset.setTo(image.width + 5, statsY);
            image = phaserGame.add.image(0, 0, 'keysStat');
            image.fixedToCamera = true;
            var keysX = phaserGame.width / 4;
            image.cameraOffset.setTo(keysX, statsY + 6);
            text = this.keysText = phaserGame.add.text(0, 0, '', style);
            text.fixedToCamera = true;
            text.cameraOffset.setTo(keysX + image.width + 5, statsY);
            image = this.pulseAmmoImage = phaserGame.add.image(0, 0, 'pulseAmmoStat');
            image.fixedToCamera = true;
            var ammoX = phaserGame.width / 2;
            image.cameraOffset.setTo(ammoX, statsY);
            text = this.pulseAmmoText = phaserGame.add.text(0, 0, '', style);
            text.fixedToCamera = true;
            text.cameraOffset.setTo(ammoX + image.width + 5, statsY);
            image = this.grapeshotAmmoImage =
                phaserGame.add.image(0, 0, 'grapeshotAmmoStat');
            image.fixedToCamera = true;
            image.cameraOffset.setTo(ammoX, statsY);
            image.visible = false;
            text = this.grapeshotAmmoText = phaserGame.add.text(0, 0, '', style);
            text.fixedToCamera = true;
            text.cameraOffset.setTo(ammoX + image.width + 5, statsY);
            text.visible = false;
        };
        LevelState.prototype.updateStats = function () {
            var player = this.dunjaxGame.player;
            if (this.lastArmorValue !== player.armorLeft) {
                this.armorText.setText('' + player.armorLeft);
                this.lastArmorValue = player.armorLeft;
            }
            if (this.lastKeysValue !== player.keysHeld) {
                this.keysText.setText('' + player.keysHeld);
                this.lastKeysValue = player.keysHeld;
            }
            var ammo = player.ammoInUse;
            var isPulse = ammo instanceof PulseAmmo;
            if (this.lastAmmoUsed !== ammo) {
                this.pulseAmmoImage.visible = isPulse;
                this.pulseAmmoText.visible = isPulse;
                this.grapeshotAmmoImage.visible = !isPulse;
                this.grapeshotAmmoText.visible = !isPulse;
                this.lastAmmoUsed = ammo;
                this.lastPulseAmmoValue = this.lastGrapeshotAmmoValue = -1;
            }
            if (isPulse && this.lastPulseAmmoValue !== ammo.amount) {
                this.pulseAmmoText.setText('' + ammo.amount);
                this.lastPulseAmmoValue = ammo.amount;
            }
            if (!isPulse && this.lastGrapeshotAmmoValue !== ammo.amount) {
                this.grapeshotAmmoText.setText('' + ammo.amount);
                this.lastGrapeshotAmmoValue = ammo.amount;
            }
        };
        LevelState.prototype.createItems = function () {
            var entities = this.tilemap.objects['entities'];
            var itemEntities = entities.filter(function (e) { return e.type === 'item'; });
            for (var i = 0; i < itemEntities.length; i++) {
                var itemEntity = itemEntities[i];
                var item = new Item(ItemType[itemEntity.name], itemEntity.x + itemEntity.width / 2, itemEntity.y + itemEntity.height / 2);
                item.id = itemEntity.id;
                item.determineSize();
                this.map.entities.addEntity(item);
                item.alignWithSurroundings();
            }
        };
        LevelState.prototype.hideSightBlockedEntities = function () {
            var _this = this;
            var entities = this.map.entities.items;
            for (var i = 0; i < entities.length; i++) {
                var entity = entities[i];
                var check = function (x, y) { return _this.map.isTileDarkenedAtPixel(x, y); };
                if (check(entity.left, entity.top)
                    && check(entity.right, entity.top)
                    && check(entity.left, entity.bottom)
                    && check(entity.right, entity.bottom))
                    entity.sprite.visible = false;
            }
        };
        LevelState.prototype.restoreSightBlockedEntities = function () {
            var entities = this.map.entities.items;
            entities.forEach(function (e) { return e.sprite.visible = true; });
        };
        return LevelState;
    })(Phaser.State);
    Dunjax.LevelState = LevelState;
})(Dunjax || (Dunjax = {}));
