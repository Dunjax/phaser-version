/// <reference path="time/Clock.ts" />
/// <reference path="sounds/AllSounds.ts" />
var Dunjax;
(function (Dunjax) {
    var Clock = Dunjax.Time.Clock;
    var Program = (function () {
        function Program() {
            var phaserGame = Program.phaserGame =
                new Phaser.Game(800, 600, Phaser.AUTO, 'content');
            Dunjax.Time.clock = new Clock(phaserGame);
            this.game = new Dunjax.Game(1);
        }
        Program.prototype.startGame = function () {
            this.game.start();
        };
        return Program;
    })();
    Dunjax.Program = Program;
    function isInCamera(x, y) {
        var camera = Program.phaserGame.camera;
        return Dunjax.Geometry.contains(camera.x, camera.y, camera.width, camera.height, x, y);
    }
    Dunjax.isInCamera = isInCamera;
})(Dunjax || (Dunjax = {}));
window.onload = function () {
    var program = new Dunjax.Program();
    program.startGame();
};
