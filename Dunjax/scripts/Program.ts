﻿/// <reference path="time/Clock.ts" />
/// <reference path="sounds/AllSounds.ts" />
/// <reference path="gamestates/TitleState.ts" />

module Dunjax
{
    import Clock = Time.Clock;
    import AllSounds = Sounds.AllSounds;
    import TitleState = GameStates.TitleState;
    import IClock = Time.IClock;
    import IImage = Images.IImage;

    /**
     * Represents this game program.
     */
    export class Program
    {
        /**
         * The Phaser game object runs the Dunjax game instances this program creates.
         */
        static game: Phaser.Game;

        /**
         * The sounds used while the game is playing.
         */
        static sounds: AllSounds;

        /**
         * The clock used to time durations in the game.
         */
        static clock: IClock;

        /**
         * The random number generator used throughout the game.
         */
        static random = new Phaser.RandomDataGenerator([new Date().getTime()]);

        /**
         * The Phaser state of this game program which displays the title screen.
         */
        private titleState: TitleState;

        /**
         * An image used for any entity which is invisible.
         */
        static emptyImage: IImage;

        /**
         * Registers what the user would like the player to do.  
         */
        static userInput: IUserInput;

        /**
         * Handles creation, placement, and removal of virtual dpads and buttons.
         */
        static virtualPadManager: any;

        constructor()
        {
            // create the Phaser game object
            const phaserGame = Program.game =
                new Phaser.Game(800, 600, Phaser.AUTO, 'content');

            // create the game clock
            Program.clock = new Clock(phaserGame);
        }

        /**
         * Starts this game program displaying the title screen. 
         */
        start()
        {
            // create the title screen game state
            const game = Program.game;
            const stateManager = game.state;
            const state = this.titleState = new TitleState();
            state.onStateExited = () => this.onTitleStateExited.call(this);
            
            // add the title screen game state to the Phaser game
            const key = state.key = "Title";
            stateManager.add(key, state);

            // start the title screen game state
            stateManager.start(key);
        }

        /**
         * Informs this program that the user has signaled they want to progress
         * from the title screen.
         */
        private onTitleStateExited()
        {
            // create a Dunjax game
            const game = new Game();
            game.onGameOver = () => this.onGameOver.call(this);

            // start the first level of the game
            game.startCurrentLevel();
        }

        /**
         * Informs this program that the currently running game is now over.
         */
        private onGameOver()
        {
            // return to the title screen
            const game = Program.game;
            const stateManager = game.state;
            stateManager.start(this.titleState.key);
        }
    }
}

window.onload = () => {
    var program = new Dunjax.Program();
    program.start();
};
