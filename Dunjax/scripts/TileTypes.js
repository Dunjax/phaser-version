var Dunjax;
(function (Dunjax) {
    var TileTypes = (function () {
        function TileTypes() {
        }
        TileTypes.initialize = function () {
            TileTypes.sightBlockingTiles.forEach(function (tileId) {
                return TileTypes.sightBlockingTilesMap[tileId.toString()] = tileId;
            });
        };
        TileTypes.impassibleTiles = [
            TileIds.WallRock1,
            TileIds.WallRock2,
            TileIds.ClosedDoorBrick,
            TileIds.LockedDoorBrick,
            TileIds.WallTerraceDown,
            TileIds.WallTerraceDownLeft,
            TileIds.WallTerraceDownRight,
            TileIds.WallTerrace,
            TileIds.ClosedDoorCave,
            TileIds.LockedDoorCave
        ];
        TileTypes.downHalfImpassibleTiles = [
            TileIds.RockBrick1, TileIds.RockBrick2,
            TileIds.TrashBrick1, TileIds.TrashBrick2, TileIds.TrashBrick3,
            TileIds.TrashBrick4,
            TileIds.RockCave1, TileIds.RockCave2,
            TileIds.TrashCave1, TileIds.TrashCave2, TileIds.TrashCave3, TileIds.TrashCave4,
            TileIds.Ledge1Left, TileIds.Ledge1A, TileIds.Ledge1B,
            TileIds.Ledge1C, TileIds.Ledge1Right,
        ];
        TileTypes.upHalfImpassibleTiles = [
            TileIds.ProjectingLedge,
        ];
        TileTypes.leftDownSlopingTiles = [
            TileIds.SlideTerraceBrickLeft,
            TileIds.SlideTerraceRockLeft,
            TileIds.SlideRockCaveLeft,
            TileIds.SlideRockBrickLeft,
        ];
        TileTypes.rightDownSlopingTiles = [
            TileIds.SlideTerraceBrickRight,
            TileIds.SlideTerraceRockRight,
            TileIds.SlideRockCaveRight,
            TileIds.SlideRockBrickRight,
        ];
        TileTypes.leftUpSlopingTiles = [
            TileIds.SlideTerraceBrickUpLeft,
            TileIds.SlideTerraceRockUpLeft,
            TileIds.SlideRockCaveUpLeft,
            TileIds.SlideRockBrickUpLeft,
        ];
        TileTypes.rightUpSlopingTiles = [
            TileIds.SlideTerraceBrickUpRight,
            TileIds.SlideTerraceRockUpRight,
            TileIds.SlideRockCaveUpRight,
            TileIds.SlideRockBrickUpRight,
        ];
        TileTypes.sightBlockingTiles = [
            TileIds.Darkness,
            TileIds.WallRock1,
            TileIds.WallRock2,
            TileIds.SecretDoorRock,
            TileIds.ClosedDoorBrick,
            TileIds.LockedDoorBrick,
            TileIds.TerraceRockUp,
            TileIds.SecretDoorTerraceWall,
            TileIds.WallTerraceDown,
            TileIds.WallTerraceDownLeft,
            TileIds.WallTerraceDownRight,
            TileIds.WallTerrace,
            TileIds.ClosedDoorCave,
            TileIds.LockedDoorCave,
        ];
        TileTypes.sightBlockingTilesMap = Object.create(null);
        return TileTypes;
    })();
    Dunjax.TileTypes = TileTypes;
    TileTypes.initialize();
})(Dunjax || (Dunjax = {}));
//# sourceMappingURL=TileTypes.js.map