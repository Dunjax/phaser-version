var Dunjax;
(function (Dunjax) {
    Dunjax.userInput;
    var UserInput = (function () {
        function UserInput() {
            var keyboard = Dunjax.Program.phaserGame.input.keyboard;
            var cursors = keyboard.createCursorKeys();
            this.commandKeys = [
                cursors.left,
                cursors.right,
                cursors.up,
                cursors.down,
                keyboard.addKey(Phaser.Keyboard.V),
                keyboard.addKey(Phaser.Keyboard.F),
                keyboard.addKey(Phaser.Keyboard.C),
                keyboard.addKey(Phaser.Keyboard.X),
            ];
        }
        UserInput.prototype.isSignaled = function (command) {
            return this.commandKeys[command].isDown;
        };
        return UserInput;
    })();
    Dunjax.UserInput = UserInput;
})(Dunjax || (Dunjax = {}));
