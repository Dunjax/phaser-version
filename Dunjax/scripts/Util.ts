﻿module Dunjax
{
    /**
     * Returns an integer from the given range, the boundaries of which are inclusive.
     */
    export function randomInt(min: number, max: number)
    {
        return Program.random.integerInRange(min, max);
    }

    /**
     * Returns a real number from the given range.
     */
    export function randomReal(min: number, max: number)
    {
        return Program.random.realInRange(min, max);
    }

    /**
     * Returns whether the given pixel location is currently within the game's viewport.
     */
    export function isInViewport(x: number, y: number)
    {
        const camera = Program.game.camera;
        return Geometry.contains(camera.x, camera.y, camera.width, camera.height, x, y);
    }

    /**
     * Returns how many pixels of movement equates to the given speed (in pixels/second),
     * relative to the amount of time which has elapsed since the previous game turn.
     */
    export function getMovementForSpeed(speed: number): number
    {
        return Math.abs(-speed * Program.game.time.physicsElapsed);
    }

    /**
     * Is used to declare the type of objects we wish to use as a string-property 
     * to number map.
     */
    export interface IStringToNumberMap
    {
        [key: string]: number;
    }

    /**
     * Is used to declare the type of objects we wish to use as a string-property 
     * to boolean map.
     */
    export interface IStringToBooleanMap
    {
        [key: string]: boolean;
    }
} 