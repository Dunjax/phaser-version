var Dunjax;
(function (Dunjax) {
    var Animations;
    (function (Animations) {
        var Animation = (function () {
            function Animation() {
            }
            Animation.prototype.getIsDone = function () { return this.isDone; };
            Animation.prototype.getCurrentImage = function () {
                return this.frameSequence != null
                    ? this.frameSequence.getFrame(this.currentFrameIndex).image : null;
            };
            Animation.prototype.getFrameSequence = function () { return this.frameSequence; };
            Animation.prototype.setFrameSequence = function (sequence) {
                if (!this.isDone && this.frameSequence === sequence)
                    return false;
                this.frameSequence = sequence;
                this.isDone = false;
                this.currentFrameIndex = 0;
                this.frameStartTime = Dunjax.Time.clock.time;
                return true;
            };
            Animation.prototype.checkForAdvance = function () {
                var sequence = this.frameSequence;
                if (this.isDone || sequence == null)
                    return false;
                var advanced = false;
                if (this.isTimeToAdvanceToNextFrame()) {
                    if (this.hasMoreFramesToPlay()) {
                        if (sequence.requiresNodToAdvance && !sequence.isNodGivenToAdvance)
                            return false;
                        this.advanceToNextFrame();
                        advanced = true;
                        sequence.isNodGivenToAdvance = false;
                    }
                    else
                        advanced = this.playedLastFrame();
                }
                return advanced;
            };
            Animation.prototype.isTimeToAdvanceToNextFrame = function () {
                var frame = this.frameSequence.getFrame(this.currentFrameIndex);
                return Dunjax.Time.clock.time - this.frameStartTime >= frame.duration;
            };
            Animation.prototype.hasMoreFramesToPlay = function () {
                return this.currentFrameIndex < this.frameSequence.framesCount - 1;
            };
            Animation.prototype.advanceToNextFrame = function () {
                this.currentFrameIndex++;
                this.frameStartTime = Dunjax.Time.clock.time;
            };
            Animation.prototype.playedLastFrame = function () {
                var advanced = false;
                if (this.frameSequence.isRepeating) {
                    this.restart();
                    advanced = true;
                }
                else
                    this.isDone = true;
                return advanced;
            };
            Animation.prototype.restart = function () {
                this.currentFrameIndex = 0;
                this.frameStartTime = Dunjax.Time.clock.time;
                this.isDone = false;
            };
            Animation.prototype.getCurrentFrameDuration = function () {
                return this.frameSequence.getFrame(this.currentFrameIndex).duration;
            };
            return Animation;
        })();
        Animations.Animation = Animation;
    })(Animations = Dunjax.Animations || (Dunjax.Animations = {}));
})(Dunjax || (Dunjax = {}));
