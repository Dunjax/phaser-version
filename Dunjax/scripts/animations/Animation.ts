﻿module Dunjax.Animations
{
    import IImage = Images.IImage;

    /* Implementation */
    export class Animation implements IAnimation
    {
        /* Implementation */
        private currentFrameIndex: number;

        /* Implementation */
        isDone: boolean;

        /**
         * The game time (in ms) when the current frame of this animation
         * became current.
         */
        private frameStartTime: number;

        /* Implementation */
        get currentImage(): IImage
        {
            return this._frameSequence != null
                ? this._frameSequence.getFrame(this.currentFrameIndex).image : null;
        }

        /* Implementation */
        private _frameSequence: IFrameSequence;
        get frameSequence(): IFrameSequence { return this._frameSequence; }
        set frameSequence(value: IFrameSequence)
        {
            // if the given sequence is already currently in progress, do nothing
            // so it may finish
            if (!this.isDone && this._frameSequence === value) return;

            // position this animation at the start of the given sequence
            this._frameSequence = value;
            this.isDone = false;
            this.currentFrameIndex = 0;
            this.frameStartTime = Program.clock.time;
        }

        /* Implementation */
        isNodGivenToAdvance: boolean;

        /* Implementation */
        checkForAdvance(): boolean
        {
            // if this animation is done, or has no frame sequence to display,
            // there is no advance
            const sequence = this._frameSequence;
            if (this.isDone || sequence == null) return false;

            // if the current frame's duration is complete
            let advanced = false;
            if (this.isTimeToAdvanceToNextFrame()) {
                // if there are more frames to play
                if (this.hasMoreFramesToPlay()) {
                    // if our frame sequence requires a nod to advance, which hasn't 
                    // been given, it cannot advance
                    if (sequence.requiresNodToAdvance && !this.isNodGivenToAdvance)
                        return false;

                    // advance to the next frame
                    this.advanceToNextFrame();
                    advanced = true;
                    this.isNodGivenToAdvance = false;
                }

                // otherwise, the last frame has been played
                else advanced = this.onPlayedLastFrame();
            }

            return advanced;
        }
        
        /**
         * Returns whether this animation's current frame has played for its intended duration.
         */
        private isTimeToAdvanceToNextFrame(): boolean
        {
            const frame = this._frameSequence.getFrame(this.currentFrameIndex);
            return Program.clock.time - this.frameStartTime >= frame.duration;
        }
        
        /**
         * Returns whether there are frames that have yet to be played in this
         * animation's current frame sequence.
         */
        private hasMoreFramesToPlay(): boolean
        {
            return this.currentFrameIndex < this._frameSequence.framesCount - 1;
        }
        
        /**
         * Advances this animation to the next frame in its current frame-sequence.
         */
        private advanceToNextFrame(): void
        {
            this.currentFrameIndex++;
            this.frameStartTime = Program.clock.time;
        }
        
        /**
         * Is called when this animation has finished playing the last frame in 
         * its current frame sequence.
         * 
         * @return  Whether this animation was advanced to a next frame.
         */
        private onPlayedLastFrame(): boolean
        {
            // if the sequence repeats, restart it
            let advanced = false;
            if (this._frameSequence.isRepeating) {
                this.restart();
                advanced = true;
            }

            // otherwise, the sequence is done
            else this.isDone = true;

            return advanced;
        }

        /* Implementation */
        restart(): void
        {
            this.currentFrameIndex = 0;
            this.frameStartTime = Program.clock.time;
            this.isDone = false;
        }
    }
}