var Dunjax;
(function (Dunjax) {
    var Animations;
    (function (Animations) {
        var Frame = (function () {
            function Frame(image) {
                this.defaultFrameDuration = 125;
                this.image = image;
                this.duration = this.defaultFrameDuration;
            }
            return Frame;
        })();
        Animations.Frame = Frame;
    })(Animations = Dunjax.Animations || (Dunjax.Animations = {}));
})(Dunjax || (Dunjax = {}));
