﻿module Dunjax.Animations
{
    import IImage = Images.IImage;

    /* Implementation */
    export class Frame implements IFrame
    {
        /* Implementation */
        image: IImage;

        /* Implementation */
        duration: number;

        constructor(image: IImage)
        {
            this.image = image;
            this.duration = 125;
        }
    }
} 