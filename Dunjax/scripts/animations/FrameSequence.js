/// <reference path="../images/Image.ts" />
var Dunjax;
(function (Dunjax) {
    var Animations;
    (function (Animations) {
        var Image = Dunjax.Images.Image;
        var FrameSequence = (function () {
            function FrameSequence(imagePrefix, length, isRepeating, repeatedFrames, images) {
                this.imagePrefix = imagePrefix;
                this.isRepeating = isRepeating;
                this.frames = [];
                for (var i = 0; i < length; i++) {
                    var image = this.getFrameImage(i, repeatedFrames, images, this.frames);
                    if (image == null
                        && (images == null || i >= images.length - 1))
                        break;
                    var frame = new Animations.Frame(image);
                    this.frames.push(frame);
                }
                if (this.frames.length === 0)
                    throw new Error("No frames found for sequence with prefix " + imagePrefix);
            }
            FrameSequence.prototype.getFrame = function (index) { return this.frames[index]; };
            Object.defineProperty(FrameSequence.prototype, "framesCount", {
                get: function () { return this.frames.length; },
                enumerable: true,
                configurable: true
            });
            FrameSequence.prototype.getFrameImage = function (frameIndex, repeatedFrames, images, fromFrames) {
                var image;
                if (images != null && frameIndex < images.length) {
                    image = images[frameIndex];
                }
                else if (repeatedFrames != null
                    && frameIndex < repeatedFrames.length
                    && repeatedFrames[frameIndex] >= 0) {
                    image = fromFrames[repeatedFrames[frameIndex]].image;
                }
                else
                    image = new Image(this.imagePrefix + (frameIndex + 1));
                return image;
            };
            FrameSequence.prototype.setDurationOfAllFrames = function (duration) {
                this.frames.forEach(function (f) { return f.duration = duration; });
            };
            return FrameSequence;
        })();
        Animations.FrameSequence = FrameSequence;
    })(Animations = Dunjax.Animations || (Dunjax.Animations = {}));
})(Dunjax || (Dunjax = {}));
