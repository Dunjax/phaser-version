﻿/// <reference path="../images/Image.ts" />

module Dunjax.Animations
{
    import IImage = Images.IImage;
    import Image = Images.Image;
    import IFrame = Animations.IFrame;

    /* Implementation */
    export class FrameSequence implements IFrameSequence
    {
        /* Implementation */
        imagePrefix: string;

        /**
         * The sequence of frames this object encapsulates.
         */
        private frames: IFrame[];

        /* Implementation */
        getFrame(index: number): IFrame { return this.frames[index]; }

        /* Implementation */
        get framesCount(): number { return this.frames.length; }

        /* Implementation */
        isRepeating: boolean;

        /* Implementation */
        requiresNodToAdvance: boolean;

        /* Implementation */
        switchToWhenDone: IFrameSequence;

        /**
         * repeatedFrames -
         *      if provided, each non-negative entry n in the ith position in
         *      this array means to reuse the image of frame n for the ith
         *      frame in this sequence
         * images -
         *      if provided, is an array of images to use for this sequence's
         *      frames, rather than trying to load them from disk. This allows
         *      sharing of images across sequences
         */
        constructor(
            imagePrefix: string, length: number, isRepeating: boolean,
            repeatedFrames: number[], images: IImage[])
        {
            this.imagePrefix = imagePrefix;
            this.isRepeating = isRepeating;

            // for each frame that will be in this sequence
            this.frames = [];
            for (let i = 0; i < length; i++) {
                // get the image for this next frame; if there is none, then
                // if we aren't out of given images to process, we are
                // done creating frames for this sequence
                const image = this.getFrameImage(i, repeatedFrames, images, this.frames);
                if (image == null
                    && (images == null || i >= images.length - 1)) break;
        		
                // create the next frame for this sequence using the image just loaded
                const frame = new Frame(image);
                this.frames.push(frame);
            }

            // if no frames were added to this sequence above, it's an error
            if (this.frames.length === 0)
                throw new Error(
                    `No frames found for sequence with prefix ${imagePrefix}`);
        }

        /**
         * Returns the image for the frame of the given index, culled from one
         * of three sources:
         * 
         *  1) the given set of images, when such a set is given
         *  2) an earlier frame (from the given list of frames), 
         *     when repeated-frames data is given, and the value of the given
         *     index in that data is non-negative
         *  3) from disk, whether the previous two conditions don't apply 
         */
        private getFrameImage(
            frameIndex: number, repeatedFrames: number[],
            images: IImage[], fromFrames: IFrame[]): IImage
        {
            // if we were given a set of images to use, and there is an entry
            // in that set for the frame of the given index
            let image: IImage;
            if (images != null && frameIndex < images.length) {
                // use the image from that set
                image = images[frameIndex];
            }
            
            // else, if repeated-frames data was given, and there is an entry
            // in that data for the frame of the given index
            else if (repeatedFrames != null
                && frameIndex < repeatedFrames.length
                && repeatedFrames[frameIndex] >= 0) {
                // use the image from the repeated frame for this frame
                image = fromFrames[repeatedFrames[frameIndex]].image;
            }

            // otherwise, retrieve the image of the current frame index number
            else image = new Image(this.imagePrefix + (frameIndex + 1));

            return image;
        }

        /* Implementation */
        setDurationOfAllFrames(duration: number): void
        {
            this.frames.forEach(f => f.duration = duration);
        }
    }
} 