﻿module Dunjax.Animations
{
    import IImage = Images.IImage; 
    
    /**
     * Manages timed progressions through sequences of frames.
     */
    export interface IAnimation
    {
        /**
         * Has this animation check to see if it should advance to the next frame 
         * in its sequence, and do so if it is indeed time.
         *
         * @return  Whether the animation advanced.
         */
        checkForAdvance(): boolean;

        /**
         * The current frame-sequence being displayed for this animation. 
         * When setting this value, if the given sequence is already in progress 
         * within this animation, nothing is done.
         */
        frameSequence: IFrameSequence;

        /**
         * The current image to be displayed for this animation.
         */
        /* GetOnly */
        currentImage: IImage;

        /**
         * Whether the progression through this animation's current frame 
         * sequence has been completed.
         */
        isDone: boolean;

        /**
         * Has this animation restart, playing its current frame sequence from the
         * beginning.
         */
        restart(): void;

        /**
         * If this animation's current frame sequence requires an external permission 
         * be given before this animation may advance to the next frame, setting this field
         * to true signifies that permission.  This field will be reset to false after
         * the advance has occurred.
         */
        isNodGivenToAdvance: boolean;
    }
}