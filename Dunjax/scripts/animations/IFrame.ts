﻿module Dunjax.Animations
{
    import IImage = Images.IImage; 
    
    /**
     * One frame in an animation's frame-sequence.
     */
    export interface IFrame
    {
        /**
         * The image to display for this frame.  
         */
        image: IImage;

        /**
         * How long (in ms) this frame should be displayed before moving on to the next.
         */
        duration: number;
    }
} 