﻿module Dunjax.Animations
{
    /**
     * A timed sequence of frames through which an animation may progress.
     */
    export interface IFrameSequence
    {
        /**
         * Returns the filename which starts the name of each of the
         * frames in this sequence.
         */
        imagePrefix: string;

        /**
         * Returns how many frames there are in this sequence.
         */
        /* GetOnly */
        framesCount: number;
         
        /**
         * Returns the frame of the given index within this sequence.
         */
        getFrame(index: number): IFrame;
         
        /**
         * Returns whether this frame sequence should start over at its beginning 
         * once it has completed.
         */
        isRepeating: boolean;

        /**
         * Whether this sequence requires that an external permission be given to its
         * playing animation before that animation may advance to the next frame.
         */
        requiresNodToAdvance: boolean;

        /**
         * A different frame sequence than this one, which should be switched to
         * after this sequence finishes.    
         */
        switchToWhenDone: IFrameSequence;

        /**
         * Sets the duration of all the frames in this sequence to the value given.
         */
        setDurationOfAllFrames(duration: number): void;
    }

    /**
     * The nominal frame sequence to display for entities which aren't visible.
     */
    export var invisibleFrameSequence: IFrameSequence;

    /**
     * Create the miscellaneous frame sequences declared above.
     */
    export function createMiscFrameSequences(): void
    {
        if (invisibleFrameSequence == null)
            invisibleFrameSequence =
                new FrameSequence("invisible", 1, true, null, [Program.emptyImage]);
    }
}