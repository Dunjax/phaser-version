/// <reference path="MapEntity.ts" />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var pixelsPerTenFeet = Dunjax.Maps.pixelsPerTenFeet;
        var Animation = Dunjax.Animations.Animation;
        var AnimationEntity = (function (_super) {
            __extends(AnimationEntity, _super);
            function AnimationEntity(type, sequence, x, y) {
                _super.call(this, x, y);
                this.defaultIntervalUntilNextAction = 1000;
                this.animationEntityType = type;
                if (sequence != null) {
                    this.animation = new Animation();
                    this.animation.setFrameSequence(sequence);
                }
                else {
                    this.width = this.height = pixelsPerTenFeet;
                }
            }
            AnimationEntity.prototype.determineAnimation = function () { };
            AnimationEntity.prototype.getIntervalUntilNextAction = function () {
                return this.animation != null
                    ? this.animation.getCurrentFrameDuration()
                    : this.defaultIntervalUntilNextAction;
            };
            return AnimationEntity;
        })(Entities.MapEntity);
        Entities.AnimationEntity = AnimationEntity;
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
