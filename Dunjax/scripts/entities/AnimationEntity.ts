﻿/// <reference path="MapEntity.ts" />
/// <reference path="../animations/Animation.ts" />

module Dunjax.Entities
{
    import IFrameSequence = Animations.IFrameSequence;

    /* Implementation */
    export class AnimationEntity extends MapEntity implements IAnimationEntity
    {
        /* Implementation */
        animationEntityType: AnimationEntityType;

        constructor(type: AnimationEntityType, sequence: IFrameSequence, x: number, y: number)
        {
            super(x, y);
            this.animationEntityType = type;
            this.animation.frameSequence =
                sequence != null ? sequence : Animations.invisibleFrameSequence;
        }

        /* OverriddenValue */
        protected shouldAlignWithGroundOrCeiling(): boolean { return false; }

        /* OverriddenValue */
        protected getFrameSequenceForState(): IFrameSequence
        {
            return this.animation.frameSequence;
        }
    }
}