var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        /**
         * Specifies a time at which a map-entity is planning to act, plus associated
         * information.
         */
        var EntityAction = (function () {
            function EntityAction(entity, time) {
                this.entity = entity;
                this.time = time;
            }
            return EntityAction;
        })();
        Entities.EntityAction = EntityAction;
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
//# sourceMappingURL=EntityAction.js.map