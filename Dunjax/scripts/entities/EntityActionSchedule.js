var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var clock = Dunjax.Time.clock;
        var EntityActionSchedule = (function () {
            function EntityActionSchedule() {
                /**
                 * Holds the entity actions which are scheduled to occur during each 1 millisecond
                 * chunk of game time, mapped by the whole number floor of that millisecond
                 * interval.  For example, actions scheduled from 24 to 24.999... ms are mapped
                 * with a key of 24.
                 */
                this.actionsByTime = {};
                /**
                 * The latest game time for which entity actions have been removed
                 * from this schedule.
                 */
                this.lastTimeActionsRemoved = -1;
                /**
                 * The maximum amount of time (in ms) from the current game time that
                 * getNextActionTime() should search for actions.
                 */
                this.nextActionTimeUpperBound = 3000;
            }
            /* Implementation */
            EntityActionSchedule.prototype.getNextActionTime = function () {
                // for each millisecond of game time from the last time for which actions
                // were removed from this schedule, to some upper bound above the 
                // current game time (to avoid an infinite loop)
                var currentTimeFloored = Math.floor(clock.time);
                var maxTimeToSearchTo = currentTimeFloored + this.nextActionTimeUpperBound;
                for (var i = this.lastTimeActionsRemoved + 1; i <= maxTimeToSearchTo; i++) {
                    // if there is at least one action scheduled for this time, 
                    // return this time
                    if (this.actionsByTime[i] != null)
                        return i;
                }
                // no actions were found, so return our upper bound
                return maxTimeToSearchTo;
            };
            /* Implementation */
            EntityActionSchedule.prototype.removeActionsScheduledForTime = function (time) {
                // if this schedule contains no list of actions for the given
                // time, return null
                time = Math.floor(time);
                var actionsByTime = this.actionsByTime;
                if (actionsByTime[time] === null)
                    return null;
                // remove and return the list of actions for the given time
                var result = actionsByTime[time];
                actionsByTime[time] = null;
                this.lastTimeActionsRemoved = time;
                return result;
            };
            /* Implementation */
            EntityActionSchedule.prototype.addAction = function (action) {
                // if there is no list of actions at the given action's time, create one
                action.time = Math.floor(action.time);
                var actionsByTime = this.actionsByTime;
                var actionTimes = actionsByTime[action.time];
                if (actionTimes === null || actionTimes === undefined)
                    actionsByTime[action.time] = actionTimes = [];
                // add the given action to the list found (or created) above
                actionTimes.push(action);
            };
            /* Implementation */
            EntityActionSchedule.prototype.containsAction = function (action) {
                // if there is no list of actions at the given action's time, 
                // this schedule does not contain a matching action
                var actionTimes = this.actionsByTime[action.time];
                if (actionTimes == null)
                    return false;
                // for each action in the list retrieved above
                for (var i = 0; i < actionTimes.length; i++) {
                    // if this action matches that given, we've found a match
                    if (actionTimes[i] === action)
                        return true;
                }
                // no match was found
                return false;
            };
            /* Implementation */
            EntityActionSchedule.prototype.moveAllActionsToTime = function (toTime) {
                // for each time for which there are actions stored
                // in this schedule
                var actionsByTime = this.actionsByTime;
                for (var time in actionsByTime) {
                    if (!actionsByTime.hasOwnProperty(time))
                        continue;
                    // remove the actions stored at this time
                    var actions = actionsByTime[time];
                    actionsByTime[time] = null;
                    // re-schedule the actions for the given time
                    var toTimeActions = actionsByTime[toTime];
                    if (toTimeActions === null)
                        toTimeActions = actionsByTime[toTime] = [];
                    actions.forEach(function (a) { return toTimeActions.push(a); });
                }
                ;
            };
            return EntityActionSchedule;
        })();
        Entities.EntityActionSchedule = EntityActionSchedule;
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
//# sourceMappingURL=EntityActionSchedule.js.map