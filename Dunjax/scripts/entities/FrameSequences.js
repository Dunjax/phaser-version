/// <reference path="../animations/FrameSequence.ts" />
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var FrameSequences = (function () {
            function FrameSequences() {
            }
            FrameSequences.prototype.createFrameSequence = function (args) {
                var fullName = "";
                var prefix = this.imagesPrefix;
                var name = args.name;
                if (name != null)
                    fullName = this.imagesPath + prefix +
                        (prefix.length > 0 ? name.makeFirstLetterUppercase() : name);
                return new Dunjax.Animations.FrameSequence(fullName, args.length, args.isRepeating, args.repeatedFrames, args.images);
            };
            FrameSequences.prototype.args = function (name, length, others) {
                if (length === void 0) { length = 1; }
                if (others === void 0) { others = {}; }
                others.name = name;
                others.length = length;
                return others;
            };
            FrameSequences.prototype.create = function (name, length) {
                return this.createFrameSequence(this.args(name, length));
            };
            FrameSequences.prototype.createWith = function (name, length, others) {
                return this.createFrameSequence(this.args(name, length, others));
            };
            return FrameSequences;
        })();
        Entities.FrameSequences = FrameSequences;
        var CreateFrameSequenceArguments = (function () {
            function CreateFrameSequenceArguments(name) {
                this.isRepeating = false;
                this.repeatedFrames = null;
                this.images = null;
                this.name = name;
            }
            return CreateFrameSequenceArguments;
        })();
        Entities.CreateFrameSequenceArguments = CreateFrameSequenceArguments;
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
