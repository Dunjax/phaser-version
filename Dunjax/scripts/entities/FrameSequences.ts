﻿/// <reference path="../animations/FrameSequence.ts" />

module Dunjax.Entities
{
    import IImage = Images.IImage;
    import IFrameSequence = Animations.IFrameSequence;
    
    /**
     * The different frame-sequences used to depict the various activities of
     * entities of a particular class. Subtypes should override this class to specify
     * those sequences.
	 */
    export class FrameSequences
    {
        /**
         * The string at the front of the filename of each of the images 
         * in its frame sequences.
         */
        protected imagesPrefix = "";

        /**
         * The path (relative to the images folder) to the folder containing this object's
         * frame sequences.
         */
        protected imagesPath: string;

        /**
         * Creates a frame sequence using the given arguments.
         */
        protected createFrameSequence(args: ICreateFrameSequenceArguments): IFrameSequence
        {
            // determine the frame-sequence's image-prefix by combining
            // our path and prefix with the given name
            let fullName = "";
            const prefix = this.imagesPrefix;
            const name = args.name;
            if (name != null)
                fullName = this.imagesPath + prefix +
                (prefix.length > 0 ? name.makeFirstLetterUppercase() : name);

            // create and return the new frame sequence
            return new Animations.FrameSequence(
                fullName, args.length, args.isRepeating, args.repeatedFrames, args.images);
        }

        /**
         * Assembles a ICreateFrameSequenceArguments object from the given parameters. 
         */
        protected args(
            name: string, length: number = 1, others: ICreateFrameSequenceArguments = {}):
            ICreateFrameSequenceArguments
        {
            others.name = name;
            others.length = length;
            if (others.isRepeating == null) others.isRepeating = false;
            return others;
        }

        /* Shorthand */
        protected create(name: string, length: number): IFrameSequence
        {
            return this.createFrameSequence(this.args(name, length));
        }

        /* Shorthand */
        protected createWith(
            name: string, length: number, others: ICreateFrameSequenceArguments):
            IFrameSequence
        {
            return this.createFrameSequence(this.args(name, length, others));
        }
    }

    /**
     * The arguments used to create a frame sequence, in an interface form such that they don't
     * all need to be provided, due to duck typing.  These correspond to the parameters 
     * of the FrameSequence constructor.  
     */
    export interface ICreateFrameSequenceArguments
    {
        name?: string;
        length?: number;
        isRepeating?: boolean;
        repeatedFrames?: number[];
        images?: IImage[];
    }
}