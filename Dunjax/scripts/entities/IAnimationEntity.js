var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        (function (AnimationEntityType) {
            AnimationEntityType[AnimationEntityType["Item"] = 0] = "Item";
            AnimationEntityType[AnimationEntityType["Spatter"] = 1] = "Spatter";
            AnimationEntityType[AnimationEntityType["FinalRoomTrigger"] = 2] = "FinalRoomTrigger";
        })(Entities.AnimationEntityType || (Entities.AnimationEntityType = {}));
        var AnimationEntityType = Entities.AnimationEntityType;
        ;
        function asAnimationEntity(entity) {
            return entity.animationEntityType !== undefined
                ? entity : null;
        }
        Entities.asAnimationEntity = asAnimationEntity;
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
