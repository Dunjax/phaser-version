﻿module Dunjax.Entities
{
    /**
     * A simple entity which is stationary and animates.
     */
    export interface IAnimationEntity extends IMapEntity
    {
        /**
         * What kind of animation entity this is.
         */
        animationEntityType: AnimationEntityType;
    }

    /**
     * The different kinds of animation entities.
     */
    export enum AnimationEntityType { Item, Spatter, FinalRoomTrigger };

    /* DownCaster */
    export function asAnimationEntity(entity: IMapEntity): IAnimationEntity
    {
        return (<any>entity).animationEntityType !== undefined
            ? <IAnimationEntity>entity : null;
    }
}
