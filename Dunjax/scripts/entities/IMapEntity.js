var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        (function (Side) {
            Side[Side["Left"] = 0] = "Left";
            Side[Side["Right"] = 1] = "Right";
            Side[Side["NeitherSide"] = 2] = "NeitherSide";
        })(Entities.Side || (Entities.Side = {}));
        var Side = Entities.Side;
        ;
        (function (VerticalSide) {
            VerticalSide[VerticalSide["Top"] = 0] = "Top";
            VerticalSide[VerticalSide["Bottom"] = 1] = "Bottom";
            VerticalSide[VerticalSide["NeitherVerticalSide"] = 2] = "NeitherVerticalSide";
        })(Entities.VerticalSide || (Entities.VerticalSide = {}));
        var VerticalSide = Entities.VerticalSide;
        ;
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
