﻿module Dunjax.Entities
{
    import IMap = Maps.IMap;
    import IRectangle = Geometry.IRectangle;
    import IImage = Images.IImage; 
    
    /**
     * An entity which exists within a game map.
     */
    export interface IMapEntity extends IRectangle
    {
        /**
	     * This entity's ID, which is unique over the current running of this program.
	     */
        id: number;
    	
        /**
         * The map in which this entity resides.
         */
        map: IMap;

        /**
         * Whether this entity is within the bounds of the game's viewport (aka camera),
         * or is fairly close to the bounds.  This determines whether the entity should be
         * eligible for action during the current game turn.
         */
        isInOrNearViewport: boolean;

        /**
         * Is called once per game turn to have this entity compute its isInOrNearViewport
         * value, to be used for the rest of the turn.
         */
        determineIsInOrNearViewport(): void;

        /**
         * Whether this entity is unobscured by line-of-sight blockage from the player.
         */
        shouldBeDrawn: boolean;

        /**
         * Is the object that whatever underlying game framework is using to represent 
         * this entity.
         */
        frameworkObject: Object;

        /**
         * Returns whether this entity is normally passible.
         */
        isPassible: boolean;

        /**
         * Is called when this entity gets to perform its actions for the current game turn.
         */
        act(): void;
        
        /**
         * Has this entity determine from its current state what the value of its 
         * image field should be.
         */
        determineImage(): void;
        
        /**
         * The current image that visually represents this entity.
         */
        image: IImage;
        
        /**
         * Returns on which side the given entity is of this entity.
         */
        getSideEntityIsOn(entity: IMapEntity, tolerance?: number): Side;

        /**
         * Returns on which vertical side the given entity is of this entity.
         */
        getVerticalSideEntityIsOn(entity: IMapEntity, tolerance?: number): VerticalSide;

        /**
         * Returns whether this entity's map is passible at the given distance from this
         * entity's given side.  
         */
        isPassibleToSide(side: Side, dx: number, considerBeings?: boolean): boolean;

        /**
         * Returns whether this entity's map is passible at the given distance from this
         * entity's given vertical-side. 
         */ 
        isPassibleToVerticalSide(
            side: VerticalSide, dy: number, considerBeings?: boolean): boolean;
        
        /**
         * Whether this entity needs to take no further action during the
         * game.  Examples where this would be true would include entities which
         * have been removed from the map, as well as those which remain but which
         * stay inactive, such as a non-animating carcass. 
         */
        isPermanentlyInactive: boolean;

        /**
         * Moves this entity up or down on its map so that its top and bottom 
         * parts aren't stuck in something impassible. Also moves this entity 
         * down if necessary to keep it from floating above the ground, but 
         * only if it dwells on the ground. Such actions are necessary since it's 
         * hard to specify the exact, correct positions of entities using the map editor.
         * 
         * May also perform other, specialty alignment, as necessary.
         */
        alignWithSurroundings(): void;
    }

    /**
     * The horizontal sides of an entity.
     */
    export enum Side { Left, Right, NeitherSide };

    /**
     * The vertical sides of an entity.
     */
    export enum VerticalSide { Top, Bottom, NeitherVerticalSide };
}