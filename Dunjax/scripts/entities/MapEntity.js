/// <reference path="../maps/IMap.ts" />
/// <reference path="IMapEntity.ts" />
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var pixelsPerTenFeet = Dunjax.Maps.pixelsPerTenFeet;
        var Animation = Dunjax.Animations.Animation;
        var MapEntity = (function () {
            function MapEntity(x, y) {
                this.isNearEnoughToCameraToAct = false;
                this.animation = new Animation();
                var sprite = this.sprite = new Phaser.Sprite(Dunjax.Program.phaserGame, x, y);
                sprite.anchor.setTo(0.5, 0.5);
                Dunjax.Program.phaserGame.add.existing(sprite);
            }
            Object.defineProperty(MapEntity.prototype, "x", {
                get: function () { return this.sprite.x; },
                set: function (value) {
                    this.sprite.x = value;
                    if (this.sprite.body != null)
                        this.sprite.body.shape.pos.x = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(MapEntity.prototype, "y", {
                get: function () { return this.sprite.y; },
                set: function (value) {
                    this.sprite.y = value;
                    if (this.sprite.body != null)
                        this.sprite.body.shape.pos.y = value;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(MapEntity.prototype, "left", {
                get: function () { return this.sprite.x - Math.abs(this.sprite.width) / 2; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(MapEntity.prototype, "right", {
                get: function () { return this.sprite.x + Math.abs(this.sprite.width) / 2 - 1; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(MapEntity.prototype, "top", {
                get: function () { return this.sprite.y - Math.abs(this.sprite.height) / 2; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(MapEntity.prototype, "bottom", {
                get: function () { return this.sprite.y + Math.abs(this.sprite.height) / 2 - 1; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(MapEntity.prototype, "width", {
                get: function () { return Math.abs(this.sprite.width); },
                set: function (value) { this.sprite.width = value; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(MapEntity.prototype, "height", {
                get: function () { return Math.abs(this.sprite.height); },
                set: function (value) { this.sprite.height = value; },
                enumerable: true,
                configurable: true
            });
            MapEntity.prototype.setToDefaultSize = function () {
                this.width = this.height = pixelsPerTenFeet;
            };
            MapEntity.prototype.act = function () {
                if (this.isPermanentlyInactive)
                    return;
                this.determineIsNearEnoughToCameraToAct();
                if (!this.isNearEnoughToCameraToAct)
                    return;
                this.checkForAnimationAdvanceOrFinish();
                if (this.map == null)
                    return;
                this.actAfterAnimationChecked();
                if (this.sprite.game != null) {
                    this.determineAnimation();
                    this.determineImage();
                    this.determineSize();
                }
                this.afterActed();
            };
            MapEntity.prototype.actAfterAnimationChecked = function () { };
            MapEntity.prototype.afterActed = function () { };
            MapEntity.prototype.getDefaultImage = function () {
                throw new Error("A subtype needs to implement this.");
            };
            MapEntity.prototype.determineImage = function () {
                var old = this.image;
                var current = this.animation.getFrameSequence() != null
                    ? this.animation.getCurrentImage()
                    : this.getDefaultImage();
                if (current !== old) {
                    this.sprite.loadTexture(current.key);
                    this.image = current;
                }
            };
            MapEntity.prototype.determineSize = function () {
                if (this.image == null)
                    this.determineImage();
                var image = this.image;
                if (image == null)
                    return;
                var imageWidth = image.width, imageHeight = image.height;
                var absWidth = Math.abs(this.width);
                var absHeight = Math.abs(this.height);
                if (absWidth !== imageWidth || absHeight !== imageHeight) {
                    this.x -= (imageWidth - absWidth) / 2;
                    this.y -= (imageHeight - absHeight) / 2;
                    this.width = imageWidth * (this.width < 0 ? -1 : 1);
                    this.height = imageHeight * (this.height < 0 ? -1 : 1);
                }
            };
            MapEntity.prototype.checkForAnimationAdvanceOrFinish = function () {
                var animation = this.animation;
                if (animation == null)
                    return;
                var wasDone = animation.getIsDone();
                animation.checkForAdvance();
                if (!wasDone && animation.getIsDone()) {
                    this.animationDone(animation.getFrameSequence());
                }
            };
            MapEntity.prototype.animationDone = function (sequence) { };
            MapEntity.prototype.getSideEntityIsOn = function (entity) {
                if (entity.x < this.x)
                    return Entities.Side.Left;
                if (entity.x > this.x)
                    return Entities.Side.Right;
                return Entities.Side.NeitherSide;
            };
            MapEntity.prototype.getVerticalSideEntityIsOn = function (entity) {
                if (entity.y < this.y)
                    return Entities.VerticalSide.Top;
                if (entity.y > this.y)
                    return Entities.VerticalSide.Bottom;
                return Entities.VerticalSide.NeitherVerticalSide;
            };
            MapEntity.prototype.isOnGround = function () {
                return !this.map.isPassible(this.x, this.bottom + 1);
            };
            MapEntity.prototype.isCeilingDwelling = function () { return false; };
            MapEntity.prototype.isGroundDwelling = function () { return true; };
            MapEntity.prototype.alignWithSurroundings = function () {
                if (this.shouldAlignWithGroundOrCeiling()) {
                    this.moveUpUntilAboveImpassible();
                    this.moveDownUntilBelowImpassible();
                    if (this.isGroundDwelling())
                        this.moveDownUntilOnGround();
                    if (this.isCeilingDwelling())
                        this.moveUpUntilContactingCeiling();
                }
                this.alignSpecially();
            };
            MapEntity.prototype.shouldAlignWithGroundOrCeiling = function () { return true; };
            MapEntity.prototype.alignSpecially = function () { };
            MapEntity.prototype.moveUpUntilAboveImpassible = function () {
                var limit = this.top - 4 * pixelsPerTenFeet;
                while (!this.map.isPassible(this.x, this.bottom) && this.top > limit)
                    this.sprite.y--;
            };
            MapEntity.prototype.moveDownUntilBelowImpassible = function () {
                var limit = this.top + 4 * pixelsPerTenFeet;
                while (!this.map.isPassible(this.x, this.top) && this.top < limit)
                    this.sprite.y++;
            };
            MapEntity.prototype.moveDownUntilOnGround = function () {
                var limit = this.top + 4 * pixelsPerTenFeet;
                while (this.map.isFallThroughable(this.x, this.bottom + 1) && this.top < limit)
                    this.sprite.y++;
            };
            MapEntity.prototype.moveUpUntilContactingCeiling = function () {
                var limit = this.top - 4 * pixelsPerTenFeet;
                while (this.map.isPassible(this.x, this.top - 1) && this.top > limit)
                    this.sprite.y--;
            };
            MapEntity.prototype.isPassibleToSide = function (side, set) {
                var x = side === Entities.Side.Left ? this.left - 1 : this.right + 1;
                var map = this.map;
                return map.isPassible(x, this.y, set)
                    && map.isPassible(x, this.top, set)
                    && map.isPassible(x, this.bottom, set);
            };
            MapEntity.prototype.isPassibleToVerticalSide = function (side, set) {
                var y = side === Entities.VerticalSide.Top ? this.top - 1 : this.bottom + 1;
                var map = this.map;
                return map.isPassible(this.x, y, set)
                    && map.isPassible(this.left, y, set)
                    && map.isPassible(this.right, y, set);
            };
            MapEntity.prototype.removeFromPlay = function () {
                this.isPermanentlyInactive = true;
                this.map.entities.removeEntity(this);
                this.sprite.destroy();
            };
            MapEntity.prototype.determineAnimation = function () {
                var animation = this.animation;
                var oldSequence = animation.getFrameSequence();
                var sequence = this.getFrameSequenceForState();
                if (sequence !== oldSequence || animation.getIsDone()) {
                    if (oldSequence != null && !animation.getIsDone())
                        this.animationDone(oldSequence);
                    animation.setFrameSequence(sequence);
                    animation.restart();
                    this.onAnimationChanged();
                }
            };
            MapEntity.prototype.onAnimationChanged = function () { };
            MapEntity.prototype.getFrameSequenceForState = function () {
                throw new Error("Must override this.");
            };
            MapEntity.prototype.getIntervalUntilNextAction = function () {
                throw new Error("Subclasses must override this method.");
            };
            MapEntity.prototype.playIfExists = function (sound) {
                if (sound != null)
                    sound.play();
            };
            MapEntity.resetEntitiesIdCounter = function () {
                this.numMapEntities = 1;
            };
            MapEntity.prototype.determineIsNearEnoughToCameraToAct = function () {
                var camera = Dunjax.Program.phaserGame.camera;
                var maxDistance = 100;
                this.isNearEnoughToCameraToAct =
                    Dunjax.Geometry.intersects(this.left, this.top, this.width, this.height, camera.x - maxDistance, camera.y - maxDistance, camera.width + 2 * maxDistance, camera.height + 2 * maxDistance);
            };
            return MapEntity;
        })();
        Entities.MapEntity = MapEntity;
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
