﻿/// <reference path="../maps/IMap.ts" />
/// <reference path="IMapEntity.ts" />

module Dunjax.Entities
{
    import IMap = Maps.IMap;
    import IAnimation = Animations.IAnimation;
    import IImage = Images.IImage;
    import IFrameSequence = Animations.IFrameSequence;
    import pixelsPerTenFeet = Maps.pixelsPerTenFeet;
    import ISound = Sounds.ISound;
    import Animation = Animations.Animation;

    /* Implementation */
    export class MapEntity implements IMapEntity
    {
        /**
         * The underlying game framework's representation of this entity.
         */
        protected sprite: Phaser.Sprite;

        /**
         * This entity's bounding rectangle.
         */
        private _x = 0;
        private _y = 0;
        private _width = 1;
        private _height = 1;

        /**
         * The sides of this entity's bounding rectangle.
         */
        private _left = 0;
        private _right = 0;
        private _top = 0;
        private _bottom = 0;

        /* Implementation */
        get x(): number { return this._x; }

        /* Implementation */
        set x(value: number)
        {
            this.sprite.x = value;
            if (this.sprite.body != null) this.sprite.body.shape.pos.x = value;
            this.updateX();
        } 

        /**
         * Updates this entity's x-based locational fields, based on the underlying game 
         * framework's x-value for this entity.
         */
        private updateX(): void
        {
            this._x = this.sprite.body != null ? this.sprite.body.shape.pos.x : this.sprite.x;
            const halfWidth = this._width / 2;
            this._left = this._x - halfWidth;
            this._right = this._x + halfWidth - 1;
        }

        /* Implementation */
        get y(): number { return this._y; }

        /* Implementation */
        set y(value: number)
        {
             this.sprite.y = value;
             if (this.sprite.body != null) this.sprite.body.shape.pos.y = value;
             this.updateY();
        } 

        /**
         * Updates this entity's y-based locational fields, based on the underlying game 
         * framework's y-value for this entity.
         */
        private updateY(): void
        {
            this._y = this.sprite.body != null ? this.sprite.body.shape.pos.y : this.sprite.y;
            const halfHeight = this._height / 2;
            this._top = this._y - halfHeight;
            this._bottom = this._y + halfHeight - 1;
        }

        /* Implementation */
        get left(): number { return this._left; }
        get right(): number { return this._right; }
        get top(): number { return this._top; }
        get bottom(): number { return this._bottom; }

        /* Implementation */
        get width(): number { return this._width; }
        get height(): number { return this._height; }

        /**
         * Updates this entity's locational fields, based on the underlying game 
         * framework's size values for this entity.
         */
        private updateSize(): void
        {
            this._width = Math.abs(this.sprite.width);
            this._height = this.sprite.height;
            this.updateX();
            this.updateY();
        }

        /* Implementation */
        isInOrNearViewport = false;

        /* Implementation */
        get shouldBeDrawn(): boolean { return this.sprite.visible; }
        set shouldBeDrawn(value: boolean) { this.sprite.visible = value; }

        /* Implementation */
        get frameworkObject(): Object { return this.sprite; }
        set frameworkObject(value: Object) { this.sprite = <Phaser.Sprite>value; }

        /* Implementation */
        id: number;

        /* Implementation */
        map: IMap;

        /**
         * The animation currently being displayed for this entity.
         */
        protected animation: IAnimation = new Animation();

        /* Implementation */
        isPermanentlyInactive = false;

        /* Implementation */
        image: IImage;

        /**
         * Whether this entity is currently eligible to fall using this game's mechanism 
         * for falling (and not the one provided by the underlying game framework).
         */
        /* OverridableValue */
        protected shouldCheckForFall = false;

        /**
         * The game time when this entity's current fall (if any) started, to be used to
         * determine its current fall speed.
         */
        protected currentFallStartTime: number;

        constructor(x: number, y: number)
        {
            this.createFrameworkObject(x, y);
        }

        /**
         * Creates the object the underlying game framework will use to represent this entity.
         */
        private createFrameworkObject(x: number, y: number): void
        {
            const sprite = this.sprite = new Phaser.Sprite(Program.game, x, y);
            sprite.anchor.setTo(0.5, 0.5);
            this.updateX();
            this.updateY();
            Program.game.add.existing(sprite);
        }
        
        /* Implementation */
        act(): void
        {
            if (this.isPermanentlyInactive) return;

            this.checkForAnimationAdvanceOrFinish();

            // if the above caused this entity to be removed from the map, it should
            // take no further action
            if (this.map == null) return;

            if (this.shouldCheckForFall) this.checkForFall();

            this.actAfterAnimationChecked();

            // if, after acting, this entity is still in the game
            if (this.sprite.game != null) {
                // determine its visuals
                this.determineAnimation();
                this.determineImage();
            }

            this.onActed();
        }

        /* Hook */
        protected actAfterAnimationChecked(): void {}

        /* Hook */
        protected onActed(): void {}

        /* OverridableValue */
        protected getDefaultImage(): IImage
        {
            throw new Error("A subtype needs to implement this.");
        }

        /* Implementation */
        determineImage(): void
        {
            // if a change in image should occur
            const old = this.image;
            const current =
                this.animation.frameSequence != null
                    ? this.animation.currentImage
                    : this.getDefaultImage();
            if (current !== old) {
                // make the change
                this.sprite.loadTexture(current.key);
                this.image = current;
                this.updateSize();
            }
        }
        
        /**
         * Checks whether this entity should advance in its current animation 
         * sequence and/or whether that sequence has completed.
         */
        protected checkForAnimationAdvanceOrFinish(): void
        {
            // if this entity has no animation, there is nothing to do
            const animation = this.animation;
            if (animation == null) return;

            // check to see if this entity's current animation should advance to
            // the next frame
            const wasDone = animation.isDone;
            animation.checkForAdvance();

            // if the above advance (if any) caused this entity's 
            // current animation to finish
            if (!wasDone && animation.isDone) {
                // do whatever post-finish processing that needs to be done
                const sequence = animation.frameSequence;
                this.onAnimationDone(sequence);

                // if the animation's frame sequence prescribes switching to another 
                // when done, do so
                if (sequence.switchToWhenDone != null)
                    animation.frameSequence = sequence.switchToWhenDone;
            }
        }

        /* Hook */
        protected onAnimationDone(sequence: IFrameSequence): void {}

        /* Implementation */
        isPassible: boolean;

        /* Implementation */
        getSideEntityIsOn(entity: IMapEntity, tolerance: number = 0): Side
        {
            if (Math.abs(entity.x - this.x) <= 0.5 + tolerance) return Side.NeitherSide;
            return entity.x < this.x ? Side.Left : Side.Right;
        }

        /* Implementation */
        getVerticalSideEntityIsOn(entity: IMapEntity, tolerance: number = 0): VerticalSide
        {
            if (Math.abs(entity.y - this.y) <= 0.5 + tolerance)
                return VerticalSide.NeitherVerticalSide;
            return entity.y < this.y ? VerticalSide.Top : VerticalSide.Bottom;
        }

        /**
         * Returns whether this entity is currently on the ground (vs. in the air
         * or on a ladder).
         */
        protected isOnGround(): boolean
        {
            return !this.map.isPassible(this.x, this.bottom + 1);
        }

        /**
         * Returns whether this entity spends most (or all) of its time on the 
         * ceiling.
         */
        /* OverridableValue */
        protected isCeilingDwelling(): boolean { return false; }

        /**
         * Returns whether this entity spends most (or all) of its time on the 
         * ground.
         */
        /* OverridableValue */
        protected isGroundDwelling(): boolean { return true; }

        /* Implementation */
        alignWithSurroundings(): void
        {
            if (this.shouldAlignWithGroundOrCeiling()) {
                this.moveUpUntilAboveImpassible();

                this.moveDownUntilBelowImpassible();

                if (this.isGroundDwelling()) this.moveDownUntilOnGround();

                if (this.isCeilingDwelling()) this.moveUpUntilContactingCeiling();
            }

            this.alignSpecially();
        }

        /* OverridableValue */
        protected shouldAlignWithGroundOrCeiling(): boolean { return true; }

        /* Hook */
        protected alignSpecially(): void {}

        /**
         * Moves this entity upwards to be above what impassible thing (if any) 
         * it is currently over.
         */
        protected moveUpUntilAboveImpassible(): void
        {
            // while this entity's lower-y value is impassible,
            // move this entity up one increment
            const limit = this.top - 4 * pixelsPerTenFeet;
            while (!this.map.isPassible(this.x, this.bottom, false) && this.top > limit)
                this.y--;
        }

        /**
         * Moves this entity downwards to be below what impassible thing (if any) 
         * it is currently over.
         */
        protected moveDownUntilBelowImpassible(): void
        {
            // while this entity's y value is impassible,
            // move this entity down one increment
            const limit = this.top + 4 * pixelsPerTenFeet;
            while (!this.map.isPassible(this.x, this.top, false) && this.top < limit)
                this.y++;
        }

        /**
         * Moves this entity downwards to be resting on the non-fall-throughable
         * thing it is currently above.
         */
        protected moveDownUntilOnGround(): void
        {
            // while just below this entity is fall-throughable,
            // move this entity down one increment
            const limit = this.top + 4 * pixelsPerTenFeet;
            while (this.map.isFallThroughable(this.x, this.bottom + 1) && this.top < limit)
                this.y++;
        }

        /**
         * Moves this entity upwards to be contacting the impassible thing 
         * it is currently under.
         */
        protected moveUpUntilContactingCeiling(): void
        {
            // while just above this entity is passible,
            // move this entity up one increment
            const limit = this.top - 4 * pixelsPerTenFeet;
            while (this.map.isPassible(this.x, this.top - 1, false) && this.top > limit)
                this.y--;
        }

        /* Implementation */
        isPassibleToSide(side: Side, dx: number, considerBeings = true): boolean
        {
            const x = side === Side.Left ? this.left - dx : this.right + dx;
            const map = this.map;
            return map.isVerticalLinePassible(x, this.y, this.height, considerBeings);
        }

        /* Implementation */
        isPassibleToVerticalSide(
            side: VerticalSide, dy: number, considerBeings = true): boolean
        {
            const y = side === VerticalSide.Top ? this.top - dy : this.bottom + dy;
            const map = this.map;
            return map.isHorizontalLinePassible(this.x, y, this.width, considerBeings);
        }

        /**
         * Makes this entity permanently inactive and removes it from its map, 
         * two actions which must commonly be performed together.
         */
        protected removeFromPlay(): void
        {
            this.isPermanentlyInactive = true;
            this.map.entities.removeEntity(this);
            this.sprite.destroy();
        }

        /**
         * Determine whether there should be a change in which frame seqeuence this
         * entity's animation is currently displaying, and if so, makes that change.
         */
        protected determineAnimation(): void
        {
            // determine which frame sequence this entity's animation is displaying
            const animation = this.animation;
            const oldSequence = animation.frameSequence; 
            
            // if the frame sequence this entity's animation *should* be displaying
            // is different
            const sequence = this.getFrameSequenceForState();
            if (sequence !== oldSequence) {
                // if there was a different frame sequence being displayed, and
                // that animation was not done, end it now so the next frame
                // sequence may start
                if (oldSequence != null && !animation.isDone)
                    this.onAnimationDone(oldSequence);

                // start the next frame sequence running
                animation.frameSequence = sequence;
                animation.restart();
                this.onAnimationChanged();
            }
        }

        /* Hook */
        protected onAnimationChanged(): void { }

        /* OverridableValue */
        protected getFrameSequenceForState(): IFrameSequence
        {
            throw new Error("Must override this.");
        }

        /* Shorthand */
        protected playIfExists(sound: ISound): void
        {
            if (sound != null) sound.play();
        }

        /* Implementation */
        determineIsInOrNearViewport(): void
        {
            // ensure this entity's locational fields are up to date
            this.updateX();
            this.updateY();

            // determine whether this entity is in or near the game's viewport
            const camera = Program.game.camera;
            const maxDistance = 100;
            this.isInOrNearViewport =
                Geometry.intersects(
                    this.left, this.top, this.width, this.height,
                    camera.x - maxDistance, camera.y - maxDistance,
                    camera.width + 2 * maxDistance, camera.height + 2 * maxDistance);
        }

        /**
         * Has this entity fall, if it can do so.
         */
        protected checkForFall(): void
        {
            // if this entity can fall, have it fall
            const fallAmount = this.canFall();
            if (fallAmount > 0) this.y += fallAmount;

            // otherwise
            else {
                // the fall was blocked; check for falling no more
                this.shouldCheckForFall = false;
                this.onFallBlocked();
            }
        }

        /**
         * Returns the amount (if any) this entity can fall this turn.
         */
        protected canFall(): number
        {
            // return whether the area just beneath this entity's bottom-center
            // is not passible (note we don't use fall-throughable-ness 
            // as we want entity carcasses and spatters to fall through 
            // terrain like ladders)
            const x = this.x;
            const y = this.bottom;
            const width = this.width;
            const map = this.map;
            const check = (dy: number) => {
                return map.isPassible(x - width / 4, y + dy)
                    && map.isPassible(x + width / 4, y + dy);
            }
            const accelerationInPixelsPerMilliSecond = .55;
            const timeFalling = Math.max(Program.clock.time - this.currentFallStartTime, 1);
            const speed = accelerationInPixelsPerMilliSecond * timeFalling;
            const dy = Dunjax.getMovementForSpeed(speed);
            if (check(dy)) return dy;
            if (check(1)) return 1;
            return 0;
        }

        /* Hook */
        protected onFallBlocked(): void { }
    }
}