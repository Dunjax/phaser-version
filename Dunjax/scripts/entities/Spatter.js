var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var FallDuration = Dunjax.Time.FallDuration;
        var Spatter = (function (_super) {
            __extends(Spatter, _super);
            function Spatter(sequence, x, y) {
                _super.call(this, Entities.AnimationEntityType.Spatter, sequence, x, y);
                this.betweenFallsDuration = new FallDuration();
                this.betweenFallsDuration.fallStarted();
            }
            Spatter.prototype.getIntervalUntilNextAction = function () {
                return this.betweenFallsDuration.lengthLeft;
            };
            Spatter.prototype.actAfterAnimationChecked = function () {
                var result = this.canFall();
                if (result.canFall)
                    this.fall();
                else if (result.fallBlocked && this.animation.getIsDone())
                    this.isPermanentlyInactive = true;
            };
            Spatter.prototype.canFall = function () {
                var result = {
                    canFall: false,
                    fallBlocked: false
                };
                if (!this.betweenFallsDuration.isDone)
                    return result;
                var y = this.bottom;
                if (!this.map.isPassible(this.x - 4, y)
                    || !this.map.isPassible(this.x + 4, y)) {
                    result.fallBlocked = true;
                    return result;
                }
                result.canFall = true;
                return result;
            };
            Spatter.prototype.fall = function () {
                this.y++;
            };
            return Spatter;
        })(Entities.AnimationEntity);
        Entities.Spatter = Spatter;
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
