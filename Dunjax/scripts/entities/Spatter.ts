﻿module Dunjax.Entities
{
    import IFrameSequence = Animations.IFrameSequence; 
    
    /**
     * Displays a being's spatter sequence animation.  May fall.
     */
    export class Spatter extends AnimationEntity
    {
        constructor(sequence: IFrameSequence, x: number, y: number)
        {
            super(AnimationEntityType.Spatter, sequence, x, y);
            this.shouldCheckForFall = true;
            this.currentFallStartTime = Program.clock.time;
        }
        
        /* HookOverride */
        protected onFallBlocked()
        {
            // if this spatter's animation is done, it never needs to act, again
            if (this.animation.isDone) this.isPermanentlyInactive = true;
        }

        /* HookOverride */
        protected onAnimationDone()
        {
            // if this spatter is not falling, it never needs to act, again
            if (!this.shouldCheckForFall) this.isPermanentlyInactive = true;
        }
    }
}
