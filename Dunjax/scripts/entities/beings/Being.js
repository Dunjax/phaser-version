/// <reference path="../MapEntity.ts" />
/// <reference path="../FrameSequences.ts" />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Beings;
        (function (Beings) {
            var MapEntity = Entities.MapEntity;
            var FallDuration = Dunjax.Time.FallDuration;
            var Being = (function (_super) {
                __extends(Being, _super);
                function Being(x, y) {
                    _super.call(this, x, y);
                    this.isPassible = false;
                }
                Being.prototype.getIsFacingLeft = function () { return this.isFacingLeft; };
                Being.prototype.getDefaultImage = function () {
                    return this.beingFrameSequences.still.getFrame(0).image;
                };
                Being.prototype.afterActed = function () {
                    this.sprite.scale.x = this.isFacingLeft ? -1 : 1;
                    this.map.entities.onBeingActed(this);
                };
                Being.prototype.onDamaged = function (amount) {
                    this.health -= amount;
                    this.health = Math.max(this.health, 0);
                    if (this.isDead)
                        this.die();
                };
                Object.defineProperty(Being.prototype, "isDead", {
                    get: function () { return this.health <= 0; },
                    enumerable: true,
                    configurable: true
                });
                Being.prototype.onStruck = function (damage, x, y) {
                    if (x === void 0) { x = this.x; }
                    if (y === void 0) { y = this.y; }
                    if (this.isVulnerableAt(x, y))
                        this.onStruckWhereVulnerable(x, y, damage);
                };
                Being.prototype.onStruckWhereVulnerable = function (x, y, damage) {
                    this.onDamaged(damage);
                    if (!this.isDead)
                        this.survivedStrike(x, y);
                };
                Being.prototype.beforeSurvivedStrike = function () { };
                Being.prototype.survivedStrike = function (x, y) {
                    this.beforeSurvivedStrike();
                    if (this.hasSpatterSequence())
                        this.showSpatterSequence(x, y);
                    this.isHit = true;
                    this.playIfExists(this.beingSounds.hit);
                };
                Being.prototype.hasSpatterSequence = function () {
                    return this.beingFrameSequences.spatter != null;
                };
                Being.prototype.showSpatterSequence = function (x, y) {
                    var onLeftSide = x < this.x;
                    var entity = new Entities.Spatter(this.beingFrameSequences.spatter, 0, 0);
                    entity.x = onLeftSide ? this.left - entity.width : this.right + 1;
                    entity.y = this.y - entity.height / 2;
                    this.map.entities.addEntity(entity);
                };
                Being.prototype.die = function () {
                    this.beforeDying();
                    this.isDying = true;
                    this.playIfExists(this.beingSounds.death);
                    var duration = this.betweenFallsAsCarcassDuration = new FallDuration();
                    duration.fallStarted();
                };
                Being.prototype.beforeDying = function () { };
                Being.prototype.isStrikableAt = function (x, y) {
                    if (this.isDead || !Dunjax.Geometry.rectContains(this, x, y))
                        return false;
                    return this.isStrikableAtContainedLocation(x, y);
                };
                Being.prototype.isStrikableAtContainedLocation = function (x, y) {
                    return true;
                };
                Being.prototype.isVulnerableAt = function (x, y) {
                    if (!this.isStrikableAt(x, y))
                        return false;
                    return this.isVulnerableAtStrikableLocation(x, y);
                };
                Being.prototype.isVulnerableAtStrikableLocation = function (x, y) {
                    return true;
                };
                Being.prototype.animationDone = function (sequence) {
                    this.beforeAnimationDoneAsBeing(sequence);
                    var sequences = this.beingFrameSequences;
                    if (sequence === sequences.turn
                        || sequence === sequences.ceilingTurn) {
                        this.isTurning = false;
                    }
                    if (sequence === sequences.hit) {
                        this.isHit = false;
                    }
                    if (sequence === sequences.death
                        || sequence === sequences.ceilingDeath)
                        this.onDied();
                };
                Being.prototype.beforeAnimationDoneAsBeing = function (sequence) { };
                Being.prototype.onDied = function () {
                    this.isDying = false;
                    this.isDead = true;
                    if (!this.hasCarcassSequence())
                        this.removeFromPlay();
                };
                Being.prototype.hasCarcassSequence = function () {
                    return this.getFrameSequenceForState() != null;
                };
                Being.prototype.getFrameSequenceForState = function () {
                    var sequences = this.beingFrameSequences;
                    var sequence;
                    if (this.isDead)
                        sequence = sequences.carcass;
                    else if (this.isDying)
                        sequence = sequences.death;
                    if (sequence == null) {
                        var subtypeSequence = this.getFrameSequenceForStateAsBeingSubtype();
                        if (subtypeSequence !== null)
                            sequence = subtypeSequence;
                        else if (this.isHit)
                            sequence = sequences.hit;
                        else if (this.isTurning)
                            sequence = sequences.turn;
                        else if (this.isMoving)
                            sequence = sequences.move;
                        else
                            sequence = sequences.still;
                    }
                    return sequence;
                };
                Being.prototype.getFrameSequenceForStateAsBeingSubtype = function () {
                    return null;
                };
                Being.prototype.actAfterAnimationChecked = function () {
                    this.actAfterAnimationChecked_AsBeingSubtype();
                    if (this.isDead) {
                        var result = this.canFallAsCarcass();
                        if (result.canFall)
                            this.fallAsCarcass();
                        else if (result.fallBlocked && this.isDead)
                            this.isPermanentlyInactive = true;
                    }
                };
                Being.prototype.actAfterAnimationChecked_AsBeingSubtype = function () { };
                Being.prototype.canFallAsCarcass = function () {
                    var result = {
                        canFall: false,
                        fallBlocked: false
                    };
                    if (!this.betweenFallsAsCarcassDuration.isDone)
                        return result;
                    var y = this.bottom + 1;
                    if (!this.map.isPassible(this.x - 4, y)
                        || !this.map.isPassible(this.x + 4, y)) {
                        result.fallBlocked = true;
                        return result;
                    }
                    result.canFall = true;
                    return result;
                };
                Being.prototype.fallAsCarcass = function () {
                    this.y++;
                };
                Being.prototype.getIntervalUntilNextAction = function () {
                    return this.isDead ?
                        this.betweenFallsAsCarcassDuration.lengthLeft : 100;
                };
                return Being;
            })(MapEntity);
            Beings.Being = Being;
            var BeingSoundsGroup = (function () {
                function BeingSoundsGroup() {
                }
                return BeingSoundsGroup;
            })();
            Beings.BeingSoundsGroup = BeingSoundsGroup;
            var BeingFrameSequences = (function (_super) {
                __extends(BeingFrameSequences, _super);
                function BeingFrameSequences() {
                    _super.apply(this, arguments);
                }
                return BeingFrameSequences;
            })(Entities.FrameSequences);
            Beings.BeingFrameSequences = BeingFrameSequences;
        })(Beings = Entities.Beings || (Entities.Beings = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
