﻿/// <reference path="../MapEntity.ts" />
/// <reference path="../FrameSequences.ts" />

module Dunjax.Entities.Beings
{
    import ISound = Sounds.ISound;
    import IFrameSequence = Animations.IFrameSequence;
    import MapEntity = Entities.MapEntity;
    import IImage = Images.IImage;

    export class Being extends MapEntity implements IBeing
    {
        /**
         * An abstraction of this being's well-being.  When this value goes below 
         * one, this being dies.
         */
	    health: number;
         
        /* Implementation */
        isFacingLeft = false;

        /* Implementation */
        isMoving = false;

        /* Implementation */
        isTurning = false;

        /* Implementation */
        isHit = false;

        /* Implementation */
        isDying = false;

        /* Implementation */
        isDead = false;

        /**
         * The sounds emitted by this being. 
         */
        protected beingSounds: BeingSounds;

        /**
	     * The frame sequences used to depict this being in its various states. 
	     */
        protected beingFrameSequences: BeingFrameSequences;

        constructor(x: number, y: number)
        {
            super(x, y);
            this.isPassible = false;
        }

        /* OverridableValue */
        protected getDefaultImage(): IImage
        {
            return this.beingFrameSequences.still.getFrame(0).image;
        }

        /* HookOverride */
        protected onActed()
        {
            this.sprite.scale.x = this.isFacingLeft ? -1 : 1;
        }

        /**
	     * Informs this being that it has been damaged for the given amount.
	     */
        protected onDamaged(amount: number)
        {
            // subtract the damage from this being's health
            this.health -= amount;
            this.health = Math.max(this.health, 0);

            // if this being's health has reach zero, it dies
            if (this.health <= 0) this.die();
        }

        /* Implementation */
        onStruck(damage: number, x: number = this.x, y: number = this.y)
        {
            if (this.isVulnerableAt(x, y))
                this.onStruckWhereVulnerable(x, y, damage);
        }
        
        /**
         * Informs this being that it has been struck at the given vulnerable 
         * location for the given amount of damage.
         */
        protected onStruckWhereVulnerable(x: number, y: number, damage: number)
        {
            this.onDamaged(damage);

            if (!this.isDying) this.survivedStrike(x, y);
        }

        /* Hook */
        protected beforeSurvivedStrike() {}

        /**
         * Informs this being that it has survived a damaging strike at the given location.
         */
        protected survivedStrike(x: number, y: number)
        {
            this.beforeSurvivedStrike();

            if (this.hasSpatterSequence()) this.showSpatterSequence(x, y);

            if (this.beingFrameSequences.hit != null) this.isHit = true;

            this.playIfExists(this.beingSounds.hit);
        }
        
        /**
         * Returns whether a spatter sequence is specified for this being.
         */
        protected hasSpatterSequence(): boolean
        {
            return this.beingFrameSequences.spatter != null;
        }
        
        /**
         * Show this being's spatter sequence on the side of this being
         * which is implied by the given location.
         */
        protected showSpatterSequence(x: number, y: number)
        {
            // create a spatter that will display this being's spatter sequence
            const onLeftSide = x < this.x;
            const spatter = new Spatter(this.beingFrameSequences.spatter, 0, 0); 
            
            // add the spatter entity to the map on the proper side of this being
            spatter.determineImage();
            spatter.x =
                onLeftSide ? this.left - spatter.width / 2 : this.right + spatter.width / 2;
            spatter.y = this.bottom - spatter.height / 2;
            this.map.entities.addEntity(spatter);
        }

        /**
         * Has this being perform its death protocol.
         */
        protected die()
        {
            this.beforeDying();

            // this being is now dying
            this.isDying = true;
            
            // play this being's death sound
            this.playIfExists(this.beingSounds.death);
        }

        /* Hook */
        protected beforeDying() {}

        /* Implementation */
        isStrikableAt(x: number, y: number): boolean
        {
            // if this being is dead or dying, or does not contain the given location,
            // it is not strikable 
            if (this.isDead || this.isDying || !Geometry.rectContains(this, x, y)) return false;

            return this.isStrikableAtContainedLocation(x, y);
        }

        /* OverridableValue */
        protected isStrikableAtContainedLocation(x: number, y: number): boolean
        {
            return true;
        }

        /**
         * Returns whether the given location corresponds to a vulnerable spot 
         * on this being.
         */
        protected isVulnerableAt(x: number, y: number): boolean
        {
            if (!this.isStrikableAt(x, y)) return false;

            return this.isVulnerableAtStrikableLocation(x, y);
        }

        /* OverridableValue */
        protected isVulnerableAtStrikableLocation(x: number, y: number): boolean
        {
             return true;
        }

        /* HookOverride */
        protected onAnimationDone(sequence: IFrameSequence)
        {
            this.onBeforeAnimationDoneAsBeing(sequence);
            
            // if this being was turning
            const sequences = this.beingFrameSequences;
            if (sequence === sequences.turn || sequence === sequences.ceilingTurn) {
                // it no longer is
                this.isTurning = false;
            }

            // if this being was hit
            if (sequence === sequences.hit) {
                // it no longer is
                this.isHit = false;
            }
        	
            // if this being was dying, it has died
            if (sequence === sequences.death
                || sequence === sequences.ceilingDeath) this.onDied();
        }

        /* Hook */
        protected onBeforeAnimationDoneAsBeing(sequence: IFrameSequence) { }
        
        /**
         * Informs this being that it has finished dying.
         */
        protected onDied()
        {
            // this being is now dead
            this.isDying = false;
            this.isDead = true;
            
            // if this being has no carcass sequence, remove it from its map
            if (!this.doesLeaveCarcass()) this.removeFromPlay();

            // otherwise
            else {
                // start this carcass checking for a fall
                this.shouldCheckForFall = true;
                this.currentFallStartTime = Program.clock.time;
            }
        }

        /* OverridableValue */
        protected doesLeaveCarcass(): boolean
        {
            return this.beingFrameSequences.carcass != null;
        }

        /**
         * Returns the frame sequence this being uses to represent itself
         * while in its current state.
         */
        protected getFrameSequenceForState(): IFrameSequence
        {
            const sequences = this.beingFrameSequences;
            let sequence: IFrameSequence;
            if (this.isDying) sequence = sequences.death;
            else if (this.isDead) sequence = sequences.carcass;
            else if (this.isHit) sequence = sequences.hit;
            else if (this.isTurning) sequence = sequences.turn;
            else if (this.isMoving) sequence = sequences.move;
            else sequence = sequences.still;

            return this.getFrameSequenceForStateAsBeingSubtype(sequence);
        }
        
        /* OverridableValue */
        protected getFrameSequenceForStateAsBeingSubtype(beingSequenceChosen: IFrameSequence):
            IFrameSequence
        {
            return beingSequenceChosen;
        }

        /* HookOverride */
        protected actAfterAnimationChecked()
        {
            this.actAfterAnimationCheckedAsBeingSubtype();
        }

        /* Hook */
        protected actAfterAnimationCheckedAsBeingSubtype() { }

        /* HookOverride */
        protected onFallBlocked()
        {
            // if this being is dead, meaning it was falling as a carcass, 
            // it never needs to act, again
            if (this.isDead) this.isPermanentlyInactive = true;
        }
    }

    /**
     * The different sounds issued by beings of a particular class.  
     */
    export class BeingSounds
    {
        hit: ISound;
        death: ISound;
    }
      
    /**
     * The different frame-sequences used to depict the various activities of
     * beings of a particular class. 
     */
    export class BeingFrameSequences extends FrameSequences
    {
        still: IFrameSequence;
        move: IFrameSequence;
        turn: IFrameSequence;
        ceilingStill: IFrameSequence;
        ceilingMove: IFrameSequence;
        ceilingTurn: IFrameSequence;
        death: IFrameSequence;
        ceilingDeath: IFrameSequence;
        spatter: IFrameSequence;
        hit: IFrameSequence;
        carcass: IFrameSequence;
    }
}
 