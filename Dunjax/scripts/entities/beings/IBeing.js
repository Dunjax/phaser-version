var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Beings;
        (function (Beings) {
            function asBeing(entity) {
                return entity.isDead !== undefined ? entity : null;
            }
            Beings.asBeing = asBeing;
        })(Beings = Entities.Beings || (Entities.Beings = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
