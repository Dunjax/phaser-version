﻿module Dunjax.Entities.Beings
{
    import IMapEntity = Entities.IMapEntity;

    export interface IBeing extends IMapEntity
    {
        /**
         * Whether this being is currently moving to another location.
         */
        isMoving: boolean;

        /**
         * Whether this being is in the process of turning to face the other 
         * horizontal direction.
         */
        isTurning: boolean;

        /**
         * Whether this being is in the just-hit (for damage) state.
         */
        isHit: boolean;

        /**
         * Whether this being is in the process of dying.
         */
        isDying: boolean;

        /**
         * Whether this being is currently facing left (vs. right).
         */
        isFacingLeft: boolean;
        
        /**
         * Returns whether the given location corresponds to a strikable spot on 
         * this being.
         */
        isStrikableAt(x: number, y: number): boolean;
        
        /**
         * Informs this being that it has been struck at the given location,
         * by something that normally causes the given amount of damage.
         */
        onStruck(damage: number, x?: number, y?: number): void;

        /**
         * Whether this being has no health left (i.e. is dead).
         */
        isDead: boolean;
    }

    /* DownCaster */
    export function asBeing(entity: IMapEntity): IBeing
    {
        return (<any>entity).isDead !== undefined ? <any>entity : null;
    }
}