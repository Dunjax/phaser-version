﻿/// <reference path="Monster.ts" />
/// <reference path="movement/MoveAlongGroundOrCeiling.ts" />

module Dunjax.Entities.Beings.Monsters
{
    import IFrameSequence = Animations.IFrameSequence;
    import MoveAlongGroundOrCeiling = Monsters.Movement.MoveAlongGroundOrCeiling; 
    
    /**
     * A large monster with no special abilities.
     */
    export class BlueGargoyle extends Monster
    {
        constructor(x: number, y: number)
        {
            super(x, y);
            this.monsterType = MonsterType.BlueGargoyle;
            this.currentMovementMethod = new MoveAlongGroundOrCeiling(this);
            this.health = 10;
            this.betweenAttacksDuration.length = 1000;
            this.attackDamage = 15;
            this.moveSpeed = 167;
            this.beingFrameSequences = this.monsterFrameSequences =
                BlueGargoyleFrameSequences.singleton;
            this.beingSounds = this.monsterSounds = new MonsterSounds();
        }
    }

    /**
     * Specifies the frame sequences for this kind of monster.
     */
    export class BlueGargoyleFrameSequences extends MonsterFrameSequences
    {
        static singleton: BlueGargoyleFrameSequences;

        constructor()
        {
            super();
            this.imagesPrefix = "blueGargoyle";

            const create = (n: string, l: number) => this.create.call(this, n, l);
            const createWith =
                (n: string, l: number, o: ICreateFrameSequenceArguments) =>
                    this.createWith.call(this, n, l, o);
            const goToStill = (s: IFrameSequence) => s.switchToWhenDone = this.still;

            this.still = create("still", 1);
            this.move = createWith("move", 6, { isRepeating: true });
            this.attack = createWith("attack", 4, { repeatedFrames: [-1, -1, -1, 2] });
            goToStill(this.attack);
            this.turn = create("turn", 2);
            goToStill(this.turn);
            this.carcass = create("carcass", 1);
            this.death = create("death", 5);
            this.death.switchToWhenDone = this.carcass;
        }

        /* SingletonCreator */
        static create()
        {
            BlueGargoyleFrameSequences.singleton = new BlueGargoyleFrameSequences();
        }
    }
}