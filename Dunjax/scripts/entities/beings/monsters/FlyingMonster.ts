﻿/// <reference path="Monster.ts" />

module Dunjax.Entities.Beings.Monsters
{
    import IFrameSequence = Animations.IFrameSequence;
    import ISound = Sounds.ISound;
    import MonsterMovementMethod = Monsters.Movement.IMonsterMovementMethod;

    /* Implementation */
    export class FlyingMonster extends Monster implements IFlyingMonster
    {
	    /**
         * The movement method employed when this monster is moving along the ground.
         */
        protected groundMovementMethod: MonsterMovementMethod;

        /**
         * The movement method employed when this monster is flying.
         */
        protected flyMovementMethod: MonsterMovementMethod;

        /* Implementation */
        isFlying = true;

        /* Implementation */
        isFlyTurning = false;

        /* Implementation */
        isLanding = false;

        /* Implementation */
        flyMoveSpeed: number;

        /**
	     * The frame sequences used to depict this monster while its flying. 
	     */
        protected flyingMonsterFrameSequences: FlyingMonsterFrameSequences;
        
        /* Implementation */
        flyingMonsterSounds: FlyingMonsterSounds;

        constructor(x: number, y: number)
        {
            super(x, y);
        }

        /* HookOverride */
        protected onBeforeAnimationDoneAsMonster(sequence: IFrameSequence)
        {
            const sequences = this.flyingMonsterFrameSequences;
            if (this.isFlyTurning) {
                this.isFlyTurning = false;
                this.isFlying = true;

                // this monster's new facing is that of the sequence just finished
                this.isFacingLeft = !this.isFacingLeft;
            }

            else if (this.isLanding) {
                this.isLanding = false;
                this.currentMovementMethod = this.groundMovementMethod;
            }

            else if (sequence === sequences.flyDeath) this.onDied();
        }

        /* HookOverride */
        protected changeMovementMethod()
        {
            // if this monster isn't flying, change it to try flying
            if (!this.isFlying) this.currentMovementMethod = this.flyMovementMethod;

            // note that we only change to the ground-method when
            // this monster is done landing
        }

        /* OverriddenValue */
        protected getFrameSequenceForStateAsMonsterSubtype(
            monsterSequenceChosen: IFrameSequence): IFrameSequence
        {
            const sequences = this.flyingMonsterFrameSequences;
            let sequence = monsterSequenceChosen;
            if (this.isDead);
            else if (this.isDying && this.isFlying) sequence = sequences.flyDeath;
            else if (this.isDying || this.isAttacking);
            else if (this.isFlyTurning) sequence = sequences.flyTurn;
            else if (this.isLanding) sequence = sequences.land;
            else if (this.isFlying) sequence = sequences.fly;
            return sequence;
        }
    }

    /**
     * The different sounds issued by flying monsters of a particular class.  
     */
    export class FlyingMonsterSounds extends MonsterSounds
    {
        fly: ISound;
    }

    /**
     * The different frame-sequences a monster may display, beyond those of a being.
     */
    export class FlyingMonsterFrameSequences extends MonsterFrameSequences
    {
        fly: IFrameSequence;
        flyTurn: IFrameSequence;
        land: IFrameSequence;
        flyDeath: IFrameSequence;
    }
}