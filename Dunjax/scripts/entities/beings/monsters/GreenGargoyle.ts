﻿/// <reference path="movement/MoveAlongGroundOrCeiling.ts" />
/// <reference path="movement/FlyMonsterMovementMethod.ts" />

module Dunjax.Entities.Beings.Monsters
{
    import IFrameSequence = Animations.IFrameSequence;
    import MoveAlongGroundOrCeiling = Monsters.Movement.MoveAlongGroundOrCeiling;
    import FlyMonsterMovementMethod = Monsters.Movement.FlyMonsterMovementMethod; 
    
    /**
     * A basic flying monster.
     */
    export class GreenGargoyle extends FlyingMonster
    {
        constructor(x: number, y: number)
        {
            super(x, y);
            this.monsterType = MonsterType.GreenGargoyle;
            this.groundMovementMethod = new MoveAlongGroundOrCeiling(this);
            this.currentMovementMethod = this.flyMovementMethod =
                new FlyMonsterMovementMethod(this);
            this.health = 1;
            this.betweenAttacksDuration.length = 300;
            this.attackDamage = 2;
            this.moveSpeed = 165;
            this.flyMoveSpeed = 125;
            this.beingFrameSequences = this.monsterFrameSequences =
                this.flyingMonsterFrameSequences = GreenGargoyleFrameSequences.singleton;
            const sounds = this.beingSounds = this.monsterSounds = this.flyingMonsterSounds =
                new FlyingMonsterSounds();
            sounds.fly = Program.sounds.greenGargoyleFly;
        }
    }
    	
    /**
     * Specifies the frame sequences for this kind of monster.
     */
    export class GreenGargoyleFrameSequences extends FlyingMonsterFrameSequences
    {
        static singleton: GreenGargoyleFrameSequences;

        constructor()
        {
            super();
            this.imagesPrefix = "greenGargoyle";

            const create = (n: string, l: number) => this.create.call(this, n, l);
            const createWith = (n: string, l: number, o: ICreateFrameSequenceArguments) =>
                this.createWith.call(this, n, l, o);
            const goToStill = (s: IFrameSequence) => s.switchToWhenDone = this.still;

            this.still = create("still", 1);
            this.move = createWith("move", 6, { isRepeating: true });
            this.attack = createWith("attack", 4, { repeatedFrames: [-1, -1, -1, 0] });
            goToStill(this.attack);
            this.turn = create("turn", 2);
            goToStill(this.turn);
            this.carcass = create("carcass", 1);
            this.death = create("death", 6);
            this.fly =
                createWith("fly", 4, { repeatedFrames: [-1, -1, 0, -1], isRepeating: true });
            this.flyTurn = createWith("flyTurn", 4, { repeatedFrames: [-1, -1, 0, -1] });
            this.flyTurn.switchToWhenDone = this.fly;
            this.land = create("land", 1);
            this.flyDeath = create("flyDeath", 3);
        }

        /* SingletonCreator */
        static create()
        {
            GreenGargoyleFrameSequences.singleton = new GreenGargoyleFrameSequences();
        }
    }

    /**
     * The sounds emitted by this class of monster.
     */
    export class GreenGargoyleMonsterSounds extends FlyingMonsterSounds
    {
        constructor()
        {
            super();
            this.fly = Program.sounds.greenGargoyleFly;
        }
    }
}