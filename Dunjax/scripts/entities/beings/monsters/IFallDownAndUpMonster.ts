﻿module Dunjax.Entities.Beings.Monsters
{
    /**
     * A monster which can fall in either the downward or upward direction.
     */
    export interface IFallDownAndUpMonster extends IMonster
    {
        /**
         * Whether this monster is currently falling in the downward direction.
         */
        isFalling: boolean;

        /**
         * Whether this monster is currently "falling" in the upward direction.
         */
        isFallingUp: boolean;

        /**
         * How many pixels per second this monster falls, either direction.
         */
        fallSpeed: number;

        /**
         * Informs this monster (likely, by a movement algorithm) that it has 
         * started to fall.
         */
        onFallStarted(): void;
    }
}