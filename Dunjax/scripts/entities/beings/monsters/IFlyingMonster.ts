﻿module Dunjax.Entities.Beings.Monsters
{
    /**
     * A monster which can fly, in addition to move on the ground.
     */
    export interface IFlyingMonster extends IMonster
    {
        /**
         * Whether this monster is currently flying.
         */
        isFlying: boolean;

        /**
         * Whether this monster is currently turning, while flying.
         */
        isFlyTurning: boolean;

        /**
         * Whether this monster is currently ending its flight.
         */
        isLanding: boolean;

        /**
         * How many pixels per second this monster flies.
         */
        flyMoveSpeed: number;

        /**
	     * The sounds emitted by this monster when it flies. 
	     */
        flyingMonsterSounds: FlyingMonsterSounds;
    }
}