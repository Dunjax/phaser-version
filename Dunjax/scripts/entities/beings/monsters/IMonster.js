var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Beings;
        (function (Beings) {
            var Monsters;
            (function (Monsters) {
                function asMonster(being) {
                    return being.monsterType ? being : null;
                }
                Monsters.asMonster = asMonster;
                (function (MonsterType) {
                    MonsterType[MonsterType["BlueGargoyle"] = 0] = "BlueGargoyle";
                    MonsterType[MonsterType["GreenGargoyle"] = 1] = "GreenGargoyle";
                    MonsterType[MonsterType["Master"] = 2] = "Master";
                    MonsterType[MonsterType["Slime"] = 3] = "Slime";
                    MonsterType[MonsterType["Spider"] = 4] = "Spider";
                    MonsterType[MonsterType["Stalagmite"] = 5] = "Stalagmite";
                    MonsterType[MonsterType["Whirlwind"] = 6] = "Whirlwind";
                    MonsterType[MonsterType["YellowGargoyle"] = 7] = "YellowGargoyle";
                })(Monsters.MonsterType || (Monsters.MonsterType = {}));
                var MonsterType = Monsters.MonsterType;
                ;
                (function (MonsterState) {
                    MonsterState[MonsterState["Attacking"] = 40] = "Attacking";
                    MonsterState[MonsterState["Sleeping"] = 41] = "Sleeping";
                    MonsterState[MonsterState["Waking"] = 42] = "Waking";
                })(Monsters.MonsterState || (Monsters.MonsterState = {}));
                var MonsterState = Monsters.MonsterState;
                (function (FlyingMonsterState) {
                    FlyingMonsterState[FlyingMonsterState["Flying"] = 50] = "Flying";
                    FlyingMonsterState[FlyingMonsterState["FlyTurning"] = 51] = "FlyTurning";
                    FlyingMonsterState[FlyingMonsterState["Landing"] = 52] = "Landing";
                })(Monsters.FlyingMonsterState || (Monsters.FlyingMonsterState = {}));
                var FlyingMonsterState = Monsters.FlyingMonsterState;
                (function (SlimeState) {
                    SlimeState[SlimeState["Falling"] = 70] = "Falling";
                    SlimeState[SlimeState["FallingUp"] = 71] = "FallingUp";
                })(Monsters.SlimeState || (Monsters.SlimeState = {}));
                var SlimeState = Monsters.SlimeState;
                (function (StalagmiteState) {
                    StalagmiteState[StalagmiteState["Falling"] = 80] = "Falling";
                    StalagmiteState[StalagmiteState["HittingGround"] = 81] = "HittingGround";
                })(Monsters.StalagmiteState || (Monsters.StalagmiteState = {}));
                var StalagmiteState = Monsters.StalagmiteState;
                (function (MasterState) {
                    MasterState[MasterState["Firing"] = 90] = "Firing";
                    MasterState[MasterState["Sliming"] = 91] = "Sliming";
                    MasterState[MasterState["PostFiring"] = 92] = "PostFiring";
                    MasterState[MasterState["PostSliming"] = 93] = "PostSliming";
                })(Monsters.MasterState || (Monsters.MasterState = {}));
                var MasterState = Monsters.MasterState;
            })(Monsters = Beings.Monsters || (Beings.Monsters = {}));
        })(Beings = Entities.Beings || (Entities.Beings = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
