﻿module Dunjax.Entities.Beings.Monsters
{
    export interface IMonster extends IBeing
    {
        /**
         * This monster's monster-type.
         */
        monsterType: MonsterType;

        /**
         * How many pixels per second this monster moves along a surface.
         */
        moveSpeed: number;
        
        /**
         * Whether this monster is currently attacking the player.
         */
        isAttacking: boolean;

        /**
         * Whether this monster is currently asleep.
         */
        isSleeping: boolean;

        /**
         * Whether this monster is currently waking from a sleeping state.
         */
        isWaking: boolean;

        /**
         * Whether this monster is currently attached to the ceiling.
         */
        isOnCeiling: boolean;

        /**
         * Whether this monster has a turning sequence for its current state
         * of being either on the ground or the ceiling.
         */
        hasTurnSequence: boolean;
         
        /**
         * Returns whether this monster is physically close enough to the player
         * to attack him.
         */
        isCloseEnoughToAttack(): boolean;
    }

    /* DownCaster */
    export function asMonster(being: IBeing): IMonster
    {
        return (<any>being).isCloseEnoughToAttack ? <any>being : null;
    }

    /**
     * The different types of monsters that exist in the game.
     */
    export enum MonsterType
    {
        BlueGargoyle, GreenGargoyle, Master, Slime, Spider,
        Stalagmite, Whirlwind, YellowGargoyle
    };

    /**
     * The monster at the end of the game.
     */
    export interface IMaster extends IMonster
    {
        /**
         * Is fired when this master is killed, which signals the completion of the game.
         */
        onKilled: () => void;
    }
}