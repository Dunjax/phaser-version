﻿/// <reference path="Monster.ts" />
/// <reference path="../../shots/FireballShot.ts" />
/// <reference path="../../shots/SlimeShot.ts" />

module Dunjax.Entities.Beings.Monsters
{
    import IFrameSequence = Animations.IFrameSequence;
    import IMonsterMovementMethod = Monsters.Movement.IMonsterMovementMethod;
    import IMaster = Monsters.IMaster;
    import FireballShot = Entities.Shots.FireballShot;
    import SlimeShot = Entities.Shots.SlimeShot;
    
    /**
     * The monster at the end of the game.  It is stationary, but can fire 
     * fireballs at the ground, and slimeballs at the ceiling.  It's only
     * vulnerable spot is its head.  The master starts out sleeping, and is
     * awoken only only when it is hit by the player, or the player gets 
     * close enough for the master to bite him. 
     */
    export class Master extends Monster implements IMonsterMovementMethod, IMaster
    {
        /**
         * Whether this master is currently firing a fireball.
         */
        private isFiring = false;

        /**
         * Whether this master is currently firing a slime-ball.
         */
        private isSliming = false;

        /**
         * Whether this master is currently recovering after firing a fireball.
         */
        private isPostFiring = false;

        /**
         * Whether this master is currently recovering after firing a slime-ball.
         */
        private isPostSliming = false;

        /**
         * Whether this master is currently roaring, after waking.
         */
        private isRoaring = false; 

        constructor(x: number, y: number)
        {
            super(x, y);
            this.monsterType = MonsterType.Master;
            this.currentMovementMethod = this;
            this.health = 40;
            this.betweenAttacksDuration.length = 1500;
            this.attackDamage = 30;
            this.isFacingLeft = true;
            this.isSleeping = true;
            this.beingFrameSequences = this.monsterFrameSequences =
                MasterFrameSequences.singleton;
            this.beingSounds = this.monsterSounds = new MonsterSounds();
        }

        /**
         * Overrriden to not flip the sprite's image, since it starts out facing the correct
         * direction.
         */
        /* HookOverride */
        protected onActed() { }

        /* Implementation */
        moveMonster(): boolean
        {
            // the master does not move
            return true;
        }

        /* HookOverride */
        protected onBeforeAnimationDoneAsMonster(sequence: IFrameSequence)
        {
            if (this.isFiring) this.launchFireballShot();
            else if (this.isSliming) this.launchSlimeShot();
            else if (this.isWaking) {
                this.isWaking = false;
                this.isRoaring = true;
                Program.sounds.masterWake.play();
            }
            else if (this.isRoaring) {
                this.isRoaring = false;
                if (Dunjax.randomInt(0, 1)) this.isFiring = true;
                else this.isSliming = true;
            }
            else if (this.isPostFiring) {
                this.isPostFiring = false;
                if (!this.map.entities.player.isDead) this.isSliming = true;
            }
            else if (this.isPostSliming) {
                this.isPostSliming = false;
                this.isFiring = true;
            }
            else if (this.isHit) this.finishedBeingHit();
            else if (this.isDying)
                this.onKilled();
        }

        /**
         * Has this master launch a fireball shot at some location along the ground
         * below and in front of it.
         */
        private launchFireballShot()
        {
            // launch a fireball shot at the ground
            const dx = -(8 + Dunjax.randomInt(0, 5));
            const dy = dx + 20;
            const shot = new FireballShot(this.left - 1, this.y + 16, dx, dy);
            this.map.entities.addEntity(shot);
            Program.sounds.fireballShot.play();
            
            // this master is now in the post-firing state
            this.isFiring = false;
            this.isPostFiring = true;
        }
        
        /**
         * Has this master launch a slime-shot at some location along the ceiling
         * above and in front of it
         */
        private launchSlimeShot()
        {
            // launch a slime shot at the ceiling
            const dx = -(8 + Dunjax.randomInt(0, 5));
            const dy = -(dx + 20);
            const shot = new SlimeShot(this.left - 1, this.y - this.height / 4 - 16, dx, dy);
            this.map.entities.addEntity(shot);
            Program.sounds.slimeballShot.play();
            
            // this master is now in the post-sliming state
            this.isSliming = false;
            this.isPostSliming = true;
        }
        
        /**
         * Informs this master that it has finished going through its hit state.
         */
        private finishedBeingHit()
        {
            // if this master is sleeping, wake it
            if (this.isSleeping) this.wake();

            else if (Dunjax.randomInt(0, 1)) this.isPostFiring = true;
            else this.isPostSliming = true;
        }

        /**
         * Wakes this master up from its sleeping state.
         */
        private wake()
        {
            // this master is now in the waking state
            this.isSleeping = false;
            this.isWaking = true;
        }

        /* OverriddenValue */
        protected isStrikableAtContainedLocation(x: number, y: number): boolean
        {
            // don't let a hit interrupt this master's waking or roaring states
            if (this.isWaking || this.isRoaring) return false;

            // if the given location falls within (an estimation of) the portion 
            // of this master's bounds that it actually occupies, it is a 
            // strikable location
            return y < this.top + this.height / 4 || x > this.left + this.width / 4;
        }

        /* OverriddenValue */
        protected isVulnerableAtStrikableLocation(x: number, y: number): boolean
        {
            // if the given location is at this master's head, it is a 
            // vulnerable location
            return x > this.left + this.width / 4
                && y >= this.top + this.height / 4
                && y <= this.y + 4;
        }

        /* OverriddenValue */
        protected shouldAlignWithGroundOrCeiling(): boolean { return false; }

        /* OverriddenValue */
        protected getFrameSequenceForStateAsMonsterSubtype(
            monsterSequenceChosen: IFrameSequence): IFrameSequence
        {
            const sequences = MasterFrameSequences.singleton;
            let sequence = monsterSequenceChosen;
            if (this.isDead || this.isDying || this.isHit);
            else if (this.isRoaring) sequence = sequences.roar;
            else if (this.isPostFiring) sequence = sequences.postFire;
            else if (this.isPostSliming) sequence = sequences.postSlime;
            else if (this.isFiring) sequence = sequences.fire;
            else if (this.isSliming) sequence = sequences.slime;
            return sequence;
        }

        /* Implementation */
        onKilled: () => void;

        /* HookOverride */
        protected beforeDying()
        {
            this.isFiring = false;
            this.isSliming = false;
            this.isPostFiring = false;
            this.isPostSliming = false;
        }

        /* HookOverride */
        protected actBeforeActingAsMonster()
        {
            if (this.isSleeping && this.isCloseEnoughToAttack()) this.wake();
        }
    }

    /**
     * Specifies the frame sequences for this kind of monster.
     */
    export class MasterFrameSequences extends MonsterFrameSequences
    {
        static singleton: MasterFrameSequences;

        /**
         * Sequences which are specific to the master.
         */
        roar: IFrameSequence;
        fire: IFrameSequence;
        slime: IFrameSequence;
        postFire: IFrameSequence;
        postSlime: IFrameSequence;

        constructor()
        {
            super();
            this.imagesPrefix = "master";

            const create = (n: string, l: number) => this.create.call(this, n, l);
            const createWith =
                (n: string, l: number, o: ICreateFrameSequenceArguments) =>
                    this.createWith.call(this, n, l, o);

            this.sleep = create("sleep", 1);
            this.wake = create("wake", 4);
            this.still = create("still", 1);
            this.fire = create("fire", 5);
            this.slime = create("slime", 4);

            // the roar sequence is just the first part of the slime sequence
            let images = [
                this.slime.getFrame(0).image,
                this.slime.getFrame(1).image,
            ];
            this.roar = createWith("roar", 2, { images: images });
            this.roar.getFrame(1).duration = 1200;

            // the post-fire seqeuence is just the front portion of the fire 
            // sequence, in reverse
            images = [
                this.fire.getFrame(2).image,
                this.fire.getFrame(1).image,
                this.fire.getFrame(0).image,
                this.still.getFrame(0).image,
            ];
            this.postFire = createWith("postFire", images.length, { images: images });
            const restDuration = 3000;
            this.postFire.getFrame(3).duration = restDuration;

            // the post-slime seqeuence is just the front portion of the slime 
            // sequence, in reverse
            images = [
                this.slime.getFrame(1).image,
                this.slime.getFrame(0).image,
                this.still.getFrame(0).image,
            ];
            this.postSlime = createWith("postSlime", images.length, { images: images });
            this.postSlime.getFrame(2).duration = restDuration;

            this.death = create("death", 7);
            this.death.setDurationOfAllFrames(100);
            this.death.getFrame(6).duration = 3000;

            this.carcass = createWith("carcass", 1, { images: [this.death.getFrame(6).image] });
            this.death.switchToWhenDone = this.carcass;

            this.hit = createWith("hit", 1, { images: [this.death.getFrame(0).image] });

            this.attack = createWith("attack", 1, { images: [this.death.getFrame(0).image] });
            this.attack.getFrame(0).duration = 500;
        }

        /* SingletonCreator */
        static create()
        {
            MasterFrameSequences.singleton = new MasterFrameSequences();
        }
    }
} 