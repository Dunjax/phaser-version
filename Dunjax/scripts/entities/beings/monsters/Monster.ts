﻿/// <reference path="../Being.ts" />
/// <reference path="../../../time/Duration.ts" />

module Dunjax.Entities.Beings.Monsters
{
    import Being = Beings.Being;
    import BeingSounds = Beings.BeingSounds;
    import Side = Entities.Side;
    import Duration = Time.Duration;
    import IFrameSequence = Animations.IFrameSequence;
    import IPlayer = Beings.Player.IPlayer;
    import ISound = Sounds.ISound;
    import MonsterMovementMethod = Monsters.Movement.IMonsterMovementMethod;

    /* Implementation */
    export class Monster extends Being implements IMonster
    {
        /* Implementation */
        monsterType: MonsterType;
                 
        /* Implementation */
        isAttacking = false;

        /* Implementation */
        isSleeping = false;

        /* Implementation */
        isWaking = false;

        /* Implementation */
        moveSpeed: number;

        /**
         * The damage done per normal attack by this monster. 
         */
        /* OverridableValue */
	    protected attackDamage: number;

        /**
         * The movement method currently being employed by this monster.
         */
        protected currentMovementMethod: MonsterMovementMethod;

        /* Implementation */
        isOnCeiling = false;

        /**
	     * The frame sequences used to depict this monster in its various monster-specific states. 
	     */
        protected monsterFrameSequences: MonsterFrameSequences;

        /**
	     * The sounds emitted by this monster. 
	     */
        protected monsterSounds: MonsterSounds;

	    /**
	     * Keeps track of how long it's been since the last time this monster attacked.
	     */
        protected betweenAttacksDuration = new Duration(500, false);

        constructor(x: number, y: number)
        {
            super(x, y);
        }

        /**
         * Has this monster start an attack on the player.  
         */
        private startAttack()
        {
            // turn this monster towards the player
            this.isFacingLeft =
                this.getSideEntityIsOn(this.map.entities.player) === Side.Left;

            this.isAttacking = true;

            this.playIfExists(this.monsterSounds.attack);
        }

        /**
         * Returns whether this monster is able to initiate an attack 
         * on the player.
         */
        private canStartAttack(): boolean
        {
            // if this monster is already attacking, it cannot attack again
            if (this.isAttacking) return false;
            
            // if this monster is sleeping or waking, it cannot attack 
            if (this.isSleeping || this.isWaking) return false;

            // if it hasn't been long enough since the last attack, this monster 
            // cannot attack now
            if (!this.betweenAttacksDuration.isDone) return false;
            
            // if the player is dead, this monster may not attack
            if (this.map.entities.player.isDead) return false;

            return this.isCloseEnoughToAttack();
        }

        /* HookOverride */
        protected onBeforeAnimationDoneAsBeing(sequence: IFrameSequence)
        {
            this.onBeforeAnimationDoneAsMonster(sequence);

            // if this monster was attacking, finish the attack
            if (this.isAttacking) this.finishAttack();
        }

        /* Hook */
        protected onBeforeAnimationDoneAsMonster(sequence: IFrameSequence) { }
        
        /**
         * Has this monster finish up its current attack and go back to what it
         * was doing before attack was begun.
         */
        private finishAttack()
        {
            // if this monster is still close enough to strike the player,
            // the player is struck
            if (this.isCloseEnoughToAttack()) this.onPlayerStruck();

            // this monster is no longer attacking
            this.isAttacking = false;
            
            // this monster must wait a bit from this point before attacking again 
            this.betweenAttacksDuration.start();
        }

        /**
         * Informs this monster that it has struck the player.
         */
        private onPlayerStruck()
        {
            // if this monster's attack is damaging, inform the player of the damage
            const player = this.map.entities.player;
            if (this.attackDamage > 0)
                player.onStruck(this.attackDamage, player.x, player.y);

            if (!player.isDead) this.onPlayerHit(player);
        }
        
        /**
         * Returns whether this monster is physically close enough to the player
         * to attack him.
         */
        isCloseEnoughToAttack(): boolean
        {
            // if the player is too far from the side of this monster
            const player = this.map.entities.player;
            const sideBeingIsOn = this.getSideEntityIsOn(player);
            const maxDistance = 4;
            if ((sideBeingIsOn === Side.Left && this.left - player.right > maxDistance)
                || sideBeingIsOn === Side.Right && player.left - this.right > maxDistance) {
                // it's not close enough to attack
                return false;
            }

            // if this monster is too far above or below the player
            // (allowing for more leeway if the monster is directly above or below
            // him), it's not close enough
            const isDirectlyAboveOrBelow = this.x >= player.left && this.x <= player.right;
            const margin = isDirectlyAboveOrBelow ? 4 : 0;
            if (this.top > player.bottom + margin || this.bottom < player.top - margin)
                return false;

            return true;
        }

        /**
         * Returns whether this monster is currently allowed to move.
         */
        private canMove(): boolean
        {
            // if this monster cannot move while in its current state,  
            // it cannot move
            if (!this.canMoveInCurrentState()) return false;

            return true;
        }
        
        /**
         * Returns whether this monster can normally move while in its current state.
         */
        protected canMoveInCurrentState(): boolean
        {
            return !this.isAttacking && !this.isSleeping && !this.isWaking && !this.isHit;
        }
        
        /**
         * Has this monster perform whatever kind of movement it is capable of.
         */
        private move()
        {
            const moved = this.currentMovementMethod.moveMonster();

            if (!moved && !this.isCloseEnoughToAttack()) this.changeMovementMethod();
        }

        /**
         * Has this monster check to see if it should switch movement methods (if
         * it has more than one to choose from) due to the current method not
         * producing any movement.
         */
        /* Hook */
        protected changeMovementMethod() {}

        /* HookOverride */
        protected actAfterAnimationCheckedAsBeingSubtype()
        {
            this.actBeforeActingAsMonster();

            if (!this.isDead && !this.isDying) this.actWhenAlive();
        }
        
        /* Hook */
        protected actBeforeActingAsMonster() {}
        
        /**
         * Performs the actions conducted by this monster while it's alive.
         */
        private actWhenAlive()
        {
            if (this.canStartAttack()) this.startAttack();

            if (this.canMove()) this.move();
        }
        
        /**
         * Informs this monster that it has just scored a hit on the given player. 
         */
        /* Hook */
        protected onPlayerHit(player: IPlayer) {}

        /* HookOverride */
        protected getFrameSequenceForStateAsBeingSubtype(
            beingSequenceChosen: IFrameSequence): IFrameSequence
        {
            const sequences = this.monsterFrameSequences;
            let sequence = beingSequenceChosen;
            const isOnCeiling = this.isOnCeiling;
            if (this.isDead || this.isHit);
            else if (this.isDying && isOnCeiling) sequence = sequences.ceilingDeath;
            else if (this.isDying);
            else if (this.isSleeping) sequence = sequences.sleep;
            else if (this.isWaking) sequence = sequences.wake;
            else if (this.isAttacking && isOnCeiling) sequence = sequences.ceilingAttack;
            else if (this.isAttacking) sequence = sequences.attack;
            else if (this.isTurning && isOnCeiling) sequence = sequences.ceilingTurn;
            else if (this.isMoving && isOnCeiling) sequence = sequences.ceilingMove;
            else if (!this.isMoving && isOnCeiling) sequence = sequences.ceilingStill;

            return this.getFrameSequenceForStateAsMonsterSubtype(sequence);
        }

        /* OverridableValue */
        protected getFrameSequenceForStateAsMonsterSubtype(
            monsterSequenceChosen: IFrameSequence): IFrameSequence
        {
            return monsterSequenceChosen;
        }

        /* Implementation */
        get hasTurnSequence(): boolean
        {
            const sequences = this.monsterFrameSequences;
            return this.isOnCeiling
                ? sequences.ceilingTurn != null
                : sequences.turn != null;
        }
    }

    /**
     * The different sounds issued by monsters of a particular class.  
     */
    export class MonsterSounds extends BeingSounds
    {
        attack: ISound;

        constructor()
        {
            super();
            this.death = Program.sounds.monsterDeath;
        }
    }

    /**
     * The different frame-sequences a monster may display, beyond those of a being.
     */
    export class MonsterFrameSequences extends BeingFrameSequences
    {
        attack: IFrameSequence;
        sleep: IFrameSequence;
        wake: IFrameSequence;
        ceilingAttack: IFrameSequence;

        constructor()
        {
            super();
            this.imagesPath = "entities/monsters/";
        }
    }
}