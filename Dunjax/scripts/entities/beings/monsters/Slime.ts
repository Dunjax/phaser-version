﻿/// <reference path="Monster.ts" />

module Dunjax.Entities.Beings.Monsters
{
    import IFrameSequence = Animations.IFrameSequence;
    import FallDownAndUpMovementMethod = Monsters.Movement.FallDownAndUpMovementMethod; 

    /**
     * A monster that can move along either the floor or the ceiling, 
     * falling up and down intermittently between the two.
     */
    export class Slime extends Monster implements IFallDownAndUpMonster
    {
        /* Implementation */
        isFalling = false;

        /* Implementation */
        isFallingUp = false;

        /* Implementation */
        fallSpeed = 5 * Maps.tileHeight;

        constructor(x: number, y: number)
        {
            super(x, y);
            this.monsterType = MonsterType.Slime;
            this.currentMovementMethod = new FallDownAndUpMovementMethod(this);
            this.health = 8;
            this.betweenAttacksDuration.length = 1000;
            this.attackDamage = 4;
            this.moveSpeed = 4 * Maps.tileWidth;
            this.beingFrameSequences = this.monsterFrameSequences = SlimeFrameSequences.singleton;
            this.beingSounds = this.monsterSounds = new MonsterSounds();
        }

        /* OverriddenValue */
        protected getFrameSequenceForStateAsMonsterSubtype(
            monsterSequenceChosen: IFrameSequence): IFrameSequence
        {
            const sequences = SlimeFrameSequences.singleton;
            let sequence = monsterSequenceChosen;
            if (this.isDead || this.isDying || this.isAttacking);
            else if (this.isFallingUp) sequence = sequences.fallUp;
            else if (this.isFalling) sequence = sequences.fall;
            else if (this.isAttacking && this.isFalling) sequence = sequences.ceilingAttack;
            return sequence;
        }

        /* HookOverride */
        protected alignSpecially()
        {
            // determine whether this slime is starting out on the ceiling
            if (!this.map.isPassible(this.x, this.top - 1)) this.isOnCeiling = true;
        }

        /* OverriddenValue */
        protected isGroundDwelling(): boolean { return false; }

        /* Implementation */
        onFallStarted()
        {
            Program.sounds.slimeJump.play();
        }
    }

    /**
     * Specifies the frame sequences for this kind of monster.
     */
    export class SlimeFrameSequences extends MonsterFrameSequences
    {
        static singleton: SlimeFrameSequences;

        /**
         * Sequences which are specific to stalagmites.
         */
        fall: IFrameSequence;
        fallUp: IFrameSequence;

        constructor()
        {
            super();
            this.imagesPrefix = "slime";

            const create = (n: string, l: number) => this.create.call(this, n, l);
            const createWith =
                (n: string, l: number, o: ICreateFrameSequenceArguments) =>
                    this.createWith.call(this, n, l, o);

            this.still = create("still", 1);
            this.move = createWith("move", 5, { repeatedFrames: [-1, -1, -1, -1, -1, 1] });
            this.ceilingMove =
                createWith("ceilingMove", 5, { repeatedFrames: [-1, -1, -1, -1, -1, 1] });
            this.fall = create("fall", 4);
            this.fallUp = create("fallUp", 5);
            this.ceilingStill = create("ceilingStill", 1);
            this.death = create("death", 3);
            this.ceilingDeath = create("ceilingDeath", 3);

            // the attack sequence is just the back portion of the move sequence
            let images = [
                this.move.getFrame(2).image,
                this.move.getFrame(3).image,
                this.move.getFrame(4).image,
            ];
            this.attack = createWith("attack", 3, { images: images });

            // the ceiling-attack sequence is just the back portion of the ceiling-move sequence
            images = [
                this.ceilingMove.getFrame(2).image,
                this.ceilingMove.getFrame(3).image,
                this.ceilingMove.getFrame(4).image,
            ];
            this.ceilingAttack = createWith("ceilingAttack", 3, { images: images });
        }

        /* SingletonCreator */
        static create()
        {
            SlimeFrameSequences.singleton = new SlimeFrameSequences();
        }
    }
}