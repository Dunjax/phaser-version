﻿/// <reference path="Monster.ts" />
/// <reference path="../../shots/WebShot.ts" />

module Dunjax.Entities.Beings.Monsters
{
    import IFrameSequence = Animations.IFrameSequence;
    import MoveAlongGroundOrCeiling = Monsters.Movement.MoveAlongGroundOrCeiling;
    import IDuration = Time.IDuration;
    import Duration = Time.Duration;
    import Point = Geometry.Point;
    import WebShot = Entities.Shots.WebShot; 
    
    /**
     * A monster which can shoot webs at the player.
     */
    export class Spider extends Monster
    {
	    /**
	     * Keeps track of how long it's been since the last time this spider 
	     * fired a web-shot.
	     */
        private static betweenFiresDurationLength = 1500;
        private betweenFiresDuration: IDuration =
            new Duration(Spider.betweenFiresDurationLength);

        constructor(x: number, y: number)
        {
            super(x, y);
            this.monsterType = MonsterType.Spider;
            this.currentMovementMethod = new MoveAlongGroundOrCeiling(this);
            this.health = 20;
            this.attackDamage = 8;
            this.moveSpeed = 167;
            this.isSleeping = true;
            this.beingFrameSequences = this.monsterFrameSequences =
                SpiderFrameSequences.singleton;
            this.beingSounds = this.monsterSounds = new MonsterSounds();
        }

        /* HookOverride */
        protected actBeforeActingAsMonster()
        {
            if (this.isSleeping && this.shouldWake()) this.wake();

            if (this.canFire()) this.fire();
        }

        /**
         * Returns whether this spider is in a suitable condition where it should
         * waken from its (presumed) slumber.
         */
        protected shouldWake(): boolean
        {
            // if this spider is sufficiently close to the player to 
            // wake up, and it can see the player
            const player = this.map.entities.player;
            return Math.abs(this.x - player.x) < 3 * Maps.pixelsPerTenFeet
                && Math.abs(this.y - player.y) < 3 * Maps.pixelsPerTenFeet
                && this.map.isLocationVisibleFrom(player.x, player.y, this.x, this.y);
        }
        
        /**
         * Has this spider awaken from its (presumed) slumber.
         */
        protected wake()
        {
            this.isSleeping = false;
            this.isWaking = true;
            this.alignWithSurroundings();
            Program.sounds.spiderWake.play();
            this.betweenFiresDuration.start();
        }
        
        /**
         * Returns whether this spider is currently in a state where it can 
         * fire a web at the player.
         */
        protected canFire(): boolean
        {
            const player = this.map.entities.player;
            if (this.isDead || this.isSleeping || this.isWaking
                || !this.betweenFiresDuration.isDone || player.isDead)
                return false;
            
            // return whether this spider can see the player
            const front = this.getFrontLocation();
            return this.map.isLocationVisibleFrom(player.x, player.y, front.x, front.y);
        }
        
        /**
         * Returns the location of this spider's web-emitting front on its map.
         */
        private getFrontLocation(): Point
        {
            return {
                x: this.isFacingLeft ? this.left + 4 : this.right - 4,
                y: this.y
            };
        }

        /* HookOverride */
        protected onBeforeAnimationDoneAsMonster(sequence: IFrameSequence)
        {
            // if this spider is done coming out of its tunnel, it's now in its
            // normal state
            if (sequence === this.monsterFrameSequences.wake) this.isWaking = false;
        }

        /* OverriddenValue */
        protected isStrikableAtContainedLocation(x: number, y: number): boolean
        {
            // if this spider is sleeping or waking, it is still in its tunnel, and
            // therefore cannot be hit
            return !this.isSleeping && !this.isWaking;
        }
    	
	    /**
	     * Has this spider fire a web-shot at the player. 
	     */
	    protected fire()
        {
            // create a web-shot at this spider's front, aimed at the player
            const front = this.getFrontLocation();
            const player = this.map.entities.player;
            const shot = new WebShot(front.x, front.y, player.x - front.x, player.y - front.y);
            this.map.entities.addEntity(shot);
            Program.sounds.webFire.play();
            
            // randomize this spider's next fire-time so that all spiders 
            // don't fire at once
            this.betweenFiresDuration.length =
                Spider.betweenFiresDurationLength / 2 +
                    Dunjax.randomInt(0, Spider.betweenFiresDurationLength);
            this.betweenFiresDuration.start();
        }

        /* OverriddenValue */
        protected shouldAlignWithGroundOrCeiling(): boolean
        {
            return !this.isSleeping;
        }

        /* HookOverride */
        protected alignSpecially()
        {
            // align this spider with its background passageway 
            this.x -= this.left % Maps.pixelsPerTenFeet;
            this.y -= this.top % Maps.pixelsPerTenFeet;
        }
    }

    /**
     * Specifies the frame sequences for this kind of monster.
     */
    export class SpiderFrameSequences extends MonsterFrameSequences
    {
        static singleton: SpiderFrameSequences;

        constructor()
        {
            super();
            this.imagesPrefix = "spider";

            const create = (n: string, l: number) => this.create.call(this, n, l);
            const createWith =
                (n: string, l: number, o: ICreateFrameSequenceArguments) =>
                    this.createWith.call(this, n, l, o);
            const goToStill = (s: IFrameSequence) => s.switchToWhenDone = this.still;

            this.sleep = createWith("sleep", 1, { images: [Program.emptyImage] });
            this.wake = create("wake", 7);
            this.still = create("still", 1);
            this.move = create("move", 6);
            this.turn = create("turn", 1);
            goToStill(this.turn);
            this.carcass = create("carcass", 1);
            this.death = create("death", 3);
            this.death.switchToWhenDone = this.carcass;

            // the attack sequence is the same as the back half of the move sequence
            const images = [
                this.move.getFrame(3).image,
                this.move.getFrame(4).image,
                this.move.getFrame(5).image
            ];
            this.attack = createWith("attack", 3, { images: images });
            goToStill(this.attack);
        }

        /* SingletonCreator */
        static create()
        {
            SpiderFrameSequences.singleton = new SpiderFrameSequences();
        }
    }
}