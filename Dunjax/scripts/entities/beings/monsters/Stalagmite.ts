﻿/// <reference path="Monster.ts" />
/// <reference path="../../../maps/ITileType.ts" />

module Dunjax.Entities.Beings.Monsters
{
    import IFrameSequence = Animations.IFrameSequence;
    import IImage = Images.IImage;
    import IMonsterMovementMethod = Monsters.Movement.IMonsterMovementMethod;

    /**
     * A monster that, once woken, falls from the ceiling to the ground.
     */
    export class Stalagmite extends Monster implements IMonsterMovementMethod
    {
        /**
         * Whether this stalagmite is currently falling to the ground.
         */
        private isFalling: boolean;

        /**
         * Whether this stalagmite is currently hitting the ground, after falling.
         */
        private isHittingGround: boolean;

        constructor(x: number, y: number)
        {
            super(x, y);
            this.monsterType = MonsterType.Stalagmite;
            this.currentMovementMethod = this;
            this.health = 100;
            this.moveSpeed = 10 * Maps.tileHeight;
            this.betweenAttacksDuration.length = 1000 / this.moveSpeed;
            this.isSleeping = true;
            this.isFalling = false;
            this.beingFrameSequences = this.monsterFrameSequences =
                StalagmiteFrameSequences.singleton;
            this.beingSounds = this.monsterSounds = new MonsterSounds();
        }

        /* OverriddenValue */
        protected getDefaultImage(): IImage
        {
            return StalagmiteFrameSequences.singleton.sleep.getFrame(0).image;
        }

        /* HookOverride */
        protected actBeforeActingAsMonster()
        {
            if (this.isSleeping && this.shouldWake()) this.wake();
        }
        
        /**
         * Returns whether this stalagmite is in a suitable condition where it should
         * waken from its (presumed) slumber.
         */
        private shouldWake(): boolean
        {
            // return whether this stalagmite is sufficiently close to the player to 
            // wake up (including being above the player), and can see the player
            const player = this.map.entities.player;
            return player.y > this.y
                && Math.abs(this.x - player.x) / (player.y - this.y) < 0.5
                && this.map.isLocationVisibleFrom(player.x, player.y, this.x, this.y);
        }

        /**
         * Has this stalagmite awaken from its (presumed) slumber.
         */
        private wake()
        {
            this.isSleeping = false;
            this.isWaking = true;
            Program.sounds.stalagmiteWake.play();
        }

        /* OverriddenValue */
        protected canMoveInCurrentState(): boolean
        {
            return this.isFalling;
        }

        /* Implementation */
        moveMonster(): boolean
        {
            // if it's passible just below this stalagmite
            const map = this.map;
            const dy = Dunjax.getMovementForSpeed(this.moveSpeed);
            if (map.isPassible(this.left + 6, this.bottom + dy) &&
                map.isPassible(this.right - 6, this.bottom + dy)) {
                // move this stalagmite down one increment
                this.y += dy;
            }

            // otherwise
            else {
                // this stalagmite is now hitting the ground
                this.isFalling = false;
                this.isHittingGround = true;
            }

            // if stalagmite is passing through the player,
            // the player takes a hit
            const player = map.entities.player;
            if (Geometry.rectContains(player, this.left + 8, this.bottom)
                || Geometry.rectContains(player, this.right - 8, this.bottom))
                player.onStruck(Math.floor(dy));

            return true;
        }

        /* HookOverride */
        protected onBeforeAnimationDoneAsMonster(sequence: IFrameSequence)
        {
            if (this.isWaking) {
                this.isWaking = false;
                this.isFalling = true;
            }
            else if (this.isHittingGround) {
                this.map.entities.removeEntity(this);
            }
        }

        /* OverriddenValue */
        isCloseEnoughToAttack(): boolean
        {
            // a stalagmite attacks through its movement, rather than by an 
            // explicit attack
            return false;
        }

        /* OverriddenValue */
        protected isCeilingDwelling(): boolean { return true; }

        /* OverriddenValue */
        protected isGroundDwelling(): boolean { return false; }

        /* OverriddenValue */
        protected getFrameSequenceForStateAsMonsterSubtype(
            monsterSequenceChosen: IFrameSequence): IFrameSequence
        {
            const sequences = StalagmiteFrameSequences.singleton;
            let sequence = monsterSequenceChosen;
            if (this.isDead);
            else if (this.isHittingGround) sequence = sequences.hitGround;
            else if (this.isFalling) sequence = sequences.fall;
            return sequence;
        }
    }

    /**
     * Specifies the frame sequences for this kind of monster.
     */
    export class StalagmiteFrameSequences extends MonsterFrameSequences
    {
        static singleton: StalagmiteFrameSequences;

        /**
         * Sequences which are specific to stalagmites.
         */
        fall: IFrameSequence;
        hitGround: IFrameSequence;

        constructor()
        {
            super();
            this.imagesPrefix = "stalagmite";

            const create = (n: string, l: number) => this.create.call(this, n, l);
            this.sleep = create("sleep", 1);
            this.wake = create("wake", 2);
            this.fall = create("fall", 1);
            this.hitGround = create("hitGround", 4);
        }

        /* SingletonCreator */
        static create()
        {
            StalagmiteFrameSequences.singleton = new StalagmiteFrameSequences();
        }
    }
}