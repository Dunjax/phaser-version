﻿/// <reference path="Monster.ts" />

module Dunjax.Entities.Beings.Monsters
{
    import IFrameSequence = Animations.IFrameSequence;
    import MoveAlongGroundOrCeiling = Monsters.Movement.MoveAlongGroundOrCeiling;
    import IPlayer = Beings.Player.IPlayer;
    import IImage = Images.IImage;

    /**
     * A monster that sends the player flying when it hits him.
     */
    export class Whirlwind extends Monster
    {
        constructor(x: number, y: number)
        {
            super(x, y);
            this.monsterType = MonsterType.Whirlwind;
            this.currentMovementMethod = new MoveAlongGroundOrCeiling(this);
            this.health = 10;
            this.betweenAttacksDuration.length = 600;
            this.attackDamage = 0;
            this.moveSpeed = 250;
            this.beingFrameSequences = this.monsterFrameSequences =
                WhirlwindFrameSequences.singleton;
            this.beingSounds = this.monsterSounds = new MonsterSounds();
            this.beingSounds.death = null;
        }

        /* HookOverride */
        protected onPlayerHit(player: IPlayer)
        {
            // the player is whirlwinded
            player.onWhirlwinded(this.isFacingLeft ? Side.Left : Side.Right);
        }
    }

    /**
     * Specifies the frame sequences for this kind of monster.
     */
    export class WhirlwindFrameSequences extends MonsterFrameSequences
    {
        static singleton: WhirlwindFrameSequences;

        constructor()
        {
            super();
            this.imagesPrefix = "whirlwind";

            const createWith =
                (n: string, l: number, o: ICreateFrameSequenceArguments) =>
                    this.createWith.call(this, n, l, o);
            const goToStill = (s: IFrameSequence) => s.switchToWhenDone = this.still;

            this.still = this.move = createWith("move", 8, { isRepeating: true });
            this.attack = createWith("attack", 1, { images: [this.still.getFrame(0).image] });
            goToStill(this.attack);

            // the death sequence is the same as the normal sequence, except with
            // blanks thrown in to make the whirlwind look like it's blinking out
            const deathImages: IImage[] = [];
            const imagesCount = this.move.framesCount * 2;
            for (let i = 0; i < imagesCount; i++)
                deathImages[i] =
                    (i % 2 === 1)
                        ? this.move.getFrame(Math.floor(i / 2)).image
                        : Program.emptyImage;
            this.death = createWith("death", deathImages.length, { images: deathImages });
            this.death.setDurationOfAllFrames(140);
        }

        /* SingletonCreator */
        static create()
        {
            WhirlwindFrameSequences.singleton = new WhirlwindFrameSequences();
        }
    }
} 