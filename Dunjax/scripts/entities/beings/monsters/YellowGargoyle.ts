﻿/// <reference path="Monster.ts" />

module Dunjax.Entities.Beings.Monsters
{
    import IFrameSequence = Animations.IFrameSequence;
    import MoveAlongGroundOrCeiling = Monsters.Movement.MoveAlongGroundOrCeiling; 
    
    /**
     * A monster with no special abilities.
     */
    export class YellowGargoyle extends Monster
    {
        constructor(x: number, y: number)
        {
            super(x, y);
            this.monsterType = MonsterType.YellowGargoyle;
            this.currentMovementMethod = new MoveAlongGroundOrCeiling(this);
            this.health = 2;
            this.attackDamage = 3;
            this.moveSpeed = 140;
            this.beingFrameSequences = this.monsterFrameSequences =
                YellowGargoyleFrameSequences.singleton;
            this.beingSounds = this.monsterSounds = new MonsterSounds();
        }
    }

    /**
     * Specifies the frame sequences for this kind of monster.
     */
    export class YellowGargoyleFrameSequences extends MonsterFrameSequences
    {
        static singleton: YellowGargoyleFrameSequences;

        constructor()
        {
            super();
            this.imagesPrefix = "yellowGargoyle";

            const create = (n : string, l : number) => this.create.call(this, n, l);
            const createWith =
                (n: string, l: number, o: ICreateFrameSequenceArguments) =>
                    this.createWith.call(this, n, l, o);
            const goToStill = (s: IFrameSequence) => s.switchToWhenDone = this.still;

            this.still = create("still", 1);
            this.move = createWith("move", 6, { isRepeating: true });
            this.attack = createWith("attack", 4, { repeatedFrames: [-1, -1, -1, 2] });
            goToStill(this.attack);
            this.turn = create("turn", 2);
            goToStill(this.turn);
            this.carcass = create("carcass", 1);
            this.death = create("death", 6);
            this.death.switchToWhenDone = this.carcass;
            this.spatter = create("spatter", 5);
        }

        /* SingletonCreator */
        static create()
        {
            YellowGargoyleFrameSequences.singleton = new YellowGargoyleFrameSequences();
        }
    }
}