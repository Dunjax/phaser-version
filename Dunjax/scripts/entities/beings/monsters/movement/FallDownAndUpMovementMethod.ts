﻿/// <reference path="MonsterMovementMethod.ts" />

module Dunjax.Entities.Beings.Monsters.Movement
{
    import Duration = Time.Duration; 
    
    /**
     * A movement method which causes the affected monster to move along
     * the ground or ceiling normally, then fall down or up to the ground below or
     * ceiling above, then continue the pattern.
     */
    export class FallDownAndUpMovementMethod extends MonsterMovementMethod
    {
        /**
         * The monster this movement-method is moving.
         */
        protected fallMonster: IFallDownAndUpMonster;

        /**
         * The movement method employed to move the monster along the ground or ceiling.
         */
        protected groundAndCeilingMovementMethod: MoveAlongGroundOrCeiling;

        /**
         * Keeps track of how much time is left for the monster to move along
         * the ground (or the ceiling) until it should fall down (or up).
         */
        protected static betweenFallsDurationLength = 700;
        protected betweenFallsDuration = new Duration(0, false);

        constructor(monster: IFallDownAndUpMonster)
        {
            super(monster);
            this.fallMonster = monster;
            this.groundAndCeilingMovementMethod = new MoveAlongGroundOrCeiling(monster);

            // decide which direction the monster should initially fall
            if (Dunjax.randomInt(0, 1) === 0) monster.isFalling = true;
            else monster.isFallingUp = true;
        }

        /* Implementation */
        moveMonster(): boolean
        {
            // if the monster is at a point where it could start falling, move appropriately
            if (this.couldStartFall()) this.moveWhenCouldStartFall();

            // else, if the monster is falling, have it fall some more
            else if (this.fallMonster.isFalling) this.fall(false);

            // else, if the monster is falling up, have it fall up some more 
            else if (this.fallMonster.isFallingUp) this.fall(true);

            return true;
        }

        /**
         * Returns whether the monster is in a state where it could start a fall.
         */
        protected couldStartFall(): boolean
        {
            const monster = this.fallMonster;
            return !monster.isFalling && !monster.isFallingUp && !monster.isDying
                && !monster.isTurning && !monster.isHit && !monster.isAttacking
                && !monster.isCloseEnoughToAttack();
        }

        /**
         * Has the monster under the presumption that it's in a state/position to
         * start a fall.
         */
        protected moveWhenCouldStartFall()
        {
            // if it's time for the monster to fall/fall-up, start the fall
            if (this.betweenFallsDuration.isDone) this.startFall();

            // otherwise, try to move along the ground or ceiling normally 
            else this.groundAndCeilingMovementMethod.moveMonster();
        }

        /**
         * Starts the monster on a fall.
         */
        protected startFall()
        {
            const monster = this.fallMonster;
            monster.isMoving = false;
            if (monster.isOnCeiling) monster.isFalling = true;
            else monster.isFallingUp = true;
            monster.onFallStarted();
        }

        /**
         * Has the monster try to fall one increment downward or upward. 
         */
        protected fall(upward: boolean)
        {
            // if the monster can fall some amount
            const amount = this.canFall(upward);
            if (amount !== 0) {
                // move the monster by that amount
                this.monster.y += amount;
            }

            // otherwise, end the fall
            else this.endFall(upward);
        }

        /**
         * Returns whether the map is fall-througable (for the monster) in the given direction,
         * first trying at a distance corresponding to the monster's movement speed, then
         * one pixel away.  If neither location is fall-throughable, zero is returned.
         */
        protected canFall(upward: boolean): number
        {
            // test the location coresponding to the monster's movement speed
            const monster = this.fallMonster;
            const x = monster.x;
            const y = upward ? monster.top : monster.bottom;
            const width = monster.width;
            const map = monster.map;
            const check = (dy : number) => {
                return map.isFallThroughable(x - width / 4, y + dy)
                    && map.isFallThroughable(x + width / 4, y + dy);                
            }
            let dy = Dunjax.getMovementForSpeed(monster.fallSpeed) * (upward ? -1 : 1);
            if (check(dy)) return dy;

            // test the location one pixel away
            dy = upward ? -1 : 1;
            if (check(dy)) return dy;

            // return that neither location was fall-throughable
            return 0;
        }

        /**
         * Ends the fall the monster is presumed to be currently in.
         */
        protected endFall(fallWasUpward: boolean)
        {
            // the monster is no longer falling
            const monster = this.fallMonster;
            monster.isFalling = monster.isFallingUp = false;

            // determine randomly the duration of time till the next fall
            const duration = this.betweenFallsDuration;
            duration.length =
                FallDownAndUpMovementMethod.betweenFallsDurationLength
                    * Dunjax.randomReal(0.5, 1.5);
            this.betweenFallsDuration.start();

            // the monster is now considered to be on the ground/ceiling
            monster.isOnCeiling = fallWasUpward;
        }
    }
}
 