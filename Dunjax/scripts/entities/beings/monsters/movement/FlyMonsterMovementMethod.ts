﻿/// <reference path="MonsterMovementMethod.ts" />

module Dunjax.Entities.Beings.Monsters.Movement
{
    import Duration = Time.Duration;
    import IDuration = Time.IDuration;

    /**
     * A movement algorithm for monster flight.
     */
    export class FlyMonsterMovementMethod extends MonsterMovementMethod
    {
        /**
         * The monster this movement-method is moving, through flight.
         */
        flyingMonster: IFlyingMonster;

        /**
	     * Whether the monster is currently allowed to fly diagonally.  We 
	     * don't always let it fly diagonally, as then its path looks too simple
	     * and unnatural.
	     */
        private mayFlyDiagonally = true;

        /**
         * Keeps track of how long the monster's current spell of diagonal flight
         * has lasted.
         */
        private diagonalFlightDuration: IDuration = new Duration(1000);

        /**
         * Keeps track of how long it's been since the last time the monster's
         * flight sound was played.
         */
        private durationBetweenFlightSounds: IDuration = new Duration(270);

        /**
         * The vertical direction in which the monster is currently pointed to fly.
         */
        private verticalFlightDirection: VerticalSide;

        /**
         * Sometimes, this flying monster needs to fly up or down to get around
         * an obstacle, when the player is in the opposite vertical direction.
         * In such a case we don't we the monster to revert to flying towards
         * the player vertically until a way around the obstacle is found 
         * and taken.  When this variable is true, flight should continue 
         * in the current vertical direction until the obstacle is no longer 
         * in the way, or the monster runs into something.
         */
        private isFlyingVerticallyToPassObstacle = false;

        /**
         * Sometimes, this flying monster needs to fly left or right to get around
         * an obstacle, when the player is in the opposite horizontal direction.
         * In such a case we don't we the monster to revert to flying horizontally
         * towards the player until a way around the obstacle is found
         * and taken.  When this variable is true, flight should continue 
         * in the current horizontal direction  until the obstacle is no longer 
         * in the way, or the monster runs into something.
         */
        private isFlyingHorizontallyToPassObstacle = false;

        /**
         * While the monster is taking off to fly, how many fly-moves must still
         * elapse before the takeoff is considered complete. 
         */
        private takeoffDistanceLeft = 0;

        constructor(monster: IFlyingMonster)
        {
            super(monster);
            this.flyingMonster = monster;
        }

        /* Implementation */
        moveMonster(): boolean
        {
            const monster = this.flyingMonster;
            if (monster.isLanding) return true;

            if (!monster.isFlying) {
                this.startFlight();
                return true;
            }

            if (this.shouldLand()) {
                this.land();
                return true;
            }

            return this.flyMove();
        }

        /**
         * Returns whether the monster's state and position is such that it can 
         * (and should) immediately land on a surface.
         */
        private shouldLand(): boolean
        {
            // return whether the monster's feet are at ground level and it's not 
            // in the process of taking off, and it's not flying in such 
            // a way as to pass an obstacle (in which case, it shouldn't land
            // because it's trying to find its way around something)
            const monster = this.flyingMonster;
            monster.y = Math.round(monster.y);
            return !this.isFlyingHorizontallyToPassObstacle
                && !this.isFlyingVerticallyToPassObstacle
                && this.takeoffDistanceLeft <= 0
                && !monster.map.isPassible(monster.x, monster.bottom + 1);
        }

        /**
         * Puts the monster in the landing state.
         */
        private land()
        {
            const monster = this.flyingMonster;
            monster.isFlying = monster.isFlyTurning = false;
            monster.isLanding = true;
        }
        
        /**
         * Starts the monster taking off to fly.
         */
        private startFlight()
        {
            // change the monster back to flying
            const monster = this.flyingMonster;
            monster.isFlying = true;
            this.isFlyingHorizontallyToPassObstacle = false;
            this.isFlyingVerticallyToPassObstacle = false;
            this.verticalFlightDirection = VerticalSide.Top;

            // remember that the monster is just taking off
            this.takeoffDistanceLeft = 6;
        }

        /**
         * Tries to make the monster fly one increment towards the player,
         * for some periods diagonally, and for others not. Will also attempt to fly
         * the monster vertically or horizontally to pass an obstacle if one is 
         * in the way.
         * 
         * @return  Whether a fly-movement took place.
         */
        private flyMove(): boolean
        {
            // if the monster should fly-turn
            const monster = this.flyingMonster;
            let movedHorizontally = false;
            let flyMoved = false;
            let amountMoved = 0;
            if (this.shouldFlyTurn()) {
                // do so
                monster.isFlyTurning = true;
                flyMoved = true;
            }

            // if the monster should try to fly-move horizontally
            else if (this.shouldFlyMoveHorizontally()) {
                // if it can do so
                const amount = this.canFlyMoveHorizontally();
                if (amount > 0) {
                    // do so
                    this.flyMoveHorizontally(amount);
                    flyMoved = movedHorizontally = true;
                    amountMoved = amount;
                }

                // otherwise, it is blocked
                else this.onHorizontalFlyMoveBlocked();
            }

            // check to see if it's time to allow/disallow diagonal flight 
            // for the next period of time
            if (this.diagonalFlightDuration.isDone)
                this.mayFlyDiagonally = !this.mayFlyDiagonally;

            this.checkShouldFlyVerticallyToPassObstacle(movedHorizontally);

            this.checkToDetermineVerticalFlightDirection();

            // if the monster should try to fly-move vertically
            if (this.shouldFlyMoveVertically(movedHorizontally)) {
                // if it can do so
                const amount = this.canFlyMoveVertically();
                if (amount > 0) {
                    // do so
                    this.flyMoveVertically(amount);
                    flyMoved = true;
                    amountMoved = Math.max(amount, amountMoved);
                }

                // otherwise, it is blocked
                else this.onVerticalFlyMoveBlocked(movedHorizontally);
            }

            // if any flight-move occurred above, and it's an okay time to play
            // the monster's flight sound, play it
            if (flyMoved && this.shouldPlayFlightSound())
                monster.flyingMonsterSounds.fly.play();

            // if any flight-move occurred above, and the monster is taking off, 
            // one more movement during the takeoff has just occurred
            if (flyMoved && this.takeoffDistanceLeft > 0) 
                this.takeoffDistanceLeft -= amountMoved;

            return flyMoved;
        }

        /**
         * Returns whether the monster should begin a fly-turn.
         */
        private shouldFlyTurn(): boolean
        {
            // return whether it is not already fly-turning,
            // if it is not currently flying horizontally to pass an obstacle, 
            // and is facing away from the player
            const monster = this.flyingMonster;
            return !monster.isFlyTurning
                && !this.isFlyingHorizontallyToPassObstacle
                && this.isPlayerToSide(monster.width / 2)
                && monster.isFacingLeft !== this.isPlayerOnLeft();
        }
        
        /**
         * Returns whether the monster should fly-move horizontally one increment
         * in the direction of its current facing. 
         */
        private shouldFlyMoveHorizontally(): boolean
        {
            // return whether the monster is not fly-turning,
            // is either to one side of the player or 
            // is flying horizontally to try to get past an obstacle,
            // and isn't flying horizontally to try to get past an obstacle
            // while it could move vertically (as the fact that horizontal
            // moves occur first means that monster would otherwise miss
            // certain vertical openings which may allow it past the obstacle)
            const monster = this.flyingMonster;
            return !monster.isFlyTurning
                && (this.isPlayerToSide() || this.isFlyingHorizontallyToPassObstacle)
                && !(this.isFlyingHorizontallyToPassObstacle
                    && this.isPlayerToVerticalSide(monster.height / 2 - 1)
                    && this.canFlyMoveVertically() > 0);
        }

        /**
         * Checks whether the monster's vertical flight direction should be 
         * changed, and if it does, changes it to be towards the player.
         */
        private checkToDetermineVerticalFlightDirection()
        {
            const monster = this.flyingMonster;
            if (!this.isFlyingVerticallyToPassObstacle && this.isPlayerToVerticalSide())
                this.verticalFlightDirection =
                    monster.getVerticalSideEntityIsOn(monster.map.entities.player);
        }

        /**
         * Returns whether the monster should fly-move vertically one increment
         * in the direction of its current vertical facing. 
         */
        private shouldFlyMoveVertically(movedHorizontally: boolean): boolean
        {
            const monster = this.flyingMonster;
            return (this.isFlyingVerticallyToPassObstacle
                || ((this.mayFlyDiagonally || !movedHorizontally)
                    && (this.isPlayerToVerticalSide()
                        || (!movedHorizontally && !monster.isCloseEnoughToAttack()))));
        }

        /**
         * Assuming that the monster has just performed a flight-move, 
         * this returns whether it's flight sound should be played to 
         * accompany that movement. 
         */
        private shouldPlayFlightSound()
        {
            // return whether it's been long enough since the last time this 
            // monster's flight sound was played, and the monster can see the 
            // player from its location
            const monster = this.flyingMonster;
            const player = monster.map.entities.player;
            return this.durationBetweenFlightSounds.isDone
                && monster.map.isLocationVisibleFrom(player.x, player.y, monster.x, monster.y);
        }

        /**
         * Returns whether the monster is currently capable of fly-moving
         * horizontally one increment in the direction of its current facing. 
         */
        private canFlyMoveHorizontally(): number
        {
            // if the monster is fly-turning, then no
            var monster = this.flyingMonster;
            if (monster.isFlyTurning) return 0;

            // check passability to the side of the monster's facing
            const check = (dx: number) => {
                return monster.isPassibleToSide(monster.isFacingLeft ? Side.Left : Side.Right, dx);
            };
            const dx = Dunjax.getMovementForSpeed(monster.flyMoveSpeed);
            if (check(dx)) return dx;
            if (check(1)) return 1;
            return 0;
        }
        
        /**
         * Flies the monster one increment to the side of its current facing.
         */
        private flyMoveHorizontally(amount: number)
        {
            // move the monster one increment to the side the monster is facing 
            const monster = this.flyingMonster;
            monster.x += amount * (monster.isFacingLeft ? -1 : 1);
        }

        /**
         * Informs the monster that it's most recent fly-move to the given side
         * was blocked by something impassable.
         */
        private onHorizontalFlyMoveBlocked()
        {
            // if the monster has been flying horizontally to pass an 
            // intervening obstacle, that movement has proven to be blocked, 
            // so do so no longer
            const monster = this.flyingMonster;
            const dx = Dunjax.getMovementForSpeed(monster.flyMoveSpeed);
            if (this.isFlyingHorizontallyToPassObstacle)
                this.isFlyingHorizontallyToPassObstacle = false;

            // otherwise, if the monster isn't close enough to
            // attack the player, can't move vertically, a random 
            // chance says yes (since always doing this will limit the situations 
            // in which the monster can find a way to reach the player), 
            // and it's passable to opposite side of the monster
            else if (this.canFlyMoveVertically() === 0
                && !monster.isCloseEnoughToAttack()
                && Dunjax.randomInt(0, 1) === 0
                && monster.isPassibleToSide(monster.isFacingLeft ? Side.Right : Side.Left, dx)) {
                // have the monster begin to try to fly horizontally in the opposite
                // direction from that tried above; such action helps
                // the monster find a way out of situations when it is
                // enclosed in a "C" or reverse-"C" terrain feature by
                // moving towards the opening
                monster.isFlyTurning = true;
                this.isFlyingHorizontallyToPassObstacle = true;
            }
        }

        /**
         * Returns whether the monster is currently capable of fly-moving
         * vertically one increment in the direction of its current vertical facing. 
         */
        private canFlyMoveVertically(): number
        {
            var monster = this.flyingMonster;
            const check =
                (dy: number) =>
                    monster.isPassibleToVerticalSide(this.verticalFlightDirection, dy);
            const dy = Dunjax.getMovementForSpeed(monster.flyMoveSpeed);
            if (check.call(this, dy)) return dy;
            if (check.call(this, 1)) return 1;
            return 0;
        }

        /**
         * Detm's whether the monster is in a state to start
         * flying vertically in order to maneuver around an obstacle.
         * 
         * @param movedHorizontally     Whether the monster has already flown
         *                              horizontally this game turn.
         */
        private checkShouldFlyVerticallyToPassObstacle(movedHorizontally: boolean)
        {
            // if the monster isn't already flying to avoid an obstacle, 
            // is level vertically with the player, 
            // it hasn't yet moved horizontally this turn, 
            // isn't fly-turning,
            // and isn't close enough to the player to attack
            const monster = this.flyingMonster;
            if (!this.isFlyingVerticallyToPassObstacle
                && !this.isFlyingHorizontallyToPassObstacle
                && !movedHorizontally
                && !this.isPlayerToVerticalSide(monster.height / 2 - 1)
                && !monster.isFlyTurning
                && !monster.isCloseEnoughToAttack()) {
                // have the monster choose a vertical direction randomly
                // to which to try to fly around the obstruction
                this.isFlyingVerticallyToPassObstacle = true;
                this.verticalFlightDirection =
                    Dunjax.randomInt(0, 1) === 1 ? VerticalSide.Top : VerticalSide.Bottom;
            }
        }

        /**
         * Flies the monster one increment towards the monster's current
         * vertical facing. 
         */
        private flyMoveVertically(amount: number)
        {
            const monster = this.flyingMonster;
            monster.y += amount * (this.verticalFlightDirection === VerticalSide.Top ? -1 : 1);

            if (!monster.isFlyTurning) monster.isFlying = true;
        }

        /**
         * Informs the monster that it's most recent fly-move to the given vertical 
         * side was blocked by something impassable.
         * 
         * @param movedHorizontally    Whether the monster has already flown
         *                                          horizontally this game turn.
         */
        private onVerticalFlyMoveBlocked(movedHorizontally: boolean)
        {
            const monster = this.flyingMonster;
            if (this.isFlyingVerticallyToPassObstacle)
                this.isFlyingVerticallyToPassObstacle = false;

            else if (!movedHorizontally && !monster.isCloseEnoughToAttack())
                this.flyToPassObstacleToVerticalFlight();
        }

        /**
         * Has the monster fly either horizontally or vertically to pass 
         * an obstacle that's in the given vertical direction.
         */
        private flyToPassObstacleToVerticalFlight()
        {
            // if the monster is at the player's x-position, 
            // and a random chance says yes
            const monster = this.flyingMonster;
            const dx = Dunjax.getMovementForSpeed(monster.flyMoveSpeed);
            if (!this.isPlayerToSide() || Dunjax.randomInt(0, 2) > 0) {
                // have it fly in a random, passable direction horizontally 
                // to try to get around the obstacle
                this.isFlyingHorizontallyToPassObstacle = true;
                if (monster.isPassibleToSide(monster.isFacingLeft ? Side.Left : Side.Right, dx)
                    && Dunjax.randomInt(0, 2) > 0) { }
                else if (monster.isPassibleToSide(
                    monster.isFacingLeft ? Side.Right : Side.Left, dx))
                    monster.isFlyTurning = true;
                else this.isFlyingHorizontallyToPassObstacle = false;
            }

            // otherwise, if a random chance says yes
            else if (Dunjax.randomInt(0, 1) === 0) {
                // the monster should now begin to try to fly vertically 
                // in the opposite direction of the obstacle attempted above
                // in order to find a way around the intervening obstacle
                this.isFlyingVerticallyToPassObstacle = true;
                this.verticalFlightDirection =
                    (this.verticalFlightDirection === VerticalSide.Top
                        ? VerticalSide.Bottom : VerticalSide.Top);
            }
        }

        /**
         * Returns whether the player is to one vertical side of the monster (versus
         * being coincident, y-wise).
         */
        /* Shorthand */
        private isPlayerToVerticalSide(tolerance: number = 0): boolean
        {
            const monster = this.flyingMonster;
            return monster.getVerticalSideEntityIsOn(monster.map.entities.player, tolerance)
                !== VerticalSide.NeitherVerticalSide;
        }
    }
}
