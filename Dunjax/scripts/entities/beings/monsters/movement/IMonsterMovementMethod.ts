﻿module Dunjax.Entities.Beings.Monsters.Movement
{
    /**
     * An algorithm for moving a monster in a particular manner.
     */
    export interface IMonsterMovementMethod
    {
        /**
         * Applies the algorithm to move the monster.
         * 
         * @return  Whether the monster was moved.
         */
        moveMonster(): boolean;
    }
}