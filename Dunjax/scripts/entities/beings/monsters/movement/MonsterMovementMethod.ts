﻿module Dunjax.Entities.Beings.Monsters.Movement
{
    /* Implementation */
    export class MonsterMovementMethod implements IMonsterMovementMethod
    {
        /**
         * The monster this movement-method is moving.
         */
        monster: IMonster;

        constructor(monster: IMonster)
        {
            this.monster = monster;
        }

        /* Hook */
        moveMonster(): boolean
        {
            throw new Error('Must override');
        }
        
        /**
         * Returns whether the player is to one side of this monster (versus
         * being coincident, x-wise).
         */
        protected isPlayerToSide(tolerance: number = 0): boolean
        {
            const monster = this.monster;
            return monster.getSideEntityIsOn(monster.map.entities.player, tolerance)
                !== Side.NeitherSide;
        }

        /**
         * Returns whether the player is to the left of this monster.
         */
        protected isPlayerOnLeft(): boolean
        {
            const monster = this.monster;
            return monster.getSideEntityIsOn(monster.map.entities.player) === Side.Left;
        }
    }
}
