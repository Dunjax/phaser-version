﻿/// <reference path="MonsterMovementMethod.ts" />

module Dunjax.Entities.Beings.Monsters.Movement
{
    import Duration = Time.Duration; 
    
    /**
     * Tries to move the monster towards the player along the ground 
     * (or the ceiling, if this is a ceiling-dwelling monster).
     */
    export class MoveAlongGroundOrCeiling extends MonsterMovementMethod
    {
        /**
         * Whether the monster is currently moving in a wandering way 
         * (i.e. moving away from the player for a random period, to give 
         * its movement some variance).
         */
        private isWandering = false;

        /**
         * When the monster is wandering, how long that wandering should continue.
         */
        private wanderDuration = new Duration(0, false);

        constructor(monster: IMonster)
        {
            super(monster);
        }

        /* Implementation */
        moveMonster(): boolean
        {
            this.checkForWanderingEnd();

            this.checkForWanderingStart();

            const monster = this.monster;
            if (this.shouldTurn()) this.turn();

            else if (this.shouldChangeFacingWithoutTurn())
                monster.isFacingLeft = !monster.isFacingLeft;

            if (monster.isTurning) return true;

            if (this.shouldMoveToSide()) {
                const amount = this.canMoveToSide();
                if (amount > 0) {
                    this.moveToSide(amount);
                    return true;
                }

                this.stopMovement();
            }
            else monster.isMoving = false;

            return false;
        }

        /**
         * Returns whether the monster should currently move one increment
         * to the side it's facing.
         */
        private shouldMoveToSide(): boolean
        {
            return !this.monster.isTurning && (this.isPlayerToSide() || this.isWandering);
        }
        
        /**
         * Returns whether the monster can move to the side it is facing,
         * first trying a distance corresponding to the monster's movement speed, then
         * one pixel away.  If both locations are blocked or unsupported, zero is returned.
         */
        private canMoveToSide(): number
        {
            // test the location coresponding to the monster's movement speed
            var monster = this.monster;
            var side = monster.isFacingLeft ? Side.Left : Side.Right;
            const check = (dx : number) => {
                return monster.isPassibleToSide(side, dx)
                    && !monster.map.isFallThroughable(
                        (side === Side.Left ? monster.left - dx : monster.right + dx),
                        (monster.isOnCeiling ? monster.top - 1 : monster.bottom + 1));
            };
            const dx = Dunjax.getMovementForSpeed(monster.moveSpeed);
            if (check(dx)) return dx;

            // test the location one pixel away
            if (check(1)) return 1;

            // return that neither location was acceptable
            return 0;
        }

        /**
         * Moves the monster the given amount to the side of its current facing.
         */
        private moveToSide(amount: number)
        {
            const monster = this.monster;
            monster.x += amount * (monster.isFacingLeft ? -1 : 1);
            
            monster.isMoving = true;
        }

        /**
         * Is called when the monster cannot move in the direction it's facing.
         */
        private onMoveBlocked()
        {
            this.stopMovement();

            // if the monster is wandering, that wandering is over
            if (this.isWandering) this.isWandering = false;

            // otherwise, check to start wandering
            else if (Program.random.integerInRange(1, 50) === 1) this.startWandering();
        }
        
        /**
         * Takes the monster out of its moving state.
         */
        private stopMovement()
        {
            this.monster.isMoving = false;
        }

        /**
         * Returns whether the monster should turn to face the player.
         */
        private shouldTurn(): boolean
        {
            // return whether the monster is not wandering,
            // is not already turning,
            // has a turning frame-sequence,
            // and is facing the wrong direction
            const monster = this.monster;
            return !this.isWandering
                && !monster.isTurning
                && monster.hasTurnSequence
                && this.isPlayerToSide()
                && monster.isFacingLeft !== this.isPlayerOnLeft();
        }

        /**
         * Returns whether the monster should change facing (without a turn)
         * to face the player.
         */
        private shouldChangeFacingWithoutTurn(): boolean
        {
            // return whether the monster the monster is not wandering,
            // doesn't have a turning frame-sequence,
            // and is facing the wrong direction
            const monster = this.monster;
            return !this.isWandering
                && !monster.hasTurnSequence
                && this.isPlayerToSide()
                && monster.isFacingLeft !== this.isPlayerOnLeft();
        }

        /**
         * Has the monster enter its turning state.
         */
        private turn()
        {
            const monster = this.monster;
            monster.isTurning = true;

            // it is now facing the other way
            monster.isFacingLeft = !monster.isFacingLeft;

            if (monster.isMoving) this.stopMovement();
        }

        /**
         * If the monster is wandering, checks to see if it should now stop.
         */
        private checkForWanderingEnd()
        {
            if (!this.isWandering) return;

            // if the monster's wandering time is finished, stop the wandering
            if (this.wanderDuration.isDone) this.isWandering = false;
        }

        /**
         * If the monster is not wandering, checks to see if it should now start.
         */
        private checkForWanderingStart()
        {
            if (this.isWandering) return;

            // if the monster is to neither side of the player,
            // but is not close enough to attack,
            // and a random chance says yes,
            // start the wandering
            const monster = this.monster;
            if (!this.isPlayerToSide(monster.width / 2 - 1)
                && !monster.isCloseEnoughToAttack()
                && Program.random.integerInRange(1, 100) === 1) this.startWandering();
        }

        /**
         * Has the monster enter into its wandering movement mode.
         */
        private startWandering()
        {
            // enter into wandering mode
            this.isWandering = true;

            // randomly determine how long the wandering is likely to continue
            const monster = this.monster;
            this.wanderDuration.length = 1000 + Program.random.integerInRange(1, 5000);
            this.wanderDuration.start();
            
            // if the monster can turn, have it turn
            if (monster.hasTurnSequence) this.turn();

            // otherwise, change its facing
            else monster.isFacingLeft = !monster.isFacingLeft;
        }
    }
}
