﻿module Dunjax.Entities.Beings.Player
{
    import IAmmo = Ammos.IAmmo;

    export interface IPlayer extends IBeing
    {
        /**
         * Whether this player is currently falling downward.
         */
        isFalling: boolean;

        /**
         * Whether this player is currently jumping upward.
         */
        isJumping: boolean;

        /**
         * Whether this player is currently sliding down a slide.
         */
        isSliding: boolean;

        /**
         * Whether this player is currently climbing a ladder.
         */
        isClimbing: boolean;

        /**
         * Whether this player is currently landing after a fall or jump.
         */
        isLanding: boolean;

        /**
         * Whether this player is currently swinging its melee weapon.
         */
        isSwinging: boolean;

        /**
         * Whether this player is currently under the effect of having collided
         * with a whirlwind.
         */
        isWhirlwinded: boolean;

        /**
         * Whether this player is currently covered by a spider's web.
         */
        isWebbed: boolean;

        /**
         * Informs this player that it has just been webbed.
         */
        onWebbed(): void;

        /**
         * Informs this player that it has just been tossed by a whirlwind.
         */
        onWhirlwinded(toSide: Side): void;

        /**
         * How many keys this player is currently holding.
         */
        keysHeld: number;

        /**
         * How much armor this player has left to protect it.
         */
        armorLeft: number;

        /**
         * Whether this player possesses a grapeshot-gun, and therefore may 
         * employ graphshot-ammo.
         */
        hasGrapeshotGun: boolean;
        
        /**
         * The ammo this player is currently firing.
         */
        ammoInUse: IAmmo;

        /**
         * Is fired when this player dies.
         */
        onHasDied: () => void;

        /**
         * Is fired when this player reaches the exit to its current map.
         */
        onReachedExit: () => void;

        /**
         * Is fired when this player touches a final-room-trigger entity.
         */
        onReachedFinalRoomTrigger: () => void;
    }

    /* DownCaster */
    export function asPlayer(being: IBeing): IPlayer 
    {
        return (<any>being).onReachedExit != undefined ? <IPlayer>being : null;
    }
}
