/// <reference path="../Being.ts" />
/// <reference path="../../items/IItem.ts" />
/// <reference path="IPlayer.ts" />
/// <reference path="PlayerSpeeds.ts" />
/// <reference path="../../shots/IShot.ts" />
/// <reference path="ammos/PulseAmmo.ts" />
/// <reference path="ammos/GrapeshotAmmo.ts" />
/// <reference path="../../shots/HalberdShot.ts" />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Beings;
        (function (Beings) {
            var Player;
            (function (Player_1) {
                var ImpassableEntitiesSet = Dunjax.Maps.ImpassableEntitiesSet;
                var TerrainFeature = Dunjax.Maps.TerrainFeature;
                var ItemType = Entities.Items.ItemType;
                var PulseAmmo = Beings.Player.Ammos.PulseAmmo;
                var GrapeshotAmmo = Beings.Player.Ammos.GrapeshotAmmo;
                var HalberdShot = Entities.Shots.HalberdShot;
                var Player = (function (_super) {
                    __extends(Player, _super);
                    function Player() {
                        _super.call(this, 0, 0);
                        this.isFalling = false;
                        this.isJumping = false;
                        this.isSliding = false;
                        this.isClimbing = false;
                        this.isLanding = false;
                        this.isSwinging = false;
                        this.isWhirlwinded = false;
                        this.isWebbed = false;
                        this.moveSpeed = 0;
                        this.isForcedFalling = false;
                        this.whirlwindedSide = Entities.Side.NeitherSide;
                        this.whirlwindImpactDamage = 6;
                        this.pulseAmmo = new PulseAmmo();
                        this.grapeshotAmmo = new GrapeshotAmmo();
                        this.ammoInUse = this.pulseAmmo;
                        this.durations = new Player_1.PlayerDurations();
                        this.webbedDurationLength = 3000;
                        this.maxJumpHeight = 5.5 * Dunjax.Maps.pixelsPerTenFeet;
                        this.keysHeld = 0;
                        this.isFacingLeft = false;
                        this.pulseAmmo.add(300);
                        this.health = 100;
                        this.beingFrameSequences = Player_1.playerFrameSequences;
                        var sounds = this.beingSounds = new Beings.BeingSoundsGroup();
                        sounds.death = Dunjax.Program.sounds.playerDeath;
                        sounds.hit = Dunjax.Program.sounds.playerHit;
                    }
                    Player.prototype.getIntervalUntilNextAction = function () {
                        return this.durations.betweenWhirlwindedVerticalMoves.length;
                    };
                    Player.prototype.canMove = function (toLeft) {
                        if (this.isSliding || this.isWebbed || this.isWhirlwinded || this.isSwinging
                            || this.isForcedFalling)
                            return false;
                        return true;
                    };
                    Player.prototype.tryToMove = function (toLeft) {
                        if (this.isPassibleToSide(toLeft ? Entities.Side.Left : Entities.Side.Right, ImpassableEntitiesSet.BeingsSet))
                            this.moveToSide(toLeft);
                        if (!this.isTurningToSide(toLeft))
                            this.decideStateAfterMoveAttempt(toLeft);
                        this.isFacingLeft = toLeft;
                        this.checkToOpenDoor();
                    };
                    Player.prototype.isTurningToSide = function (toLeft) {
                        return this.isTurning && toLeft === this.isFacingLeft;
                    };
                    Player.prototype.decideStateAfterMoveAttempt = function (toLeft) {
                        if (toLeft !== this.isFacingLeft) {
                            this.isTurning = true;
                            this.moveSpeed = 0;
                        }
                        else
                            this.isMoving = true;
                    };
                    Player.prototype.moveToSide = function (toLeft) {
                        var body = this.sprite.body;
                        var speed = Math.min(this.moveSpeed += 10, Player_1.PlayerSpeeds.move);
                        if (toLeft)
                            body.moveLeft(speed);
                        else
                            body.moveRight(speed);
                        if (this.durations.betweenMovementSounds.isDone
                            && !this.map.isFallThroughable(this.x, this.bottom + 1))
                            Dunjax.Program.sounds.playerMove.play();
                        this.isLanding = false;
                        Player_1.playerFrameSequences.move.isNodGivenToAdvance = true;
                    };
                    Player.prototype.checkToOpenDoor = function () {
                        var x = this.isFacingLeft ? this.left - 1 : this.right + 1;
                        if (this.isOpenableDoorAt(x, this.y))
                            this.openDoorAt(x, this.y);
                    };
                    Player.prototype.isOpenableDoorAt = function (x, y) {
                        var feature = this.map.getTerrainFeatureAt(x, y);
                        return feature === TerrainFeature.ClosedDoor ||
                            (feature === TerrainFeature.LockedDoor && this.keysHeld > 0);
                    };
                    Player.prototype.openDoorAt = function (x, y) {
                        if (this.map.getTerrainFeatureAt(x, y) === TerrainFeature.LockedDoor)
                            this.keysHeld--;
                        this.map.setTerrainFeatureAt(x, y, TerrainFeature.OpenDoor);
                        Dunjax.Program.sounds.door.play();
                    };
                    Player.prototype.canJump = function () {
                        if (this.isFalling || this.isJumping || this.isWebbed || this.isOnLadder(false))
                            return false;
                        var map = this.map;
                        return !map.isFallThroughable(this.left, this.bottom + 1)
                            || !map.isFallThroughable(this.right, this.bottom + 1);
                    };
                    Player.prototype.jump = function (toSide) {
                        this.isJumping = true;
                        this.jumpDistanceSoFar = 0;
                        Dunjax.Program.sounds.playerJump.play();
                    };
                    Player.prototype.checkForClimbing = function () {
                        if (this.isOnLadder(false)) {
                            this.isClimbing = true;
                            this.isFalling = false;
                            this.endJump();
                        }
                        else
                            this.isClimbing = false;
                    };
                    Player.prototype.canClimbOrDescend = function (climb) {
                        if (this.isWebbed)
                            return false;
                        var y = climb ? this.top - 1 : this.bottom + 1;
                        var beingsSet = ImpassableEntitiesSet.BeingsSet;
                        var result = this.isOnLadder(!climb)
                            && this.map.isPassible(this.left + 4, y, beingsSet)
                            && this.map.isPassible(this.right - 4, y, beingsSet);
                        return result;
                    };
                    Player.prototype.climbOrDescend = function (climb) {
                        var body = this.sprite.body;
                        var speed = Player_1.PlayerSpeeds.climb;
                        if (climb)
                            body.moveUp(speed);
                        else
                            body.moveDown(speed);
                        Player_1.playerFrameSequences.climb.isNodGivenToAdvance = true;
                        Dunjax.Program.sounds.playerClimb.play();
                    };
                    Player.prototype.isOnLadder = function (orAbove) {
                        var ladder = TerrainFeature.Ladder;
                        var map = this.map;
                        return map.getTerrainFeatureAt(this.x, this.top) === ladder
                            || map.getTerrainFeatureAt(this.x, this.y) === ladder
                            || map.getTerrainFeatureAt(this.x, this.bottom + (orAbove ? 1 : 0)) === ladder;
                    };
                    Player.prototype.canFire = function () {
                        if (this.isSwinging || this.isWebbed)
                            return false;
                        if (!this.durations.betweenFires.isDone)
                            return false;
                        if (this.ammoInUse.amount <= 0)
                            return false;
                        return true;
                    };
                    Player.prototype.fire = function (toSide) {
                        var spot = this.getFireSpot(toSide);
                        this.ammoInUse.onFired(this.map, spot.x, spot.y, this.isFacingLeft ? Entities.Side.Left : Entities.Side.Right, toSide);
                        this.postFireFrameSequence = this.getPostFireFrameSequence(toSide);
                    };
                    Player.prototype.getFireSpot = function (toSide) {
                        var _a = [this.x, this.y], spotX = _a[0], spotY = _a[1];
                        if (toSide === Entities.VerticalSide.Top || toSide === Entities.VerticalSide.Bottom) {
                            spotX += this.isFacingLeft && !this.isOnLadder(false) ? -2 : 4;
                            spotY = (toSide === Entities.VerticalSide.Top) ? this.top - 1 : this.bottom + 1;
                        }
                        else {
                            spotX = this.isFacingLeft ? this.left - 1 : this.right + 1;
                            spotY -= this.isJumping ? 8 : 3;
                        }
                        return { x: spotX, y: spotY };
                    };
                    Player.prototype.getPostFireFrameSequence = function (fireSide) {
                        var firingUp = (fireSide === Entities.VerticalSide.Top), firingDown = (fireSide === Entities.VerticalSide.Bottom);
                        var sequence = null;
                        var sequences = Player_1.playerFrameSequences;
                        if (this.isJumping && fireSide === Entities.VerticalSide.NeitherVerticalSide) { }
                        else if (this.isSliding) {
                            sequence = firingUp ? sequences.fireUpOnSlide : sequences.fireOnSlide;
                        }
                        else if (this.isOnLadder(false)) {
                            if (firingUp)
                                sequence = sequences.fireUpOnLadder;
                            else if (firingDown)
                                sequence = sequences.fireDownOnLadder;
                            else
                                sequence = sequences.fireOnLadder;
                        }
                        else {
                            if (firingUp)
                                sequence = sequences.fireUp;
                            else if (firingDown)
                                sequence = sequences.fireDown;
                            else
                                sequence = sequences.fire;
                        }
                        return sequence;
                    };
                    Player.prototype.checkForFall = function () {
                        if (this.canFall())
                            this.isFalling = true;
                        else if (this.isFalling && this.cantFallDueToBlocked && !this.isDead
                            && !this.isWhirlwinded)
                            this.land();
                    };
                    Player.prototype.canFall = function () {
                        this.cantFallDueToBlocked = false;
                        this.isForcedFalling = false;
                        if (this.isClimbing || this.isJumping || this.isSliding || this.isWhirlwinded)
                            return false;
                        var dx = Math.floor(this.sprite.deltaX);
                        var y = this.bottom + 1;
                        var map = this.map;
                        if (!this.isFalling && Math.abs(dx) > 1) {
                            for (var i = 0; i <= Math.abs(dx); i++) {
                                var step = dx < 0 ? -i : i;
                                var testLeft = Math.floor(this.left) - dx + step;
                                var testRight = Math.floor(this.right) - dx + step;
                                if (map.isFallThroughable(testLeft, y)
                                    && !map.isFallThroughable(testLeft - 1, y)
                                    && map.isFallThroughable(testRight, y)
                                    && !map.isFallThroughable(testRight + 1, y)) {
                                    var pos = this.sprite.body.shape.pos;
                                    pos.x = testLeft + this.width / 2;
                                    pos.y += 2;
                                    this.isForcedFalling = true;
                                    return true;
                                }
                            }
                        }
                        if (!map.isFallThroughable(this.left, y)
                            || !map.isFallThroughable(this.right, y)) {
                            this.cantFallDueToBlocked = true;
                            return false;
                        }
                        return true;
                    };
                    Player.prototype.land = function () {
                        this.isFalling = false;
                        this.isLanding = true;
                        Dunjax.Program.sounds.playerLand.play();
                    };
                    Player.prototype.checkForWhirlwindedMove = function () {
                        if (!this.hasWhirlwindedMoveDue())
                            return;
                        if (this.isPassibleToSide(this.whirlwindedSide, ImpassableEntitiesSet.NoneSet)) {
                            this.x += this.whirlwindedSide === Entities.Side.Left ? -1 : 1;
                        }
                        else {
                            this.endWhirlwinded();
                            Dunjax.Program.sounds.shotImpact.play();
                            this.onStruck(this.whirlwindImpactDamage);
                        }
                    };
                    Player.prototype.hasWhirlwindedMoveDue = function () {
                        return this.isWhirlwinded && this.durations.betweenWhirlwindedMoves.isDone;
                    };
                    Player.prototype.checkForWhirlwindedVerticalMove = function () {
                        if (!this.hasWhirlwindedVerticalMoveDue())
                            return;
                        if (this.isPassibleToVerticalSide(Entities.VerticalSide.Top, ImpassableEntitiesSet.NoneSet)) {
                            this.y--;
                        }
                        else {
                            this.whirlwindedVerticalMoveEnded = true;
                            Dunjax.Program.sounds.shotImpact.play();
                            this.onStruck(this.whirlwindImpactDamage);
                        }
                    };
                    Player.prototype.hasWhirlwindedVerticalMoveDue = function () {
                        return this.isWhirlwinded
                            && !this.whirlwindedVerticalMoveEnded
                            && this.durations.betweenWhirlwindedVerticalMoves.isDone;
                    };
                    Player.prototype.checkForSlide = function () {
                        if (this.canSlideToSide(Entities.Side.Left))
                            this.slide(Entities.Side.Left);
                        else if (this.canSlideToSide(Entities.Side.Right))
                            this.slide(Entities.Side.Right);
                        else if (this.isSliding)
                            this.isSliding = false;
                        if (!this.isSliding)
                            Dunjax.Program.sounds.playerSlide.stop();
                    };
                    Player.prototype.canSlideToSide = function (toSide) {
                        return this.isSlideUnderneath(toSide === Entities.Side.Left);
                    };
                    Player.prototype.isSlideUnderneath = function (toLeft) {
                        var slide = toLeft ? TerrainFeature.SlideLeft : TerrainFeature.SlideRight;
                        return this.map.getTerrainFeatureAt(toLeft ? this.right : this.left, this.bottom + 3) === slide
                            || this.map.getTerrainFeatureAt(toLeft ? this.right : this.left, this.bottom + 1) === slide;
                    };
                    Player.prototype.slide = function (toSide) {
                        var toLeft = (toSide === Entities.Side.Left);
                        this.isFacingLeft = toLeft;
                        this.isSliding = true;
                        Dunjax.Program.sounds.playerSlide.play();
                        this.isFalling = false;
                        this.isLanding = false;
                    };
                    Player.prototype.canTryToJumpMove = function () {
                        if (!this.isJumping)
                            return false;
                        var map = this.map;
                        if (this.isSliding
                            && (!map.isPassible(this.left, this.top - (this.height / 4))
                                || !map.isPassible(this.right, this.top - (this.height / 4))))
                            return false;
                        return true;
                    };
                    Player.prototype.tryToJumpMove = function (jumpForceStillBeingAdded) {
                        var isBlocked = this.isJumpMoveBlocked();
                        if (!isBlocked)
                            this.sprite.body.moveUp(Player_1.PlayerSpeeds.jump);
                        else
                            this.jumpMoveBlocked(jumpForceStillBeingAdded);
                        this.jumpDistanceSoFar += isBlocked ? 4 : Math.abs(this.sprite.deltaY);
                        if (this.isJumping)
                            Dunjax.Program.sounds.playerThrust.play();
                    };
                    Player.prototype.hasMaxJumpHeightBeenReached = function (jumpForceStillBeingAdded) {
                        return this.isJumping &&
                            (this.jumpDistanceSoFar >= this.maxJumpHeight || !jumpForceStillBeingAdded);
                    };
                    Player.prototype.isJumpMoveBlocked = function () {
                        var beingSet = ImpassableEntitiesSet.BeingsSet;
                        var jumpFudgeMargin = 1;
                        var map = this.map;
                        var y = this.top - 1;
                        return !map.isPassible(this.left + jumpFudgeMargin, y, beingSet)
                            || !map.isPassible(this.right - jumpFudgeMargin, y, beingSet);
                    };
                    Player.prototype.jumpMoveBlocked = function (jumpForceStillBeingAdded) {
                        if (!jumpForceStillBeingAdded)
                            this.endJump();
                    };
                    Player.prototype.endJump = function () {
                        this.isJumping = false;
                        Dunjax.Program.sounds.playerThrust.stop();
                    };
                    Player.prototype.canSwingMeleeWeapon = function () {
                        if (this.isSwinging || this.isWebbed || this.isWhirlwinded || this.isClimbing)
                            return false;
                        return this.durations.betweenSwings.isDone;
                    };
                    Player.prototype.swingMeleeWeapon = function () {
                        this.isSwinging = true;
                    };
                    Player.prototype.canSwitchGuns = function () {
                        return this.durations.betweenGunSwitches.isDone;
                    };
                    Player.prototype.switchGuns = function () {
                        var oldAmmo = this.ammoInUse;
                        if (this.ammoInUse === this.pulseAmmo && this.hasGrapeshotGun)
                            this.ammoInUse = this.grapeshotAmmo;
                        else
                            this.ammoInUse = this.pulseAmmo;
                        if (this.ammoInUse !== oldAmmo)
                            Dunjax.Program.sounds.playerSwitchGuns.play();
                    };
                    Player.prototype.onWebbed = function () {
                        if (this.isWebbed)
                            return;
                        this.isWebbed = true;
                        Dunjax.Program.sounds.playerWebbed.play();
                    };
                    Player.prototype.checkForTerrainFeature = function () {
                        var feature = this.map.getTerrainFeatureAt(this.x, this.y);
                        if (this.isJumping
                            && feature === TerrainFeature.Stalagmite
                            && this.durations.betweenStalagtiteHits.isDone) {
                            this.onStruck(4);
                        }
                        if (feature === TerrainFeature.ExitCave) {
                            this.onReachedExit();
                            return;
                        }
                        if (feature === TerrainFeature.SlimePool
                            && this.durations.betweenSlimePoolHits.isDone) {
                            this.onStruck(16);
                        }
                        if (feature === TerrainFeature.Spikes
                            && this.durations.betweenSpikesHits.isDone) {
                            this.onStruck(12);
                        }
                    };
                    Player.prototype.beforeSurvivedStrike = function () {
                        this.endJump();
                    };
                    Player.prototype.checkForItem = function () {
                        var item = this.map.entities.getItemInBounds(this);
                        if (item == null)
                            return;
                        this.acceptItemEffects(item);
                        item.onConsumed();
                    };
                    Player.prototype.acceptItemEffects = function (item) {
                        var type = item.itemType;
                        if (type === ItemType.PulseAmmo)
                            this.pulseAmmo.add(30);
                        else if (type === ItemType.PulseAmmoBig) {
                            this.pulseAmmo.add(100);
                            while (this.ammoInUse !== this.pulseAmmo)
                                this.switchGuns();
                        }
                        else if (type === ItemType.GrapeshotAmmo)
                            this.grapeshotAmmo.add(30);
                        else if (type === ItemType.GrapeshotGun) {
                            this.grapeshotAmmo.add(100);
                            this.hasGrapeshotGun = true;
                            while (this.ammoInUse !== this.grapeshotAmmo)
                                this.switchGuns();
                        }
                        else if (type === ItemType.ShieldPowerUp)
                            this.health += 50;
                        else if (type === ItemType.ShieldPowerPack)
                            this.health += 200;
                        else if (type === ItemType.Key)
                            this.keysHeld++;
                    };
                    Player.prototype.actAfterAnimationChecked_AsBeingSubtype = function () {
                        this.actAsPlayer();
                    };
                    Player.prototype.actAsPlayer = function () {
                        if (this.isDead) {
                            this.checkForSlide();
                            return;
                        }
                        this.endNonterminatedStates();
                        this.checkForSlide();
                        this.checkForFall();
                        var leftSignaled = Dunjax.userInput.isSignaled(Dunjax.Command.Left), rightSignaled = Dunjax.userInput.isSignaled(Dunjax.Command.Right);
                        if (leftSignaled || rightSignaled) {
                            if (this.canMove(leftSignaled))
                                this.tryToMove(leftSignaled);
                            else
                                this.isMoving = false;
                        }
                        else {
                            this.isMoving = false;
                            this.moveSpeed = 0;
                        }
                        this.checkForClimbing();
                        var upSignalled = Dunjax.userInput.isSignaled(Dunjax.Command.Up), downSignalled = Dunjax.userInput.isSignaled(Dunjax.Command.Down);
                        if (upSignalled || downSignalled) {
                            if (this.canClimbOrDescend(upSignalled))
                                this.climbOrDescend(upSignalled);
                        }
                        else if (this.isClimbing)
                            this.sprite.body.setZeroVelocity();
                        if (Dunjax.userInput.isSignaled(Dunjax.Command.SwitchGuns)) {
                            if (this.canSwitchGuns())
                                this.switchGuns();
                        }
                        if (Dunjax.userInput.isSignaled(Dunjax.Command.Swing)) {
                            if (this.canSwingMeleeWeapon())
                                this.swingMeleeWeapon();
                        }
                        var jumpSignaled = Dunjax.userInput.isSignaled(Dunjax.Command.Jump);
                        if (jumpSignaled) {
                            var side = leftSignaled ? Entities.Side.Left :
                                (rightSignaled ? Entities.Side.Right : Entities.Side.NeitherSide);
                            if (this.canJump())
                                this.jump(side);
                        }
                        if (Dunjax.userInput.isSignaled(Dunjax.Command.Fire))
                            if (this.canFire())
                                this.fire(upSignalled
                                    ? Entities.VerticalSide.Top
                                    : (downSignalled
                                        ? Entities.VerticalSide.Bottom : Entities.VerticalSide.NeitherVerticalSide));
                            else
                                this.checkForOutOfAmmoSound();
                        if (this.hasMaxJumpHeightBeenReached(jumpSignaled))
                            this.endJump();
                        if (this.canTryToJumpMove())
                            this.tryToJumpMove(jumpSignaled);
                        this.checkForWhirlwindedMove();
                        this.checkForWhirlwindedVerticalMove();
                        if (this.shouldNoLongerBeWhirlwinded())
                            this.endWhirlwinded();
                        this.checkForItem();
                        this.checkForTerrainFeature();
                        this.checkForSpecialEntity();
                        this.determineGravity();
                        this.determineFriction();
                        this.determineBounce();
                    };
                    Player.prototype.endNonterminatedStates = function () {
                        if (this.isLanding &&
                            this.animation.getFrameSequence() !== Player_1.playerFrameSequences.land)
                            this.isLanding = false;
                        if (this.isSwinging &&
                            this.animation.getFrameSequence() !== Player_1.playerFrameSequences.swing)
                            this.isSwinging = false;
                    };
                    Player.prototype.onWhirlwinded = function (toSide) {
                        if (this.isWhirlwinded)
                            return;
                        this.whirlwindedSide = toSide;
                        this.isWhirlwinded = true;
                        this.whirlwindedVerticalMoveEnded = false;
                        this.durations.whirlwindEffect.start();
                        Dunjax.Program.sounds.playerWhirlwinded.play();
                    };
                    Player.prototype.shouldNoLongerBeWhirlwinded = function () {
                        return this.isWhirlwinded && this.durations.whirlwindEffect.isDone;
                    };
                    Player.prototype.endWhirlwinded = function () {
                        this.whirlwindedSide = Entities.Side.NeitherSide;
                        this.isWhirlwinded = false;
                    };
                    Player.prototype.beforeAnimationDoneAsBeing = function (sequence) {
                        var sequences = Player_1.playerFrameSequences;
                        if (sequence === sequences.swing)
                            this.onMeleeWeaponSwung();
                        else if (sequence === sequences.land)
                            this.isLanding = false;
                        else if (sequence === sequences.webbed) {
                            this.isWebbed = false;
                            this.isMoving = true;
                        }
                        if (sequence === sequences.death)
                            this.onDied();
                        if (sequence === this.postFireFrameSequence)
                            this.postFireFrameSequence = null;
                    };
                    Player.prototype.onMeleeWeaponSwung = function () {
                        var dir = this.getHalberdShotDirection();
                        var loc = this.getHalberdShotLocation();
                        var shot = new HalberdShot(loc.x, loc.y, dir.x, dir.y);
                        this.map.entities.addEntity(shot);
                        Dunjax.Program.sounds.halberdFire.play();
                        this.isSwinging = false;
                    };
                    Player.prototype.getHalberdShotDirection = function () {
                        return { x: this.isFacingLeft ? -1 : 1, y: 0 };
                    };
                    Player.prototype.getHalberdShotLocation = function () {
                        return { x: this.isFacingLeft ? this.left : this.right, y: this.y };
                    };
                    Player.prototype.getFrameSequenceForStateAsBeingSubtype = function () {
                        var sequence = null;
                        var sequences = Player_1.playerFrameSequences;
                        var postFireSequence = this.postFireFrameSequence;
                        if (this.isWebbed)
                            sequence = sequences.webbed;
                        else if (this.isWhirlwinded)
                            sequence = sequences.whirlwinded;
                        else if (this.isSwinging)
                            sequence = sequences.swing;
                        else if (this.isClimbing && postFireSequence == null)
                            sequence = sequences.climb;
                        else if (this.isSliding)
                            sequence = sequences.slide;
                        else if (this.isJumping)
                            sequence = sequences.jump;
                        else if (this.isLanding)
                            sequence = sequences.land;
                        else if (this.isMoving)
                            sequence = sequences.move;
                        else if (postFireSequence != null)
                            sequence = postFireSequence;
                        if (postFireSequence != null && sequence !== postFireSequence)
                            this.postFireFrameSequence = null;
                        return sequence;
                    };
                    Player.prototype.beforeDying = function () {
                        Dunjax.Program.sounds.playerThrust.stop();
                        Dunjax.Program.sounds.playerSlide.stop();
                    };
                    Player.prototype.checkForOutOfAmmoSound = function () {
                        if (this.ammoInUse.amount <= 0)
                            Dunjax.Program.sounds.outOfAmmo.play();
                    };
                    Object.defineProperty(Player.prototype, "armorLeft", {
                        get: function () { return this.health; },
                        enumerable: true,
                        configurable: true
                    });
                    Player.prototype.checkForSpecialEntity = function () {
                        var entity = this.map.entities.getAnimationEntityInBounds(this);
                        if (entity == null)
                            return;
                        if (entity.animationEntityType === Entities.AnimationEntityType.FinalRoomTrigger) {
                            this.reachedFinalRoomTrigger();
                            this.map.entities.removeEntity(entity);
                        }
                    };
                    Player.prototype.determineGravity = function () {
                        var gravity = 1;
                        if (this.isJumping || this.isClimbing || this.isOnLadder(true))
                            gravity = 0;
                        var body = this.sprite.body;
                        body.gravityScale = gravity;
                    };
                    Player.prototype.determineFriction = function () {
                        var friction = 0.2;
                        if (this.isClimbing)
                            friction = 1;
                        else if (this.isSliding)
                            friction = 0;
                        var body = this.sprite.body;
                        body.friction = friction;
                    };
                    Player.prototype.determineBounce = function () {
                        var bounce = 0.2;
                        if (this.isClimbing || this.isOnLadder(true))
                            bounce = 0;
                        var body = this.sprite.body;
                        body.bounce = bounce;
                    };
                    return Player;
                })(Beings.Being);
                Player_1.Player = Player;
            })(Player = Beings.Player || (Beings.Player = {}));
        })(Beings = Entities.Beings || (Entities.Beings = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
