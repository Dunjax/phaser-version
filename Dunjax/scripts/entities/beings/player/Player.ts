﻿/// <reference path="../Being.ts" />
/// <reference path="../../items/IItem.ts" />
/// <reference path="IPlayer.ts" />
/// <reference path="PlayerSpeeds.ts" />
/// <reference path="../../shots/IShot.ts" />
/// <reference path="ammos/PulseAmmo.ts" />
/// <reference path="ammos/GrapeshotAmmo.ts" />
/// <reference path="../../shots/HalberdShot.ts" />

module Dunjax.Entities.Beings.Player
{
    import TerrainFeature = Maps.TerrainFeature;
    import IItem = Entities.Items.IItem;
    import ItemType = Entities.Items.ItemType;
    import IFrameSequence = Animations.IFrameSequence;
    import PulseAmmo = Beings.Player.Ammos.PulseAmmo;
    import GrapeshotAmmo = Beings.Player.Ammos.GrapeshotAmmo;
    import IAmmo = Beings.Player.Ammos.IAmmo;
    import Point = Geometry.Point;
    import HalberdShot = Entities.Shots.HalberdShot;

    /* Implementation */
    export class Player extends Being implements IPlayer
    {
        /* Implementation */
        isFalling = false;

        /* Implementation */
        isJumping = false;

        /* Implementation */
        isSliding = false;

        /* Implementation */
        isClimbing = false;

        /* Implementation */
        isLanding = false;

        /* Implementation */
        isSwinging = false;

        /* Implementation */
        isWhirlwinded = false;

        /* Implementation */
        isWebbed = false;

        /**
         * This player's current movement speed in pixels/second.  The player accelerates
         * from the start movement speed to a maximum speed.
         */
        private moveSpeed = 0;
        private static startMoveSpeed = 80;

        /**
         * This player's current climbing speed in pixels/second.  The player accelerates
         * from the start climbing speed to a maximum speed.
         */
        private climbSpeed = 0;
        private static startClimbSpeed = 0;

        /**
         * Whether this player is currently in a fall due to moving over an opening 
         * which is only as wide as it is, such that the normal check for a fall 
         * would usually not trigger one.
         */
        private isForcedFalling = false;

        /**
	     * The frame sequences used to depict this player in its various player-specific states. 
	     */
        private playerFrameSequences: PlayerFrameSequences;

        /**
         * A frame sequence chosen to display for this player after it has just fired (if it 
         * just has), which depends on what else it was doing at the time.  This sequence
         * is only used if there isn't a more important one to display for this player's
         * current activity.
         */
        private postFireFrameSequence: IFrameSequence;

	    /**
	     * To which side this player is currently in the midst of a toss by a whirlwind.
	     */
        private whirlwindedSide = Side.NeitherSide;
    	
        /**
         * Whether this player's vertical movement due to a current instance
         * of being whirlwinded has ended, due to the player hitting something
         * impassable.
         */
        private whirlwindedVerticalMoveEnded: boolean;
        
        /**
         * How much damage this player takes when impacting a solid surface
         * due to being whirlwinded.
         */
        private whirlwindImpactDamage = 6;

        /**
         * This player's current supply of pulse ammo.
         */
        private pulseAmmo: IAmmo = new PulseAmmo();

        /**
         * This player's current supply of grapeshot ammo.
         */
        private grapeshotAmmo: IAmmo = new GrapeshotAmmo();

        /* Implementation */
        ammoInUse = this.pulseAmmo;

        /**
         * The various durations affecting this player.
         */
        private durations = new PlayerDurations();
        
        /**
         * How long (in ms) a player will stay webbed.
         */
        private webbedDurationLength = 3000;
        
        /**
         * The maximum height a jump by this player can reach.
         */
        private maxJumpHeight = 5.5 * Maps.pixelsPerTenFeet;

        /**
         * The moves (or, attempts to move) executed so far during this player's 
         * current jump.
         */
        private jumpDistanceSoFar: number;

        /* Implementation */
        keysHeld = 0;

        /* Implementation */
        hasGrapeshotGun: boolean;
        
        constructor()
        {
            super(0, 0);
            this.isFacingLeft = false;
            this.pulseAmmo.add(300);
            this.health = 100;
            this.beingFrameSequences = this.playerFrameSequences =
                PlayerFrameSequences.singleton;

            // set this player's sounds
            const sounds = this.beingSounds = new Beings.BeingSounds();
            sounds.death = Program.sounds.playerDeath;
            sounds.hit = Program.sounds.playerHit;
        }

        /**
         * Returns whether this player can currently move along the ground in 
         * the given direction.
         */
        private canMove(toLeft: boolean): boolean
        {
            // this player can't move while in certain states
            if (this.isSliding || this.isWebbed || this.isWhirlwinded || this.isSwinging
                || this.isForcedFalling)
                return false;

            return this.isPassibleToSide(toLeft ? Side.Left : Side.Right, 1);
        }
            
        /**
         * Tries to move this player some increment in the given direction.
         */
        private move(toLeft: boolean)
        {
            this.moveToSide(toLeft);

            if (!this.isTurningToSide(toLeft)) this.decideStateAfterMoveAttempt(toLeft);

            this.isFacingLeft = toLeft;
        }
        
        /**
         * Returns whether this player is currently turning to the given direction.
         */
        private isTurningToSide(toLeft: boolean): boolean
        {
            return this.isTurning && toLeft === this.isFacingLeft;
        }
        
        /**
         * Assigns this player its state after (presumedly) having just attempted 
         * to move in the given direction.
         */
        private decideStateAfterMoveAttempt(toLeft: boolean)
        {
            // if this player isn't facing the direction just moved
            if (toLeft !== this.isFacingLeft) {
                // this player is now turning
                this.isTurning = true;
                this.moveSpeed = Player.startMoveSpeed;
            }

            // otherwise, this player is now moving
            else this.isMoving = true;
        }

	    /**
         * Moves this player some increment in the given direction.
         */
        private moveToSide(toLeft: boolean)
        {
            // move this player some increment in the given direction
            const body = this.sprite.body;
            const speed = this.moveSpeed =
                this.isClimbing
                    ? PlayerSpeeds.climb / 2 // have to slow player down as no friction is present 
                    : Math.min(this.moveSpeed += 10, PlayerSpeeds.move);
            if (toLeft) body.moveLeft(speed); else body.moveRight(speed);

            // if there's solid ground under the player's new location,
            // play this player's foot-movement sound
            if (this.durations.betweenMovementSounds.isDone
                && !this.map.isFallThroughable(this.x, this.bottom + 1))
                Program.sounds.playerMove.play();

            this.isLanding = false;

            // since movement occurred, the movement frame sequence may proceed
            this.animation.isNodGivenToAdvance = true;
        }
        
        /**
	     * Checks to see if there is a closed door to the side of this player
	     * that he is facing, and if so, tries to open it.
	     */
        private checkToOpenDoor()
        {
            const x = this.isFacingLeft ? this.left - 1 : this.right + 1;
            if (this.isOpenableDoorAt(x, this.y)) this.openDoorAt(x, this.y);
        }
        
        /**
         * Returns whether there is a door at the given location that this player
         * can open.
         */
        private isOpenableDoorAt(x: number, y: number): boolean
        {
            const feature = this.map.getTerrainFeatureAt(x, y);
            return feature === TerrainFeature.ClosedDoor ||
                (feature === TerrainFeature.LockedDoor && this.keysHeld > 0);
        }
        
        /**
         * Opens the (presumed) door at the given location.
         */
        private openDoorAt(x: number, y: number)
        {
            // if the door is locked, use one of this player's keys
            if (this.map.getTerrainFeatureAt(x, y) === TerrainFeature.LockedDoor)
                this.keysHeld--;

            // change the closed door to an open door
            this.map.setTerrainFeatureAt(x, y, TerrainFeature.OpenDoor);

            Program.sounds.door.play();
        }

        /**
         * Returns whether this player can currently start a jump towards the 
         * given side, or straight up if neither side is specified.
         */
        private canJump(): boolean
        {
            // this player may not jump when in certain states
            if (this.isFalling || this.isJumping || this.isWebbed || this.isOnLadder(false)
                || this.isSliding)
                return false;

            // return whether there's solid terrain under this player at either 
            // its left or right sides
            const map = this.map;
            return !map.isFallThroughable(this.left, this.bottom + 1)
                || !map.isFallThroughable(this.right, this.bottom + 1);
        }
        
        /**
         * Starts this player on a jump to the given side, or straight up if 
         * neither side is specified.
         */
        private jump(toSide: Side)
        {
            // start this player on the jump
            this.isJumping = true;
            this.jumpDistanceSoFar = 0;

            Program.sounds.playerJump.play();
        }

        /**
         * If this player is climbing, checks whether to end the climb due to him no 
         * longer being on a ladder.  Also, keeps the player from drifting sideways
         * while on or above a ladder, due to a lack of friction and gravity.
         */
        private checkForClimbing()
        {
            // if this player is climbing
            if (this.isClimbing)
                // if this player is still on a ladder, keep him from drifting to the side,
                // due to the lack of friction and gravity
                if (this.isOnLadder(false)) this.makeXStill();
                // otherwise, end the climb
                else this.isClimbing = false;

            // if the player is just above a ladder, don't let him slide due to the 
            // absence of friction and gravity
            if (this.isAboveLadder()) this.makeXStill();
        }

        /**
         * Returns whether this player is positioned just above a ladder.
         */
        /* Shorthand */
        private isAboveLadder(): boolean
        {
            return this.isOnLadder(true) && !this.isOnLadder(false);
        }
        
        /**
         * Returns whether this player can climb or descend (according to the 
         * given value) the ladder it's presumed to be on.
         */
        private canClimbOrDescend(climb: boolean): boolean
        {
            if (this.isWebbed) return false;

            // return whether this player is on a ladder, and above (for a climb) 
            // or below (for a descend) him is passible on both his sides
            const y = climb ? this.top - 1 : this.bottom + 1;
            const result = this.isOnLadder(!climb)
                && this.map.isPassible(this.left + 4, y, true)
                && this.map.isPassible(this.right - 4, y, true);
            return result;
        }

        /**
         * Has this player climb or descend (according to the given value) one 
         * increment on the ladder it's presumed to be on.
         */
        private climbOrDescend(climb: boolean)
        {
            // move this player upwards (for a climb) or downwards 
            // (for a descend) one increment
            const body = this.sprite.body;
            const speed = Math.min(this.climbSpeed += 5, PlayerSpeeds.climb);
            if (climb) body.moveUp(speed); else body.moveDown(speed);
            this.isClimbing = true;

            this.animation.isNodGivenToAdvance = true;

            Program.sounds.playerClimb.play();
        }

        /**
         * Returns whether any part of the middle vertical line through this 
         * player is on a ladder.
         *
         * @param orAbove       Whether a ladder just beneath this player counts.
         */
        private isOnLadder(orAbove: boolean): boolean
        {
            // return whether the terrain feature at this player's top-center,
            // center, or bottom-center (or just below it, if orAbove is specified) 
            // is a ladder
            const ladder = TerrainFeature.Ladder;
            const map = this.map;
            return map.getTerrainFeatureAt(this.x, this.top) === ladder
                || map.getTerrainFeatureAt(this.x, this.y) === ladder
                || map.getTerrainFeatureAt(
                    this.x, this.bottom + (orAbove ? 1 : 0)) === ladder;
        }

        /**
         * Returns whether this player can fire its weapon (using its current ammo). 
         */
        private canFire(): boolean
        {
            // this player may not fire while swinging its melee weapon, or while webbed
            if (this.isSwinging || this.isWebbed) return false;

            if (!this.durations.betweenFires.isDone) return false;

            // if this player is out of ammo, it cannot fire
            if (this.ammoInUse.amount <= 0) return false;

            return true;
        }
        
        /**
         * Has this player fire its weapon (using its current) ammo to the 
         * given vertical side, or horizontally if neither side is specified.
         */
        private fire(toSide: VerticalSide)
        {
            // signal this player's current ammo that it's been fired, so it may 
            // create the appropriate shot(s) at the firing spot
            const spot = this.getFireSpot(toSide);
            this.ammoInUse.onFired(
                this.map, spot.x, spot.y,
                this.isFacingLeft ? Side.Left : Side.Right, toSide);

            this.postFireFrameSequence = this.getPostFireFrameSequence(toSide);
        }

        /**
         * Returns the location on this player from whence shots currently will be 
         * fired toward the given vertical side (or horizontally, if neither side 
         * is specified).
         */
        private getFireSpot(toSide: VerticalSide): Point
        {
            // if this player is firing upwards or downwards, the shot needs to 
            // start out just above or below the player's gun-tip
            let [spotX, spotY] = [this.x, this.y];
            if (toSide === VerticalSide.Top || toSide === VerticalSide.Bottom) {
                spotX += this.isFacingLeft && !this.isOnLadder(false) ? -2 : 4;
                spotY = (toSide === VerticalSide.Top) ? this.top - 1 : this.bottom + 1;
            }

            // otherwise, the shot needs to start out just to the side of the 
            // player's gun-tip
            else {
                spotX = this.isFacingLeft ? this.left - 1 : this.right + 1;
                spotY -= this.isJumping ? 8 : 3;
            }

            return { x: spotX, y: spotY };
        }
        
        /**
         * Chooses an animation for this player's firing of a shot, 
         * based on its current state.
         */
        private getPostFireFrameSequence(fireSide: VerticalSide): IFrameSequence
        {
            // if this player is jumping and not firing up or down,
            // its current animation will do
            const firingDown = (fireSide === VerticalSide.Bottom);
            const firingUp = (fireSide === VerticalSide.Top);
            let sequence: IFrameSequence = null;
            const sequences = this.playerFrameSequences;
            if (this.isJumping && fireSide === VerticalSide.NeitherVerticalSide) { }

            // else, if this player is sliding
            else if (this.isSliding) {
                // use the appropriate fire-while-sliding animation
                sequence = firingUp ? sequences.fireUpOnSlide : sequences.fireOnSlide;
            }

            // else, if this player is on a ladder
            else if (this.isOnLadder(false)) {
                // use the appropriate fire-while-on-a-ladder animation
                if (firingUp) sequence = sequences.fireUpOnLadder;
                else if (firingDown) sequence = sequences.fireDownOnLadder;
                else sequence = sequences.fireOnLadder;
            }

            // otherwise
            else {
                // use the appropriate normal fire animation
                if (firingUp) sequence = sequences.fireUp;
                else if (firingDown) sequence = sequences.fireDown;
                else sequence = sequences.fire;
            }

            return sequence;
        }

        /**
         * Checks to see if a fall by this player is currently possible, and if 
         * it is, has this player fall one increment downwards.
         * If the fall movement is blocked, then if the player was falling,
         * it is made to start landing.
         */
        private checkForFallAsPlayer()
        {
            if (this.canFallAsPlayer()) this.isFalling = true;

            else if (this.isFalling && this.cantFallDueToBlocked && !this.isDead
                && !this.isDying && !this.isWhirlwinded)
                this.land();
        }

        /**
         * Whether the last call to CanFall() returned false because the terrain
         * under the player wasn't fall-throughable.
         */
        private cantFallDueToBlocked: boolean;
        
        /**
         * Returns whether this player is currently in a state and location to
         * fall.
         */
        private canFallAsPlayer(): boolean
        {
            this.cantFallDueToBlocked = false;
            this.isForcedFalling = false;

            // this player cannot fall when in certain states
            if (this.isClimbing || this.isJumping || this.isSliding || this.isWhirlwinded)
                return false;

            if (this.checkForMissedHole()) return true;

            // if the terrain just beneath this player isn't fall-throughable on either side
            const y = Math.floor(this.bottom) + 1;
            const map = this.map;
            if (!map.isFallThroughable(this.left, y)
                || !map.isFallThroughable(this.right, y)) {
                // this player can't fall due to being blocked
                this.cantFallDueToBlocked = true;
                return false;
            }

            return true;
        }

        /**
         * Checks at each pixel position through which this player moved horiztonally
         * during its previous action to see if there is a hole underneath through which
         * it should fall.  If there is, starts this playing on a forced fall through that hole.
         * Returns whether such a forced fall was started.
         */
        private checkForMissedHole(): boolean
        {
            // if this player is already falling, or has moved one pixel or less horizontally
            // since the last call, there is nothing to do
            const dx = Math.floor(this.sprite.deltaX);
            const absDx = Math.abs(dx);
            if (this.isFalling || absDx <= 1) return false;

            // for each pixel this player moved horizontally since the last call
            const y = Math.floor(this.bottom) + 1;
            const map = this.map;
            for (let i = 0; i <= absDx; i++) {
                // if the area just beneath this player (for this pixel of movement) 
                // is fall-throughable, but the terrain just to other side (and beneath) is not
                const step = dx < 0 ? -i : i;
                const testLeft = Math.floor(this.left) - dx + step;
                const testRight = Math.floor(this.right) - dx + step;
                if (map.isFallThroughable(testLeft, y)
                    && !map.isFallThroughable(testLeft - 1, y)
                    && map.isFallThroughable(testRight, y)
                    && !map.isFallThroughable(testRight + 1, y)) {
                    // start this player on a forced fall, because Phaser's gravity
                    // will not cause this player to fall, as it won't be checking for 
                    // the fall at this pixel of movement; the forced fall includes 
                    // putting the player x-wise evenly at this pixel of movement,
                    // and starting him downward
                    this.x = testLeft + this.width / 2;
                    this.y += 4;
                    this.isForcedFalling = true;
                    return true;
                }
            }

            return false;
        }

        /**
         * Has this player start to land after having fallen.
         */
        private land()
        {
            // start this player landing
            this.isFalling = false;
            this.isLanding = true;
            Program.sounds.playerLand.play();
        }
        
        /**
         * Checks to see if this player can (and should) do a whirlwinded-move,
         * and if so, attempts to do so.
         */
        private checkForWhirlwindedMove()
        {
            // if this player has no whirlwinded-move due, then do nothing
            if (!this.hasWhirlwindedMoveDue()) return;
            
            const body = this.sprite.body;
            const speed = PlayerSpeeds.whirlwindedMove;
            if (this.whirlwindedSide === Side.Left) body.moveLeft(speed);
            else body.moveRight(speed);

            // if it is not passible on the side of this player in the 
            // direction of its whirlwinded-ness
            const dx = Dunjax.getMovementForSpeed(speed);
            if (!this.isPassibleToSide(this.whirlwindedSide, dx, false)) {
                // the whirlwinded-ness is over, and the player takes damage
                // from the impact
                this.endWhirlwinded();
                Program.sounds.shotImpact.play();
                this.onStruck(this.whirlwindImpactDamage);
            }
        }
        
        /**
         * Returns whether this player has a whirlwinded move ready to take place.
         */
        private hasWhirlwindedMoveDue(): boolean
        {
            return this.isWhirlwinded && this.durations.betweenWhirlwindedMoves.isDone;
        }

        /**
         * Checks to see if this player can (and should) do a whirlwinded 
         * vertical-move, and if so, attempts to do so.
         */
        private checkForWhirlwindedVerticalMove()
        {
            // if this player has no whirlwinded vertical-move due, then do nothing
            if (!this.hasWhirlwindedVerticalMoveDue()) return;

            const body = this.sprite.body;
            const speed = PlayerSpeeds.whirlwindedMove;
            body.moveUp(speed);

            // if it is not passible at the top of this player
            if (!this.isPassibleToVerticalSide(VerticalSide.Top, 0)) {
                // the player's vertical movement is done for this 
                // instance of being whirlwinded
                this.whirlwindedVerticalMoveEnded = true;

                // the player takes damage from the impact
                Program.sounds.shotImpact.play();
                this.onStruck(this.whirlwindImpactDamage);
            }
        }

        /**
         * Returns whether this player has a whirlwinded vertical-move 
         * ready to take place.
         */
        private hasWhirlwindedVerticalMoveDue(): boolean
        {
            return this.isWhirlwinded
                && !this.whirlwindedVerticalMoveEnded
                && this.durations.betweenWhirlwindedVerticalMoves.isDone;
        }

        /**
         * Checks to see if this player's current situation indicates it should
         * slide, and if so, slides this player.
         */
        private checkForSlide()
        {
            if (this.canSlideToSide(Side.Left)) this.slide(Side.Left);

            else if (this.canSlideToSide(Side.Right)) this.slide(Side.Right);

            else if (this.isSliding) this.isSliding = false;

            // if this player isn't sliding, make sure the sliding sound isn't playing
            const sound = Program.sounds.playerSlide;
            if (!this.isSliding && sound.isPlaying) sound.stop();
        }
        
        /**
         * Returns whether this player is currently in a state to slide in the 
         * given direction.
         */
        private canSlideToSide(toSide: Side): boolean
        {
            // return whether there is a slide below this player on the side of the 
            // given slide direction
            return this.isSlideUnderneath(toSide === Side.Left);
        }
        
        /**
         * Returns whether there is a slide of the given direction underneath this
         * player.
         */
        private isSlideUnderneath(toLeft: boolean): boolean
        {
            // look 3 pixels below the player's lower corner which would be
            // nearest the slide (as looking only 1 pixel below often doesn't 
            // sense the slide)
            const slide = toLeft ? TerrainFeature.SlideLeft : TerrainFeature.SlideRight;
            return this.map.getTerrainFeatureAt(
                toLeft ? this.right : this.left, this.bottom + 3) === slide
                || this.map.getTerrainFeatureAt(
                    toLeft ? this.right : this.left, this.bottom + 1) === slide;
        }

        /**
         * Has this player slide one increment in the given direction.
         */
        private slide(toSide: Side)
        {
            // this player's facing is matched to the direction of the slide
            const toLeft = (toSide === Side.Left);
            this.isFacingLeft = toLeft;

            // this player is now sliding (the actual slide movement is performed
            // by the physics engine)
            this.isSliding = true;
            if (this.durations.betweenSlidesSounds.isDone) Program.sounds.playerSlide.play();
            this.isFalling = false;
            this.isLanding = false;
        }
        
        /**
         * Returns whether this player is in state where it can move extra horizontally,
         * whlie jumping.
         */
        private canTryToJumpMove(): boolean
        {
            if (!this.isJumping || this.isSliding) return false;

            return true;
        }

        /**
         * Tries to have this player perform a jump-move (that is, an upwards
         * movement as part of a presumed jump).
         *
         * @param jumpForceStillBeingAdded  
         *      If this isn't true, the jump will continue only to a certain 
         *      minimum jump height.  If it is true, which means the user is
         *      still pressing the jump button, the jump will continue while
         *      unimpeded up to the maximum jump height.
         */
        private tryToJumpMove(jumpForceStillBeingAdded: boolean)
        {
            const isBlocked = this.isJumpMoveBlocked();
            if (!isBlocked) this.sprite.body.moveUp(PlayerSpeeds.jump);

            else this.jumpMoveBlocked(jumpForceStillBeingAdded);
            
            // record that this jump has covered the further distance the player
            // moved upward last logic loop (or, a default high distance if the jump
            // is currently blocked)
            this.jumpDistanceSoFar += isBlocked ? 4 : Math.abs(this.sprite.deltaY);

            // if this player is still jumping, play its thrust sound
            if (this.isJumping && this.durations.betweenThrustSounds.isDone)
                Program.sounds.playerThrust.play();
        }
        
        /**
         * Returns whether this player's current presumed jump has reached its
         * maximum height.  
         * 
         * @param jumpForceStillBeingAdded      See TryToJumpMove().
         */
        private hasMaxJumpHeightBeenReached(jumpForceStillBeingAdded: boolean): boolean
        {
            // return whether this player is jumping, and has reached the max 
            // height for this jump, or the user is no longer pressing the jump key
            return this.isJumping &&
                (this.jumpDistanceSoFar >= this.maxJumpHeight || !jumpForceStillBeingAdded);
        }
        
        /**
         * Returns whether this player's current presumed jump-move is blocked
         * by something impassible in the map.
         */
        private isJumpMoveBlocked(): boolean
        {
            const jumpFudgeMargin = Math.abs(this.sprite.deltaX);
            const map = this.map;
            const y = this.top - 1;
            return !map.isPassible(this.left + jumpFudgeMargin, y, true)
                || !map.isPassible(this.right - jumpFudgeMargin, y, true);
        }

        /**
         * Informs this player that it's current presumed jump-move is blocked
         * by something impassible in the map.
         */
        private jumpMoveBlocked(jumpForceStillBeingAdded: boolean)
        {
            // if jump force is no longer being added (since, if it was, the 
            // jump-thrust would keep him aloft), end this jump
            if (!jumpForceStillBeingAdded) this.endJump();
        }
        
        /**
         * Has this player leave its jumping state.
         */
        private endJump()
        {
            this.isJumping = false;
            Program.sounds.playerThrust.stop();
        }
        
        /**
         * Retewens whether this player is in a state where it can start the 
         * swinging of its melee weapon.
         */
        private canSwingMeleeWeapon(): boolean
        {
            if (this.isSwinging || this.isWebbed || this.isWhirlwinded || this.isClimbing)
                return false;

            return this.durations.betweenSwings.isDone;
        }
        
        /**
         * Has this player begin to start the swinging of its melee weapon.
         */
        private swingMeleeWeapon()
        {
            // this player is now swinging
            this.isSwinging = true;
        }

	    /**
	     * Returns whether this player is in a state to switch to using the next 
	     * gun in its internal sequence of guns.
	     */
        private canSwitchGuns(): boolean
        {
            return this.durations.betweenGunSwitches.isDone;
        }
    	
        /**
         * Has this player to switch to using the next gun (if there is one) in its 
         * internal sequence of guns.
         */
        private switchGuns()
        {
            // if the current ammo being used is the pulse-ammo, and this player
            // has a graphshot-gun, use the graphshot-ammo
            const oldAmmo = this.ammoInUse;
            if (this.ammoInUse === this.pulseAmmo && this.hasGrapeshotGun)
                this.ammoInUse = this.grapeshotAmmo;
            
            // otherwise, use the pulse-ammo
            else this.ammoInUse = this.pulseAmmo;
            
            // if what ammo is in use changed above, play a switch sound
            if (this.ammoInUse !== oldAmmo) Program.sounds.playerSwitchGuns.play();
        }

        /* Implementation */
        onWebbed()
        {
            // if this player is already webbed, do nothing
            if (this.isWebbed) return;

            // start this player being webbed
            this.isWebbed = true;
            Program.sounds.playerWebbed.play();
        }

        /* Implementation */
        onReachedExit: () => void;

        /**
	     * Checks to see what terrain feature this player is currently on, 
	     * and administers its effects.
	     */
        private checkForTerrainFeature()
        {
            // if this player is jumping into a stalagmite, and it's been long
            // enough since the last stalagmite hit he's taken
            const feature = this.map.getTerrainFeatureAt(this.x, this.y);
            if (this.isJumping
                && feature === TerrainFeature.Stalagmite
                && this.durations.betweenStalagtiteHits.isDone) {
                // the stalagmite causes damage
                this.onStruck(4);
            }

            // if this player has reached the exit cave
            if (feature === TerrainFeature.ExitCave) {
                // fire an event, so that the game knows to change maps
                this.onReachedExit();
                return;
            }

            // if this player is in a slime pool, and it's been long
            // enough since the last slime damage he's taken
            if (feature === TerrainFeature.SlimePool
                && this.durations.betweenSlimePoolHits.isDone) {
                // the slime causes damage
                this.onStruck(16);
            }

            // if this player is in spikes, and it's been long
            // enough since the last spikes damage he's taken
            if (feature === TerrainFeature.Spikes
                && this.durations.betweenSpikesHits.isDone) {
                // the spikes cause damage
                this.onStruck(12);
            }
        }

        /* HookOverride */
        protected beforeSurvivedStrike()
        {
            // this player is no longer jumping (if it was)
            this.endJump();
        }

        /**
	     * Checks to see if this player is on an item, and if so, takes the item
	     * and confers its benefits on this player.
	     */
        private checkForItem()
        {
            // if there is no item at the player's location, then there 
            // is nothing to do
            const item = this.map.entities.getItemInBounds(this);
            if (item == null) return;

            this.acceptItemEffects(item);

            item.onConsumed();
        }
    	
	    /**
	     * Confers the effects of the given item upon this player.
	     */
        private acceptItemEffects(item: IItem)
        {
            const type = item.itemType;
            if (type === ItemType.PulseAmmo) this.pulseAmmo.add(30);
            else if (type === ItemType.PulseAmmoBig) {
                this.pulseAmmo.add(100);
                while (this.ammoInUse !== this.pulseAmmo) this.switchGuns();
            }
            else if (type === ItemType.GrapeshotAmmo) this.grapeshotAmmo.add(30);
            else if (type === ItemType.GrapeshotGun) {
                this.grapeshotAmmo.add(100);
                this.hasGrapeshotGun = true;
                while (this.ammoInUse !== this.grapeshotAmmo) this.switchGuns();
            }
            else if (type === ItemType.ShieldPowerUp) this.health += 50;
            else if (type === ItemType.ShieldPowerPack) this.health += 200;
            else if (type === ItemType.Key) this.keysHeld++;
        }

        /* HookOverride */
        protected actAfterAnimationCheckedAsBeingSubtype()
        {
            this.actAsPlayer();
        }

        /**
         * Executes this player's actions for the current game turn.
         */
        private actAsPlayer()
        {
            if (this.isDead || this.isDying) {
                this.actWhenDyingOrDead();
                return;
            }

            this.endNonterminatedStates();

            // a slide should come before a fall, because a sliding player can't fall,
            // while a falling player can slide
            this.checkForSlide();
            this.checkForFallAsPlayer();

            const userInput = Program.userInput;
            var jumpSignaled = userInput.isSignaled(Command.Jump);
            if (this.hasMaxJumpHeightBeenReached(jumpSignaled)) this.endJump();
            if (this.canTryToJumpMove()) this.tryToJumpMove(jumpSignaled);

            this.checkForWhirlwindedMove();
            this.checkForWhirlwindedVerticalMove();
            if (this.shouldNoLongerBeWhirlwinded()) this.endWhirlwinded();

            this.checkForClimbing();

            // if climbing or descending is requested, try to do that
            var upSignalled = userInput.isSignaled(Command.Up),
                downSignalled = userInput.isSignaled(Command.Down);
            if (upSignalled || downSignalled) {
                if (this.canClimbOrDescend(upSignalled)) this.climbOrDescend(upSignalled);
                else if (this.isClimbing) this.makeStill();
            }
            else {
                if (this.isClimbing) this.makeStill();
                this.climbSpeed = Player.startClimbSpeed;
            }

            // if switching of guns is requested, try to do that
            if (userInput.isSignaled(Command.SwitchGuns)) {
                if (this.canSwitchGuns()) this.switchGuns();
            }

            // if swinging of the melee weapon is requested, try to do that
            if (userInput.isSignaled(Command.Swing)) {
                if (this.canSwingMeleeWeapon()) this.swingMeleeWeapon();
            }

            // if a jump is requested, try to jump
            var leftSignaled = userInput.isSignaled(Command.Left),
                rightSignaled = userInput.isSignaled(Command.Right);
            if (jumpSignaled) {
                var side = leftSignaled ? Side.Left :
                    (rightSignaled ? Side.Right : Side.NeitherSide);
                if (this.canJump()) this.jump(side);
            }

            // if a firing is requested, try to fire
            if (userInput.isSignaled(Command.Fire))
                if (this.canFire())
                    this.fire(
                        upSignalled
                            ? VerticalSide.Top
                            : (downSignalled
                                ? VerticalSide.Bottom : VerticalSide.NeitherVerticalSide));
                else this.checkForOutOfAmmoSound();

            this.checkForItem();
            this.checkForTerrainFeature();
            this.checkForSpecialEntity();

            // if moving left or right is requested, try to do that
            if (leftSignaled || rightSignaled) {
                if (this.canMove(leftSignaled)) this.move(leftSignaled);
                else {
                    this.isMoving = false;
                    if (this.isFacingLeft !== leftSignaled) {
                        this.isTurning = true;
                        this.isFacingLeft = leftSignaled;
                    }
                }
                this.checkToOpenDoor();
            }
            else {
                this.isMoving = false;
                this.moveSpeed = Player.startMoveSpeed;
            }

            this.determineGravity();
            this.determineFriction();
            this.determineBounce();
        }

        /**
         * Ends certain temporary states this player may be in which may no longer be valid,
         * but were not exited after the end of the associated animation, likely due to that
         * animation being superceded in display by a more important one.
         */
        private endNonterminatedStates()
        {
            const sequence = this.animation.frameSequence;
            const sequences = this.playerFrameSequences;
            if (this.isLanding && sequence !== sequences.land)
                this.isLanding = false;
            if (this.isSwinging && (sequence !== sequences.swing || this.animation.isDone)) 
                this.isSwinging = false;
        }

        /**
         * Performs the subset of this player's actions which are the only ones applicable
         * after it has died.
         */
        private actWhenDyingOrDead()
        {
            this.checkForSlide();
            this.determineGravity();
            this.determineFriction();
            this.determineBounce();
        }

        /**
         * Zeroes any momentum this player may have.
         */
        private makeStill(): void
        {
            this.sprite.body.setZeroVelocity();
        }

        /**
         * Zeroes any horizontal momentum this player may have.
         */
        private makeXStill(): void
        {
            this.sprite.body.shape.oldpos.x = this.sprite.body.shape.pos.x;
        }

        /* Implementation */
        onWhirlwinded(toSide: Side)
        {
            // if this player is already whirlwinded, do nothing
            if (this.isWhirlwinded) return;

            // this player is now whirlwinded
            this.whirlwindedSide = toSide;
            this.isWhirlwinded = true;
            this.whirlwindedVerticalMoveEnded = false;
            this.durations.whirlwindEffect.start();

            Program.sounds.playerWhirlwinded.play();
        }
    	
        /**
         * Returns whether this player should no longer be whirlwinded, including
         * taking into account whether the player is whirlwinded to begin with.
         */
        private shouldNoLongerBeWhirlwinded(): boolean
        {
            return this.isWhirlwinded && this.durations.whirlwindEffect.isDone;
        }

        /**
         * Ends this player's presumed current instance of being whirlwinded.
         */
        private endWhirlwinded()
        {
            this.whirlwindedSide = Side.NeitherSide;
            this.isWhirlwinded = false;
        }

        /* Implementation */
        onHasDied: () => void;

        /* HookOverride */
        protected onBeforeAnimationDoneAsBeing(sequence: IFrameSequence)
        {
            const sequences = this.playerFrameSequences;
            if (sequence === sequences.swing) this.onMeleeWeaponSwung();

            else if (sequence === sequences.land)
                this.isLanding = false;

            else if (sequence === sequences.webbed) {
                this.isWebbed = false;

                // this player is now moving, to make it look like
                // it was working hard to escape the web
                this.isMoving = true;
            }

            // if this player just finished dying, report that fact
            if (sequence === sequences.death) this.onHasDied();

            if (sequence === this.postFireFrameSequence)
                this.postFireFrameSequence = null;
        }
        
        /**
         * Informs this player that it has just finished swinging its melee weapon.
         */
        private onMeleeWeaponSwung()
        {
            // start a halberd shot to the side of this player that's he facing
            const dir = this.getHalberdShotDirection();
            const loc = this.getHalberdShotLocation();
            const shot = new HalberdShot(loc.x, loc.y, dir.x, dir.y);
            this.map.entities.addEntity(shot);

            Program.sounds.halberdFire.play();

            // this player is no longer swinging
            this.isSwinging = false;
        }
        
        /**
         * Returns the direction that a halberd shot should have when issued from
         * this player's current orientation.
         */
        private getHalberdShotDirection(): Point
        {
            return { x: this.isFacingLeft ? -1 : 1, y: 0 }
        }
        
        /**
         * Returns the location that a halberd shot should have when issued from
         * this player's current location and orientation.
         */
        private getHalberdShotLocation(): Point
        {
            return { x: this.isFacingLeft ? this.left : this.right, y: this.y };
        }

        /* HookOverride */
        protected getFrameSequenceForStateAsBeingSubtype(
            beingSequenceChosen: IFrameSequence): IFrameSequence
        {
            const sequences = this.playerFrameSequences;
            let sequence = beingSequenceChosen;
            const postFireSequence = this.postFireFrameSequence;
            if (this.isDead || this.isDying);
            else if (this.isWebbed) sequence = sequences.webbed;
            else if (this.isWhirlwinded) sequence = sequences.whirlwinded;
            else if (this.isSwinging) sequence = sequences.swing;
            else if (this.isHit);
            else if (this.isClimbing && postFireSequence == null)
                sequence = sequences.climb;
            else if (this.isSliding) sequence = sequences.slide;
            else if (this.isJumping) sequence = sequences.jump;
            else if (this.isLanding && postFireSequence == null) sequence = sequences.land;
            else if (this.isMoving) sequence = sequences.move;
            else if (postFireSequence != null) sequence = postFireSequence;

            if (postFireSequence != null && sequence !== postFireSequence)
                this.postFireFrameSequence = null;

            return sequence;
        }

        /* HookOverride */
        protected beforeDying()
        {
            // stop any sounds that may have been eminating from this player
            // while it was alive 
            Program.sounds.playerThrust.stop();
            Program.sounds.playerSlide.stop();
        }

        /**
         * If this player is out of ammo, play a sound to inform the user.
         */
        private checkForOutOfAmmoSound()
        {
            if (this.ammoInUse.amount <= 0) Program.sounds.outOfAmmo.play();
        }

        /* Implementation */
        get armorLeft(): number { return this.health; }

        /**
         * Checks whether this player is contacting an entity which doesn't 
         * fall into the usual categories (e.g. beings or items), and if so,
         * takes the appropriate action for that entity. 
         */
        private checkForSpecialEntity()
        {
            // if there is no special entity at the player's location, 
            // then there is nothing to do
            const entity = this.map.entities.getAnimationEntityInBounds(this);
            if (entity == null) return;

            // if the special entity is a final-room-trigger
            if (entity.animationEntityType === AnimationEntityType.FinalRoomTrigger) {
                // signal that fact
                this.onReachedFinalRoomTrigger();

                // remove the trigger
                this.map.entities.removeEntity(entity);
            }
        }

        /* Implementation */
        onReachedFinalRoomTrigger: () => void;

        /**
         * Determine what gravity factor should be acting upon this player, given its 
         * current state.
         */
        private determineGravity()
        {
            let gravity = 1;
            if (this.isSliding || this.isDying || this.isDead);
            else if (this.isJumping || this.isClimbing) gravity = 0;
            const body = this.sprite.body;
            body.gravityScale = gravity;
        }

        /**
         * Determine what friction factor should be acting upon this player, given its 
         * current state.
         */
        private determineFriction()
        {
            let friction = 0.2;
            if (this.isClimbing || this.isDying || this.isDead) friction = 1;
            else if (this.isSliding || this.isFalling) friction = 0;
            const body = this.sprite.body;
            body.friction = friction;
        }

        /**
         * Determine what bounce factor should be acting upon this player, given its 
         * current state.
         */
        private determineBounce()
        {
            let bounce = 0.2;
            if (this.isClimbing || this.isOnLadder(true) || this.isSliding || this.isJumping
                || this.isDying || this.isDead)
                bounce = 0;
            const body = this.sprite.body;
            body.bounce = bounce;
        }

        /* OverriddenValue */
        protected canFall(): number
        {
            // a player falls only due to Phaser's gravity
             return 0;
        }
    }
}
