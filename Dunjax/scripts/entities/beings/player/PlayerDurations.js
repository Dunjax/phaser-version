/// <reference path="../../../time/Duration.ts" />
/// <reference path="../../../time/FallDuration.ts" />
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Beings;
        (function (Beings) {
            var Player;
            (function (Player) {
                var Duration = Dunjax.Time.Duration;
                var PlayerDurations = (function () {
                    function PlayerDurations() {
                        this.betweenFires = new Duration(140);
                        this.betweenSwings = new Duration(250);
                        this.betweenGunSwitches = new Duration(350);
                        this.betweenStalagtiteHits = new Duration(300);
                        this.betweenSlimePoolHits = new Duration(700);
                        this.betweenSpikesHits = new Duration(1000);
                        this.betweenWhirlwindedMoves = new Duration(3);
                        this.betweenWhirlwindedVerticalMoves = new Duration(2);
                        this.whirlwindEffect = new Duration(1500, false);
                        this.betweenMovementSounds = new Duration(200);
                    }
                    return PlayerDurations;
                })();
                Player.PlayerDurations = PlayerDurations;
            })(Player = Beings.Player || (Beings.Player = {}));
        })(Beings = Entities.Beings || (Entities.Beings = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
