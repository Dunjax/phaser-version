﻿/// <reference path="../../../time/Duration.ts" />

module Dunjax.Entities.Beings.Player
{
    import IDuration = Time.IDuration;
    import Duration = Time.Duration;
    
    /**
     * Durations which determine the frequency of a player's actions,
     * or the length of effects on the player.
     */
    export class PlayerDurations
    {
        betweenFires: IDuration = new Duration(140);
        betweenSwings: IDuration = new Duration(250);
        betweenGunSwitches: IDuration = new Duration(650);
        betweenStalagtiteHits: IDuration = new Duration(300);
        betweenSlimePoolHits: IDuration = new Duration(700);
        betweenSpikesHits: IDuration = new Duration(1000);
        betweenWhirlwindedMoves: IDuration = new Duration(3);
        betweenWhirlwindedVerticalMoves: IDuration = new Duration(2);
        whirlwindEffect: IDuration = new Duration(1500, false);
        betweenMovementSounds: IDuration = new Duration(200);
        betweenThrustSounds: IDuration = new Duration(400);
        betweenSlidesSounds: IDuration = new Duration(200);
    }
 }