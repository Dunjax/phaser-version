/// <reference path="../Being.ts" />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Beings;
        (function (Beings) {
            var Player;
            (function (Player) {
                Player.playerFrameSequences;
                var PlayerFrameSequences = (function (_super) {
                    __extends(PlayerFrameSequences, _super);
                    function PlayerFrameSequences() {
                        var _this = this;
                        _super.call(this);
                        this.imagesPath = "entities/player/";
                        this.imagesPrefix = "player";
                        var create = function (n, l) { return _this.create.call(_this, n, l); };
                        var createWith = function (n, l, o) { return _this.createWith.call(_this, n, l, o); };
                        this.move = createWith("move", 4, { isRepeating: true });
                        this.move.requiresNodToAdvance = true;
                        this.still =
                            createWith("still", 1, { images: [this.move.getFrame(3).image] });
                        this.turn = create("turn", 2);
                        this.death = create("death", 7);
                        this.hit = create("hit", 1);
                        this.carcass = createWith("carcass", 1, { isRepeating: true });
                        this.jump = create("jump", 3);
                        this.climb =
                            createWith("climb", 3, { isRepeating: true, repeatedFrames: [-1, -1, -1, 1] });
                        this.climb.requiresNodToAdvance = true;
                        this.slide = create("slide", 1);
                        this.land = create("land", 2);
                        this.land.getFrame(this.land.framesCount - 1).duration = 1000;
                        this.swing = create("swing", 4);
                        this.swing.setDurationOfAllFrames(40);
                        var lengthen = function (s) { return s.getFrame(0).duration = 500; };
                        this.fire = create("fire", 1);
                        lengthen(this.fire);
                        this.fireUp = create("fireUp", 1);
                        lengthen(this.fireUp);
                        this.fireDown = create("fireDown", 1);
                        lengthen(this.fireDown);
                        this.fireOnLadder =
                            createWith("fireOnLadder", 3, { repeatedFrames: [-1, -1, -1, 1] });
                        lengthen(this.fireOnLadder);
                        this.fireUpOnLadder =
                            createWith("fireUpOnLadder", 3, { repeatedFrames: [-1, -1, -1, 1] });
                        lengthen(this.fireUpOnLadder);
                        this.fireDownOnLadder =
                            createWith("fireDownOnLadder", 3, { repeatedFrames: [-1, -1, -1, 1] });
                        lengthen(this.fireDownOnLadder);
                        this.fireOnSlide = this.fire;
                        this.fireUpOnSlide = create("fireUpOnSlide", 1);
                        lengthen(this.fireUpOnSlide);
                        this.whirlwinded =
                            createWith("whirlwinded", 1, { images: [this.move.getFrame(0).image] });
                        this.webbed = create("webbed", 2);
                        var lastFrameIndex = this.webbed.framesCount - 1;
                        this.webbed.getFrame(lastFrameIndex).duration = 3000;
                    }
                    return PlayerFrameSequences;
                })(Beings.BeingFrameSequences);
                Player.PlayerFrameSequences = PlayerFrameSequences;
            })(Player = Beings.Player || (Beings.Player = {}));
        })(Beings = Entities.Beings || (Entities.Beings = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
