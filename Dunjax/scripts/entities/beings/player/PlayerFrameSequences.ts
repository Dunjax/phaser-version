﻿/// <reference path="../Being.ts" />

module Dunjax.Entities.Beings.Player
{
    import IFrameSequence = Animations.IFrameSequence;

    /**
     * Specifies player-specific frame-sequences.
     */
    export class PlayerFrameSequences extends BeingFrameSequences
    {
        static singleton: PlayerFrameSequences;

        /**
         * The different frame-sequences a player may display, beyond those of a being.
         */
        jump: IFrameSequence;
        climb: IFrameSequence;
        slide: IFrameSequence;
        land: IFrameSequence;
        swing: IFrameSequence;
        fire: IFrameSequence;
        fireUp: IFrameSequence;
        fireDown: IFrameSequence;
        fireOnLadder: IFrameSequence;
        fireUpOnLadder: IFrameSequence;
        fireDownOnLadder: IFrameSequence;
        fireOnSlide: IFrameSequence;
        fireUpOnSlide: IFrameSequence;
        whirlwinded: IFrameSequence;
        webbed: IFrameSequence;

        constructor()
        {
            super();
            this.imagesPath = "entities/player/"; 
            this.imagesPrefix = "player";

            var create = (n: string, l: number) => this.create.call(this, n, l);
            var createWith =
                (n: string, l: number, o: ICreateFrameSequenceArguments) =>
                    this.createWith.call(this, n, l, o);
            var goToStill = (s: IFrameSequence) => s.switchToWhenDone = this.still;
            var lengthen = (s: IFrameSequence) => {
                s.getFrame(0).duration = 500;
                goToStill(s);
            };

            this.move = createWith("move", 4, { isRepeating: true });
            this.move.requiresNodToAdvance = true;

            this.still =
                createWith("still", 1, { images: [this.move.getFrame(3).image] });
            this.turn = create("turn", 2);
            this.death = create("death", 7);

            this.hit = create("hit", 1);
            goToStill(this.hit);

            this.carcass = createWith("carcass", 1, { isRepeating: true });
            this.jump = create("jump", 3);
            this.climb =
                createWith(
                    "climb", 3, { isRepeating: true, repeatedFrames: [-1, -1, -1, 1] });
            this.climb.requiresNodToAdvance = true;
            this.slide = create("slide", 1);

            this.land = create("land", 2);
            this.land.getFrame(this.land.framesCount - 1).duration = 1000;
            goToStill(this.land);

            this.swing = create("swing", 4);
            this.swing.setDurationOfAllFrames(40);
            goToStill(this.swing);

            this.fire = createWith("fire", 1, { images: [this.move.getFrame(3).image] });
            lengthen(this.fire);
            this.fireUp = create("fireUp", 1);
            lengthen(this.fireUp);
            this.fireDown = create("fireDown", 1);
            lengthen(this.fireDown);
            this.fireOnLadder =
                createWith("fireOnLadder", 3, { repeatedFrames: [-1, -1, -1, 1] });
            lengthen(this.fireOnLadder);
            this.fireUpOnLadder =
                createWith("fireUpOnLadder", 3, { repeatedFrames: [-1, -1, -1, 1] });
            lengthen(this.fireUpOnLadder);
            this.fireDownOnLadder =
                createWith("fireDownOnLadder", 3, { repeatedFrames: [-1, -1, -1, 1] });
            lengthen(this.fireDownOnLadder);
            this.fireOnSlide = this.fire;
            this.fireUpOnSlide = create("fireUpOnSlide", 1);
            lengthen(this.fireUpOnSlide);

            this.whirlwinded =
                createWith("whirlwinded", 1, { images: [this.move.getFrame(0).image] });

            this.webbed = create("webbed", 2);
            var lastFrameIndex = this.webbed.framesCount - 1;
            this.webbed.getFrame(lastFrameIndex).duration = 3000;
        }

        /* SingletonCreator */
        static create()
        {
            PlayerFrameSequences.singleton = new PlayerFrameSequences();
        }
    }
}
