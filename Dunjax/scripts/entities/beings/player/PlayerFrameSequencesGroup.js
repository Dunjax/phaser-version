var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Beings;
        (function (Beings) {
            var Player;
            (function (Player) {
                var FramePriority = Dunjax.Animations.FramePriority;
                /**
                 * Specifies player-specific frame-sequences.
                 */
                var PlayerFrameSequencesGroup = (function (_super) {
                    __extends(PlayerFrameSequencesGroup, _super);
                    function PlayerFrameSequencesGroup() {
                        _super.call(this);
                        /* OverriddenValue */
                        this.imagesPath = "entities/player/";
                        /* OverriddenValue */
                        this.imagesPrefix = "player";
                        var args = this.args;
                        var create = function (name) { return create(args(name)); };
                        var createWith = function (name, others) { return create(args(name, others)); };
                        this.move = create("move");
                        this.still = createWith("still", { images: [this.move[0].image] });
                        this.turn = create("turn");
                        this.death = createWith("death", { priority: FramePriority.BeingDeath });
                        this.hit = createWith("hit", { priority: FramePriority.BeingHit });
                        this.carcass =
                            createWith("carcass", { isRepeating: true, priority: FramePriority.BeingDeath });
                        this.jump = create("jump");
                        this.midJump = createWith("midJump", { images: [this.jump[2].image] });
                        this.climb = createWith("climb", { repeatedFrames: [-1, -1, -1, 1] });
                        this.slide = create("slide");
                        this.land = create("land");
                        this.swing = createWith("swing", { priority: FramePriority.BeingHit });
                        this.swing.setDurationOfAllFrames(40);
                        this.fire = create("fire");
                        this.fireUp = create("fireUp");
                        this.fireDown = create("fireDown");
                        this.fireUp[0].Duration = this.fireUp.getMirror()[0].Duration =
                            this.fireDown[0].Duration = this.fireDown.getMirror()[0].Duration = 500;
                        this.fireOnLadder =
                            createWith("fireOnLadder", { repeatedFrames: [-1, -1, -1, 1] });
                        this.fireUpOnLadder =
                            createWith("fireUpOnLadder", { repeatedFrames: [-1, -1, -1, 1] });
                        this.fireDownOnLadder =
                            createWith("fireDownOnLadder", { repeatedFrames: [-1, -1, -1, 1] });
                        this.fireOnSlide = this.fire;
                        this.fireUpOnSlide = create("fireUpOnSlide");
                        this.whirlwinded = createWith("whirlwinded", { images: [this.move[0].image] });
                        this.webbed = createWith("webbed", { priority: FramePriority.BeingWebbed });
                        var lastFrameIndex = this.webbed.getNumFrames() - 1;
                        this.webbed.getFrame(lastFrameIndex).duration =
                            this.webbed.getMirror().getFrame(lastFrameIndex).duration = 3000;
                    }
                    /* Shorthand */
                    PlayerFrameSequencesGroup.prototype.args = function (name, others) {
                        if (others === void 0) { others = {}; }
                        others.name = name;
                        return others;
                    };
                    return PlayerFrameSequencesGroup;
                })(Dunjax.Entities.Beings.BeingFrameSequences);
                Player.PlayerFrameSequencesGroup = PlayerFrameSequencesGroup;
            })(Player = Beings.Player || (Beings.Player = {}));
        })(Beings = Entities.Beings || (Entities.Beings = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
//# sourceMappingURL=PlayerFrameSequencesGroup.js.map