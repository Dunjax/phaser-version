/// <reference path="../../../maps/ITileType.ts" />
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Beings;
        (function (Beings) {
            var Player;
            (function (Player) {
                var PlayerSpeeds = (function () {
                    function PlayerSpeeds() {
                    }
                    PlayerSpeeds.move = 8 * Dunjax.Maps.tileWidth;
                    PlayerSpeeds.jump = 5 * Dunjax.Maps.tileHeight;
                    PlayerSpeeds.climb = 4 * Dunjax.Maps.tileHeight;
                    return PlayerSpeeds;
                })();
                Player.PlayerSpeeds = PlayerSpeeds;
            })(Player = Beings.Player || (Beings.Player = {}));
        })(Beings = Entities.Beings || (Entities.Beings = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
