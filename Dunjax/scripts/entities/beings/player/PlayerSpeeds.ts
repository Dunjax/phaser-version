﻿/// <reference path="../../../maps/ITileType.ts" />

module Dunjax.Entities.Beings.Player
{
    /**
     * The different speeds at which the player moves.
     */
    export class PlayerSpeeds
    {
        static move = 7 * Maps.tileWidth;
        static jump = 5 * Maps.tileHeight;
        static climb = 4 * Maps.tileHeight;
        static whirlwindedMove = 2 * PlayerSpeeds.move;
    }
}