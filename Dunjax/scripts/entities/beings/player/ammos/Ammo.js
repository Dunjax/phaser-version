var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Beings;
        (function (Beings) {
            var Player;
            (function (Player) {
                var Ammos;
                (function (Ammos) {
                    var Ammo = (function () {
                        function Ammo() {
                            this.amount = 0;
                        }
                        Ammo.prototype.onFired = function (map, firingX, firingY, toSide, toVerticalSide) {
                            this.amount--;
                            this.createShots(map, firingX, firingY, toSide, toVerticalSide);
                        };
                        Ammo.prototype.add = function (amount) {
                            this.amount += amount;
                        };
                        Ammo.prototype.createShots = function (map, firingX, firingY, toSide, toVerticalSide) { };
                        return Ammo;
                    })();
                    Ammos.Ammo = Ammo;
                })(Ammos = Player.Ammos || (Player.Ammos = {}));
            })(Player = Beings.Player || (Beings.Player = {}));
        })(Beings = Entities.Beings || (Entities.Beings = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
