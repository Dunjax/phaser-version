﻿module Dunjax.Entities.Beings.Player.Ammos
{
    import IAmmo = Ammos.IAmmo;
    import IMap = Maps.IMap;

    export class Ammo implements IAmmo
    {
        type: AmmoType;

        /**
         * How many units of ammo this object currently represents.
         */
        amount: number = 0;

        /* Implementation */
        onFired(
            map: IMap, firingX: number, firingY: number,
            toSide: Side, toVerticalSide: VerticalSide)        
        {
            this.amount--;

            this.createShots(map, firingX, firingY, toSide, toVerticalSide);
        }

        /* Implementation */
        add(amount: number)
        {
            this.amount += amount;
        }

        /**
         * Has this ammo create shots at the given location, of the type associated
         * with this ammo's type.
         *
         * See this.fired() for parameter descriptions.
         */
        /* Hook */
        protected createShots(
            map: IMap, firingX: number, firingY: number, toSide: Side,
            toVerticalSide: VerticalSide) { }
       
    }
} 