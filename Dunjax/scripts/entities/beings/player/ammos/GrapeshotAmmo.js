/// <reference path="../../../shots/IShot.ts" />
/// <reference path="../../../shots/GrapeshotShot.ts" />
/// <reference path="../../../../sounds/AllSounds.ts" />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Beings;
        (function (Beings) {
            var Player;
            (function (Player) {
                var Ammos;
                (function (Ammos) {
                    var GrapeshotShot = Entities.Shots.GrapeshotShot;
                    var GrapeshotAmmo = (function (_super) {
                        __extends(GrapeshotAmmo, _super);
                        function GrapeshotAmmo() {
                            _super.apply(this, arguments);
                        }
                        GrapeshotAmmo.prototype.createShots = function (map, firingX, firingY, toSide, toVerticalSide) {
                            for (var i = 0; i < 3; i++) {
                                var dirX = 0, dirY = 0;
                                if (toVerticalSide === Entities.VerticalSide.Top) {
                                    dirY = -5;
                                    if (i === 1)
                                        dirX = -1;
                                    if (i === 2)
                                        dirX = 1;
                                }
                                else if (toVerticalSide === Entities.VerticalSide.Bottom) {
                                    dirY = 5;
                                    if (i === 1)
                                        dirX = -1;
                                    if (i === 2)
                                        dirX = 1;
                                }
                                else if (toSide === Entities.Side.Left) {
                                    dirX = -5;
                                    if (i === 1)
                                        dirY = -1;
                                    if (i === 2)
                                        dirY = 1;
                                }
                                else {
                                    dirX = 5;
                                    if (i === 1)
                                        dirY = -1;
                                    if (i === 2)
                                        dirY = 1;
                                }
                                var shot = new GrapeshotShot(firingX, firingY, dirX, dirY);
                                map.entities.addEntity(shot);
                            }
                            Dunjax.Program.sounds.grapeFire.play();
                        };
                        return GrapeshotAmmo;
                    })(Ammos.Ammo);
                    Ammos.GrapeshotAmmo = GrapeshotAmmo;
                })(Ammos = Player.Ammos || (Player.Ammos = {}));
            })(Player = Beings.Player || (Beings.Player = {}));
        })(Beings = Entities.Beings || (Entities.Beings = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
