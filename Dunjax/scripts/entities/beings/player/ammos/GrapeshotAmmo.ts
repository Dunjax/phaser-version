﻿/// <reference path="../../../shots/IShot.ts" />
/// <reference path="../../../shots/GrapeshotShot.ts" />

module Dunjax.Entities.Beings.Player.Ammos
{
    import IMap = Maps.IMap;
    import GrapeshotShot = Entities.Shots.GrapeshotShot; 
    
    /**
     * Produces a three-way kind of shot.
     */
    export class GrapeshotAmmo extends Ammo
    {
        constructor()
        {
            super();
            this.type = AmmoType.Grapeshot;
        }

        /* HookOverride */
        protected createShots(
            map: IMap, firingX: number, firingY: number, toSide: Side,
            toVerticalSide: VerticalSide)
        {
            // for each of the three shots that will be created
            for (var i = 0; i < 3; i++) {
                // detm the direction of this shot based on the given parameters
                var dirX = 0, dirY: number = 0;
                if (toVerticalSide === VerticalSide.Top) {
                    dirY = -5;
                    if (i === 1) dirX = -1;
                    if (i === 2) dirX = 1;
                }
                else if (toVerticalSide === VerticalSide.Bottom) {
                    dirY = 5;
                    if (i === 1) dirX = -1;
                    if (i === 2) dirX = 1;
                }
                else if (toSide === Side.Left) {
                    dirX = -5;
                    if (i === 1) dirY = -1;
                    if (i === 2) dirY = 1;
                }
                else {
                    dirX = 5;
                    if (i === 1) dirY = -1;
                    if (i === 2) dirY = 1;
                }

                // create the shot with the direction detm'd above
                var shot = new GrapeshotShot(firingX, firingY, dirX, dirY);

                // add the shot to map at the given location
                map.entities.addEntity(shot);
            }

            Program.sounds.grapeFire.play();
        }
    }
}