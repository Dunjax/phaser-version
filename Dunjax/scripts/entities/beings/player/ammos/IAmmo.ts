﻿module Dunjax.Entities.Beings.Player.Ammos
{
    import IMap = Maps.IMap;
    
    /**
     * Represents the player's held amount of a particular type of ammunition.
     */
    export interface IAmmo
    {
        type: AmmoType;

        /**
         * Returns how many units of ammo this object currently represents.
         */
        amount: number;
         
        /**
         * Adds the given amount to that represented by this ammo object.
         */
        add(amount: number): void;

        /**
         * Informs this ammo that it has been fired from the given location on the 
         * given map towards the given horizontal and vertical sides of this
         * ammo's possessor.  This object is then responsible for creating one or
         * more shots on the map. 
         */
        onFired(
            map: IMap, firingX: number, firingY: number,
            toSide: Side, toVerticalSide: VerticalSide): void;
    }

    export enum AmmoType { Pulse, Grapeshot }
}