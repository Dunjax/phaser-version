/// <reference path="Ammo.ts" />
/// <reference path="../../../shots/PulseShot.ts" />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Beings;
        (function (Beings) {
            var Player;
            (function (Player) {
                var Ammos;
                (function (Ammos) {
                    var PulseShot = Entities.Shots.PulseShot;
                    var PulseAmmo = (function (_super) {
                        __extends(PulseAmmo, _super);
                        function PulseAmmo() {
                            _super.apply(this, arguments);
                        }
                        PulseAmmo.prototype.createShots = function (map, firingX, firingY, toSide, toVerticalSide) {
                            var dirX = 0, dirY = 0;
                            if (toVerticalSide === Entities.VerticalSide.Top)
                                dirY = -1;
                            else if (toVerticalSide === Entities.VerticalSide.Bottom)
                                dirY = 1;
                            else if (toSide === Entities.Side.Left)
                                dirX = -1;
                            else
                                dirX = 1;
                            var shot = new PulseShot(firingX, firingY, dirX, dirY);
                            map.entities.addEntity(shot);
                            Dunjax.Program.sounds.pulseFire.play();
                        };
                        return PulseAmmo;
                    })(Ammos.Ammo);
                    Ammos.PulseAmmo = PulseAmmo;
                })(Ammos = Player.Ammos || (Player.Ammos = {}));
            })(Player = Beings.Player || (Beings.Player = {}));
        })(Beings = Entities.Beings || (Entities.Beings = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
