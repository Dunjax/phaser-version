﻿/// <reference path="Ammo.ts" />
/// <reference path="../../../shots/PulseShot.ts" />

module Dunjax.Entities.Beings.Player.Ammos
{
    import IMap = Maps.IMap;
    import PulseShot = Entities.Shots.PulseShot; 
    
    /**
     * Produces the player's normal shot.
     */
    export class PulseAmmo extends Ammo
    {
        constructor()
        {
            super();
            this.type = AmmoType.Pulse;
        }

        /* HookOverride */
        protected createShots(
            map: IMap, firingX: number, firingY: number, toSide: Side,
            toVerticalSide: VerticalSide)
        {
            // detm the direction of the shot based on the given parameters
            var dirX = 0, dirY: number = 0;
            if (toVerticalSide === VerticalSide.Top) dirY = -1;
            else if (toVerticalSide === VerticalSide.Bottom) dirY = 1;
            else if (toSide === Side.Left) dirX = -1;
            else dirX = 1;

            // create the shot with the direction detm'd above, centered 
            // on the given location
            var shot = new PulseShot(firingX, firingY, dirX, dirY);
                
            // center the shot on the given location
            map.entities.addEntity(shot);

            Program.sounds.pulseFire.play();
        }
    }
}