var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Items;
        (function (Items) {
            (function (ItemType) {
                ItemType[ItemType["PulseAmmo"] = 0] = "PulseAmmo";
                ItemType[ItemType["PulseAmmoBig"] = 1] = "PulseAmmoBig";
                ItemType[ItemType["GrapeshotAmmo"] = 2] = "GrapeshotAmmo";
                ItemType[ItemType["GrapeshotGun"] = 3] = "GrapeshotGun";
                ItemType[ItemType["ShieldPowerUp"] = 4] = "ShieldPowerUp";
                ItemType[ItemType["ShieldPowerPack"] = 5] = "ShieldPowerPack";
                ItemType[ItemType["Key"] = 6] = "Key";
            })(Items.ItemType || (Items.ItemType = {}));
            var ItemType = Items.ItemType;
            ;
            function asItem(entity) {
                return entity.itemType !== undefined ? entity : null;
            }
            Items.asItem = asItem;
        })(Items = Entities.Items || (Entities.Items = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
