﻿module Dunjax.Entities.Items
{
    /**
     * An item which is of benefit to the player.
     */
    export interface IItem extends IAnimationEntity
    {
        /**
         * This item's item-type.
         */
        itemType: ItemType;
    	
        /**
         * Informs this item that it has been consumed.
         */
        onConsumed(): void;
    }

    /**
     * The different kinds of items that are in the game. 
     */
    export enum ItemType {
        PulseAmmo, PulseAmmoBig, GrapeshotAmmo, GrapeshotGun,
        ShieldPowerUp, ShieldPowerPack, Key
    };

    /* DownCaster */
    export function asItem(entity: IMapEntity): IItem
    {
        return (<any>entity).itemType !== undefined ? <IItem>entity : null;
    }
}