/// <reference path="ItemFrameSequences.ts" />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Items;
        (function (Items) {
            var Item = (function (_super) {
                __extends(Item, _super);
                function Item(itemType, x, y) {
                    _super.call(this, Entities.AnimationEntityType.Item, Item.frameSequences.sequences[itemType], x, y);
                    this.itemType = itemType;
                }
                Item.prototype.onConsumed = function () {
                    this.removeFromPlay();
                    Dunjax.Program.sounds.itemTaken.play();
                };
                Item.prototype.shouldAlignWithGroundOrCeiling = function () {
                    return this.itemType !== Items.ItemType.Key;
                };
                return Item;
            })(Entities.AnimationEntity);
            Items.Item = Item;
        })(Items = Entities.Items || (Entities.Items = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
