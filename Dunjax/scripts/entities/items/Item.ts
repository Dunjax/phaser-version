﻿/// <reference path="ItemFrameSequences.ts" />

module Dunjax.Entities.Items
{
    /* Implementation */
    export class Item extends AnimationEntity implements IItem
    {
        /* Implementation */
        itemType: ItemType;
                
        constructor(itemType: ItemType, x: number, y: number)
        {
            super(
                AnimationEntityType.Item,
                ItemFrameSequences.singleton.sequences[itemType], x, y);
            this.itemType = itemType;
            this.isPassible = true;
        }

        /* Implementation */
        onConsumed()   
        {
            this.removeFromPlay();

            Program.sounds.itemTaken.play();
        }

        /* OverriddenValue */
        protected shouldAlignWithGroundOrCeiling(): boolean
        {
             return this.itemType !== ItemType.Key;
        }
    }
}