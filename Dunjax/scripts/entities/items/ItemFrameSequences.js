/// <reference path="IItem.ts" />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Items;
        (function (Items) {
            var ItemFrameSequences = (function (_super) {
                __extends(ItemFrameSequences, _super);
                function ItemFrameSequences() {
                    var _this = this;
                    _super.call(this);
                    this.imagesPath = "entities/items/";
                    this.imagesPrefix = "";
                    this.sequences = [];
                    var create = function (name, length) { return _this.createWith(name, length, { isRepeating: true }); };
                    this.sequences[Items.ItemType.PulseAmmo] = create("pulseAmmo", 4);
                    this.sequences[Items.ItemType.PulseAmmoBig] = create("pulseAmmoBig", 4);
                    this.sequences[Items.ItemType.GrapeshotAmmo] = create("grapeshotAmmo", 3);
                    this.sequences[Items.ItemType.GrapeshotGun] = create("grapeshotGun", 4);
                    this.sequences[Items.ItemType.ShieldPowerUp] = create("shieldPowerUp", 3);
                    this.sequences[Items.ItemType.ShieldPowerPack] = create("shieldPowerPack", 3);
                    this.sequences[Items.ItemType.Key] = create("key", 1);
                }
                return ItemFrameSequences;
            })(Entities.FrameSequences);
            Items.ItemFrameSequences = ItemFrameSequences;
        })(Items = Entities.Items || (Entities.Items = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
