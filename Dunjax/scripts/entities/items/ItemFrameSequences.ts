﻿/// <reference path="IItem.ts" />

module Dunjax.Entities.Items
{
    import FrameSequence = Animations.IFrameSequence;

    /**
     * Specifies the frame sequences for the different kinds of items.
     */
    export class ItemFrameSequences extends FrameSequences
    {
        static singleton: ItemFrameSequences;

        /**
         * The frame sequences, by item type index.
         */
        sequences: FrameSequence[] = [];

        constructor()
        {
            super();
            this.imagesPath = "entities/items/"; 

            const create =
                (name: string, length: number) =>
                    this.createWith(name, length, { isRepeating: true });
            const sequences = this.sequences;
            sequences[ItemType.PulseAmmo] = create("pulseAmmo", 4);
            sequences[ItemType.PulseAmmoBig] = create("pulseAmmoBig", 4);
            sequences[ItemType.GrapeshotAmmo] = create("grapeshotAmmo", 3);
            sequences[ItemType.GrapeshotGun] = create("grapeshotGun", 4);
            sequences[ItemType.ShieldPowerUp] = create("shieldPowerUp", 3);
            sequences[ItemType.ShieldPowerPack] = create("shieldPowerPack", 3);
            sequences[ItemType.Key] = create("key", 1);
        }

        /* SingletonCreator */
        static create()
        {
            ItemFrameSequences.singleton = new ItemFrameSequences();
        }
    }
}