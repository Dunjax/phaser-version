﻿/// <reference path="Shot.ts" />

module Dunjax.Entities.Shots
{
    /**
     * Is a stationary spot of fire, started by a fireball-hit.
     */
    export class FireSpot extends Shot
    {
        constructor(x: number, y: number)  
        {
            super(ShotType.FireSpot, x, y, 0, 0);
            this.existenceDuration.length = 1000;
            this.isAMover = false;
            this.impactDamage = 10;
            this.shotFrameSequences = FireSpotFrameSequences.singleton;
            this.targetBeingType = TargetBeingType.Player;
        }
    }

    /**
     * Specifes the frame sequences used for this kind of shot.
     */
    export class FireSpotFrameSequences extends ShotFrameSequences
    {
        static singleton: FireSpotFrameSequences;

        constructor()
        {
            super();
            const create =
                (name: string, length: number) =>
                    this.createWith(name, length, { isRepeating: true });
            this.imagesPrefix = "fireSpot";
            this.stationary = create("", 11);
            this.stationary.setDurationOfAllFrames(60);
        }

        /* SingletonCreator */
        static create()
        {
            FireSpotFrameSequences.singleton = new FireSpotFrameSequences();
        }
    }
}