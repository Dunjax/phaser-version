﻿/// <reference path="Shot.ts" />

module Dunjax.Entities.Shots
{
    import Duration = Time.Duration; 
    
    /**
     * When a fireball hits the ground, it is replaced by a shot of this kind,
     * which is invisible.  It creates fire-spots to either side of itself over 
     * a certain distance, and over a certain amount of time.
     */
    export class FireballHit extends Shot
    {
	    /**
	     * How many pairs of fire-spots this fireball-hit must set before
	     * going away.
	     */
	    protected static numFireSpotPairsToSet = 5;
    	
	    /**
	     * How many pairs of fire-spots this fireball-hit has set so far.
	     */
	    protected numFireSpotPairsSet = 0;
    	
        /**
         * How long (in ms) this shot should wait before setting the next pair
         * of fire-spots.
         */
        protected betweenFireSpotPairsDuration = new Duration(100, true);

        constructor(x: number, y: number)  
        {
            super(ShotType.FireballHit, x, y, 0, 0);
            this.isAMover = false;
            this.isImpacter = false;
            this.betweenFireSpotPairsDuration.start();

            this.shotFrameSequences = new ShotFrameSequences();
            this.shotFrameSequences.stationary = Animations.invisibleFrameSequence;
        }

        /* HookOverride */
        protected actBeforeMove()
	    {
            // if it hasn't been long enough since this shot's last act
		    // (equating actions with moves for this purpose), don't act this time
            if (!this.betweenFireSpotPairsDuration.isDone) return;
            
		    // start fire-spots at an equal and increasing distance on the 
            // left and right sides of this fireball-hit
            const spotWidth = 32, spotHeight = 32;
	        const y = this.map.getYJustAboveGround(this.x, this.y) - spotHeight / 2;
	        const dx = (this.numFireSpotPairsSet + 1) * spotWidth;
	        let spot = new FireSpot(this.x - dx, y);
	        this.map.entities.addEntity(spot);
            spot = new FireSpot(this.x + dx, y);
            this.map.entities.addEntity(spot);

		    // if this fireball-hit has spawned all of its firespot pairs, remove it
            if (++this.numFireSpotPairsSet === FireballHit.numFireSpotPairsToSet)
                this.removeFromPlay();
	    }
    }
}