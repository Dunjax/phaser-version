﻿/// <reference path="Shot.ts" />

module Dunjax.Entities.Shots
{
    import IBeing = Entities.Beings.IBeing; 
    
    /**
     * Is fired by the master.  Hits the ground, and then causes a fireball-hit
     * at that location. 
     */
    export class FireballShot extends Shot
    {
        constructor(x: number, y: number, directionX: number, directionY: number)  
        {
            super(ShotType.FireballShot, x, y, directionX, directionY);
            this.speed = 333;
            this.impactDamage = 50;
            this.shotFrameSequences = FireballShotFrameSequences.singleton;
            this.targetBeingType = TargetBeingType.Player;
            this.impactSound = Program.sounds.fireballImpact;
        }

        /* Overrride */
        protected preRemovedFromPlayDueToImpact(beingHit: IBeing)
        {
            // if this shot hit a being, there is nothing more to do
            if (beingHit != null) return;

            // start a fireball-hit at the impact location
            const shot = new FireballHit(this.x, this.y);
            this.map.entities.addEntity(shot);
        }
    }

    /**
     * Specifes the frame sequences used for this kind of shot.
     */
    export class FireballShotFrameSequences extends ShotFrameSequences
    {
        static singleton: FireballShotFrameSequences;

        constructor()
        {
            super();
            this.imagesPrefix = "fireballShot";

            const create =
                (name: string, length: number) =>
                    this.createWith(name, length, { isRepeating: true });
            this.downAngle = create("downAngle", 1);
        }

        /* SingletonCreator */
        static create()
        {
            FireballShotFrameSequences.singleton = new FireballShotFrameSequences();
        }
    }
} 