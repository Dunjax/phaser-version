/// <reference path="Shot.ts" />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Shots;
        (function (Shots) {
            var TargetBeingType = Shots.TargetBeingType;
            var GrapeshotShot = (function (_super) {
                __extends(GrapeshotShot, _super);
                function GrapeshotShot(x, y, startDirX, startDirY) {
                    _super.call(this, Shots.ShotType.GrapeshotShot, x, y, startDirX, startDirY);
                    this.speed = 500;
                    this.impactDamage = 1;
                    this.shotFrameSequences = GrapeshotShot.frameSequences;
                    this.targetBeingType = TargetBeingType.Monster;
                }
                return GrapeshotShot;
            })(Shots.Shot);
            Shots.GrapeshotShot = GrapeshotShot;
            var GrapeshotShotFrameSequences = (function (_super) {
                __extends(GrapeshotShotFrameSequences, _super);
                function GrapeshotShotFrameSequences() {
                    var _this = this;
                    _super.call(this);
                    var create = function (name, length) { return _this.createWith(name, length, { isRepeating: true }); };
                    this.imagesPrefix = "grapeShot";
                    this.level = create("level", 1),
                        this.up = create("up", 1);
                    this.down = create("down", 1);
                    this.upAngle = create("upAngle", 1);
                    this.downAngle = create("downAngle", 1);
                }
                return GrapeshotShotFrameSequences;
            })(Shots.ShotFrameSequences);
            Shots.GrapeshotShotFrameSequences = GrapeshotShotFrameSequences;
        })(Shots = Entities.Shots || (Entities.Shots = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
