﻿/// <reference path="Shot.ts" />

module Dunjax.Entities.Shots
{
    import TargetBeingType = Shots.TargetBeingType; 
    
    /**
     * One of the three shots fired at a time by the player's grapeshot gun.
     */
    export class GrapeshotShot extends Shot
    {
        constructor(x: number, y: number, startDirX: number, startDirY: number)  
        {
            super(ShotType.GrapeshotShot, x, y, startDirX, startDirY);
            this.speed = 500;
            this.impactDamage = 1;
            this.shotFrameSequences = GrapeshotShotFrameSequences.singleton;
            this.targetBeingType = TargetBeingType.Monster;
        }
    }

    /**
     * Specifes the frame sequences used for this kind of shot.
     */
    export class GrapeshotShotFrameSequences extends ShotFrameSequences
    {
        static singleton: GrapeshotShotFrameSequences;

        constructor()
        {
            super();
            const create =
                (name: string, length: number) =>
                    this.createWith(name, length, { isRepeating: true });
            this.imagesPrefix = "grapeShot";
            this.level = create("level", 1),
            this.up = create("up", 1);
            this.down = create("down", 1);
            this.upAngle = create("upAngle", 1);
            this.downAngle = create("downAngle", 1);
        }

        /* SingletonCreator */
        static create()
        {
            GrapeshotShotFrameSequences.singleton = new GrapeshotShotFrameSequences();
        }
    }
}