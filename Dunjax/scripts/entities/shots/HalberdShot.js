/// <reference path="Shot.ts" />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Shots;
        (function (Shots) {
            var pixelsPerTenFeet = Dunjax.Maps.pixelsPerTenFeet;
            var HalberdShot = (function (_super) {
                __extends(HalberdShot, _super);
                function HalberdShot(x, y, startDirX, startDirY) {
                    _super.call(this, Shots.ShotType.HalberdShot, x, y, startDirX, startDirY);
                    this.speed = 500;
                    this.impactDamage = 3;
                    this.shotFrameSequences = HalberdShot.frameSequences;
                    this.targetBeingType = Shots.TargetBeingType.Monster;
                }
                HalberdShot.prototype.getRange = function () { return pixelsPerTenFeet; };
                return HalberdShot;
            })(Shots.Shot);
            Shots.HalberdShot = HalberdShot;
            var HalberdShotFrameSequences = (function (_super) {
                __extends(HalberdShotFrameSequences, _super);
                function HalberdShotFrameSequences() {
                    _super.call(this);
                    this.imagesPrefix = "halberdShot";
                    this.level = this.create("level", 1);
                }
                return HalberdShotFrameSequences;
            })(Shots.ShotFrameSequences);
            Shots.HalberdShotFrameSequences = HalberdShotFrameSequences;
        })(Shots = Entities.Shots || (Entities.Shots = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
