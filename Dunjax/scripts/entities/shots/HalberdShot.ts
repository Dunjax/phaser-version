﻿/// <reference path="Shot.ts" />

module Dunjax.Entities.Shots
{
    import pixelsPerTenFeet = Maps.pixelsPerTenFeet;

    /**
     * The shot issued by the player's melee weapon.  It has a very short range.
     */
    export class HalberdShot extends Shot
    {
        constructor(x: number, y: number, startDirX: number, startDirY: number)  
        {
            super(ShotType.HalberdShot, x, y, startDirX, startDirY);
            this.speed = 500;
            this.impactDamage = 3;
            this.shotFrameSequences = HalberdShotFrameSequences.singleton;
            this.targetBeingType = TargetBeingType.Monster;
        }

        /* OverriddenValue */
        protected getRange(): number { return pixelsPerTenFeet; } 
    }

    /**
     * Specifes the frame sequences used for this kind of shot.
     */
    export class HalberdShotFrameSequences extends ShotFrameSequences
    {
        static singleton: HalberdShotFrameSequences;

        constructor()
        {
            super();
            this.imagesPrefix = "halberdShot";
            this.level = this.create("level", 1);
        }

        /* SingletonCreator */
        static create()
        {
            HalberdShotFrameSequences.singleton = new HalberdShotFrameSequences();
        }
    }
}