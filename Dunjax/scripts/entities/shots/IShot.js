var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Shots;
        (function (Shots) {
            (function (ShotType) {
                ShotType[ShotType["FireballHit"] = 0] = "FireballHit";
                ShotType[ShotType["FireballShot"] = 1] = "FireballShot";
                ShotType[ShotType["FireSpot"] = 2] = "FireSpot";
                ShotType[ShotType["GrapeshotShot"] = 3] = "GrapeshotShot";
                ShotType[ShotType["HalberdShot"] = 4] = "HalberdShot";
                ShotType[ShotType["PulseShot"] = 5] = "PulseShot";
                ShotType[ShotType["SlimeballHit"] = 6] = "SlimeballHit";
                ShotType[ShotType["SlimeShot"] = 7] = "SlimeShot";
                ShotType[ShotType["SlimeSpot"] = 8] = "SlimeSpot";
                ShotType[ShotType["WebShot"] = 9] = "WebShot";
            })(Shots.ShotType || (Shots.ShotType = {}));
            var ShotType = Shots.ShotType;
            ;
            function asShot(entity) {
                return entity.shotType ? entity : null;
            }
            Shots.asShot = asShot;
        })(Shots = Entities.Shots || (Entities.Shots = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
