﻿module Dunjax.Entities.Shots
{
    /**
     * A shot of a weapon or monster attack.
     */
    export interface IShot extends IMapEntity
    {
        /**
         * This shot's shot-type, which is one of those listed below.
         */
        shotType: ShotType;
        
        /**
         * This shot's current unit vector direction. 
         */
        directionX: number;
        directionY: number;
        
        /**
          * The amount of damage this shot causes when it impacts something.
          */
        impactDamage: number;
    }

    /**
     * The different kinds of shots that are in the game. 
     */
    export enum ShotType {
        FireballHit, FireballShot, FireSpot, GrapeshotShot,
        HalberdShot, PulseShot, SlimeShotHit, SlimeShot, SlimeSpot, WebShot
    };

    /* DownCaster */
    export function asShot(entity: IMapEntity): IShot
    {
        return (<any>entity).shotType ? <any>entity : null;
    }
}