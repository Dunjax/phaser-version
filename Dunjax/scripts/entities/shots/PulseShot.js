/// <reference path="Shot.ts" />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Shots;
        (function (Shots) {
            var TargetBeingType = Shots.TargetBeingType;
            var PulseShot = (function (_super) {
                __extends(PulseShot, _super);
                function PulseShot(x, y, startDirX, startDirY) {
                    _super.call(this, Shots.ShotType.PulseShot, x, y, startDirX, startDirY);
                    this.speed = 600;
                    this.impactDamage = 1;
                    this.shotFrameSequences = PulseShot.frameSequences;
                    this.targetBeingType = TargetBeingType.Monster;
                }
                return PulseShot;
            })(Shots.Shot);
            Shots.PulseShot = PulseShot;
            var PulseShotFrameSequences = (function (_super) {
                __extends(PulseShotFrameSequences, _super);
                function PulseShotFrameSequences() {
                    var _this = this;
                    _super.call(this);
                    var create = function (name, length) { return _this.createWith(name, length, { isRepeating: true }); };
                    this.imagesPrefix = "pulseShot";
                    this.level = create("level", 1),
                        this.up = create("up", 1);
                    this.down = create("down", 1);
                }
                return PulseShotFrameSequences;
            })(Shots.ShotFrameSequences);
            Shots.PulseShotFrameSequences = PulseShotFrameSequences;
        })(Shots = Entities.Shots || (Entities.Shots = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
