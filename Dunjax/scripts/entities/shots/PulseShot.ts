﻿/// <reference path="Shot.ts" />

module Dunjax.Entities.Shots
{
    import TargetBeingType = Shots.TargetBeingType; 
    
    /**
     * The shot fired by the player's normal gun.
     */
    export class PulseShot extends Shot
    {
        constructor(x: number, y: number, directionX: number, directionY: number)  
        {
            super(ShotType.PulseShot, x, y, directionX, directionY);
            this.speed = 600;
            this.impactDamage = 1;
            this.shotFrameSequences = PulseShotFrameSequences.singleton;
            this.targetBeingType = TargetBeingType.Monster;
        }
    }

    /**
     * Specifes the frame sequences used for this kind of shot.
     */
    export class PulseShotFrameSequences extends ShotFrameSequences
    {
        static singleton: PulseShotFrameSequences;

        constructor()
        {
            super();
            const create =
                (name: string, length: number) =>
                    this.createWith(name, length, { isRepeating: true });
            this.imagesPrefix = "pulseShot";
            this.level = create("level", 1),
            this.up = create("up", 1);
            this.down = create("down", 1);
        }

        /* SingletonCreator */
        static create()
        {
            PulseShotFrameSequences.singleton = new PulseShotFrameSequences();
        }
    }
}