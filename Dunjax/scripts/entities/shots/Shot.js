/// <reference path="../MapEntity.ts" />
/// <reference path="ShotFrameSequences.ts" />
/// <reference path="../../time/Duration.ts" />
/// <reference path="../../sounds/AllSounds.ts" />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Shots;
        (function (Shots) {
            var Duration = Dunjax.Time.Duration;
            var pixelsPerTenFeet = Dunjax.Maps.pixelsPerTenFeet;
            var Shot = (function (_super) {
                __extends(Shot, _super);
                function Shot(shotType, x, y, directionX, directionY) {
                    _super.call(this, x, y);
                    this.isAMover = true;
                    this.impactSound = Dunjax.Program.sounds.shotImpact;
                    this.targetBeingType = TargetBeingType.Player;
                    this.shotType = shotType;
                    if (directionX !== 0 || directionY !== 0) {
                        var u = Dunjax.Geometry.getUnitVector(0, 0, directionX, directionY);
                        _a = [u.x, u.y], this.directionX = _a[0], this.directionY = _a[1];
                        var physics = Dunjax.Program.phaserGame.physics;
                        physics.ninja.enableAABB(this.sprite);
                        this.sprite.body.setZeroVelocity();
                        if (directionY === 0)
                            this.angle = directionX > 0 ? 0 : 180;
                        else
                            this.angle =
                                Phaser.Math.radToDeg(Math.atan2(this.directionY, this.directionX));
                    }
                    this.distanceUntilDone = this.getRange();
                    this.existenceDuration = new Duration(Number.MAX_VALUE, false);
                    this.existenceDuration.start();
                    var _a;
                }
                Shot.prototype.getIntervalUntilNextAction = function () {
                    return this.betweenMovesDurationLength;
                };
                Shot.prototype.actAfterAnimationChecked = function () {
                    if (this.existenceDuration.isDone) {
                        this.removeFromPlay();
                        return;
                    }
                    if (this.frameSequence == null)
                        this.frameSequence =
                            this.getFrameSequenceForDirection(this.directionX, this.directionY);
                    this.actBeforeMove();
                    if (this.isAMover)
                        this.moveInCurrentDirection();
                };
                Shot.prototype.actBeforeMove = function () { };
                Shot.prototype.getFrameSequenceForState = function () { return this.frameSequence; };
                Shot.prototype.getFrameSequenceForDirection = function (dirX, dirY) {
                    var slope = (dirX !== 0) ? dirY / dirX :
                        1000 * (dirY > 0 ? 1 : -1);
                    var sequences = this.shotFrameSequences;
                    this.sprite.scale.x = dirX < 0 ? -1 : 1;
                    if (dirX === 0 && dirY === 0) {
                        return sequences.stationary;
                    }
                    if (slope >= -0.5 && slope <= 0.5) {
                        return sequences.level;
                    }
                    if (slope >= 2.0 || slope <= -2.0) {
                        return dirY < 0 ? sequences.up : sequences.down;
                    }
                    if ((slope > 0.5 && slope < 2.0)
                        || (slope < -0.5 && slope > -2.0)) {
                        return dirY < 0 ? sequences.upAngle : sequences.downAngle;
                    }
                    throw new Error("Direction corresponds to no condition");
                };
                Shot.prototype.getTargetBeingStruck = function () {
                    var entities = this.map.entities;
                    var being = (this.targetBeingType === TargetBeingType.Player
                        ? entities.getPlayerAt(this.x, this.y)
                        : entities.getMonsterAt(this.x, this.y));
                    if (being == null)
                        return null;
                    return being.isStrikableAt(this.x, this.y) ? being : null;
                };
                Shot.prototype.moveInCurrentDirection = function () {
                    var sprite = this.sprite;
                    var body = sprite.body;
                    body.moveTo(this.speed, this.angle);
                    var targetBeingStruck = this.checkForTargetBeingStruck();
                    if (!targetBeingStruck) {
                        if (!this.map.isPassible(this.x, this.y))
                            this.impacted(null);
                        else {
                            var pos = body.shape.pos, oldPos = body.shape.oldpos;
                            this.distanceUntilDone -=
                                Phaser.Math.distance(0, 0, pos.x - oldPos.x, pos.y - oldPos.y);
                            if (this.distanceUntilDone <= 0)
                                this.removeFromPlay();
                        }
                    }
                };
                Shot.prototype.checkForTargetBeingStruck = function () {
                    var being = this.getTargetBeingStruck();
                    if (being != null) {
                        this.targetBeingStruck(being);
                        return true;
                    }
                    return false;
                };
                Shot.prototype.targetBeingStruck = function (being) {
                    if (this.impactDamage > 0)
                        being.onStruck(this.x, this.y, this.impactDamage);
                    this.impacted(being);
                };
                Shot.prototype.impacted = function (beingHit) {
                    this.playIfExists(this.impactSound);
                    this.preRemovedFromPlayDueToImpact(beingHit);
                    this.removeFromPlay();
                };
                Shot.prototype.preRemovedFromPlayDueToImpact = function (beingHit) { };
                Shot.prototype.getRange = function () { return 25 * pixelsPerTenFeet; };
                return Shot;
            })(Entities.MapEntity);
            Shots.Shot = Shot;
            (function (TargetBeingType) {
                TargetBeingType[TargetBeingType["Player"] = 0] = "Player";
                TargetBeingType[TargetBeingType["Monster"] = 1] = "Monster";
            })(Shots.TargetBeingType || (Shots.TargetBeingType = {}));
            var TargetBeingType = Shots.TargetBeingType;
            ;
        })(Shots = Entities.Shots || (Entities.Shots = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
