﻿/// <reference path="../MapEntity.ts" />
/// <reference path="ShotFrameSequences.ts" />
/// <reference path="../../time/Duration.ts" />

module Dunjax.Entities.Shots
{
    import IDuration = Time.IDuration;
    import Duration = Time.Duration;
    import IFrameSequence = Animations.IFrameSequence;
    import IBeing = Beings.IBeing;
    import pixelsPerTenFeet = Maps.pixelsPerTenFeet;

    /* Implementation */
    export class Shot extends MapEntity implements IShot
    {
        /* Implementation */
        shotType: ShotType;

        /* Implementation */
        directionX = 0;
        directionY = 0;

	    /**
         * The grouping of frame sequences this shot will use to depict its travel in
         * various directions.
         */
        protected shotFrameSequences: ShotFrameSequences;
        
        /**
         * Whether this shot moves, vs. being meant to stay in one place.
         */
        protected isAMover = true;

        /**
         * The speed at which this shot travels, in pixels/second.
         */
        protected speed: number;
    	
        /**
         * How many moves this shot has left to make before it is finished
         */
        protected distanceUntilDone: number;

        /**
         * Keeps track of how long time-wise this shot should exist.
         */
        protected existenceDuration: IDuration;

        /**
         * The frame sequence used to depict this shot.
         */
        protected frameSequence: IFrameSequence;

        /* Implementation */
        impactDamage: number;

        /**
         * The angle (in radians) of the direction this shot is travelling.
         */
        protected angle: number;

        /**
         * Whether this shot will impact the thing it hits (vs. perform some other action).
         */
        protected isImpacter = true; 

        /**
         * Returns whether this kind of shot is meant to hit the player, or
         * the monsters.
         */
        protected targetBeingType = TargetBeingType.Player;
    	
        constructor(shotType: ShotType, x: number, y: number, directionX: number, directionY: number)
        {
            super(x, y);
            this.shotType = shotType;
            this.isPassible = true;

            // if directional data was given
            if (directionX !== 0 || directionY !== 0) {
                // convert that data to a unit vector
                const u = Geometry.getUnitVector(0, 0, directionX, directionY);
                [this.directionX, this.directionY] = [u.x, u.y];

                // also store the unit vector as an angle
                if (directionY === 0)
                    this.angle = directionX > 0 ? 0 : Math.PI;
                else this.angle = Math.atan2(this.directionY, this.directionX);
            }

            this.distanceUntilDone = this.getRange();

            this.existenceDuration = new Duration(Number.MAX_VALUE, false);
            this.existenceDuration.start();
        }

        /**
         * Returns the sound this shot makes when it hits something impassible.
         */
        /* OverridableValue */
        protected impactSound = Program.sounds.shotImpact;
    	
        /** 
        * Overridden to specify a shot's usual action.
        */
        /* HookOverride */
        protected actAfterAnimationChecked()
        {
            // if not already done, determine this shot's frame sequence (we have to wait until
            // now, since doing so in the constructor would require a virtual method per shot type
            // to return the sequences to choose from)
            if (!this.frameSequence)
                this.frameSequence =
                    this.getFrameSequenceForDirection(this.directionX, this.directionY);

            this.actBeforeMove();

            // if this shot is a mover, move it
            let distanceMoved = 0;
            if (this.isAMover) distanceMoved = this.moveInCurrentDirection();

            this.checkForFinish(distanceMoved);
        }
        
        /* Hook */
        protected actBeforeMove() {}
        
        /* OverriddenValue */
        protected getFrameSequenceForState(): IFrameSequence { return this.frameSequence; }

        /**
         * Returns this shot's frame-sequence which corresponds to the given 
         * direction.
         */
        protected getFrameSequenceForDirection(dirX: number, dirY: number): IFrameSequence
        {
            // detm the slope of the given direction, which we'll use to categorize
            // that direction, below
            const slope = (dirX !== 0) ? dirY / dirX : 
                1000 * (dirY > 0 ? 1 : -1); // if the direction is stationary
            const sequences = this.shotFrameSequences;
            this.sprite.scale.x = dirX < 0 ? -1 : 1;
            if (dirX === 0 && dirY === 0) {
                // return the stationary sequence
                return sequences.stationary;
            }
            
            // else, if the direction is leftwards or rightwards
            if (slope >= -0.5 && slope <= 0.5) {
                // return a level sequence
                return sequences.level;
            }
            
            // else, if the direction is upwards or downwards
            if (slope >= 2.0 || slope <= -2.0) {
                // return an up or down sequence 
                return dirY < 0 ? sequences.up : sequences.down;
            }
            
            // else, if the direction towards one of the four diagonals
            if ((slope > 0.5 && slope < 2.0) 
                || (slope < -0.5 && slope > -2.0)) {
                // return the appropriate diagonal sequence
                return dirY < 0 ? sequences.upAngle : sequences.downAngle;
            }
            
            // this should never be reached
            throw new Error("Direction corresponds to no condition");
        }

        /**
         * Returns the target being (if any) occupying this shot's center with a 
         * strikable location.
         */
        protected getTargetBeingStruck(): IBeing
        {
            // if there's no target being at this shot's center, no being was struck
            const entities = this.map.entities;
            const player = entities.player;
            const being = <IBeing>
                (this.targetBeingType === TargetBeingType.Player
                    ? (Geometry.rectContains(player, this.x, this.y) ? player : null)
                    : entities.getMonsterAt(this.x, this.y));
            if (being == null) return null;
            
            // return whether the target being at this shot's center is strikable
            // at the location of this shot's center
            return being.isStrikableAt(this.x, this.y) ? being : null;
        }

        /**
         * Moves this shot some increment along its current direction.
         */
        protected moveInCurrentDirection(): number
        {
            // move this shot in its current direction
            const magnitude = this.speed * Program.game.time.physicsElapsed;
            this.x += magnitude * Math.cos(this.angle);
            this.y += magnitude * Math.sin(this.angle);
            return magnitude;
        }

        /**
         * Checks whether this shot's travel should be finished, after having traveled
         * the given distance this turn.
         */
        protected checkForFinish(distanceMovedThisTurn: number)
        {
            // if this shot is an impacter, and it struck its target being, it has finished
            if (this.isImpacter && this.checkForTargetBeingStruck()) return;

            // if this shot is an impacter, and has run into something impassible 
            if (this.isImpacter && !this.map.isPassible(this.x, this.y)) {
                // handle the impact, which should finish this shot
                this.impacted(null);
                return;
            }

            // if this shot has moved its full distance
            this.distanceUntilDone -= distanceMovedThisTurn;
            if (this.distanceUntilDone <= 0) {
                // remove it
                this.removeFromPlay();
                return;
            }

            // if this shot has lasted its full duration
            if (this.existenceDuration.isDone) {
                // remove it
                this.removeFromPlay();
                return;
            }
        }
        
        /**
         * Checks (and returns) whether this shot has struck a target being.
         */
        protected checkForTargetBeingStruck(): boolean
        {
            const being = this.getTargetBeingStruck();
            if (being != null) {
                this.targetBeingStruck(being);
                return true;
            }

            return false;
        }
    	
        /**
         * Informs this shot that it has struck the given target being.
         */
        protected targetBeingStruck(being: IBeing)
        {
            if (this.impactDamage > 0) being.onStruck(this.impactDamage, this.x, this.y);                
            
            this.impacted(being);
        }
            
        /**
         * Informs this shot that it has made impact with something.
         * 
         * @param beingHit	If the impact was with a being, the being impacted.
         */
        protected impacted(beingHit: IBeing)
        {
            this.playIfExists(this.impactSound);
    		
            this.preRemovedFromPlayDueToImpact(beingHit);
    	    
            this.removeFromPlay();
        }

        /* Hook */
        protected preRemovedFromPlayDueToImpact(beingHit: IBeing) {}

        /* OverridableValue */
        protected getRange(): number { return 25 * pixelsPerTenFeet; } 
    }

    /**
     * The kind of being (player vs. monster) that a kind of shot is 
     * intended to hit.
     */
    export enum TargetBeingType { Player, Monster };
}