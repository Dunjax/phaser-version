/// <reference path="../FrameSequences.ts" />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Dunjax;
(function (Dunjax) {
    var Entities;
    (function (Entities) {
        var Shots;
        (function (Shots) {
            var ShotFrameSequences = (function (_super) {
                __extends(ShotFrameSequences, _super);
                function ShotFrameSequences() {
                    _super.call(this);
                    this.imagesPath = "entities/shots/";
                }
                return ShotFrameSequences;
            })(Entities.FrameSequences);
            Shots.ShotFrameSequences = ShotFrameSequences;
        })(Shots = Entities.Shots || (Entities.Shots = {}));
    })(Entities = Dunjax.Entities || (Dunjax.Entities = {}));
})(Dunjax || (Dunjax = {}));
