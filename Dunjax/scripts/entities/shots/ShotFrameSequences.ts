﻿/// <reference path="../FrameSequences.ts" />

module Dunjax.Entities.Shots
{
    import IFrameSequence = Animations.IFrameSequence; 
    
    /**
     * A grouping of the frame-sequences used to depict the various directions
     * a shot may travel.  Subtypes should subclass this to specify their sequences.
     */
    export class ShotFrameSequences extends FrameSequences
    {
        /**
         * The sequences for the directions a shot may travel.  Not all of these are
         * applicable to all shot types.
         */
        level: IFrameSequence;
        up: IFrameSequence;
        down: IFrameSequence;
        upAngle: IFrameSequence;
        downAngle: IFrameSequence;
        stationary: IFrameSequence;

        constructor()
        {
            super();
            this.imagesPath = "entities/shots/";
        }
    }
}
 