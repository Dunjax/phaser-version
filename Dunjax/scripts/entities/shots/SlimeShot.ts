﻿/// <reference path="Shot.ts" />

module Dunjax.Entities.Shots
{
    import IBeing = Entities.Beings.IBeing; 
    
    /**
     * Is fired by the master.  Hits the ceiling, and then causes a slimel-hit
     * at that location. 
     */
    export class SlimeShot extends Shot
    {
        constructor(x: number, y: number, directionX: number, directionY: number)  
        {
            super(ShotType.SlimeShot, x, y, directionX, directionY);
            this.speed = 250;
            this.impactDamage = 50;
            this.shotFrameSequences = SlimeShotFrameSequences.singleton;
            this.targetBeingType = TargetBeingType.Player;
            this.impactSound = Program.sounds.slimeShotImpact;
        }

        /* Overrride */
        protected preRemovedFromPlayDueToImpact(beingHit: IBeing)
        {
            // if this shot hit a being, there is nothing more to do
            if (beingHit != null) return;

            // start a slime-shot-hit at the impact location
            const shot = new SlimeShotHit(this.x, this.y);
            this.map.entities.addEntity(shot);
        }
    }

    /**
     * Specifes the frame sequences used for this kind of shot.
     */
    export class SlimeShotFrameSequences extends ShotFrameSequences
    {
        static singleton: SlimeShotFrameSequences;

        constructor()
        {
            super();
            const create =
                (name: string, length: number) =>
                    this.createWith(name, length, { isRepeating: true });
            this.imagesPrefix = "slimeShot";
            this.upAngle = create("upAngle", 1);
        }

        /* SingletonCreator */
        static create()
        {
            SlimeShotFrameSequences.singleton = new SlimeShotFrameSequences();
        }
    }
} 