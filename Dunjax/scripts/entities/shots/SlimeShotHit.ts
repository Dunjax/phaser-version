﻿/// <reference path="Shot.ts" />

module Dunjax.Entities.Shots
{
    import Duration = Time.Duration;
    import Slime = Entities.Beings.Monsters.Slime;
    
    /**
     * When a slimeball hits the ceiling, it is replaced by a shot of this kind,
     * which is invisible.  It creates slime-spots to either side of itself over 
     * a certain distance, and over a certain amount of time.  Within those
     * slime-spots it also creates a slime monster.
     */
    export class SlimeShotHit extends Shot
    {
	    /**
	     * How many pairs of slime-spots this slimeball-hit must set before
	     * going away.
	     */
        protected static numSlimeSpotPairsToSet = 5;
    	
	    /**
	     * How many pairs of slime-spots this slimeball-hit has set so far.
	     */
	    protected numSlimeSpotPairsSet = 0;

        /**
         * How long (in ms) this shot should wait before setting the next pair
         * of slime-spots.
         */
        protected betweenSlimeSpotPairsDuration = new Duration(140, true);

        constructor(x: number, y: number)  
        {
            super(ShotType.SlimeShotHit, x, y, 0, 0);
            this.isAMover = false;
            this.isImpacter = false;
            this.betweenSlimeSpotPairsDuration.start();

            this.shotFrameSequences = new ShotFrameSequences();
            this.shotFrameSequences.stationary = Animations.invisibleFrameSequence;
        }

        /* HookOverride */
        protected actBeforeMove()
        {
            // if it hasn't been long enough since this shot's last act
            // (equating actions with moves for this purpose), don't act this time
            if (!this.betweenSlimeSpotPairsDuration.isDone) return;

            // start slime-spots at an equal and increasing distance on the 
            // left and right sides of this slime-shot-hit
            const spotWidth = 32;
            const y = this.map.getYJustBelowCeiling(this.x, this.y);
            const dx = (this.numSlimeSpotPairsSet + 1) * spotWidth;
            let spot = new SlimeSpot(this.x - dx, y);
            this.map.entities.addEntity(spot);
            spot = new SlimeSpot(this.x + dx, y);
            this.map.entities.addEntity(spot);

            // if this slimeball-hit has spawned all of its slime-spot pairs,
            // replace it with a newly created slime monster
            if (++this.numSlimeSpotPairsSet === SlimeShotHit.numSlimeSpotPairsToSet) {
                this.createSlime(spotWidth);
                this.removeFromPlay();
            }
        }
        
        /**
         * Replaces this slimeball-hit on the map with a newly created slime
         * monster, which is placed within this slimeball-hit's generated 
         * slime-spots. 
         */
        protected createSlime(spotWidth: number)
        {
            // create a new slime monster somewhere within this slimeball-hit's
            // slime-spots
            const x = this.left
                + Dunjax.randomInt(0, SlimeShotHit.numSlimeSpotPairsToSet * spotWidth)
                * (Dunjax.randomInt(0, 1) ? -1 : 1);
            const y = this.map.getYJustBelowCeiling(this.x, this.y) + this.height / 2 - 1;
            const monster = new Slime(x, y);
            this.map.entities.addEntity(monster);
        }
    }
}