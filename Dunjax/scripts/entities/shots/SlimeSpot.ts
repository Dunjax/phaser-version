﻿/// <reference path="Shot.ts" />

module Dunjax.Entities.Shots
{
    /**
     * Is a stationary spot of slime, started by a slime-shot-hit.
     */
    export class SlimeSpot extends Shot
    {
        constructor(x: number, y: number)  
        {
            super(ShotType.SlimeSpot, x, y, 0, 0);
            this.existenceDuration.length = 1000;
            this.isAMover = false;
            this.impactDamage = 5;
            this.shotFrameSequences = SlimeSpotFrameSequences.singleton;
            this.targetBeingType = TargetBeingType.Player;
        }
    }

    /**
     * Specifes the frame sequences used for this kind of shot.
     */
    export class SlimeSpotFrameSequences extends ShotFrameSequences
    {
        static singleton: SlimeSpotFrameSequences;

        constructor()
        {
            super();
            const create =
                (name: string, length: number) =>
                    this.createWith(name, length, { isRepeating: true });
            this.imagesPrefix = "slimeSpot";
            this.stationary = create("", 2);
            this.stationary.setDurationOfAllFrames(60);
        }

        /* SingletonCreator */
        static create()
        {
            SlimeSpotFrameSequences.singleton = new SlimeSpotFrameSequences();
        }
    }
}