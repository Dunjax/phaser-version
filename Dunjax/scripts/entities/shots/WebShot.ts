﻿/// <reference path="Shot.ts" />
/// <reference path="../beings/player/Player.ts" />

module Dunjax.Entities.Shots
{
    import IBeing = Entities.Beings.IBeing;
    import Player = Entities.Beings.Player; 
    
    /**
     * Is the shot fired by a spider.
     */
    export class WebShot extends Shot
    {
        constructor(x: number, y: number, startDirX: number, startDirY: number)  
        {
            super(ShotType.PulseShot, x, y, startDirX, startDirY);
            this.speed = 333;
            this.impactDamage = 0;
            this.shotFrameSequences = WebShotFrameSequences.singleton;
            this.targetBeingType = TargetBeingType.Player;
            this.impactSound = Program.sounds.webImpact;
        }

        /* HookOverride */
        protected preRemovedFromPlayDueToImpact(beingHit: IBeing)
        {
            if (beingHit == null) return;

            // if this web-shot impacted the player, the player is webbed
            const player = Player.asPlayer(beingHit);
            if (player != null) player.onWebbed();
        }
    }

    /**
     * Specifes the frame sequences used for this kind of shot.
     */
    export class WebShotFrameSequences extends ShotFrameSequences
    {
        static singleton: WebShotFrameSequences;

        constructor()
        {
            super();
            const create =
                (name: string, length: number) =>
                    this.createWith(name, length, { isRepeating: true });
            this.imagesPrefix = "webShot";
            this.level = this.downAngle = this.down = create("level", 1);
            this.up = this.upAngle = create("upAngle", 1);
        }

        /* SingletonCreator */
        static create()
        {
            WebShotFrameSequences.singleton = new WebShotFrameSequences();
        }
    }
}