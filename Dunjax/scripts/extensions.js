Array.prototype.remove = function (which) {
    return this.splice(this.indexOf(which), 1)[0];
};
Array.prototype.contains = function (object) {
    return this.indexOf(object) >= 0;
};
String.prototype.makeFirstLetterUppercase = function () {
    var s = this;
    if (s.length === 0)
        return s;
    return s.substring(0, 1).toUpperCase() + s.substring(1);
};
