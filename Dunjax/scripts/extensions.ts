﻿
/**
 * Array extensions.
 */
interface Array<T>
{
    remove(which: T): T;
    contains(object: T): boolean;
}

Array.prototype.remove = function <T>(which: T)
{
    return this.splice(this.indexOf(which), 1)[0];
}

Array.prototype.contains = function <T>(object: T) {
    return this.indexOf(object) >= 0;
}

/**
 * String extensions.
 */
interface String
{
    makeFirstLetterUppercase(): string;
}

String.prototype.makeFirstLetterUppercase = function(): string
{
    var s = <string>this;
    if (s.length === 0) return s;
    return s.substring(0, 1).toUpperCase() + s.substring(1);
}

 