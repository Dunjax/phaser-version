﻿
module Dunjax.GameStates
{
    import Item = Entities.Items.Item;
    import YellowGargoyle = Entities.Beings.Monsters.YellowGargoyle;
    import GreenGargoyle = Entities.Beings.Monsters.GreenGargoyle;
    import Stalagmite = Entities.Beings.Monsters.Stalagmite;
    import Whirlwind = Entities.Beings.Monsters.Whirlwind;
    import Spider = Entities.Beings.Monsters.Spider;
    import BlueGargoyle = Entities.Beings.Monsters.BlueGargoyle;
    import Slime = Entities.Beings.Monsters.Slime;
    import Master = Entities.Beings.Monsters.Master;
    import IMapEntity = Entities.IMapEntity;
    import ItemType = Entities.Items.ItemType;
    import AnimationEntity = Entities.AnimationEntity;
    import AnimationEntityType = Entities.AnimationEntityType;
    import IMap = Maps.IMap;
    import BlueGargoyleFrameSequences = Entities.Beings.Monsters.BlueGargoyleFrameSequences;
    import GreenGargoyleFrameSequences = Entities.Beings.Monsters.GreenGargoyleFrameSequences;
    import MasterFrameSequences = Entities.Beings.Monsters.MasterFrameSequences;
    import SlimeFrameSequences = Entities.Beings.Monsters.SlimeFrameSequences;
    import SpiderFrameSequences = Entities.Beings.Monsters.SpiderFrameSequences;
    import StalagmiteFrameSequences = Entities.Beings.Monsters.StalagmiteFrameSequences;
    import WhirlwindFrameSequences = Entities.Beings.Monsters.WhirlwindFrameSequences;
    import YellowGargoyleFrameSequences = Entities.Beings.Monsters.YellowGargoyleFrameSequences;
    import PlayerFrameSequences = Entities.Beings.Player.PlayerFrameSequences;
    import ItemFrameSequences = Entities.Items.ItemFrameSequences;
    import PulseShotFrameSequences = Entities.Shots.PulseShotFrameSequences;
    import FireballShotFrameSequences = Entities.Shots.FireballShotFrameSequences;
    import GrapeshotShotFrameSequences = Entities.Shots.GrapeshotShotFrameSequences;
    import HalberdShotFrameSequences = Entities.Shots.HalberdShotFrameSequences;
    import WebShotFrameSequences = Entities.Shots.WebShotFrameSequences;
    import SlimeShotFrameSequences = Entities.Shots.SlimeShotFrameSequences;
    import FireSpotFrameSequences = Entities.Shots.FireSpotFrameSequences;
    import SlimeSpotFrameSequences = Entities.Shots.SlimeSpotFrameSequences;

    /**
     * Calls all the class methods which are responsible for creating frame sequences,
     * to get all the sequences created.
     */
    export function createFrameSequences()
    {
        Animations.createMiscFrameSequences();

        const classes = [
            ItemFrameSequences, PlayerFrameSequences,
            PulseShotFrameSequences, GrapeshotShotFrameSequences,
            HalberdShotFrameSequences, WebShotFrameSequences,
            FireballShotFrameSequences, SlimeShotFrameSequences,
            FireSpotFrameSequences, SlimeSpotFrameSequences,
            BlueGargoyleFrameSequences, GreenGargoyleFrameSequences, MasterFrameSequences,
            SlimeFrameSequences, SpiderFrameSequences, StalagmiteFrameSequences,
            WhirlwindFrameSequences, YellowGargoyleFrameSequences
        ];
        classes.forEach(c => c.create());
    }

    interface IHasType
    {
        type: string;
    }

    /**
     * Creates the entities specified in the given Phaser map, and adds them to 
     * the given Dunjax map.
     */
    export function createEntities(tilemap: Phaser.Tilemap, map: IMap)
    {
        // get the data for the entities from the Phaser map
        const allEntityDatas = tilemap.objects['entities'];

        // for each entity data-object which is for an item, a monster, or a trigger
        const entityDatas =
            allEntityDatas.filter(
                (e: IHasType) => e.type === 'item' || e.type === 'monster' || e.type === 'trigger');
        for (let i = 0; i < entityDatas.length; i++) {
            const data = entityDatas[i];

            // create the kind of entity that this entity data-object represents
            const isItem = data.type === 'item';
            const x = data.x + data.width / 2;
            const y = data.y + data.height / 2;
            let entity: IMapEntity;
            if (isItem) entity = new Item(<any>ItemType[data.name], x, y);
            else if (data.name === 'YellowGargoyle') entity = new YellowGargoyle(x, y);
            else if (data.name === 'GreenGargoyle') entity = new GreenGargoyle(x, y);
            else if (data.name === 'Stalagmite') entity = new Stalagmite(x, y);
            else if (data.name === 'Whirlwind') entity = new Whirlwind(x, y);
            else if (data.name === 'Spider') entity = new Spider(x, y);
            else if (data.name === 'BlueGargoyle') entity = new BlueGargoyle(x, y);
            else if (data.name === 'Slime') entity = new Slime(x, y);
            else if (data.name === 'Master') entity = new Master(x, y);
            else if (data.name === 'FinalRoomTrigger')
                entity = new AnimationEntity(AnimationEntityType.FinalRoomTrigger, null, x, y);
            else continue;
            entity.id = data.id;
            entity.determineImage();

            // add the entity to the Dunjax map
            map.entities.addEntity(entity);
            entity.alignWithSurroundings();
        }
    }

    /**
     * Tells the Ninja physics system which portions of the tiles used in the maps
     * are impassible.
     */
    export function setNinjaTileTypes(tilemap: Phaser.Tilemap, layer: Phaser.TilemapLayer):
        Phaser.Physics.Ninja.Body[]
    {
        const phaserGame = Program.game;
        const physics = phaserGame.physics;
        var typeMap: IStringToNumberMap = {};
        var impassibleId = 1;
        Maps.impassibleTiles.forEach(t => typeMap[t.toString()] = impassibleId);

        var downHalfImpassibleId = 30;
        Maps.downHalfImpassibleTiles.forEach(t => typeMap[t.toString()] = downHalfImpassibleId);

        var upHalfImpassibleId = 32;
        Maps.upHalfImpassibleTiles.forEach(t => typeMap[t.toString()] = upHalfImpassibleId);

        var leftDownSlopingId = 3;
        Maps.leftDownSlopingTiles.forEach(t => typeMap[t.toString()] = leftDownSlopingId);

        var rightDownSlopingId = 2;
        Maps.rightDownSlopingTiles.forEach(t => typeMap[t.toString()] = rightDownSlopingId);

        var leftUpSlopingId = 4;
        Maps.leftUpSlopingTiles.forEach(t => typeMap[t.toString()] = leftUpSlopingId);

        var rightUpSlopingId = 5;
        Maps.rightUpSlopingTiles.forEach(t => typeMap[t.toString()] = rightUpSlopingId);

        return physics.ninja.convertTilemap(tilemap, layer, typeMap);
    }
}