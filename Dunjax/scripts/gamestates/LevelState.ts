﻿/// <reference path="../maps/TileTypes.ts" />
/// <reference path="../maps/Map.ts" />
/// <reference path="../entities/beings/player/Player.ts" />
/// <reference path="../entities/beings/monsters/YellowGargoyle.ts" />
/// <reference path="../entities/IAnimationEntity.ts" />

module Dunjax.GameStates
{
    import Player = Entities.Beings.Player.Player;
    import ITiledMap = Maps.ITiledMap;
    import Sound = Sounds.Sound;
    import DPad = Phaser.VirtualJoystick.DPad;
    import Button = Phaser.VirtualJoystick.Button;

    /**
     * Is a state the game program is in when one of the game's levels is being played.
     */
    export class LevelState extends Phaser.State
    {
        /**
         * Which level of the game is being played in this state. Starts with 1.
         */
        private levelNumber: number;

        /**
         * The game of which this state is presenting one level.
         */
        private dunjaxGame: IGame;

        /**
         * The Dunjax map object which represents this state's level.
         */
        private map: ITiledMap;

        /**
         * The Phaser map and map-layer objects which represent this state's level.
         */
        private tilemap: Phaser.Tilemap;
        private layer: Phaser.TilemapLayer;

        /**
         * The Ninja physics system's represenation of the tiles in this state's level map.
         */
        private ninjaTiles: Phaser.Physics.Ninja.Body[];

        /**
         * The song played while this state's level is being played (except when the boss
         * song is playing).
         */
        private levelSong: Phaser.Sound;

        /**
         * The song played while the player is encountering this state's level boss (if 
         * there is one).
         */
        private bossSong: Phaser.Sound;

        /**
         * The key the user presses to dismiss the story display once this state's level
         * has loaded.
         */
        private dismissStoryKey: Phaser.Key;

        /**
         * Whether the user has dismissed the story display after this state's level 
         * has loaded.
         */
        private isStoryDismissed = false;

        /**
         * The component used to display this state's level story to the user.
         */
        private storyDisplayer: StoryDisplayer;

        /**
         * The component used to display the player's stats to the user.
         */
        private statsDisplay: StatsDisplay;

        /**
         * Provides the sight blocking of the map and entities which are outside the 
         * player's line of sight.
         */
        private sightBlocker: SightBlocker;

        /**
         * The virtual d-pad used for user input, and its associated buttons.
         */
        private dPad: DPad;
        private swingButton: Button;
        private fireButton: Button;
        private jumpButton: Button;
        private switchGunsButton: Button;

        constructor(levelNumber: number, dunjaxGame: IGame)
        {
            super();
            this.levelNumber = levelNumber;
            this.dunjaxGame = dunjaxGame;
        }

        /* Overrride */
        preload()
        {
            // display the story
            const displayer = this.storyDisplayer = new StoryDisplayer(this.levelNumber);
            displayer.nextLine();

            // load the virtual d-pad images
            this.load.atlas('dpad', 'images/dpad.png', 'images/dpad.json');

            // load the level's JSON data from the map file
            const game = Program.game;
            game.load.tilemap(
                'map', `maps/${this.levelNumber}.json`, null,
                Phaser.Tilemap.TILED_JSON);

            if (Sound.soundsEnabled) {
                // load the songs to be played
                game.load.audio(`song${this.levelNumber}`, `songs/dunjax${this.levelNumber}.mp3`);
                if (this.levelNumber === 2) game.load.audio(`bossSong`, `songs/dunjaxBoss.mp3`);
            }
        }

        /* Overrride */
        create()
        {
            // add the loaded map (and its tiles) into the Phaser game
            const game = Program.game;
            const tilemap = this.tilemap = game.add.tilemap('map');
            tilemap.addTilesetImage('tiles');
            const layer = this.layer = tilemap.createLayer('tiles');
            layer.resizeWorld();
            game.time.advancedTiming = true;

            // start the Ninja physics system
            const physics = game.physics;
            physics.startSystem(Phaser.Physics.NINJA);

            // determine Ninja's view of the map's tiles
            this.ninjaTiles = setNinjaTileTypes(tilemap, layer);

            // inform the Dunjax game that this state's level has loaded, so it 
            // may create its corresponding abstractions
            this.dunjaxGame.onLevelLoaded(tilemap);

            // if this level has a master, subscribe to its relevant events
            const dunjaxMap = this.dunjaxGame.map;
            const master = dunjaxMap.entities.master;
            if (master != null) master.onKilled = () => this.onMasterKilled.call(this);

            // create the sight-blocker for the map, and connect it to the map's relevant events
            this.sightBlocker = new SightBlocker(<ITiledMap>dunjaxMap);
            dunjaxMap.onLineOfSightChangeOccurred =
                () => this.sightBlocker.isSightBlockRefreshNeeded = true;

            // have Ninja control the player's physics
            const player = <Player>this.dunjaxGame.player;
            const playerSprite = <Phaser.Sprite>player.frameworkObject;
            physics.ninja.enableAABB(playerSprite);

            // attach this level to any pertinent events from the Dunjax game
            this.dunjaxGame.onLevelCompleted = () => this.onLevelCompleted.call(this);

            // attach this level to any pertinent events from the player
            player.onReachedFinalRoomTrigger =
                () => this.onPlayerReachedFinalRoomTrigger.call(this);

            // have the Phaser game's camera follow the player
            game.camera.follow(playerSprite);
            game.camera.bounds = null;

            // if another level hasn't previously created our user input object, create it now
            // (this has to be done in create(), as this is when the Phaser input object
            // is first available)
            if (Program.userInput == null) Program.userInput = new UserInput();

            this.setupDpadAndButtons();

            // create and add the stats display
            this.statsDisplay = new StatsDisplay();
            this.statsDisplay.addStats();

            // add the sounds to the Phaser game (this is a no-op if they've previously 
            // been added)
            Program.sounds.addSounds();

            // specify which key will dismiss the story display
            this.dismissStoryKey = this.game.input.keyboard.addKey(Phaser.Keyboard.ENTER);

            if (Sound.soundsEnabled) {
                // add this level's song to the game
                this.levelSong = game.add.audio(`song${this.levelNumber}`);

                // if this level has a boss, add its song to the game
                if (this.levelNumber === 2) this.bossSong = game.add.audio(`bossSong`);
            }

            // pause the game so it doesn't run or display while the story is being displayed
            game.paused = true;
            game.world.visible = false;
        }

        /**
         * Creates the virtual d-pad and buttons used for user input for this state's level.
         */
        private setupDpadAndButtons()
        {
            // if not already loaded, load the virtual joystick plugin
            if (Program.virtualPadManager == null)
                Program.virtualPadManager = Program.game.plugins.add(Phaser.VirtualJoystick);
            
            // add a virtual d-pad and buttons, in a not-yet-visible state
            const padManager = Program.virtualPadManager;
            const dPad = this.dPad = <DPad>padManager.addDPad(0, 0, 200, 'dpad');
            dPad.alignBottomLeft(0);
            const swingButton = this.swingButton =
                padManager.addButton(500, 520, 'dpad', 'button1-up', 'button1-down');
            const fireButton = this.fireButton =
                padManager.addButton(615, 485, 'dpad', 'button2-up', 'button2-down');
            const jumpButton = this.jumpButton =
                padManager.addButton(730, 450, 'dpad', 'button3-up', 'button3-down');
            const switchGunsButton = this.switchGunsButton =
                padManager.addButton(300, 520, 'dpad', 'button1-up', 'button1-down');

            // hide the virtual d-pad and buttons  
            dPad.visible = swingButton.visible = fireButton.visible = jumpButton.visible =
                switchGunsButton.visible = false;

            // set the virtual d-pad and buttons into our user-input object
            const input = <UserInput>Program.userInput;
            input.dPad = dPad;
            input.buttonARight = swingButton;
            input.buttonB = fireButton;
            input.buttonC = jumpButton;
            input.buttonALeft = switchGunsButton;
        }

        /* Overrride */
        pauseUpdate()
        {
            // if the user hasn't previously dismissed the story display
            if (!this.isStoryDismissed) {
                // if the user is now dismissing the story display
                if (this.dismissStoryKey.isDown || this.game.input.pointer1.isDown) {
                    // dismiss the story display
                    this.isStoryDismissed = true;
                    this.storyDisplayer.onDismissed();

                    // unpause the game
                    const game = Program.game;
                    game.paused = false;

                    // start the game clock running
                    Program.clock.start();

                    // play this level's song
                    if (Sound.soundsEnabled) this.levelSong.play(null, 0, 0.5, true);
                }
            }
        }

        /* Overrride */
        update()
        {
            // have the Dunjax game perform its next game turn
            const game = this.dunjaxGame;
            game.doTurn();

            // have Ninja handle collisions between the player and the map's tiles
            const player = game.player;
            const playerSprite = <Phaser.Sprite>player.frameworkObject;
            const body = playerSprite.body;
            this.ninjaTiles.forEach(t => body.aabb.collideAABBVsTile(t.tile));

            // perform the sight blocking for this update
            this.sightBlocker.restoreSightBlockedEntities();
            const tilemap = this.tilemap;
            this.sightBlocker.checkToAddSightBlocking(() => tilemap.layers[0].dirty = true);
            this.sightBlocker.hideSightBlockedEntities();

            // update the stats display
            this.statsDisplay.updateStats(player);

            // if the d-pad and associated buttons aren't showing, and the user is touching 
            // the screen, then show them henceforth
            if (!this.dPad.visible && this.game.input.pointer1.isDown) 
                this.dPad.visible = this.swingButton.visible = this.fireButton.visible =
                    this.jumpButton.visible = true;

            // if the switch guns button isn't visible, but the d-pad is, then show the
            // button if the player has the grapeshot gun
            if (!this.switchGunsButton.visible && this.dPad.visible)
                this.switchGunsButton.visible = player.hasGrapeshotGun;

            // make the game world visible if its still invisible due to this level having
            // just started
            const world = Program.game.world;
            if (!world.visible) world.visible = true;
        }

        /**
         * Informs this level-state that the game of which this level is a part is over.
         */
        onGameOver()
        {
            this.removeDpadAndButtons();

            if (Sound.soundsEnabled) {
                // stop any playing songs
                if (this.levelSong.isPlaying) this.levelSong.stop();
                if (this.bossSong && this.bossSong.isPlaying) this.bossSong.stop();
            }
        }

        /**
         * Is called when the player touches the final room trigger.
         */
        private onPlayerReachedFinalRoomTrigger()
        {
            if (Sound.soundsEnabled) {
                // switch from playing the level song to playing the boss song
                this.levelSong.stop();
                this.bossSong.play(null, 0, 0.5, true);
            }
        }

        /**
         * Is called when the player completes this state's level.
         */
        private onLevelCompleted()
        {
            this.removeDpadAndButtons();

            if (Sound.soundsEnabled) {
                // stop playing the level song
                this.levelSong.stop();
            }

            // pause the game for a short while, so the level ending sound can
            // fully play before we move on
            const game = this.game;
            game.paused = true;
            setTimeout(() => game.paused = false, 3000);
        }

        /**
         * Is called when the player has won the game.
         */
        private onMasterKilled()
        {
            this.removeDpadAndButtons();

            // puase the game
            const game = this.game;
            game.paused = true;

            // display the ending story
            new StoryDisplayer(-1).nextLine();
        }

        /**
         * Removes the virtual d-pad and buttons used for user input for this state's level.
         */
        private removeDpadAndButtons()
        {
            Program.virtualPadManager.destroy();
        }
    }
}