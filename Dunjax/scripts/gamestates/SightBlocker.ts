﻿module Dunjax.GameStates
{
    import ITiledMap = Maps.ITiledMap;
    import tileWidth = Maps.tileWidth;
    import tileHeight = Maps.tileHeight;
    type darkenedTile = { x: number; y: number };

    /**
     * Hides map tiles and entities from display which aren't within the player's
     * line-of-sight.  The updating of which map tiles are sight blocked occurs
     * only when the player's center crosses a tile boundary.
     */
    export class SightBlocker
    {
        /**
         * The map whose tiles should be sight-blocked as necessary.
         */
        private map: ITiledMap; 

        /**
         * The player's position the last time this sight-blocker determined whether
         * tiles' sight-blocked status should be updated.
         */
        private oldPlayerTileX: number;
        private oldPlayerTileY: number;

        /**
         * The tiles which are currently deemed as sight-blocked by this sight-blocker.
         */
        private darkenedTiles: darkenedTile[] = [];

        /**
         * A pool for unused darkened-tile objects, to avoid unnecessary object churn.
         */
        private darkenedTilePool: darkenedTile[] = [];

        /**
         * When set to true, this sight blocker will perform an update of which tiles 
         * are sight-blocked even if the player hasn't crossed a tile boundary.
         */
        isSightBlockRefreshNeeded: boolean;

        constructor(map: ITiledMap)
        {
            this.map = map;
        }

        /**
         * Checks whether updating the sight-blocked status of tiles is warranted,
         * and if it is, performs the update, and signals it has done so by calling 
         * the given method.
         */
        checkToAddSightBlocking(onSightBlockingAdded: () => void)
        {
            // if either we have been told specifically to update tile sight blocking,
            // or the player has crossed a tile boundary since the last call to here
            const player = this.map.entities.player;
            if (player == null) return;
            const playerTileX = Maps.toTileX(player.x);
            const playerTileY = Maps.toTileY(player.y);
            if (this.isSightBlockRefreshNeeded
                || playerTileX !== this.oldPlayerTileX
                || playerTileY !== this.oldPlayerTileY) {
                // remember the new location of the player for the next time this is called
                this.oldPlayerTileX = playerTileX;
                this.oldPlayerTileY = playerTileY;

                // make all tiles not sight-blocked
                this.restoreDarkenedTiles();

                // determine which tiles should be sight-blocked
                this.addSightBlocking();
                onSightBlockingAdded();
                this.isSightBlockRefreshNeeded = false;
            }
        }

        /**
         * Tests each tile within the game's camera to determine whether it is blocked
         * from the player's sight, and darkens the tile if it is.
         */
        private addSightBlocking()
        {
            var phaserGame = Program.game;
            var camera = phaserGame.camera;

            // for each tile x-wise that could be within the camera's bounds
            const player = this.map.entities.player;
            var [fromX, fromY] = [Maps.toTileMidX(player.x), Maps.toTileMidY(player.y)];
            var cameraTileX = Maps.toTileLeft(camera.x) - tileWidth,
                cameraTileY = Maps.toTileTop(camera.y) - tileHeight;
            var x1: number, y1: number, x2: number, y2: number;
            var xLimit = camera.x + camera.width + tileWidth;
            var yLimit = camera.y + camera.height + tileHeight;
            for (var x = cameraTileX; x <= xLimit; x += tileWidth) {
                // for each tile y-wise that could be within the camera's bounds
                for (var y = cameraTileY; y <= yLimit; y += tileHeight, x2 = undefined) {
                    if (!this.map.containsLocation(x, y)) continue;
                    
                    // test code to limit sight blocking to within a short distance of the player
                    //if (x < player.left - 3 * tileWidth
                    //    || x > player.right + 3 * tileWidth
                    //    || y < player.top - 3 * tileHeight
                    //    || y > player.bottom + 3 * tileHeight) continue;

                    // determine the (up to) two line segments to test for sight-blockedness; 
                    // these segments run from the center of the tile the player's center occupies,
                    // to the middle of the sides of this tile which are facing the player
                    var right = x + tileWidth - 1;
                    var bottom = y + tileHeight - 1;
                    var midX = x + tileWidth / 2;
                    var midY = y + tileHeight / 2;
                    var isToLeft = right < player.left;
                    var isToRight = x > player.right - 1;
                    var isAbove = bottom < player.top;
                    var isBelow = y > player.bottom - 1;
                    if (isToLeft && isAbove) {
                        [x1, y1] = [right, midY];
                        [x2, y2] = [midX, bottom];
                    }
                    else if (isToRight && isAbove) {
                        [x1, y1] = [x, midY];
                        [x2, y2] = [midX, bottom];
                    }
                    else if (isToLeft && isBelow) {
                        [x1, y1] = [right, midY];
                        [x2, y2] = [midX, y];
                    }
                    else if (isToRight && isBelow) {
                        [x1, y1] = [x, midY];
                        [x2, y2] = [midX, y];
                    }
                    else if (isToLeft) [x1, y1] = [right, midY];
                    else if (isToRight) [x1, y1] = [x, midY];
                    else if (isAbove) [x1, y1] = [midX, bottom];
                    else if (isBelow) [x1, y1] = [midX, y];
                    else continue;

                    // if line of sight is blocked along both segments determined above
                    if (!this.map.isLocationVisibleFrom(x1, y1, fromX, fromY)
                        && (x2 === undefined
                            || !this.map.isLocationVisibleFrom(x2, y2, fromX, fromY))) {
                        // if an object is available in the darkened tile pool
                        var darkened = this.darkenedTilePool.pop();
                        if (darkened != null) {
                            // use it
                            darkened.x = x;
                            darkened.y = y;
                        }

                        // otherwise, create a new darkened tile object
                        else darkened = { x: x, y: y };

                        // store that the tile at this position is darkened
                        this.darkenedTiles.push(darkened);
                        this.map.darkenTileAtPixel(x, y);
                    }
                }
            }
        }

        /**
         * Un-darkens all the tiles which were darkened by the last call to add sight blocking.
         */
        private restoreDarkenedTiles()
        {
            // for each currently darkened tile
            const darkenedTiles = this.darkenedTiles;
            while (darkenedTiles.length > 0) {
                // undarken this tile
                const darkened = darkenedTiles.pop();
                this.map.undarkenTileAtPixel(darkened.x, darkened.y);

                // recycle the darkened tile object in the pool, for later reuse
                this.darkenedTilePool.push(darkened);
            }
        }

        /**
         * Turns invisible any onscreen entity for which line of sight from the player 
         * is blocked.
         */
        hideSightBlockedEntities()
        {
            // for each entity in the map
            const entities = this.map.entities.entities;
            const camera = Program.game.camera;
            const view = camera.view;
            for (let i = 0; i < entities.length; i++) {
                // if this entity isn't onscreen, ignore it
                const entity = entities[i];
                const r = entity;
                if (!Geometry.intersects( // optmized away from getting isInCamera, which is slower
                    r.left, r.top, r.width, r.height,
                    camera.x, camera.y, camera.width, camera.height)) continue;

                // if line-of-sight from the player to each of the four corners of this
                // entity is blocked
                const isOutOfSight =
                    (x: number, y: number) =>
                        !view.contains(x, y) || this.map.isTileDarkenedAtPixel(x, y);
                if (isOutOfSight(entity.left, entity.top)
                    && isOutOfSight(entity.right, entity.top)
                    && isOutOfSight(entity.left, entity.bottom)
                    && isOutOfSight(entity.right, entity.bottom))
                    // don't show this entity
                    entity.shouldBeDrawn = false;
            }
        }

        /**
         * Turns visible any entity which was made invisible due to being sight-blocked 
         * from the player.
         */
        restoreSightBlockedEntities()
        {
            const entities = this.map.entities.entities;
            entities.forEach(e => e.shouldBeDrawn = true);
        }
    }
}