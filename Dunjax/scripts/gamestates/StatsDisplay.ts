﻿/// <reference path="../entities/beings/player/ammos/IAmmo.ts" />

module Dunjax.GameStates
{
    import IAmmo = Entities.Beings.Player.Ammos.IAmmo;
    import IPlayer = Entities.Beings.Player.IPlayer;
    import AmmoType = Entities.Beings.Player.Ammos.AmmoType;

    /**
     * Displays stats about the player, within the game's viewport.
     */
    export class StatsDisplay
    {
        /**
         * The readout for how much armor the player has left.
         */
        private armorText: Phaser.Text;

        /**
         * The readout for how many keys the player has.
         */
        private keysText: Phaser.Text;

        /**
         * The readout for how much pulse ammo the player has left.
         */
        private pulseAmmoText: Phaser.Text;

        /**
         * The readout for how much grapeshot ammo the player has.
         */
        private grapeshotAmmoText: Phaser.Text;
        
        /**
         * The values of the stats the last time each was updated in the display.
         * These values are kept because we wish to redraw them only when they change.
         */
        private lastArmorValue = 0;
        private lastKeysValue = -1;
        private lastAmmoUsed: IAmmo;
        private lastPulseAmmoValue = -1;
        private lastGrapeshotAmmoValue = -1;

        /**
         * The images for the ammo stats, stored here because we need to switch between 
         * them in this display, as the player changes which ammo he is using.
         */
        private pulseAmmoImage: Phaser.Image;
        private grapeshotAmmoImage: Phaser.Image;

        /**
         * Adds a stats display about the player and its items, towards the bottom
         * of the play area.
         */
        addStats()
        {
            // add the armor stat display
            const game = Program.game;
            let image = game.add.image(0, 0, 'armorStat');
            image.fixedToCamera = true;
            const statsHeight = 30;
            const statsY = game.height - statsHeight;
            image.cameraOffset.setTo(0, statsY);
            const style = { font: "bold 12pt Arial", fill: "#ffffff", align: "left" };
            let text = this.armorText = game.add.text(0, 0, '', style);
            text.fixedToCamera = true;
            text.cameraOffset.setTo(image.width + 5, statsY);

            // add the keys stat display
            image = game.add.image(0, 0, 'keysStat');
            image.fixedToCamera = true;
            const keysX = game.width / 4;
            image.cameraOffset.setTo(keysX, statsY + 6);
            text = this.keysText = game.add.text(0, 0, '', style);
            text.fixedToCamera = true;
            text.cameraOffset.setTo(keysX + image.width + 5, statsY);

            // add the pulse ammo stat display
            image = this.pulseAmmoImage = game.add.image(0, 0, 'pulseAmmoStat');
            image.fixedToCamera = true;
            const ammoX = game.width / 2;
            image.cameraOffset.setTo(ammoX, statsY);
            text = this.pulseAmmoText = game.add.text(0, 0, '', style);
            text.fixedToCamera = true;
            text.cameraOffset.setTo(ammoX + image.width + 5, statsY);

            // add the grapeshot ammo stat display
            image = this.grapeshotAmmoImage = game.add.image(0, 0, 'grapeshotAmmoStat');
            image.fixedToCamera = true;
            image.cameraOffset.setTo(ammoX, statsY);
            image.visible = false;
            text = this.grapeshotAmmoText = game.add.text(0, 0, '', style);
            text.fixedToCamera = true;
            text.cameraOffset.setTo(ammoX + image.width + 5, statsY);
            text.visible = false;
        }

        /**
         * Updates the stats values of the given player which are shown in this display, 
         * if any of them have changed.
         */
        updateStats(player: IPlayer)
        {
            // if the player's armor stat has changed
            if (this.lastArmorValue !== player.armorLeft) {
                // update it in this display
                this.armorText.setText(`${player.armorLeft}`);
                this.lastArmorValue = player.armorLeft;
            }

            // if the player's keys stat has changed
            if (this.lastKeysValue !== player.keysHeld) {
                // update it in this display
                this.keysText.setText(`${player.keysHeld}`);
                this.lastKeysValue = player.keysHeld;
            }

            // if the player has switched ammos 
            const ammo = player.ammoInUse;
            const isPulse = ammo.type === AmmoType.Pulse;
            if (this.lastAmmoUsed !== ammo) {
                // change which ammo stat we are displaying to match the ammo 
                // the player is using
                this.pulseAmmoImage.visible = isPulse;
                this.pulseAmmoText.visible = isPulse;
                this.grapeshotAmmoImage.visible = !isPulse;
                this.grapeshotAmmoText.visible = !isPulse;
                this.lastAmmoUsed = ammo;
                this.lastPulseAmmoValue = this.lastGrapeshotAmmoValue = -1;
            }

            // if the player's is using pulse ammo, and that stat has changed
            if (isPulse && this.lastPulseAmmoValue !== ammo.amount) {
                // update it in this display
                this.pulseAmmoText.setText(`${ammo.amount}`);
                this.lastPulseAmmoValue = ammo.amount;
            }

            // if the player's is using grapeshot ammo, and that stat has changed
            if (!isPulse && this.lastGrapeshotAmmoValue !== ammo.amount) {
                // update it in this display
                this.grapeshotAmmoText.setText(`${ammo.amount}`);
                this.lastGrapeshotAmmoValue = ammo.amount;
            }
        }
    }
} 