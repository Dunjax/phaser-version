﻿module Dunjax.GameStates
{
    /**
     * Progressively displays the story at the beginning of a level, or that 
     * for ending of the game.
     * 
     * Portions of this code were taken from the Phaser website.
     */
    export class StoryDisplayer
    {
        /**
         * The story currently being displayed.
         */
        private story: string[];

        /**
         * Which line in the story is currently being displayed.
         */
        private lineIndex = 0;

        /**
         * How long to wait between the display of each line in the story.
         */
        private lineDelay = 400;

        /**
         * The words in the current story line being displayed.
         */
        private words: string[];

        /**
         * Which word in the current story line is currently being displayed.
         */
        private wordIndex = 0;

        /**
         * How long to wait between the display of each word in the current story line.
         */
        private wordDelay = 120;

        /**
         * The onscreen element displaying the story.
         */
        private text: Phaser.Text;

        /**
         * The story for the game's first level.
         */
        private level1Story =
        [
            "Level 1:  After the Thieves",
            "",
            "Your spaceship having crash-landed on an unexplored",
            "planet, you awake from unconsciousness to find your",
            "vessel ransacked by beings who have since left the",
            "area. Missing is a critical element of the ship's",
            "power core, without which you cannot achieve lift-off.",
            "Donning your power armor, you follow a large group",
            "of unidentifiable footprints to a cave entrance set",
            "into the base of a nearby mountainside.",
            "",
            "With limited supplies you cannot survive on this",
            "planet for long. You must reclaim your power core",
            "at any cost.  Into the cave you go!",
        ];

        /**
         * The story for the game's second level.
         */
        private level2Story =
        [
            "Level 2:  What Dark Thing Lurks Here?",
            "",
            "It would appear the task before you is much more",
            "difficult than you had first guessed. Who or what",
            "built this massive network of caves?  Who created",
            "these horrid animations that seek your life at",
            "every turn? And more importantly, will your armor",
            "and weapons hold out against them until you can",
            "find your power core and escape?",
            "",
            "The exit from the first cave leads you down a long",
            "passageway which opens into yet another vast",
            "underground expanse...",
        ];

        /**
         * The story for the game's ending.
         */
        private endingStory =
        [
            "Their master defeated, the remaining monsters",
            "surrendered your ship's missing part to you,",
            "intent on you leaving their caves.",
            "",
            "With your ship repaired, you were able to lift",
            "off from the planet and resume your journey.",
            "",
            "Congratulations!",
            "",
            "We hope you enjoyed this game.",
            "",
            "Thank you for playing.",
        ];

        /**
         * A prompt appended at the end of each story, except that for the ending.
         */
        private continuePrompt =
        [
            "",
            "Press [Enter] (or touch device screen) to continue"
        ];

        constructor(levelNumber: number)
        {
            // use the story for the given level number, or that for the ending if the number
            // is negative
            let levelStory = this.level1Story;
            if (levelNumber === 2) levelStory = this.level2Story;
            else if (levelNumber < 0) levelStory = this.endingStory;

            // if we are not displaying the ending story, appened the continue prompt
            // to the story
            const game = Program.game;
            var story: string[] = this.story = [];
            levelStory.forEach(l => story.push(l));
            if (levelNumber >= 0) this.continuePrompt.forEach(l => story.push(l));

            // create and style the text element use to the display the story;
            // the font style for the ending story is bigger and stands out more,
            // since it will be display over the backdrop of the game's final scene
            const group = new Phaser.Group(game, game.stage);
            const font = levelNumber >= 0 ? "15px Arial" : "bold 20px Arial";
            const style = { font: font, fill: "#19de65", stroke: <string>null, strokeThickness: 0 };
            if (levelNumber < 0) {
                style.strokeThickness = 5;
                style.stroke = 'rgba(0, 0, 0, 1)';
            }
            this.text = game.add.text(32, 32, "", style, group);
            if (levelNumber < 0) this.text.setShadow(3, 3, 'rgba(0, 0, 0, 1)', 5);
        }

        /**
         * Starts the progressive display of the next line in the story.
         */
        nextLine()
        {
            // if all lines have been displayed, there is nothing more to do
            if (this.lineIndex === this.story.length) return;

            // this fixes an issue with Phaser adding an unnecessary space at the beginning
            // of the story
            if (this.lineIndex === 0) this.text.text = "";

            // split the next story line into words
            this.words = this.story[this.lineIndex++].split(' ');

            // start the display of the words in the current story line
            this.wordIndex = 0;
            setTimeout(() => this.nextWord.call(this), this.wordDelay);
        }

        /**
         * Displays the next word in the current story line, and queues the display
         * of the next word or line after that.
         */
        nextWord()
        {
            // add the next word into the story display
            this.text.text = this.text.text.concat(this.words[this.wordIndex++] + " ");

            // if this is the last word in the current story line
            if (this.wordIndex === this.words.length) {
                // queue the display of the next story line
                this.text.text += "\n";
                setTimeout(() => this.nextLine.call(this), this.lineDelay);
            }

            // queue the display of the next word
            else setTimeout(() => this.nextWord.call(this), this.wordDelay);
        }

        /**
         * Informs this story display that is has been dismissed by the user.
         */
        onDismissed()
        {
            // remove the display of the story
            this.text.destroy();
        }
    }
}