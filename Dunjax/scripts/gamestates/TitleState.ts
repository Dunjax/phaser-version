﻿module Dunjax.GameStates
{
    import AllSounds = Sounds.AllSounds;

    /**
     * Displays the title screen.
     */
    export class TitleState extends Phaser.State
    {
        private notices =
        [
            "Version 3.0.0",
            "",
            "Copyright 1991-2015 Jeff Mather",
        ];

        private creditsRoles =
        [
            "Programming, Game Concept",
            "Game Design, Level Design",
            "Graphics",
            "Music",
            "Additional Level Design",
            "Logo",
        ];

        private creditsNames =
        [
            "Jeff Mather",
            "David Niecikowski",
            "Gavin Corneveaux",
            "Edward Niecikowski",
            "George Corneveaux",
            "Howard Kistler",
        ];

        /**
         * Displays a "Loading..." message while the game's assets load from disk,
         * before the title screen is displayed.
         */
        private loadingText: Phaser.Text;

        /**
         * The key the user will use to exit this state.
         */
        private startGameKey: Phaser.Key;

        /* Overrride */
        preload()
        {
            // display the loading... message
            const game = Program.game;
            const style = { font: "bold 12pt Arial", fill: "#888888", align: "center" };
            const text = this.loadingText = game.add.text(0, 0, "Loading...", style);
            text.x = game.width / 2 - text.width / 2;
            text.y = game.height / 2 - text.height / 2;

            // load the game's images from disk
            game.load.image('logo', 'images/logo.png');
            game.load.image('tiles', 'maps/tiles.png');
            game.load.image('armorStat', 'images/armorStat.png');
            game.load.image('keysStat', 'images/entities/items/key1.png');
            game.load.image('pulseAmmoStat', 'images/entities/items/pulseAmmoBig1.png');
            game.load.image('grapeshotAmmoStat', 'images/entities/items/grapeshotGun1.png');
            Program.emptyImage = new Images.Image("empty");

            createFrameSequences();

            // if they haven't been loaded already, load the game's sounds from disk
            if (Program.sounds == null) Program.sounds = new AllSounds();
        }

        /* Overrride */
        create()
        {
            // remove the 'Loading...' message
            const game = Program.game;
            this.loadingText.destroy();

            // draw the logo
            const centerX = game.width / 2;
            const logo = game.add.image(0, 40, 'logo');
            logo.x = centerX - logo.width / 2;

            // display each line in the notices text
            var y = 160;
            const style = { font: "bold 12pt Arial", fill: "#888888", align: "center" };
            this.notices.forEach(
                line => {
                    const text = game.add.text(0, y, line, style);
                    text.x = centerX - text.width / 2,
                    y += text.height;
                });

            // display each line in the credits-roles text
            const creditsCenterX = centerX + 40;
            const creditsStartY = 270;
            y = creditsStartY;
            this.creditsRoles.forEach(
                line => {
                    const text = game.add.text(0, y, line, style);
                    text.x = creditsCenterX - 20 - text.width,
                    y += text.height;
                });

            // display each line in the credits-names text
            y = creditsStartY;
            const emphasisStyle = { font: "bold 12pt Arial", fill: "#cccccc", align: "center" };
            this.creditsNames.forEach(
                line => {
                    const text = game.add.text(0, y, line, emphasisStyle);
                    text.x = creditsCenterX + 20;
                    y += text.height;
                });

            // display the prompt to begin
            const text =
                game.add.text(0, game.height - 120,
                    "Press [Enter] (or touch device screen) to start", style);
            text.x = centerX - text.width / 2;

            // setup the key that will let the user exit this state
            this.startGameKey = this.game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        }

        /* Overrride */
        update()
        {
            // if the user wants to exit this state, signal that fact
            if (this.startGameKey.isDown || this.game.input.pointer1.isDown) this.onStateExited();
        }

        /* Event */
        onStateExited: () => void;
    }
}
