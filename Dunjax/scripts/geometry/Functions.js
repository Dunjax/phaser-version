var Dunjax;
(function (Dunjax) {
    var Geometry;
    (function (Geometry) {
        function getSlope(x1, y1, x2, y2) {
            if (x1 === x2 && y1 === y2)
                return 0;
            if (x1 === x2)
                return y2 > y1 ? 1000 : -1000;
            return (y2 - y1) / (x2 - x1);
        }
        Geometry.getSlope = getSlope;
        function getXSlope(x1, y1, x2, y2) {
            if (x1 === x2 && y1 === y2)
                return 0;
            if (y1 === y2)
                return x2 > x1 ? 1000 : -1000;
            return (x2 - x1) / (y2 - y1);
        }
        Geometry.getXSlope = getXSlope;
        function getDistance(x1, y1, x2, y2) {
            var a = y2 - y1, b = x2 - x1;
            return Math.sqrt(a * a + b * b);
        }
        Geometry.getDistance = getDistance;
        function isDistanceAtMost(x1, y1, x2, y2, d) {
            var a = y2 - y1, b = x2 - x1;
            return a * a + b * b < d * d;
        }
        Geometry.isDistanceAtMost = isDistanceAtMost;
        function getDifference(x1, y1, x2, y2) {
            return { x: x2 - x1, y: y2 - y1 };
        }
        Geometry.getDifference = getDifference;
        function getUnitVector(x1, y1, x2, y2) {
            if (x1 === x2 && y1 === y2)
                return { x: 0, y: 0 };
            var distance = getDistance(x1, y1, x2, y2);
            return { x: (x2 - x1) / distance, y: (y2 - y1) / distance };
        }
        Geometry.getUnitVector = getUnitVector;
        function isLocationInDirection(x1, y1, x2, y2, dirX, dirY) {
            var range = Math.PI / 3;
            var unit = getUnitVector(x1, y1, x2, y2);
            return getAngleBetween(dirX, dirY, unit.x, unit.y) <= range;
        }
        Geometry.isLocationInDirection = isLocationInDirection;
        function getAngleBetween(x1, y1, x2, y2) {
            var magnitude1 = getMagnitude(x1, y1), magnitude2 = getMagnitude(x2, y2);
            if (magnitude1 === 0 || magnitude2 === 0)
                return 0;
            var cosine = getDotProduct(x1, y1, x2, y2) / (magnitude1 * magnitude2);
            if (cosine > 1)
                cosine = 1;
            if (cosine < -1)
                cosine = -1;
            return Math.acos(cosine);
        }
        Geometry.getAngleBetween = getAngleBetween;
        function getMagnitude(x, y) {
            return Math.sqrt(x * x + y * y);
        }
        Geometry.getMagnitude = getMagnitude;
        function getDotProduct(x1, y1, x2, y2) {
            return x1 * x2 + y1 * y2;
        }
        Geometry.getDotProduct = getDotProduct;
        function setBounds(r, x, y, width, height) {
            r.x = x;
            r.y = y;
            r.width = width;
            r.height = height;
        }
        Geometry.setBounds = setBounds;
        function setBoundsTo(r1, r2) {
            r1.x = r2.x;
            r1.y = r2.y;
            r1.width = r2.width;
            r1.height = r2.height;
        }
        Geometry.setBoundsTo = setBoundsTo;
        function contains(rectX, rectY, rectWidth, rectHeight, x, y) {
            return x >= rectX
                && y >= rectY
                && x < rectX + rectWidth - .0001
                && y < rectY + rectHeight - .0001;
        }
        Geometry.contains = contains;
        function rectContains(r, x, y) {
            return x >= r.left
                && y >= r.top
                && x < r.right
                && y < r.bottom;
        }
        Geometry.rectContains = rectContains;
        function intersects(x1, y1, width1, height1, x2, y2, width2, height2) {
            var tw = width1;
            var th = height1;
            var rw = width2;
            var rh = height2;
            if (rw <= 0 || rh <= 0 || tw <= 0 || th <= 0)
                return false;
            var tx = x1;
            var ty = y1;
            var rx = x2;
            var ry = y2;
            rw += rx;
            rh += ry;
            tw += tx;
            th += ty;
            return ((rw < rx || rw > tx) &&
                (rh < ry || rh > ty) &&
                (tw < tx || tw > rx) &&
                (th < ty || th > ry));
        }
        Geometry.intersects = intersects;
        function intersectsWith(r1, r2) {
            return intersects(r1.left, r1.top, r1.width, r1.height, r2.left, r2.top, r2.width, r2.height);
        }
        Geometry.intersectsWith = intersectsWith;
    })(Geometry = Dunjax.Geometry || (Dunjax.Geometry = {}));
})(Dunjax || (Dunjax = {}));
