﻿module Dunjax.Geometry
{
    import IRectangle = Geometry.IRectangle;

    export type Point = { x: number, y: number };

    export function getSlope(x1: number, y1: number, x2: number, y2: number): number
    {
        if (x1 === x2 && y1 === y2) return 0;
        if (x1 === x2) return y2 > y1 ? 1000 : -1000;
        return (y2 - y1) / (x2 - x1);
    }

    export function getXSlope(x1: number, y1: number, x2: number, y2: number): number
    {
        if (x1 === x2 && y1 === y2) return 0;
        if (y1 === y2) return x2 > x1 ? 1000 : -1000;
        return (x2 - x1) / (y2 - y1);
    }

    export function getDistance(x1: number, y1: number, x2: number, y2: number): number
    {
        const b = x2 - x1;
        const a = y2 - y1;
        return Math.sqrt(a * a + b * b);
    }

    /**
     * Is quicker than getDistance(), as no square root is involved.
     */
    export function isDistanceAtMost(
        x1: number, y1: number, x2: number, y2: number, d: number): boolean
    {
        const b = x2 - x1;
        const a = y2 - y1;
        return a * a + b * b < d * d;
    }

    export function getDifference(x1: number, y1: number, x2: number, y2: number): Point
    {
        return { x: x2 - x1, y: y2 - y1 };
    }

    export function getUnitVector(x1: number, y1: number, x2: number, y2: number): Point
    {
        if (x1 === x2 && y1 === y2) return { x: 0, y: 0 };
        const distance = getDistance(x1, y1, x2, y2);
        return { x: (x2 - x1) / distance, y: (y2 - y1) / distance };
    }

    export function isLocationInDirection(
        x1: number, y1: number, x2: number, y2: number, dirX: number, dirY: number): boolean
    {
        const range = Math.PI / 3;
        const unit = getUnitVector(x1, y1, x2, y2);
        return getAngleBetween(dirX, dirY, unit.x, unit.y) <= range;
    }

    export function getAngleBetween(x1: number, y1: number, x2: number, y2: number): number
    {
        // if either given vector has a zero magnitude, return a zero result
        const magnitude2 = getMagnitude(x2, y2);
        const magnitude1 = getMagnitude(x1, y1);
        if (magnitude1 === 0 || magnitude2 === 0) return 0;

        // use the formula for the dot product between the two vectors
        // to detm the angle between them
        let cosine = getDotProduct(x1, y1, x2, y2) / (magnitude1 * magnitude2);
        if (cosine > 1) cosine = 1;
        if (cosine < -1) cosine = -1;
        return Math.acos(cosine);
    }

    export function getMagnitude(x: number, y: number): number
    {
        return Math.sqrt(x * x + y * y);
    }

    export function getDotProduct(x1: number, y1: number, x2: number, y2: number): number
    {
        return x1 * x2 + y1 * y2;
    }

    export function setBounds(
        r: IRectangle, x: number, y: number, width: number, height: number)
    {
        r.x = x;
        r.y = y;
        r.width = width;
        r.height = height;
    }

    export function setBoundsTo(r1: IRectangle, r2: IRectangle)
    {
        r1.x = r2.x;
        r1.y = r2.y;
        r1.width = r2.width;
        r1.height = r2.height;
    }

    export function contains(
        rectX: number, rectY: number, rectWidth: number, rectHeight: number,
        x: number, y: number): boolean
    {
        return x >= rectX
            && y >= rectY
            && x < rectX + rectWidth - .0001
            && y < rectY + rectHeight - .0001;
    }

    export function rectContains(r: IRectangle, x: number, y: number): boolean
    {
        return x >= r.left
            && y >= r.top
            && x < r.right
            && y < r.bottom;
    }

    export function intersects(
        x1: number, y1: number, width1: number, height1: number,
        x2: number, y2: number, width2: number, height2: number): boolean
    {
        // code from Java platform
        let tw = width1;
        let th = height1;
        let rw = width2;
        let rh = height2;
        if (rw <= 0 || rh <= 0 || tw <= 0 || th <= 0) return false;
        const tx = x1;
        const ty = y1;
        const rx = x2;
        const ry = y2;
        rw += rx;
        rh += ry;
        tw += tx;
        th += ty;
        return ((rw < rx || rw > tx) &&
            (rh < ry || rh > ty) &&
            (tw < tx || tw > rx) &&
            (th < ty || th > ry));
    }

    export function intersectsWith(r1: IRectangle, r2: IRectangle): boolean
    {
        return intersects(
            r1.left, r1.top, r1.width, r1.height,
            r2.left, r2.top, r2.width, r2.height);
    }

    export function intersectsWith2(
        r: IRectangle, left: number, top: number, width: number, height: number): boolean
    {
        return intersects(
            r.left, r.top, r.width, r.height,
            left, top, width, height);
    }
}