﻿module Dunjax.Geometry
{
    export interface IRectangle
    {
        x: number;
        y: number;
        width: number;
        height: number;

        left: number;
        right: number;
        top: number;
        bottom: number;
    }
}
