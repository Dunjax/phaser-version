var Dunjax;
(function (Dunjax) {
    var Geometry;
    (function (Geometry) {
        var Point = (function () {
            function Point() {
            }
            /* Implementation */
            Point.prototype.setX = function (to) {
                this.x = to;
                return this;
            };
            /* Implementation */
            Point.prototype.setY = function (to) {
                this.y = to;
                return this;
            };
            /* Implementation */
            Point.prototype.set = function (x, y) {
                this.x = x;
                this.y = y;
                return this;
            };
            /* Implementation */
            Point.prototype.setTo = function (location) {
                this.x = location.x;
                this.y = location.y;
                return this;
            };
            /* Implementation */
            Point.prototype.add = function (dx, dy) {
                this.x += dx;
                this.y += dy;
                return this;
            };
            /* Implementation */
            Point.prototype.equals = function (other) {
                return this.x === other.x && this.y === other.y;
            };
            /* Implementation */
            Point.prototype.getSlope = function (to) {
                if (this.equals(to))
                    return 0;
                if (this.x === to.x)
                    return to.y > this.y ? 1000 : -1000;
                return (to.y - this.y) / (to.x - this.x);
            };
            /* Implementation */
            Point.prototype.getXSlope = function (to) {
                if (this.equals(to))
                    return 0;
                if (this.y === to.y)
                    return to.x > this.x ? 1000 : -1000;
                return (to.x - this.x) / (to.y - this.y);
            };
            /* Implementation */
            Point.prototype.getDistance = function (to) {
                var a = to.y - this.y, b = to.x - this.x;
                return Math.sqrt(a * a + b * b);
            };
            /* Implementation */
            Point.prototype.isDistanceAtMost = function (to, distance) {
                var a = to.x - this.x, b = to.y - this.y;
                return a * a + b * b < distance * distance;
            };
            /* Implementation */
            Point.prototype.getDifference = function (to) {
                return new Point().set(to.x - this.x, to.y - this.y);
            };
            /* Implementation */
            Point.prototype.getUnitVector = function (to) {
                // handle the case where the two points are coincident
                if (this.equals(to))
                    return new Point();
                var distance = this.getDistance(to);
                return new Point().set((to.x - this.x) / distance, (to.y - this.y) / distance);
            };
            /* Implementation */
            Point.prototype.isLocationInDirection = function (to, direction) {
                var range = Math.PI / 3;
                return direction.getAngleBetween(this.getUnitVector(to)) <= range;
            };
            /* Implementation */
            Point.prototype.getAngleBetween = function (other) {
                // if either given vector has a zero magnitude, return a zero result
                var magnitude1 = this.getMagnitude(), magnitude2 = other.getMagnitude();
                if (magnitude1 === 0 || magnitude2 === 0)
                    return 0;
                // use the formula for the dot product between the two vectors
                // to detm the angle between them
                var cosine = this.getDotProduct(other) / (magnitude1 * magnitude2);
                if (cosine > 1)
                    cosine = 1;
                if (cosine < -1)
                    cosine = -1;
                return Math.acos(cosine);
            };
            /* Implementation */
            Point.prototype.getMagnitude = function () {
                return Math.sqrt(this.x * this.x + this.y * this.y);
            };
            /* Implementation */
            Point.prototype.getDotProduct = function (other) {
                return this.x * other.x + this.y * other.y;
            };
            return Point;
        })();
        Geometry.Point = Point;
    })(Geometry = Dunjax.Geometry || (Dunjax.Geometry = {}));
})(Dunjax || (Dunjax = {}));
//# sourceMappingURL=Point.js.map