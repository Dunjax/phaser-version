var Dunjax;
(function (Dunjax) {
    var Geometry;
    (function (Geometry) {
        var Rectangle = (function () {
            function Rectangle() {
                /**
                 * The top-left hand corner of this rectangle.
                 */
                this.location = new Geometry.Point();
                /**
                 * The dimensions of this rectangle.
                 */
                this.size = new Geometry.Size();
                this._topLeft = new Geometry.Point();
                this._topRight = new Geometry.Point();
                this._bottomLeft = new Geometry.Point();
                this._bottomRight = new Geometry.Point();
                this._center = new Geometry.Point();
                this._topCenter = new Geometry.Point();
                this._bottomCenter = new Geometry.Point();
                this._leftCenter = new Geometry.Point();
                this._rightCenter = new Geometry.Point();
            }
            Object.defineProperty(Rectangle.prototype, "width", {
                /**
                 * A property for accessing just this rectangle's width.
                 */
                get: function () { return this.size.width; },
                set: function (value) { this.size.width = value; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Rectangle.prototype, "height", {
                /**
                 * A property for accessing just this rectangle's height.
                 */
                get: function () { return this.size.height; },
                set: function (value) { this.size.height = value; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Rectangle.prototype, "leftX", {
                /**
                 * A property for accessing just this rectangle's left-x value.
                 */
                get: function () { return this.location.x; },
                set: function (value) { this.location.x = value; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Rectangle.prototype, "rightX", {
                /**
                 * A property for accessing just this rectangle's right-x value.
                 */
                get: function () { return this.location.x + this.width - 1; },
                set: function (value) { this.location.x = value - this.width + 1; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Rectangle.prototype, "topY", {
                /**
                 * A property for accessing just this rectangle's top-y value.
                 */
                get: function () { return this.location.y; },
                set: function (value) { this.location.y = value; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Rectangle.prototype, "bottomY", {
                /**
                 * A property for accessing just this rectangle's bottom-y value.
                 */
                get: function () { return this.location.y + this.height - 1; },
                set: function (value) { this.location.y = value - this.height + 1; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Rectangle.prototype, "topLeft", {
                /**
                 * This rectangle's top-left corner location.
                 */
                get: function () { return this._topLeft.setTo(this.location); },
                set: function (to) { this.location.setTo(to); },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Rectangle.prototype, "topRight", {
                /**
                 * This rectangle's top-right corner location.
                 */
                get: function () { return this._topRight.set(this.rightX, this.topY); },
                set: function (to) { this.location.set(to.x - this.width + 1, to.y); },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Rectangle.prototype, "bottomLeft", {
                /**
                 * This rectangle's bottom-left corner location.
                 */
                get: function () { return this._bottomLeft.set(this.leftX, this.bottomY); },
                set: function (to) { this.location.set(to.x, to.y - this.height + 1); },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Rectangle.prototype, "bottomRight", {
                /**
                 * This rectangle's bottom-right corner location.
                 */
                get: function () { return this._bottomRight.set(this.rightX, this.bottomY); },
                set: function (to) {
                    this.location.set(to.x - this.width + 1, to.y - this.height + 1);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Rectangle.prototype, "center", {
                /**
                 * This rectangle's center location.
                 */
                get: function () {
                    return this._center.set(this.leftX + this.width / 2, this.topY + this.height / 2);
                },
                set: function (to) {
                    this.location.set(to.x - this.width / 2, to.y - this.height / 2);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Rectangle.prototype, "topCenter", {
                /**
                 * This rectangle's top-center location.
                 */
                get: function () {
                    return this._topCenter.set(this.leftX + this.width / 2, this.topY);
                },
                set: function (to) {
                    this.location.set(to.x - this.width / 2, to.y);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Rectangle.prototype, "bottomCenter", {
                /**
                 * This rectangle's bottom-center location.
                 */
                get: function () {
                    return this._bottomCenter.set(this.leftX + this.width / 2, this.bottomY);
                },
                set: function (to) {
                    this.location.set(to.x - this.width / 2, to.y - this.height + 1);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Rectangle.prototype, "leftCenter", {
                /**
                 * This rectangle's left-center location.
                 */
                get: function () {
                    return this._leftCenter.set(this.leftX, this.topY + this.height / 2);
                },
                set: function (to) {
                    this.location.set(to.x, to.y - this.height / 2);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Rectangle.prototype, "rightCenter", {
                /**
                 * This rectangle's right-center location.
                 */
                get: function () {
                    return this._rightCenter.set(this.rightX, this.topY + this.height / 2);
                },
                set: function (to) {
                    this.location.set(to.x - this.width + 1, to.y - this.height / 2);
                },
                enumerable: true,
                configurable: true
            });
            /* Implementation */
            Rectangle.prototype.set = function (x, y, width, height) {
                this.location.set(x, y);
                this.size.set(width, height);
                return this;
            };
            /* Implementation */
            Rectangle.prototype.setTo = function (to) {
                this.location.setTo(to.topLeft);
                this.size.setTo(to.size);
                return this;
            };
            /* Implementation */
            Rectangle.prototype.addToLocation = function (dx, dy) {
                this.location.add(dx, dy);
                return this;
            };
            Rectangle.prototype.contains = function (location) {
                return location.x >= this.leftX
                    && location.y >= this.topY
                    && location.x <= this.rightX
                    && location.y <= this.bottomY;
            };
            Rectangle.prototype.intersects = function (other) {
                // code from Java platform
                var tw = this.width;
                var th = this.height;
                var rw = other.width;
                var rh = other.height;
                if (rw <= 0 || rh <= 0 || tw <= 0 || th <= 0)
                    return false;
                var tx = this.leftX;
                var ty = this.topY;
                var rx = other.leftX;
                var ry = other.topY;
                rw += rx;
                rh += ry;
                tw += tx;
                th += ty;
                // overflow || intersect
                return ((rw < rx || rw > tx) &&
                    (rh < ry || rh > ty) &&
                    (tw < tx || tw > rx) &&
                    (th < ty || th > ry));
            };
            return Rectangle;
        })();
        Geometry.Rectangle = Rectangle;
    })(Geometry = Dunjax.Geometry || (Dunjax.Geometry = {}));
})(Dunjax || (Dunjax = {}));
//# sourceMappingURL=Rectangle.js.map