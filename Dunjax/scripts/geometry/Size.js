var Dunjax;
(function (Dunjax) {
    var Geometry;
    (function (Geometry) {
        var Size = (function () {
            function Size() {
            }
            /* Implementation */
            Size.prototype.set = function (width, height) {
                this.width = width;
                this.height = height;
                return this;
            };
            /* Implementation */
            Size.prototype.setTo = function (to) {
                this.width = to.width;
                this.height = to.height;
                return this;
            };
            return Size;
        })();
        Geometry.Size = Size;
    })(Geometry = Dunjax.Geometry || (Dunjax.Geometry = {}));
})(Dunjax || (Dunjax = {}));
//# sourceMappingURL=Size.js.map