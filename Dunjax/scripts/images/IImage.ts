﻿module Dunjax.Images
{
    /**
     * An image displayed by the program.
     */
    export interface IImage
    {
        /**
         * A key used by game framework code to locate this image's underlying 
         * data representation.
         */
        key: string;

        /**
         * This image's size, in pixels.
         */
        width: number;
        height: number;
    }
}