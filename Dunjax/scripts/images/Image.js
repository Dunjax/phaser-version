/// <reference path="../Program.ts" />
var Dunjax;
(function (Dunjax) {
    var Images;
    (function (Images) {
        var Image = (function () {
            function Image(path) {
                this.key = path;
                var game = Dunjax.Program.phaserGame;
                game.load.image(this.key, "images/" + path + ".png");
                var self = this;
                game.load.onLoadComplete.addOnce(function () { return self.image = game.cache.getImage(self.key); });
            }
            Object.defineProperty(Image.prototype, "width", {
                get: function () { return this.image.width; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Image.prototype, "height", {
                get: function () { return this.image.height; },
                enumerable: true,
                configurable: true
            });
            return Image;
        })();
        Images.Image = Image;
    })(Images = Dunjax.Images || (Dunjax.Images = {}));
})(Dunjax || (Dunjax = {}));
