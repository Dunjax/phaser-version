﻿/// <reference path="../Program.ts" />

module Dunjax.Images
{
    /* Implementation */
    export class Image implements IImage
    {
        /* Implementation */
        key: string;

        /**
         * The actual image this object wraps.
         */
        private image: Phaser.Image;

        /* Implementation */
        get width(): number { return this.image.width; }
        get height(): number { return this.image.height; }

        constructor(path: string)
        {
            this.key = path;

            // load the image from the given path
            var game = Program.game;
            game.load.image(this.key, `images/${path}.png`);
            var self = this;
            game.load.onLoadComplete.addOnce(
                () => self.image = game.cache.getImage(self.key));
        }
    }
} 