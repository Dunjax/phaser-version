﻿module Dunjax
{
    /**
     * Abstracts user input from (potentially) multiple input devices.
     */
    export interface IUserInput
    {
        /**
         * Returns whether the given command is currently being given by the user.
         */
        isSignaled(command: Command): boolean;
    }

    /**
     * The different commands the user may input.
     */
    export enum Command { Left, Right, Up, Down, Swing, SwitchGuns, Fire, Jump };
} 