﻿ module Dunjax
 {
     import DPad = Phaser.VirtualJoystick.DPad;
     import Button = Phaser.VirtualJoystick.Button;

     /* Implementation */
    export class UserInput implements IUserInput
    {
        /**
         * Keyboard keys corresponding to the player commands specified in the Command enum.
         */
        private commandKeys: Phaser.Key[];

        /**
         * The virtual d-pad used for user input, and its associated buttons.
         */
        dPad: DPad;
        buttonARight: Button;
        buttonB: Button;
        buttonC: Button;
        buttonALeft: Button;

        constructor()
        {
            // populate the command-keys array with the keys for the various commands
            const keyboard = Program.game.input.keyboard;
            const cursors = keyboard.createCursorKeys();
            this.commandKeys = [
                cursors.left,
                cursors.right,
                cursors.up,
                cursors.down,
                keyboard.addKey(Phaser.Keyboard.B),
                keyboard.addKey(Phaser.Keyboard.F),
                keyboard.addKey(Phaser.Keyboard.C),
                keyboard.addKey(Phaser.Keyboard.V),
            ];
        }

        /* Implementation */
        isSignaled(command: Command): boolean
        {
            const dPad = this.dPad;
            if (dPad.isDown) {
                if (command === Command.Left && dPad.direction === Phaser.LEFT) return true;
                if (command === Command.Right && dPad.direction === Phaser.RIGHT) return true;
                if (command === Command.Up && dPad.direction === Phaser.UP) return true;
                if (command === Command.Down && dPad.direction === Phaser.DOWN) return true;
            }

            if (command === Command.Jump && this.buttonC.isDown) return true;
            if (command === Command.Fire && this.buttonB.isDown) return true;
            if (command === Command.Swing && this.buttonARight.isDown) return true;
            if (command === Command.SwitchGuns && this.buttonALeft.isDown) return true;

            // return whether the keyboard corresponding to the given command is down
            return this.commandKeys[command].isDown;
        }
    }
} 