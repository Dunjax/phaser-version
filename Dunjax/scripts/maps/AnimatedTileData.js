var Dunjax;
(function (Dunjax) {
    var Maps;
    (function (Maps) {
        var AnimatedTileData = (function () {
            function AnimatedTileData() {
                this.tileTypes = [];
                this.index = 0;
            }
            AnimatedTileData.prototype.checkForAdvance = function () {
                if (this.durationUntilNext.isDone)
                    return this.advance();
                return false;
            };
            AnimatedTileData.prototype.advance = function () {
                this.index++;
                if (this.index >= this.tileTypes.length)
                    this.index = 0;
                var next = this.tileTypes[this.index];
                var visible = false;
                var x = this.x, y = this.y;
                if (Dunjax.isInCamera(x, y) && !this.map.isTileDarkenedAtPixel(x, y)) {
                    this.map.setTileTypeAtPixel(x, y, next);
                    visible = true;
                }
                this.durationUntilNext.length = next.animationDuration;
                return visible;
            };
            return AnimatedTileData;
        })();
        Maps.AnimatedTileData = AnimatedTileData;
    })(Maps = Dunjax.Maps || (Dunjax.Maps = {}));
})(Dunjax || (Dunjax = {}));
