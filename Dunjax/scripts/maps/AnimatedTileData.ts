﻿module Dunjax.Maps
{
    import IDuration = Time.IDuration;
    import Duration = Time.Duration; 
    
    /**
     * Stores data about a tile within a map which cycles through a series of tile-types.
     */
    export class AnimatedTileData
    {
        /**
         * The map within which the tile is animating.
         */
        map: ITiledMap;

        /**
         * The tile's pixel location with the map.
         */
        x: number;
        y: number;
            
        /**
         * Keeps track of how long it's been since the last time the tile
         * advanced to the next tile-type in its animation sequence.
         */
        durationUntilNext: IDuration;

        /**
         * The sequence of tile-types through which the tile cycles as it animates.
         */
        sequence: ITileType[];

        /**
         * The index of the tile-type in the animation sequence which is currently
         * being displayed for the tile.
         */
        index = 0;

        constructor(map: ITiledMap, x: number, y: number, tileType: ITileType)
        {
            this.map = map;
            this.x = x;
            this.y = y;
            this.durationUntilNext = new Duration(tileType.animationDuration);
            this.sequence = tileType.animationSequence;
            this.index = this.sequence.indexOf(tileType);
        }

        /**
         * Checks to see whether the tile should advance to the next tile-type
         * in its animation series.  Returns whether a visible advance occurred.
         */
        checkForAdvance(): boolean
        {
            if (this.durationUntilNext.isDone) return this.advance();
            return false;
        }
            
        /**
         * Advances the tile to the next tile-type in its animation series.
         * 
         * Returns whether the advance is visible within the game's view.
         */
        private advance(): boolean
        {
            // advance to the next tile-type in the animation sequence, cycling when necessary
            this.index++;
            if (this.index >= this.sequence.length) this.index = 0;
            const next = this.sequence[this.index];
            this.durationUntilNext.length = next.animationDuration;

            // if the tile is currently within the game's camera, and is not sight-blocked
            let visible = false;
            const x = this.x, y = this.y;
            if (Dunjax.isInViewport(x, y) && !this.map.isTileDarkenedAtPixel(x, y)) {
                // the advance is visible, so make the change to the tile in the map
                this.map.setTileTypeAtPixel(x, y, next);
                visible = true;
            }

            return visible;
        }
    }
} 