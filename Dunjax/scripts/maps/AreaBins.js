var Dunjax;
(function (Dunjax) {
    var Maps;
    (function (Maps) {
        var AreaBins = (function () {
            function AreaBins() {
                this.bins = [];
                this.binWidth = 200;
                this.binHeight = 200;
            }
            AreaBins.prototype.addToBins = function (entity) {
                this.addToOrRemoveFromBins(entity, true);
            };
            AreaBins.prototype.removeFromBins = function (entity) {
                this.addToOrRemoveFromBins(entity, false);
            };
            AreaBins.prototype.addToOrRemoveFromBins = function (entity, add) {
                var binX = this.binX(entity.left);
                var binY = this.binY(entity.top);
                this.addToOrRemoveFromBin(entity, add, binX, binY);
                var binRightX = this.binX(entity.right);
                if (binRightX !== binX)
                    this.addToOrRemoveFromBin(entity, add, binRightX, binY);
                var binBottomY = this.binY(entity.bottom);
                if (binBottomY !== binY)
                    this.addToOrRemoveFromBin(entity, add, binX, binBottomY);
                if (binRightX !== binX && binBottomY !== binY)
                    this.addToOrRemoveFromBin(entity, add, binRightX, binBottomY);
            };
            AreaBins.prototype.addToOrRemoveFromBin = function (entity, add, binX, binY) {
                var bin = this.bins[binX][binY];
                if (add)
                    bin.push(entity);
                else
                    bin.splice(bin.indexOf(entity), 1);
            };
            AreaBins.prototype.binX = function (x) { return Math.floor(x / this.binWidth); };
            AreaBins.prototype.binY = function (y) { return Math.floor(y / this.binHeight); };
            AreaBins.prototype.getEntitiesFromBin = function (x, y) {
                return this.bins[this.binX(x)][this.binY(x)];
            };
            AreaBins.prototype.onMapSizeSet = function (width, height) {
                var numBinsWide = Math.ceil(width / this.binWidth + 1);
                var numBinsHigh = Math.ceil(height / this.binHeight + 1);
                var bins = this.bins;
                for (var i = 0; i < numBinsWide; i++) {
                    bins[i] = [];
                    for (var j = 0; j < numBinsHigh; j++) {
                        bins[i][j] = [];
                    }
                }
            };
            return AreaBins;
        })();
        Maps.AreaBins = AreaBins;
    })(Maps = Dunjax.Maps || (Dunjax.Maps = {}));
})(Dunjax || (Dunjax = {}));
