var Dunjax;
(function (Dunjax) {
    var Maps;
    (function (Maps) {
        (function (ImpassableEntitiesSet) {
            ImpassableEntitiesSet[ImpassableEntitiesSet["NoneSet"] = 0] = "NoneSet";
            ImpassableEntitiesSet[ImpassableEntitiesSet["BeingsSet"] = 1] = "BeingsSet";
            ImpassableEntitiesSet[ImpassableEntitiesSet["PlayerOnlySet"] = 2] = "PlayerOnlySet";
        })(Maps.ImpassableEntitiesSet || (Maps.ImpassableEntitiesSet = {}));
        var ImpassableEntitiesSet = Maps.ImpassableEntitiesSet;
        ;
        (function (TerrainFeature) {
            TerrainFeature[TerrainFeature["None"] = 0] = "None";
            TerrainFeature[TerrainFeature["LockedDoor"] = 1] = "LockedDoor";
            TerrainFeature[TerrainFeature["ClosedDoor"] = 2] = "ClosedDoor";
            TerrainFeature[TerrainFeature["OpenDoor"] = 3] = "OpenDoor";
            TerrainFeature[TerrainFeature["StartCave"] = 4] = "StartCave";
            TerrainFeature[TerrainFeature["ExitCave"] = 5] = "ExitCave";
            TerrainFeature[TerrainFeature["Ladder"] = 6] = "Ladder";
            TerrainFeature[TerrainFeature["SlideLeft"] = 7] = "SlideLeft";
            TerrainFeature[TerrainFeature["SlideRight"] = 8] = "SlideRight";
            TerrainFeature[TerrainFeature["Stalagmite"] = 9] = "Stalagmite";
            TerrainFeature[TerrainFeature["SlimePool"] = 10] = "SlimePool";
            TerrainFeature[TerrainFeature["Spikes"] = 11] = "Spikes";
        })(Maps.TerrainFeature || (Maps.TerrainFeature = {}));
        var TerrainFeature = Maps.TerrainFeature;
        ;
        Maps.pixelsPerTenFeet = 32;
    })(Maps = Dunjax.Maps || (Dunjax.Maps = {}));
})(Dunjax || (Dunjax = {}));
