﻿module Dunjax.Maps
{
    import Point = Geometry.Point;

    /**
     * A map in which the game play takes place.  A map specifies the layout of 
     * the terrain it contains.  Maps also contain map-entities of various sorts.
     */
    export interface IMap
    {
        /**
         * The entities contained within this map.
         */
        entities: IMapEntities;

        /**
         * Returns whether this map contains the given pixel location.
         */
        containsLocation(x: number, y: number): boolean;

        /**
         * Returns whether the given location on this map is passable, considering
         * beings as impassible if told to do so.
         */
        isPassible(x: number, y: number, considerBeings?: boolean): boolean;

        /**
         * Returns whether the 1-pixel wide vertical line on this map specified 
         * by the given parameters is passible.  (Though, only the ends 
         * and middle are checked.)  
         */
        isVerticalLinePassible(
            x: number, y: number, height: number, considerBeings?: boolean): boolean;
        
        /**
         * Returns whether the 1-pixel wide horizontal line on this map specified 
         * by the given parameters is passible.  (Though, only the ends 
         * and middle are checked.)  
         */
        isHorizontalLinePassible(
            x: number, y: number, width: number, considerBeings?: boolean): boolean;

        /**
         * Returns whether the given location on this map is fall-throughable.
         */
        isFallThroughable(x: number, y: number): boolean;

        /**
         * Returns whether the given location is visible from the given
         * from-location.
         */
        isLocationVisibleFrom(x: number, y: number, fromX: number, fromY: number): boolean;

        /**
         * Returns the center location of the presumably only instance of the
         * given terrain feature within this map.  Is used for purposes such as
         * finding the level entrance, to know where to start the player. 
         */
        getLocationOfTerrainFeature(feature: TerrainFeature): Point;

        /**
         * Returns the terrain feature found at the given location.
         */
        getTerrainFeatureAt(x: number, y: number): TerrainFeature;

        /**
         * Sets the given terrain feature into the given location on this map.
         * This replaces the previous feature that was there.
         * 
         * Currently, the only supported terrain feature is an open door. 
         */
        setTerrainFeatureAt(x: number, y: number, feature: TerrainFeature): void;

        /**
         * Tells this map to perform its action for the current game turn.  
         * The map is expected to give its inherent features a chance to act, as well.
         */
        act(): void;

        /**
         * Returns the y-value of the first location found at or above the one
         * given where that location is passible, and the location just below it 
         * is impassible.
         */
        getYJustAboveGround(x: number, y: number): number;

        /**
         * Returns the y-value of the first location found at or below the one
         * given where that location is passible, and the location just above it 
         * is impassible.
         */
        getYJustBelowCeiling(x: number, y: number): number;

        /**
         * Fires when some modification has been made to one or more tiles in this map
         * that would change line of sight blocking results.
         */
        onLineOfSightChangeOccurred: () => void;
    }

    /**
     * Abstract terrain features a map may contain.
     */
    export enum TerrainFeature {
        None, LockedDoor, ClosedDoor, OpenDoor, StartCave, ExitCave, Ladder,
        SlideLeft, SlideRight, Stalagmite, SlimePool, Spikes
    };

    /**
     * How many pixels in a map would equate to ten feet of distance in the world 
     * being modeled.
    */
    export const pixelsPerTenFeet = 32;
}