﻿module Dunjax.Maps
{
    import IMapEntity = Entities.IMapEntity;
    import IRectangle = Geometry.IRectangle;
    import IAnimationEntity = Entities.IAnimationEntity;
    import IItem = Entities.Items.IItem;
    import IBeing = Entities.Beings.IBeing;
    import IPlayer = Entities.Beings.Player.IPlayer;
    import IMonster = Entities.Beings.Monsters.IMonster;
    import Shot = Entities.Shots.IShot;
    import IMaster = Entities.Beings.Monsters.IMaster; 
    
    /**
     * A map in which the game play takes place.  A map specifies the layout of 
     * the terrain it contains.  Maps also contain map-entities of various sorts.
     */
    export interface IMapEntities
    {
        /**
         * The map in which the map-entities reside.
         */
        map: IMap;

        /**
         * The map-entities contained within the map.
         * Do not modify directly.  Call addEntity() and removeEntity(), instead.
         */
        entities: IMapEntity[];

        /**
         * The items contained within the map.
         * Do not modify directly.  Call addEntity() and removeEntity(), instead.
         */
        items: IItem[];

        /**
         * The monsters contained within the map.
         * Do not modify directly.  Call addEntity() and removeEntity(), instead.
         */
        monsters: IMonster[];

        /**
         * The shots contained within the map.
         * Do not modify directly.  Call addEntity() and removeEntity(), instead.
         */
        shots: Shot[];

        /**
         * The animation-entities contained within the map.
         * Do not modify directly.  Call addEntity() and removeEntity(), instead.
         */
        animationEntities: IAnimationEntity[];

        /**
         * Adds the given map-entity to those contained by the map.
         */
        addEntity(entity: IMapEntity): void;

        /**
         * Removes the given map-entity from those contained by the map.
         */
        removeEntity(entity: IMapEntity): void;

        /**
         * Returns the player (if any) which resides in the map.
         */
        player: IPlayer;
         
        /**
         * Returns the monster that's at the given location (or null, if there is 
         * none) within the map.  A dead monster will not be returned.
         */
        getMonsterAt(x: number, y: number): IMonster;

        /**
         * Returns the first being found which intersects the given pixel-bounds 
         * within the map.
         */
        getBeingInBounds(x: number, y: number, width: number, height: number): IBeing;

        /**
         * Returns the first animation entity found within the given bounds 
         * (or null, if there is none) within the map.
         */
        getAnimationEntityInBounds(bounds: IRectangle): IAnimationEntity;

        /**
         * Returns the first item found within the given bounds (or null, if there
         * is none) within the map.
         */
        getItemInBounds(bounds: IRectangle): IItem;

        /**
         * Returns the master (if any) which resides in the map.
         */
        master: IMaster;

        /**
         * Has the map's entities perform their actions for the current game turn.
         */
        act(): void;
    }
}
 