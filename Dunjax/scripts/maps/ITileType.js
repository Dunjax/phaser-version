var Dunjax;
(function (Dunjax) {
    var Maps;
    (function (Maps) {
        (function (SlideType) {
            SlideType[SlideType["NotASlide"] = 0] = "NotASlide";
            SlideType[SlideType["Left"] = 1] = "Left";
            SlideType[SlideType["Right"] = 2] = "Right";
        })(Maps.SlideType || (Maps.SlideType = {}));
        var SlideType = Maps.SlideType;
        ;
        Maps.tileWidth = 32, Maps.tileHeight = 32;
        function toTileX(x) {
            return Math.floor(x / Maps.tileWidth);
        }
        Maps.toTileX = toTileX;
        function toTileY(y) {
            return Math.floor(y / Maps.tileHeight);
        }
        Maps.toTileY = toTileY;
        function toTileLeft(x) {
            return x - x % Maps.tileWidth;
        }
        Maps.toTileLeft = toTileLeft;
        function toTileTop(y) {
            return y - y % Maps.tileHeight;
        }
        Maps.toTileTop = toTileTop;
        function toTileMidX(x) {
            return x - x % Maps.tileWidth + Maps.tileWidth / 2;
        }
        Maps.toTileMidX = toTileMidX;
        function toTileMidY(y) {
            return y - y % Maps.tileHeight + Maps.tileHeight / 2;
        }
        Maps.toTileMidY = toTileMidY;
    })(Maps = Dunjax.Maps || (Dunjax.Maps = {}));
})(Dunjax || (Dunjax = {}));
