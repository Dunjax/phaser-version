﻿module Dunjax.Maps
{
    /**
     * A square building-block of a map. 
     */
    export interface ITileType
    {
        /**
         * The index (or ID) of this tile's type amongst all others. These values
         * correspond with those stored in the map data file.
         */
        index: TileTypeId;
        
        /**
         * The terrain feature (if any) represented by this tile-type 
         * at the given map location.
         */
        getTerrainFeatureAt(x: number, y: number): TerrainFeature;

        /**
         * Whether this tile-type blocks line-of-sight.
         */
        isSightBlocking: boolean;
         
        /**
         * What slide-type this tile-type is.
         */
        slideType: SlideType;

        /**
         * How long should be spent on ths tile-type before moving on to 
         * the next one in its animation sequence (if it is in one). 
         */
        animationDuration: number;

        /**
         * The tile that comes after this one in this tile's animation sequence
         * (if there is one).
         */
        animationSequence: ITileType[];
        
        /**
         * Returns whether the point at the given map-location is fall-throughable.
         */
        isFallThroughable(x: number, y: number): boolean;

        /**
         * Returns whether the point at the given map-location is passible.
         */
        isPassible(x: number, y: number): boolean;
    }

    /**
     * The kinds of slides a tile may be.
     */
    export enum SlideType { NotASlide, Left, Right };
    	
    /**
     * Specifies a subregion of a tile's square shape.
     */
    export interface ITileRegion
    {
        /**
         * Returns whether this region contains the given coordinates within a tile.
         */
        contains(x: number, y: number): boolean;
    }

    /**
     * The size of a tile, in pixels.
     */
    export const tileWidth = 32, tileHeight = 32;

    /**
     * Returns the tile ordinate corresponding to the given pixel ordinate.
     */
    export function toTileX(x: number): number
    {
        return Math.floor(x / tileWidth);
    }

    /**
     * Returns the tile ordinate corresponding to the given pixel ordinate.
     */
    export function toTileY(y: number): number
    {
        return Math.floor(y / tileHeight);
    }

    /**
     * Returns the leftmost tile pixel ordinate corresponding to the given map pixel ordinate.
     */
    export function toTileLeft(x: number): number
    {
        return x - x % tileWidth;
    }

    /**
     * Returns the topmost tile pixel ordinate corresponding to the given map pixel ordinate.
     */
    export function toTileTop(y: number): number
    {
        return y - y % tileHeight;
    }

    /**
     * Returns a midway tile pixel ordinate corresponding to the given map pixel ordinate.
     */
    export function toTileMidX(x: number): number
    {
        return x - x % tileWidth + tileWidth / 2;
    }

    /**
     * Returns a midway tile pixel ordinate corresponding to the given map pixel ordinate.
     */
    export function toTileMidY(y: number): number
    {
        return y - y % tileHeight + tileHeight / 2;
    }
} 