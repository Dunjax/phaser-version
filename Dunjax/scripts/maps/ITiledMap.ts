﻿module Dunjax.Maps
{
    import Point = Geometry.Point;

    /**
     * A map whose terrain is composed of tiles.
     */
    export interface ITiledMap extends IMap
    {
        /**
         * Returns the tile-type at the given pixel-location within this map.
         */
        getTileTypeAtPixel(x: number, y: number): ITileType;

        /**
         * Returns the tile-type at the given tile-location within this map.
         */
        getTileType(x: number, y: number): ITileType;

        /**
         * Sets the tile-type at the given pixel-location within this map to that given.
         */
        setTileTypeAtPixel(x: number, y: number, tileType: ITileType): void;

        /**
         * Set the tile-type at the given tile-location within this map to that given.
         */
        setTileType(x: number, y: number, tileType: ITileType): void;

        /**
         * Changes the tile at the given pixel-location within this map 
         * to display as darkness.
         */
        darkenTileAtPixel(x: number, y: number): void;

        /**
         * Changes the tile at the given pixel-location within this map 
         * to display as itself, rather than darkness.
         */
        undarkenTileAtPixel(x: number, y: number): void;

        /**
         * Returns whether the tile at the given pixel-location within this map 
         * is currently displaying as darkness.
         */
        isTileDarkenedAtPixel(x: number, y: number): void;

        /**
         * Returns the center location of the tile at the given location.
         */
        getTileCenterLocation(x: number, y: number): Point;

        /**
         * Records which tiles in this map will animate, and creates the necessary
         * data structures for the animations to occur while the game is being
         * played. 
         */
        createTileAnimations(): void;
    }
}
