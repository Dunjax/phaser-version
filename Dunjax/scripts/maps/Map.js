/// <reference path="MapEntities.ts" />
/// <reference path="AnimatedTileData.ts" />
var Dunjax;
(function (Dunjax) {
    var Maps;
    (function (Maps) {
        var Duration = Dunjax.Time.Duration;
        var Map = (function () {
            function Map(tilemap) {
                this.entities = new Maps.MapEntities(this);
                this.animatedTileDatas = [];
                this.animateTilesDuration = new Duration(200);
                this.squareRootCache = [];
                this.visibilityCache = [];
                this.tilemap = tilemap;
                this.entities.onMapSizeSet(tilemap.widthInPixels, tilemap.heightInPixels);
            }
            Map.prototype.getTileTypeAtPixel = function (x, y) {
                if (!this.containsLocation(x, y))
                    return Maps.tileTypes[Maps.TileTypeId.Darkness];
                var tile = this.tilemap.getTileWorldXY(x, y);
                var darkenedIndex = tile["darkenedIndex"];
                var index = darkenedIndex != null ? darkenedIndex : tile.index;
                return Maps.tileTypes[index];
            };
            Map.prototype.getTileType = function (tileX, tileY) {
                return Maps.tileTypes[this.tilemap.getTile(tileX, tileY).index];
            };
            Map.prototype.setTileTypeAtPixel = function (x, y, tileType) {
                this.tilemap.layers[0]
                    .data[Maps.toTileY(y)][Maps.toTileX(x)].index = tileType.index;
            };
            Map.prototype.setTileType = function (tileX, tileY, tileType) {
                this.tilemap.layers[0].data[tileY][tileX].index = tileType.index;
            };
            Map.prototype.isTilePassible = function (x, y) {
                var tileType = this.getTileTypeAtPixel(x, y);
                return tileType.isPassible(x % Maps.tileWidth, y % Maps.tileHeight);
            };
            Map.prototype.isPassible = function (x, y, set) {
                if (!this.isTilePassible(x, y))
                    return false;
                var being;
                var entities = this.entities;
                var impassible = (set === Maps.ImpassableEntitiesSet.BeingsSet
                    && (being = entities.getBeingAt(x, y)) != null && !being.isPassible)
                    || (set === Maps.ImpassableEntitiesSet.PlayerOnlySet
                        && entities.player != null
                        && Dunjax.Geometry.rectContains(entities.player, x, y));
                return !impassible;
            };
            Map.prototype.containsLocation = function (x, y) {
                return x >= 0 && y >= 0
                    && x < this.tilemap.widthInPixels
                    && y < this.tilemap.heightInPixels;
            };
            Map.prototype.isFallThroughable = function (x, y) {
                return this.getTileTypeAtPixel(x, y)
                    .isFallThroughable(x % Maps.tileWidth, y % Maps.tileHeight);
            };
            Map.prototype.getVisibilityHash = function (fromX, fromY, toX, toY) {
                return (fromX << 12 | fromY).toString(16) + (toX << 12 | toY).toString(16);
            };
            Map.prototype.isLocationVisibleFrom = function (x, y, fromX, fromY) {
                if (!this.containsLocation(x, y))
                    return false;
                var fromTileX = Maps.toTileX(fromX), fromTileY = Maps.toTileY(fromY);
                var toTileX = Maps.toTileX(x), toTileY = Maps.toTileY(y);
                if (Math.abs(toTileX - fromTileX) <= 1
                    && Math.abs(toTileY - fromTileY) <= 1)
                    return true;
                var hash = this.getVisibilityHash(fromX, fromY, x, y);
                var value = this.visibilityCache[hash];
                if (value != undefined)
                    return value;
                var dx = x - fromX, dy = y - fromY;
                var squaredDistance = dx * dx + dy * dy;
                var distance = this.squareRootCache[squaredDistance];
                if (distance == undefined)
                    distance = this.squareRootCache[squaredDistance] = Math.sqrt(squaredDistance);
                var directionX = dx / distance;
                var directionY = dy / distance;
                var pixelsPerStep = Maps.tileWidth / 2;
                directionX *= pixelsPerStep;
                directionY *= pixelsPerStep;
                var visible = false;
                var testX = fromX, testY = fromY;
                var step = 0;
                var limit = 20 * Maps.tileWidth / pixelsPerStep;
                while (step++ <= limit) {
                    testX += directionX;
                    testY += directionY;
                    if (Math.floor(testX / Maps.tileWidth) === toTileX
                        && Math.floor(testY / Maps.tileHeight) === toTileY) {
                        visible = true;
                        break;
                    }
                    var tile = this.tilemap.getTileWorldXY(testX, testY);
                    if (tile == null || tile.index in Maps.sightBlockingTilesMap)
                        break;
                }
                this.visibilityCache[hash] = visible;
                return visible;
            };
            Map.prototype.getLocationOfTerrainFeature = function (feature) {
                var tileTypeId;
                if (feature === Maps.TerrainFeature.StartCave)
                    tileTypeId = Maps.TileTypeId.StartCaveDownRight;
                else if (feature === Maps.TerrainFeature.ExitCave)
                    tileTypeId = Maps.TileTypeId.ExitCaveDownLeft;
                else
                    return null;
                for (var i = 0; i < this.tilemap.width; i++) {
                    for (var j = 0; j < this.tilemap.height; j++) {
                        if (this.tilemap.getTile(i, j).index === tileTypeId) {
                            return this.asMapLocation(i, j);
                        }
                    }
                }
                return null;
            };
            Map.prototype.asMapLocation = function (x, y) {
                return {
                    x: x * Maps.tileWidth + Maps.tileWidth / 2,
                    y: y * Maps.tileHeight + Maps.tileHeight / 2
                };
            };
            Map.prototype.getTerrainFeatureAt = function (x, y) {
                return this.getTileTypeAtPixel(x, y)
                    .getTerrainFeatureAt(x % Maps.tileWidth, y % Maps.tileHeight);
            };
            Map.prototype.setTerrainFeatureAt = function (x, y, feature) {
                if (feature === Maps.TerrainFeature.OpenDoor) {
                    var tileX = Maps.toTileX(x), tileY = Maps.toTileY(y);
                    var doorType = this.getTileType(tileX, tileY);
                    var openDoorType = this.getOpenDoorTileTypeToReplaceClosedDoor(doorType);
                    this.setTileType(tileX, tileY, openDoorType);
                    this.visibilityCache = [];
                    this.lineOfSightChangeOccurred();
                    var bodies = this.tilemap.layers[0].bodies;
                    for (var i = 0; i < bodies.length; i++) {
                        var body = bodies[i];
                        if (Maps.toTileX(body.x) === tileX && Maps.toTileY(body.y) === tileY) {
                            bodies.splice(i, 1);
                            break;
                        }
                    }
                }
                else
                    throw new Error("Unsupported terrain feature");
            };
            Map.prototype.getOpenDoorTileTypeToReplaceClosedDoor = function (closedDoor) {
                var result = null;
                if (closedDoor.index === Maps.TileTypeId.ClosedDoorBrick
                    || closedDoor.index === Maps.TileTypeId.LockedDoorBrick)
                    result = Maps.tileTypes[Maps.TileTypeId.OpenDoorBrick];
                else if (closedDoor.index === Maps.TileTypeId.ClosedDoorCave
                    || closedDoor.index === Maps.TileTypeId.LockedDoorCave)
                    result = Maps.tileTypes[Maps.TileTypeId.OpenDoorCave];
                return result;
            };
            Map.prototype.act = function () {
                if (this.animateTilesDuration.isDone)
                    this.animateTiles();
            };
            Map.prototype.createTileAnimations = function () {
                for (var i = 0; i < this.tilemap.width; i++) {
                    for (var j = 0; j < this.tilemap.height; j++) {
                        var tile = this.tilemap.getTile(i, j);
                        var tileType = Maps.tileTypes[tile.index];
                        if (tileType.nextTileInAnimation != null)
                            this.createAnimationDataStorageForTile(i, j, tileType);
                    }
                }
            };
            Map.prototype.createAnimationDataStorageForTile = function (x, y, tileType) {
                var data = new Maps.AnimatedTileData();
                data.map = this;
                var loc = this.asMapLocation(x, y);
                data.x = loc.x;
                data.y = loc.y;
                data.durationUntilNext = new Duration(tileType.animationDuration);
                var next = tileType;
                do {
                    data.tileTypes.push(next);
                    next = next.nextTileInAnimation;
                } while (next !== tileType);
                this.animatedTileDatas.push(data);
            };
            Map.prototype.animateTiles = function () {
                var datasCount = this.animatedTileDatas.length;
                var isAnyVisibleAdvances = false;
                for (var i = 0; i < datasCount; i++) {
                    if (this.animatedTileDatas[i].checkForAdvance())
                        isAnyVisibleAdvances = true;
                }
                if (isAnyVisibleAdvances)
                    this.tilemap.layers[0].dirty = true;
            };
            Map.prototype.getTileCenterLocation = function (x, y) {
                return {
                    x: x - x % Maps.tileWidth + Maps.tileWidth / 2,
                    y: y - y % Maps.tileHeight + Maps.tileHeight / 2
                };
            };
            Map.prototype.getYJustAboveGround = function (x, y) {
                var limit = y - 4 * Maps.tileHeight;
                while (y > limit) {
                    if (this.isPassible(x, y) && !this.isPassible(x, y + 1))
                        return y;
                    y--;
                }
                throw new Error("Limit reached.");
            };
            Map.prototype.getYJustBelowCeiling = function (x, y) {
                var limit = y + 4 * Maps.tileHeight;
                while (y < limit) {
                    if (this.isPassible(x, y) && !this.isPassible(x, y - 1))
                        return y;
                    y++;
                }
                throw new Error("Limit reached.");
            };
            Map.prototype.darkenTileAtPixel = function (x, y) {
                var tile = this.tilemap.layers[0].data[Maps.toTileY(y)][Maps.toTileX(x)];
                tile.darkenedIndex = tile.index;
                tile.index = Maps.TileTypeId.Darkness;
            };
            Map.prototype.undarkenTileAtPixel = function (x, y) {
                var tile = this.tilemap.layers[0].data[Maps.toTileY(y)][Maps.toTileX(x)];
                tile.index = tile.darkenedIndex;
                tile.darkenedIndex = null;
            };
            Map.prototype.isTileDarkenedAtPixel = function (x, y) {
                var tile = this.tilemap.layers[0].data[Maps.toTileY(y)][Maps.toTileX(x)];
                return tile.darkenedIndex != null;
            };
            return Map;
        })();
        Maps.Map = Map;
    })(Maps = Dunjax.Maps || (Dunjax.Maps = {}));
})(Dunjax || (Dunjax = {}));
