﻿/// <reference path="MapEntities.ts" />
/// <reference path="AnimatedTileData.ts" />

module Dunjax.Maps
{
    import Tilemap = Phaser.Tilemap;
    import IDuration = Time.IDuration;
    import Duration = Time.Duration; 
    import Point = Geometry.Point;
    import IMonster = Entities.Beings.Monsters.IMonster; 
    
    /**
     * This map's terrain is implemented in terms of tiles, a fact which should
     * not be exposed to clients. 
     */
    export class Map implements ITiledMap
    {
        /**
         * The Phaser tilemap which this object wraps.  Holds all the tile values in the map.
         */
        private tilemap: Tilemap;

        /* Implementation */
        entities: IMapEntities = new MapEntities(this);
        
        /**
         * The data for the animated tiles within this map.  
         */
        private animatedTileDatas: AnimatedTileData[] = [];
        
        /**
         * Keeps track of how long it's been since the last time this map
         * checked its tiles for animation changes.  The duration amount should 
         * be a common factor of all the durations for the individual tiles
         * (to prevent those durations from being rendered inaccurate by this
         * one, as they aren't checked for completion until this one completes). 
         */
        private animateTilesDuration: IDuration = new Duration(200);

        /* Implementation */
        onLineOfSightChangeOccurred: () => void;

        /**
         * Caches square roots computed by isLocationVisibleFrom(),
         * so they don't have to be recalculated.
         */
        private squareRootCache: number[] = [];

        /**
         * Caches line-of-sight visibility results (between pairs of tiles) 
         * computed by isLocationVisibleFrom(), so they don't have to be recalculated.
         */
        private visibilityCache: IStringToBooleanMap = {};

        constructor(tilemap: Tilemap)
        {
            this.tilemap = tilemap;
        }

        /* Implementation */
        getTileTypeAtPixel(x: number, y: number): ITileType
        {
            if (!this.containsLocation(x, y)) return tileTypes[TileTypeId.Darkness];
            const tile = this.tilemap.getTileWorldXY(x, y);
            const darkenedIndex = tile.darkenedIndex;
            const index = darkenedIndex != null ? darkenedIndex : tile.index;
            return tileTypes[index];
        }

        /* Implementation */
        getTileType(tileX: number, tileY: number): ITileType
        {
            return tileTypes[this.tilemap.getTile(tileX, tileY).index];
        }

        /* Implementation */
        setTileTypeAtPixel(x: number, y: number, tileType: ITileType)
        {
            this.tilemap.layers[0]
                .data[toTileY(y)][toTileX(x)].index = tileType.index;
        }

        /* Implementation */
        setTileType(tileX: number, tileY: number, tileType: ITileType)
        {
            this.tilemap.layers[0].data[tileY][tileX].index = tileType.index;
        }

        /**
         * Returns whether the tile at the given world location in this map is passible.
         */
        protected isTilePassible(x: number, y: number): boolean
        {
            const tileType = this.getTileTypeAtPixel(x, y);
            return tileType.isPassible(x % tileWidth, y % tileHeight);
        }

        /* Implementation */
        isPassible(x: number, y: number, considerBeings = false): boolean
        {
            // if the tile at the given location isn't passible, the location 
            // isn't passible
            if (!this.isTilePassible(x, y)) return false;

            if (!considerBeings) return true;

            // return whether there are entities of the impassible set given
            // (if one was given) at the given location
            let monster: IMonster;
            const entities = this.entities;
            const impassible =
                ((monster = entities.getMonsterAt(x, y)) != null && !monster.isPassible)
                || (entities.player != null && Geometry.rectContains(entities.player, x, y));
            return !impassible;
        }
        
        /* Implementation */
        isVerticalLinePassible(
            x: number, y: number, height: number, considerBeings = true): boolean
        {
            const halfHeight = height / 2;
            const top = y - halfHeight, bottom = y + halfHeight - 1;
            if (!this.isTilePassible(x, y)
                || !this.isTilePassible(x, top)
                || !this.isTilePassible(x, bottom)) return false;

            if (considerBeings && this.entities.getBeingInBounds(x, top, 1, height) != null)
                return false;

            return true;
        }
        
        /* Implementation */
        isHorizontalLinePassible(
            x: number, y: number, width: number, considerBeings = true): boolean
        {
            const halfWidth = width / 2;
            const left = x - halfWidth, right = x + halfWidth - 1;
            if (!this.isTilePassible(x, y)
                || !this.isTilePassible(left, y)
                || !this.isTilePassible(right, y)) return false;

            if (considerBeings && this.entities.getBeingInBounds(left, y, width, 1) != null)
                return false;

            return true;
        }
        
        /**
         * Returns whether this map contains the given pixel location.
         */
        containsLocation(x: number, y: number): boolean
        {
            return x >= 0 && y >= 0
                && x < this.tilemap.widthInPixels
                && y < this.tilemap.heightInPixels;
        }

        /* Implementation */
        isFallThroughable(x: number, y: number): boolean
        {
            return this.getTileTypeAtPixel(x, y)
                .isFallThroughable(x % tileWidth, y % tileHeight);
        }

        /**
         * Conputes (and returns) a hash value from the given from- and to-locations.
         */
        private getVisibilityHash(
            fromX: number, fromY: number, toX: number, toY: number): string
        {
            return (fromX << 12 | fromY).toString(16) + (toX << 12 | toY).toString(16);
        }

        /* Implementation */
        isLocationVisibleFrom(x: number, y: number, fromX: number, fromY: number): boolean
        {
            // if the to-location is outside the map, it is not visible
            if (!this.containsLocation(x, y)) return false;

            // if the to-location is only a tile away (or less) from the from-location,
            // it is visible
            const fromTileY = Maps.toTileY(fromY);
            const fromTileX = Maps.toTileX(fromX);
            const toTileY = Maps.toTileY(y);
            const toTileX = Maps.toTileX(x);
            if (Math.abs(toTileX - fromTileX) <= 1
                && Math.abs(toTileY - fromTileY) <= 1) return true;

            // if we have a cached value for the visibility between the tiles corresponding
            // to the given coordinates, return it
            const hash = this.getVisibilityHash(fromX, fromY, x, y);
            const value = this.visibilityCache[hash];
            if (value != undefined) return value;

            // detm the direction of the line between the locations
            const dy = y - fromY;
            const dx = x - fromX;
            const squaredDistance = dx * dx + dy * dy;
            let distance = this.squareRootCache[squaredDistance];
            if (distance == undefined)
                distance = this.squareRootCache[squaredDistance] = Math.sqrt(squaredDistance);
            let directionX = dx / distance;
            let directionY = dy / distance;
            const pixelsPerStep = tileWidth / 2;
            directionX *= pixelsPerStep;
            directionY *= pixelsPerStep;

            // for each step along the line
            let visible = false;
            let testY = fromY;
            let testX = fromX;
            let step = 0;
            const limit = 20 * tileWidth / pixelsPerStep;
            while (step++ <= limit) {
                // detm the location of the next step on the line being tested
                testX += directionX;
                testY += directionY;

                // if we've reached the tile containing the given to-location
                const tileX = Math.floor(testX / tileWidth);
                const tileY = Math.floor(testY / tileHeight);
                if (tileX === toTileX && tileY === toTileY) {
                    // we should not perform any further checks, as the destination 
                    // tile should not affect the result
                    visible = true;
                    break;
                }

                // if the tile at this step blocks sight, then the given 
                // location is not visible
                const tile = this.tilemap.getTile(tileX, tileY);
                if (tile == null || tile.index in sightBlockingTilesMap) break;
            }

            // cache the visibility value we've determined
            this.visibilityCache[hash] = visible;

            return visible;
        }

        /* Implementation */
        getLocationOfTerrainFeature(feature: TerrainFeature): Point
        {
            // map the given terrain-feature to its corresponding tile-type
            let tileTypeId: TileTypeId;
            if (feature === TerrainFeature.StartCave)
                tileTypeId = TileTypeId.StartCaveDownRight;
            else if (feature === TerrainFeature.ExitCave)
                tileTypeId = TileTypeId.ExitCaveDownLeft;
            else return null;
        		
            // for each entry in this map's tiles-grid
            for (let i = 0; i < this.tilemap.width; i++) {
                for (let j = 0; j < this.tilemap.height; j++) {
                    // if the tile-type at this entry is the same as the one given
                    if (this.tilemap.getTile(i, j).index === tileTypeId) {
                        // return the location of this spot
                        return this.asMapLocation(i, j);
                    }
                }
            }

            return null;
        }
        
        /**
         * Returns the location of the center of the tile at the given 
         * tile-coordinates within this map.
         */
        protected asMapLocation(x: number, y: number): Point
        {
            return {
                x: x * tileWidth + tileWidth / 2,
                y: y * tileHeight + tileHeight / 2
            };
        }

        /* Implementation */
        getTerrainFeatureAt(x: number, y: number): TerrainFeature
        {
            // return the terrain-feature of the tile-type that's at the 
            // given location
            return this.getTileTypeAtPixel(x, y)
                .getTerrainFeatureAt(x % tileWidth, y % tileHeight);
        }

        /* Implementation */
        setTerrainFeatureAt(x: number, y: number, feature: TerrainFeature)
        {
            // if an open door is what's desired, replace the closed door at the
            // given location with the corresponding open door
            if (feature === TerrainFeature.OpenDoor) {
                const tileY = toTileY(y);
                const tileX = toTileX(x);
                const doorType = this.getTileType(tileX, tileY);
                const openDoorType = this.getOpenDoorTileTypeToReplaceClosedDoor(doorType);
                this.setTileType(tileX, tileY, openDoorType);
                this.visibilityCache = {};
                this.onLineOfSightChangeOccurred();
                const bodies = this.tilemap.layers[0].bodies;
                for (let i = 0; i < bodies.length; i++) {
                    const body = bodies[i];
                    if (toTileX(body.x) === tileX && toTileY(body.y) === tileY) {
                        bodies.splice(i, 1);
                        break;
                    }
                }
            }

            else throw new Error("Unsupported terrain feature");
        }

        /**
         * Returns the kind of open door tile-type that corresponds to the given
         * closed door tile-type.
         */
        protected getOpenDoorTileTypeToReplaceClosedDoor(closedDoor: ITileType): ITileType
        {
            let result: ITileType = null;
            if (closedDoor.index === TileTypeId.ClosedDoorBrick
                || closedDoor.index === TileTypeId.LockedDoorBrick)
                result = tileTypes[TileTypeId.OpenDoorBrick];
            else if (closedDoor.index === TileTypeId.ClosedDoorCave
                || closedDoor.index === TileTypeId.LockedDoorCave)
                result = tileTypes[TileTypeId.OpenDoorCave];
            return result;
        }

        /* Implementation */
        act()
        {
            // if it's time to animate this map's tiles, do so
            if (this.animateTilesDuration.isDone) this.animateTiles();
        }
        
        /* Implementation */
        createTileAnimations()
        {
            // for each tile in this map
            for (let i = 0; i < this.tilemap.width; i++) {
                for (let j = 0; j < this.tilemap.height; j++) {
                    const tile = this.tilemap.getTile(i, j); 
                    // if this tile's type has an animation series, 
                    // create a data structure to manage the animation
                    const tileType = tileTypes[tile.index];
                    if (tileType.animationSequence != null)
                        this.createAnimationDataStorageForTile(i, j, tileType);
                }
            }
        }
        
        /**
         * Creates a data structure to manage the animation of a tile (where the
         * given tile-type is part of the animation series to use) at the given
         * tile-location.
         */
        protected createAnimationDataStorageForTile(
            x: number, y: number, tileType: ITileType)
        {
            // create a data structure to keep track of this tile's
            // progression through its animation
            const loc = this.asMapLocation(x, y);
            const data = new AnimatedTileData(this, loc.x, loc.y, tileType);
            this.animatedTileDatas.push(data);
        }
        
        /**
         * Checks each animating tile in this map to see if it should advance to 
         * the next step in its animation.
         */
        protected animateTiles()
        {
            // for each animating tile within this map
            const datasCount = this.animatedTileDatas.length;
            let isAnyVisibleAdvances = false;
            for (let i = 0; i < datasCount; i++) {
                // check whether this tile's animation should advance
                if (this.animatedTileDatas[i].checkForAdvance())
                    isAnyVisibleAdvances = true;
            }

            if (isAnyVisibleAdvances) 
                this.tilemap.layers[0].dirty = true;
        }

        /* Implementation */
        getTileCenterLocation(x: number, y: number): Point
        {
            return {
                x: x - x % tileWidth + tileWidth / 2,
                y: y - y % tileHeight + tileHeight / 2
            };
        }

        /* Implementation */
        getYJustAboveGround(x: number, y: number): number
        {
            // keep doing this, starting at the given location
            const limit = y - 4 * tileHeight;
            while (y > limit) {
                // if the current test location is passible, and the location below
                // it by the given height is not passible, then we have our result
                if (this.isPassible(x, y) && !this.isPassible(x, y + 1)) return y;

                // try the next location upwards next time
                y--;
            }

            throw new Error("Limit reached.");
        }

        /* Implementation */
        getYJustBelowCeiling(x: number, y: number): number
        {
            // keep doing this, starting at the given location
            const limit = y + 4 * tileHeight;
            while (y < limit) {
                // if the current test location is passible, and the location below
                // it by the given height is not passible, then we have our result
                if (this.isPassible(x, y) && !this.isPassible(x, y - 1)) return y;

                // try the next location downwards next time
                y++;
            }

            throw new Error("Limit reached.");
        }

        /* Implementation */
        darkenTileAtPixel(x: number, y: number)
        {
            const tile = this.tilemap.layers[0].data[toTileY(y)][toTileX(x)];
            tile.darkenedIndex = tile.index;
            tile.index = TileTypeId.Darkness;
        }

        /* Implementation */
        undarkenTileAtPixel(x: number, y: number)
        {
            const tile = this.tilemap.layers[0].data[toTileY(y)][toTileX(x)];
            tile.index = tile.darkenedIndex;
            tile.darkenedIndex = null;
        }

        /* Implementation */
        isTileDarkenedAtPixel(x: number, y: number)
        {
            const tile = this.tilemap.layers[0].data[toTileY(y)][toTileX(x)];
            return tile.darkenedIndex != null;
        }
    }
} 