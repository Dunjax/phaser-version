/// <reference path="../entities/beings/monsters/IMonster.ts" />
var Dunjax;
(function (Dunjax) {
    var Maps;
    (function (Maps) {
        var Monsters = Dunjax.Entities.Beings.Monsters;
        var Beings = Dunjax.Entities.Beings;
        var Player = Dunjax.Entities.Beings.Player;
        var Shots = Dunjax.Entities.Shots;
        var Items = Dunjax.Entities.Items;
        var MapEntities = (function () {
            function MapEntities(map) {
                this.entities = [];
                this.beings = [];
                this.monsters = [];
                this.shots = [];
                this.items = [];
                this.animationEntities = [];
                this.beingAreaBins = new Maps.AreaBins();
                this.map = map;
            }
            MapEntities.prototype.onMapSizeSet = function (width, height) {
                this.beingAreaBins.onMapSizeSet(width, height);
            };
            MapEntities.prototype.getPlayerAt = function (x, y) {
                var player = this.player;
                if (player == null)
                    return null;
                return Dunjax.Geometry.rectContains(player, x, y) ? player : null;
            };
            MapEntities.prototype.getBeingAt = function (x, y) {
                return this.getMapEntityAt(x, y, this.beingAreaBins.getEntitiesFromBin(x, y));
            };
            MapEntities.prototype.getMonsterAt = function (x, y) {
                var being = this.getBeingAt(x, y);
                if (being == null)
                    return null;
                var monster = Monsters.asMonster(being);
                if (monster != null)
                    return monster;
                return this.getMapEntityAt(x, y, this.monsters);
            };
            MapEntities.prototype.getMapEntityAt = function (x, y, entities) {
                for (var i = 0; i < entities.length; i++) {
                    var entity = entities[i];
                    if (!Dunjax.Geometry.rectContains(entity, x, y))
                        continue;
                    var being = Beings.asBeing(entity);
                    if (being != null && being.isDead)
                        continue;
                    return entity;
                }
                return null;
            };
            MapEntities.prototype.getAnimationEntityInBounds = function (bounds) {
                return this.getMapEntityIn(bounds, this.animationEntities);
            };
            MapEntities.prototype.getItemInBounds = function (bounds) {
                return this.getMapEntityIn(bounds, this.items);
            };
            MapEntities.prototype.getMapEntityIn = function (rect, entities) {
                for (var i = 0; i < entities.length; i++) {
                    var entity = entities[i];
                    if (Dunjax.Geometry.intersectsWith(entity, rect))
                        return entity;
                }
                return null;
            };
            MapEntities.prototype.addEntity = function (entity) {
                entity.map = this.map;
                this.entities.push(entity);
                var being = Beings.asBeing(entity);
                if (being != null) {
                    this.beings.push(being);
                    this.beingAreaBins.addToBins(being);
                    var monster = Monsters.asMonster(being);
                    if (monster != null)
                        this.monsters.push(monster);
                    var player = Player.asPlayer(being);
                    if (player != null)
                        this.player = player;
                }
                var shot = Shots.asShot(entity);
                if (shot != null)
                    this.shots.push(shot);
                var anim = Dunjax.Entities.asAnimationEntity(entity);
                if (anim != null) {
                    this.animationEntities.push(anim);
                    var item = Items.asItem(anim);
                    if (item != null)
                        this.items.push(item);
                }
            };
            MapEntities.prototype.removeEntity = function (entity) {
                entity.map = null;
                this.entities.remove(entity);
                var being = Beings.asBeing(entity);
                if (being != null) {
                    this.beings.remove(being);
                    this.beingAreaBins.removeFromBins(being);
                    var monster = Monsters.asMonster(being);
                    if (monster != null)
                        this.monsters.remove(monster);
                    var player = Player.asPlayer(being);
                    if (player != null)
                        this.player = null;
                }
                var shot = Shots.asShot(entity);
                if (shot != null)
                    this.shots.remove(shot);
                var anim = Dunjax.Entities.asAnimationEntity(entity);
                if (anim != null) {
                    this.animationEntities.remove(anim);
                    var item = Items.asItem(anim);
                    if (item != null)
                        this.items.remove(item);
                }
            };
            MapEntities.prototype.onBeingActed = function (b) {
                var bins = this.beingAreaBins;
                bins.removeFromBins(b);
                bins.addToBins(b);
            };
            return MapEntities;
        })();
        Maps.MapEntities = MapEntities;
    })(Maps = Dunjax.Maps || (Dunjax.Maps = {}));
})(Dunjax || (Dunjax = {}));
