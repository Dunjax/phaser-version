﻿/// <reference path="../entities/beings/monsters/IMonster.ts" />

module Dunjax.Maps
{
    import IMapEntity = Entities.IMapEntity;
    import IRectangle = Geometry.IRectangle;
    import IAnimationEntity = Entities.IAnimationEntity;
    import IItem = Entities.Items.IItem;
    import IPlayer = Entities.Beings.Player.IPlayer;
    import IMonster = Entities.Beings.Monsters.IMonster;
    import Monsters = Entities.Beings.Monsters;
    import IShot = Entities.Shots.IShot;
    import Beings = Entities.Beings;
    import Player = Entities.Beings.Player;
    import Shots = Entities.Shots;
    import Items = Entities.Items;
    import IMaster = Entities.Beings.Monsters.IMaster;
    import IBeing = Entities.Beings.IBeing;
    import Master = Entities.Beings.Monsters.Master; 
    
    /**
     * Holds the map-entities which reside within an associated map.
     */
    export class MapEntities implements IMapEntities
    {
        /* Implementation */
        map: IMap;

        /* Implementation */
        entities: IMapEntity[] = [];

        /* Implementation */
        player: IPlayer;
         
        /* Implementation */
        monsters: IMonster[] = [];

        /**
         * The subset of the monsters in the map which intersect the game's camera.
         * For most purposes, these may be considered instead of the entire set of monsters.
         */
        private monstersInOrNearViewport: IMonster[] = [];

        /* Implementation */
        shots: IShot[] = [];

        /* Implementation */
        items: IItem[] = [];

        /**
         * The subset of the items in the map which intersect the game's camera.
         * For most purposes, these may be considered instead of the entire set of items.
         */
        private itemsInOrNearViewport: IItem[] = [];

        /* Implementation */
        animationEntities: IAnimationEntity[] = [];

        /**
         * The subset of the animation-entities in the map which intersect the game's camera.
         * For most purposes, these may be considered instead of the entire set 
         * of animation-entities.
         */
        private animationEntitiesInOrNearViewport: IAnimationEntity[] = [];

        /* Implementation */
        master: IMaster;

        constructor(map: Map)
        {
            this.map = map;
        }
        
        /* Implementation */
        getMonsterAt(x: number, y: number): IMonster
        {
            return this.getMapEntityAt(x, y, this.monstersInOrNearViewport);
        }

        /**
         * Returns the entity (out of those found in the given list)
         * which occupies the given location within this map.  If no such entity 
         * is found, null is returned.  
         */
        private getMapEntityAt<T extends IMapEntity>(x: number, y: number, entities: T[]): T
        {
            // for each entity in the given list
            for (let i = 0; i < entities.length; i++) {
                const entity = entities[i]; 
                
                // if entity contains the given location, return it
                if (Geometry.rectContains(entity, x, y)) return entity;
            }

            return null;
        }

        /* Implementation */
        getBeingInBounds(x: number, y: number, width: number, height: number): IBeing
        {
            if (Geometry.intersectsWith2(this.player, x, y, width, height)) return this.player;

            return this.getMapEntityIn2(x, y, width, height, this.monstersInOrNearViewport);
        }

        /* Implementation */
        getAnimationEntityInBounds(bounds: IRectangle): IAnimationEntity
        {
            return this.getMapEntityIn(bounds, this.animationEntitiesInOrNearViewport);
        }

        /* Implementation */
        getItemInBounds(bounds: IRectangle): IItem
        {
            return this.getMapEntityIn(bounds, this.itemsInOrNearViewport);
        }

        /**
         * Returns a list of the entities (out of those found in the given list)
         * which intersect the given rectangle.  If no such entities are found,
         * null is returned.
         */
        private getMapEntityIn<T extends IMapEntity>(
            rect: IRectangle, entities: T[]): T
        {
            // for each map-entity in the given list
            for (let i = 0; i < entities.length; i++) {
                const entity = entities[i]; 
                // if this map-entity's bounds intersect the given rectangle,
                // return it as our result
                if (Geometry.intersectsWith(entity, rect)) return entity;
            }

            return null;
        }

        /**
         * Returns a list of the entities (out of those found in the given list)
         * which intersect the given rectangle.  If no such entities are found,
         * null is returned.
         */
        private getMapEntityIn2<T extends IMapEntity>(
            x: number, y: number, width: number, height: number, entities: T[]): T
        {
            // for each map-entity in the given list
            for (let i = 0; i < entities.length; i++) {
                const entity = entities[i]; 
                // if this map-entity's bounds intersect the given rectangle,
                // return it as our result
                const r = entity;
                if (Geometry.intersects(r.left, r.top, r.width, r.height, x, y, width, height))
                    return entity;
            }

            return null;
        }

        /* Implementation */
        addEntity(entity: IMapEntity)
        {
            entity.map = this.map;
            
            // add the entity to this object's appropriate list(s) of such entities.
            this.entities.push(entity);

            const being = Beings.asBeing(entity);
            if (being != null) {
                const monster = Monsters.asMonster(being);
                if (monster != null) {
                    this.monsters.push(monster);
                    if (monster instanceof Master) 
                        this.master = monster;
                }
                const player = Player.asPlayer(being);
                if (player != null) this.player = player;
            }

            const shot = Shots.asShot(entity);
            if (shot != null) this.shots.push(shot);

            const anim = Entities.asAnimationEntity(entity);
            if (anim != null) {
                this.animationEntities.push(anim);
                const item = Items.asItem(anim);
                if (item != null) this.items.push(item);
            }
        }

        /* Implementation */
        removeEntity(entity: IMapEntity)
        {
            entity.map = null;
            
            // remove the entity from the lists of this map that were
            // containing it
            this.entities.remove(entity);

            const being = Beings.asBeing(entity);
            if (being != null) {
                const monster = Monsters.asMonster(being);
                if (monster != null) this.monsters.remove(monster);
                const player = Player.asPlayer(being);
                if (player != null) this.player = null;
            }

            const shot = Shots.asShot(entity);
            if (shot != null) this.shots.remove(shot);

            const anim = Entities.asAnimationEntity(entity);
            if (anim != null) {
                this.animationEntities.remove(anim);
                const item = Items.asItem(anim);
                if (item != null) this.items.remove(item);
            }
        }

        /* Implementation */
        act()
        {
            // determine for each entity in the map whether it is close enough to the player 
            // to be given an opportunity to act
            this.entities.forEach(e => e.determineIsInOrNearViewport());

            // update our lists of which entities are close enough to the game's viewport
            // to be considered relevant for this game turn
            this.monstersInOrNearViewport =
                this.monsters.filter(m => m.isInOrNearViewport && !m.isDead);
            this.itemsInOrNearViewport = this.items.filter(i => i.isInOrNearViewport);
            this.animationEntitiesInOrNearViewport =
                this.animationEntities.filter(a => a.isInOrNearViewport);

            // have each entity act which is close enough to the player
            this.entities.forEach(e => { if (e.isInOrNearViewport) e.act.call(e) });
        }
    }
}

