/// <reference path="ITileType.ts" />
/// <reference path="IMap.ts" />
var Dunjax;
(function (Dunjax) {
    var Maps;
    (function (Maps) {
        var TileType = (function () {
            function TileType(index, terrainFeature, terrainFeatureRegion) {
                if (terrainFeature === void 0) { terrainFeature = Maps.TerrainFeature.None; }
                if (terrainFeatureRegion === void 0) { terrainFeatureRegion = Region.all; }
                this.passibleRegion = Region.all;
                this.slideType = Maps.SlideType.NotASlide;
                this.animationDuration = 200;
                this.index = index;
                this.terrainFeature = terrainFeature;
                this.terrainFeatureRegion = terrainFeatureRegion;
            }
            TileType.prototype.isFallThroughable = function (x, y) {
                var region = this.fallThroughableRegion;
                return region != null ? region.contains(x, y) : this.isPassible(x, y);
            };
            TileType.prototype.isPassible = function (x, y) {
                var region = this.passibleRegion;
                if (region === Region.all)
                    return true;
                if (region === Region.none)
                    return false;
                return region.contains(x, y);
            };
            TileType.prototype.getTerrainFeatureAt = function (x, y) {
                return this.terrainFeatureRegion.contains(x, y)
                    ? this.terrainFeature : Maps.TerrainFeature.None;
            };
            return TileType;
        })();
        Maps.TileType = TileType;
        var Region = (function () {
            function Region(contains) {
                this.contains = contains;
            }
            Region.initialize = function () {
                this.all = new Region(function () { return true; });
                this.none = new Region(function () { return false; });
                this.up = new Region(function (x, y) { return (y < Maps.tileHeight / 2); });
                this.down = new Region(function (x, y) { return (y >= Maps.tileHeight / 2); });
                this.upLeftInclusive = new Region(function (x, y) { return (y <= -(x) + (Maps.tileWidth - 1)); });
                this.upRightInclusive = new Region(function (x, y) { return (y <= x); });
                this.downLeftInclusive = new Region(function (x, y) { return (y >= x); });
                this.downRightInclusive = new Region(function (x, y) { return (y >= -(x) + (Maps.tileWidth - 1)); });
                this.upLeftExclusive = new Region(function (x, y) { return (y < -(x) + (Maps.tileWidth - 1)); });
                this.upRightExclusive = new Region(function (x, y) { return (y < x); });
                this.downLeftExclusive = new Region(function (x, y) { return (y > x); });
                this.downRightExclusive = new Region(function (x, y) { return (y > -(x) + (Maps.tileWidth - 1)); });
            };
            return Region;
        })();
        Maps.Region = Region;
        Region.initialize();
    })(Maps = Dunjax.Maps || (Dunjax.Maps = {}));
})(Dunjax || (Dunjax = {}));
