﻿/// <reference path="ITileType.ts" />
/// <reference path="IMap.ts" />

module Dunjax.Maps
{
    /* Implementation */
    export class TileType implements ITileType
    {
        /* Implementation */
        index: TileTypeId;
        
        /**
         * The terrain feature (if any) represented by this tile.
         */
        private terrainFeature: TerrainFeature;

        /**
         * The region of this tile to which the above terrain-feature applies.
         */
        private terrainFeatureRegion: Region;

	    /**
	     * Specifies which parts of this tile are passible.
	     */
        passibleRegion = Region.all;

	    /**
	     * Specifies which parts of this tile may be fallen through by the
	     * player. If no region is specified here, this value should be assumed to
	     * equal that for passibleRegion. Ladders are an example of where the values
	     * would differ between the two.
	     */
        fallThroughableRegion: Region;

        /* Implementation */
        isSightBlocking: boolean;

        /* Implementation */
        slideType = SlideType.NotASlide;

        /* Implementation */
        animationDuration = 200;

        /* Implementation */
        animationSequence: ITileType[];

        constructor(
            index: number, 
            terrainFeature: TerrainFeature = TerrainFeature.None,
            terrainFeatureRegion: Region = Region.all)
        {
            this.index = index;
            this.terrainFeature = terrainFeature;
            this.terrainFeatureRegion = terrainFeatureRegion;
        }

        /* Implementation */
        isFallThroughable(x: number, y: number): boolean
        {
            const region = this.fallThroughableRegion;
            return region != null ? region.contains(x, y) : this.isPassible(x, y);
        }

        /* Implementation */
        isPassible(x: number, y: number): boolean
        {
            const region = this.passibleRegion;
            if (region === Region.all) return true;
            if (region === Region.none) return false;
            return region.contains(x, y);
        }

        /* Implementation */
        getTerrainFeatureAt(x: number, y: number): TerrainFeature
        {
            return this.terrainFeatureRegion.contains(x, y)
                ? this.terrainFeature : TerrainFeature.None;
        }
    }

    /**
     * Specifies a subregion of a tile's square shape.
     */
    export class Region
    {
        /**
         * Returns whether this region contains the given location.  
         * The location is presumed to be a zero-based offset into an abstract tile's bounds.
         */
        contains: (x: number, y: number) => boolean;

        constructor(contains: (x: number, y: number) => boolean)
        {
            this.contains = contains;
        }

        /**
         * The flyweight tile regions.
         */
        static all: Region;
        static none: Region;
        static up: Region;
        static down: Region;
        static upLeftInclusive: Region;
        static upRightInclusive: Region;
        static downLeftInclusive: Region;
        static downRightInclusive: Region;
        static upLeftExclusive: Region;
        static upRightExclusive: Region;
        static downLeftExclusive: Region;
        static downRightExclusive: Region;

        /* StaticInitializer */
        static initialize()
        {
            this.all = new Region(() => true);
            this.none = new Region(() => false);
            this.up = new Region((x, y) => (y < tileHeight / 2));
            this.down = new Region((x, y) => (y >= tileHeight / 2));
            this.upLeftInclusive = new Region((x, y) => (y <= tileWidth - 1 - x));
            this.upRightInclusive = new Region((x, y) => (y <= x));
            this.downLeftInclusive = new Region((x, y) => (y >= x));
            this.downRightInclusive = new Region((x, y) => (y >= tileWidth - 1 - x));
            this.upLeftExclusive = new Region((x, y) => (y < tileWidth - 1 - x));
            this.upRightExclusive = new Region((x, y) => (y < x));
            this.downLeftExclusive = new Region((x, y) => (y > x));
            this.downRightExclusive = new Region((x, y) => (y > tileWidth - 1 - x));
        }
    }

    Region.initialize();
} 