/// <reference path="TileTypeId.ts" />
/// <reference path="TileType.ts" />
var Dunjax;
(function (Dunjax) {
    var Maps;
    (function (Maps) {
        Maps.numTileTypes = 240;
        Maps.tileTypes = [];
        Maps.impassibleTiles = [
            Maps.TileTypeId.WallRock1,
            Maps.TileTypeId.WallRock2,
            Maps.TileTypeId.ClosedDoorBrick,
            Maps.TileTypeId.LockedDoorBrick,
            Maps.TileTypeId.WallTerrace,
            Maps.TileTypeId.WallTerraceDown,
            Maps.TileTypeId.WallTerraceDownLeft,
            Maps.TileTypeId.WallTerraceDownRight,
            Maps.TileTypeId.ClosedDoorCave,
            Maps.TileTypeId.LockedDoorCave
        ];
        Maps.downHalfImpassibleTiles = [
            Maps.TileTypeId.RockBrick1, Maps.TileTypeId.RockBrick2,
            Maps.TileTypeId.TrashBrick1, Maps.TileTypeId.TrashBrick2, Maps.TileTypeId.TrashBrick3,
            Maps.TileTypeId.TrashBrick4,
            Maps.TileTypeId.RockCave1, Maps.TileTypeId.RockCave2,
            Maps.TileTypeId.TrashCave1, Maps.TileTypeId.TrashCave2, Maps.TileTypeId.TrashCave3,
            Maps.TileTypeId.TrashCave4,
            Maps.TileTypeId.Ledge1Left, Maps.TileTypeId.Ledge1a, Maps.TileTypeId.Ledge1b,
            Maps.TileTypeId.Ledge1c, Maps.TileTypeId.Ledge1Right,
        ];
        Maps.upHalfImpassibleTiles = [
            Maps.TileTypeId.ProjectingLedge,
        ];
        Maps.leftDownSlopingTiles = [
            Maps.TileTypeId.SlideTerraceBrickLeft,
            Maps.TileTypeId.SlideTerraceRockLeft,
            Maps.TileTypeId.SlideRockCaveLeft,
            Maps.TileTypeId.SlideRockBrickLeft,
        ];
        Maps.rightDownSlopingTiles = [
            Maps.TileTypeId.SlideTerraceBrickRight,
            Maps.TileTypeId.SlideTerraceRockRight,
            Maps.TileTypeId.SlideRockCaveRight,
            Maps.TileTypeId.SlideRockBrickRight,
        ];
        Maps.leftUpSlopingTiles = [
            Maps.TileTypeId.SlideTerraceBrickUpLeft,
            Maps.TileTypeId.SlideTerraceRockUpLeft,
            Maps.TileTypeId.SlideRockCaveUpLeft,
            Maps.TileTypeId.SlideRockBrickUpLeft,
        ];
        Maps.rightUpSlopingTiles = [
            Maps.TileTypeId.SlideTerraceBrickUpRight,
            Maps.TileTypeId.SlideTerraceRockUpRight,
            Maps.TileTypeId.SlideRockCaveUpRight,
            Maps.TileTypeId.SlideRockBrickUpRight,
        ];
        Maps.sightBlockingTiles = [
            Maps.TileTypeId.Darkness,
            Maps.TileTypeId.WallRock1,
            Maps.TileTypeId.WallRock2,
            Maps.TileTypeId.SecretDoorRock,
            Maps.TileTypeId.ClosedDoorBrick,
            Maps.TileTypeId.LockedDoorBrick,
            Maps.TileTypeId.TerraceRockUp,
            Maps.TileTypeId.SecretDoorTerraceWall,
            Maps.TileTypeId.WallTerraceDown,
            Maps.TileTypeId.WallTerraceDownLeft,
            Maps.TileTypeId.WallTerraceDownRight,
            Maps.TileTypeId.WallTerrace,
            Maps.TileTypeId.ClosedDoorCave,
            Maps.TileTypeId.LockedDoorCave,
        ];
        Maps.sightBlockingTilesMap = Object.create(null);
        function initializeTileTypes() {
            for (var i = 0; i < Maps.numTileTypes; i++)
                Maps.tileTypes[i] = new Maps.TileType(i);
            var replace = function (id, feature) {
                return Maps.tileTypes[id] = new Maps.TileType(id, feature);
            };
            replace(Maps.TileTypeId.LadderBrick, Maps.TerrainFeature.Ladder);
            replace(Maps.TileTypeId.ClosedDoorBrick, Maps.TerrainFeature.ClosedDoor);
            replace(Maps.TileTypeId.LockedDoorBrick, Maps.TerrainFeature.LockedDoor);
            replace(Maps.TileTypeId.ClosedDoorCave, Maps.TerrainFeature.ClosedDoor);
            replace(Maps.TileTypeId.LockedDoorCave, Maps.TerrainFeature.LockedDoor);
            [
                Maps.TileTypeId.StalagmiteCave1,
                Maps.TileTypeId.StalagmiteCave2,
                Maps.TileTypeId.StalagmiteCave3,
                Maps.TileTypeId.StalagmiteCave4,
                Maps.TileTypeId.StalagmiteCave5,
                Maps.TileTypeId.StalagmiteCave6,
                Maps.TileTypeId.StalagmiteCave7,
                Maps.TileTypeId.StalagmiteCave8,
                Maps.TileTypeId.StalagmiteCave9,
                Maps.TileTypeId.StalagmiteCave10,
                Maps.TileTypeId.StalagmiteCave11,
                Maps.TileTypeId.StalagmiteBrick1,
                Maps.TileTypeId.StalagmiteBrick2,
                Maps.TileTypeId.StalagmiteBrick3,
                Maps.TileTypeId.StalagmiteBrick4,
                Maps.TileTypeId.StalagmiteBrick5,
                Maps.TileTypeId.StalagmiteBrick6,
                Maps.TileTypeId.StalagmiteBrick7,
            ].forEach(function (id) { return replace(id, Maps.TerrainFeature.Stalagmite); });
            replace(Maps.TileTypeId.ExitCaveDownLeft, Maps.TerrainFeature.ExitCave);
            replace(Maps.TileTypeId.ExitCaveDownRight, Maps.TerrainFeature.ExitCave);
            replace(Maps.TileTypeId.LadderCave, Maps.TerrainFeature.Ladder);
            replace(Maps.TileTypeId.OpenDoorBrick, Maps.TerrainFeature.OpenDoor);
            replace(Maps.TileTypeId.OpenDoorCave, Maps.TerrainFeature.OpenDoor);
            replace(Maps.TileTypeId.StartCaveDownRight, Maps.TerrainFeature.StartCave);
            replace(Maps.TileTypeId.SpikesBrick, Maps.TerrainFeature.Spikes);
            replace(Maps.TileTypeId.SpikesCave, Maps.TerrainFeature.Spikes);
            [
                Maps.TileTypeId.SlimePoolBrick1,
                Maps.TileTypeId.SlimePoolBrick2,
                Maps.TileTypeId.SlimePoolBrick3,
                Maps.TileTypeId.SlimePoolCave1,
                Maps.TileTypeId.SlimePoolCave2,
                Maps.TileTypeId.SlimePoolCave3,
            ].forEach(function (id) { return replace(id, Maps.TerrainFeature.SlimePool); });
            var loop = function (types) {
                for (var i = 0; i < types.length; i++)
                    Maps.tileTypes[types[i]].nextTileInAnimation =
                        Maps.tileTypes[types[i < types.length - 1 ? i + 1 : 0]];
            };
            loop([
                Maps.TileTypeId.TorchBrick1,
                Maps.TileTypeId.TorchBrick2,
                Maps.TileTypeId.TorchBrick3,
                Maps.TileTypeId.TorchBrick4,
            ]);
            loop([
                Maps.TileTypeId.SmallFire1,
                Maps.TileTypeId.SmallFire2,
                Maps.TileTypeId.SmallFire3,
                Maps.TileTypeId.SmallFire4,
            ]);
            loop([
                Maps.TileTypeId.Fungi1a,
                Maps.TileTypeId.Fungi1b,
                Maps.TileTypeId.Fungi1c,
                Maps.TileTypeId.Fungi1d,
                Maps.TileTypeId.Fungi1e,
                Maps.TileTypeId.Fungi1f,
            ]);
            loop([
                Maps.TileTypeId.Fungi2a,
                Maps.TileTypeId.Fungi2b,
                Maps.TileTypeId.Fungi2c,
                Maps.TileTypeId.Fungi2d,
                Maps.TileTypeId.Fungi2e,
                Maps.TileTypeId.Fungi2f,
            ]);
            loop([
                Maps.TileTypeId.TrashBrick1,
                Maps.TileTypeId.TrashBrick2,
                Maps.TileTypeId.TrashBrick3,
                Maps.TileTypeId.TrashBrick4,
            ]);
            loop([
                Maps.TileTypeId.TrashCave1,
                Maps.TileTypeId.TrashCave2,
                Maps.TileTypeId.TrashCave3,
                Maps.TileTypeId.TrashCave4,
            ]);
            loop([
                Maps.TileTypeId.SlimePoolBrick1,
                Maps.TileTypeId.SlimePoolBrick2,
                Maps.TileTypeId.SlimePoolBrick3,
            ]);
            loop([
                Maps.TileTypeId.SlimePoolCave1,
                Maps.TileTypeId.SlimePoolCave2,
                Maps.TileTypeId.SlimePoolCave3,
            ]);
            var replace1 = function (id, feature, region) {
                return Maps.tileTypes[id] = new Maps.TileType(id, feature, region);
            };
            replace1(Maps.TileTypeId.SlideTerraceBrickLeft, Maps.TerrainFeature.SlideLeft, Maps.Region.downRightInclusive);
            replace1(Maps.TileTypeId.SlideTerraceBrickRight, Maps.TerrainFeature.SlideRight, Maps.Region.downLeftInclusive);
            replace1(Maps.TileTypeId.SlideTerraceRockLeft, Maps.TerrainFeature.SlideLeft, Maps.Region.downRightInclusive);
            replace1(Maps.TileTypeId.SlideTerraceRockRight, Maps.TerrainFeature.SlideRight, Maps.Region.downLeftInclusive);
            replace1(Maps.TileTypeId.SlideRockCaveLeft, Maps.TerrainFeature.SlideLeft, Maps.Region.downRightInclusive);
            replace1(Maps.TileTypeId.SlideRockCaveRight, Maps.TerrainFeature.SlideRight, Maps.Region.downLeftInclusive);
            replace1(Maps.TileTypeId.SlideRockBrickLeft, Maps.TerrainFeature.SlideLeft, Maps.Region.downRightInclusive);
            replace1(Maps.TileTypeId.SlideRockBrickRight, Maps.TerrainFeature.SlideRight, Maps.Region.downLeftInclusive);
            var setPassibleRegion = function (types, region) {
                types.forEach(function (type) { return Maps.tileTypes[type].passibleRegion = region; });
            };
            setPassibleRegion(Maps.impassibleTiles, Maps.Region.none);
            setPassibleRegion(Maps.downHalfImpassibleTiles, Maps.Region.up);
            setPassibleRegion(Maps.upHalfImpassibleTiles, Maps.Region.down);
            setPassibleRegion(Maps.leftDownSlopingTiles, Maps.Region.upLeftExclusive);
            setPassibleRegion(Maps.rightDownSlopingTiles, Maps.Region.upRightExclusive);
            setPassibleRegion(Maps.leftUpSlopingTiles, Maps.Region.downLeftExclusive);
            setPassibleRegion(Maps.rightUpSlopingTiles, Maps.Region.downRightExclusive);
            [
                Maps.TileTypeId.LadderBrick,
                Maps.TileTypeId.LadderCave,
            ].forEach(function (type) { return Maps.tileTypes[type].fallThroughableRegion = Maps.Region.none; });
            Maps.sightBlockingTiles.forEach(function (tileId) {
                Maps.sightBlockingTilesMap[tileId.toString()] = tileId;
                Maps.tileTypes[tileId].isSightBlocking = true;
            });
            var setAnimationDuration = function (types, duration) {
                types.forEach(function (type) { return Maps.tileTypes[type].animationDuration = duration; });
            };
            var slimePoolDuration = 600;
            setAnimationDuration([
                Maps.TileTypeId.SlimePoolBrick1,
                Maps.TileTypeId.SlimePoolBrick2,
                Maps.TileTypeId.SlimePoolBrick3,
                Maps.TileTypeId.SlimePoolCave1,
                Maps.TileTypeId.SlimePoolCave2,
                Maps.TileTypeId.SlimePoolCave3,
            ], slimePoolDuration);
        }
        initializeTileTypes();
    })(Maps = Dunjax.Maps || (Dunjax.Maps = {}));
})(Dunjax || (Dunjax = {}));
