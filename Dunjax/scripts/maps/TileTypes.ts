﻿/// <reference path="TileTypeId.ts" />
/// <reference path="TileType.ts" />

module Dunjax.Maps
{
    import ITileType = Maps.ITileType; 
    
    /**
     * The total number of tile-types there are in the game.
     */
    export var numTileTypes = 240;

    export var tileTypes: TileType[] = [];

    export var impassibleTiles: number[] = [
        TileTypeId.WallRock1,
        TileTypeId.WallRock2,
        TileTypeId.ClosedDoorBrick,
        TileTypeId.LockedDoorBrick,
        TileTypeId.WallTerrace,
        TileTypeId.WallTerraceDown,
        TileTypeId.WallTerraceDownLeft,
        TileTypeId.WallTerraceDownRight,
        TileTypeId.ClosedDoorCave,
        TileTypeId.LockedDoorCave
    ];

    export var downHalfImpassibleTiles: number[] = [
        TileTypeId.RockBrick1, TileTypeId.RockBrick2,
        TileTypeId.TrashBrick1, TileTypeId.TrashBrick2, TileTypeId.TrashBrick3,
        TileTypeId.TrashBrick4,
        TileTypeId.RockCave1, TileTypeId.RockCave2,
        TileTypeId.TrashCave1, TileTypeId.TrashCave2, TileTypeId.TrashCave3,
        TileTypeId.TrashCave4,
        TileTypeId.Ledge1Left, TileTypeId.Ledge1a, TileTypeId.Ledge1b,
        TileTypeId.Ledge1c, TileTypeId.Ledge1Right,
    ];

    export var upHalfImpassibleTiles: number[] = [
        TileTypeId.ProjectingLedge,
    ];

    export var leftDownSlopingTiles: number[] = [
        TileTypeId.SlideTerraceBrickLeft,
        TileTypeId.SlideTerraceRockLeft,
        TileTypeId.SlideRockCaveLeft,
        TileTypeId.SlideRockBrickLeft,
    ];

    export var rightDownSlopingTiles: number[] = [
        TileTypeId.SlideTerraceBrickRight,
        TileTypeId.SlideTerraceRockRight,
        TileTypeId.SlideRockCaveRight,
        TileTypeId.SlideRockBrickRight,
    ];

    export var leftUpSlopingTiles: number[] = [
        TileTypeId.SlideTerraceBrickUpLeft,
        TileTypeId.SlideTerraceRockUpLeft,
        TileTypeId.SlideRockCaveUpLeft,
        TileTypeId.SlideRockBrickUpLeft,
    ];

    export var rightUpSlopingTiles: number[] = [
        TileTypeId.SlideTerraceBrickUpRight,
        TileTypeId.SlideTerraceRockUpRight,
        TileTypeId.SlideRockCaveUpRight,
        TileTypeId.SlideRockBrickUpRight,
    ];

    export var sightBlockingTiles: number[] = [
        TileTypeId.Darkness,
        TileTypeId.WallRock1,
        TileTypeId.WallRock2,
        TileTypeId.SecretDoorRock,
        TileTypeId.ClosedDoorBrick,
        TileTypeId.LockedDoorBrick,
        TileTypeId.TerraceRockUp,
        TileTypeId.SecretDoorTerraceWall,
        TileTypeId.WallTerraceDown,
        TileTypeId.WallTerraceDownLeft,
        TileTypeId.WallTerraceDownRight,
        TileTypeId.WallTerrace,
        TileTypeId.ClosedDoorCave,
        TileTypeId.LockedDoorCave,
    ];
    export var sightBlockingTilesMap: IStringToNumberMap = Object.create(null);

    function initializeTileTypes()
    {
        // start all tile types as having nothing special about them
        for (var i = 0; i < numTileTypes; i++) tileTypes[i] = new TileType(i);

        // replace those tile types which are associated with a terrain feature
        var replace =
            (id: number, feature: TerrainFeature) =>
                tileTypes[id] = new TileType(id, feature);
        replace(TileTypeId.LadderBrick, TerrainFeature.Ladder);
        replace(TileTypeId.ClosedDoorBrick, TerrainFeature.ClosedDoor);
        replace(TileTypeId.LockedDoorBrick, TerrainFeature.LockedDoor);
        replace(TileTypeId.ClosedDoorCave, TerrainFeature.ClosedDoor);
        replace(TileTypeId.LockedDoorCave, TerrainFeature.LockedDoor);
        [
            TileTypeId.StalagmiteCave1,
            TileTypeId.StalagmiteCave2,
            TileTypeId.StalagmiteCave3,
            TileTypeId.StalagmiteCave4,
            TileTypeId.StalagmiteCave5,
            TileTypeId.StalagmiteCave6,
            TileTypeId.StalagmiteCave7,
            TileTypeId.StalagmiteCave8,
            TileTypeId.StalagmiteCave9,
            TileTypeId.StalagmiteCave10,
            TileTypeId.StalagmiteCave11,
            TileTypeId.StalagmiteBrick1,
            TileTypeId.StalagmiteBrick2,
            TileTypeId.StalagmiteBrick3,
            TileTypeId.StalagmiteBrick4,
            TileTypeId.StalagmiteBrick5,
            TileTypeId.StalagmiteBrick6,
            TileTypeId.StalagmiteBrick7,
        ].forEach((id) => replace(id, TerrainFeature.Stalagmite));
        replace(TileTypeId.ExitCaveDownLeft, TerrainFeature.ExitCave);
        replace(TileTypeId.ExitCaveDownRight, TerrainFeature.ExitCave);
        replace(TileTypeId.LadderCave, TerrainFeature.Ladder);
        replace(TileTypeId.OpenDoorBrick, TerrainFeature.OpenDoor);
        replace(TileTypeId.OpenDoorCave, TerrainFeature.OpenDoor);
        replace(TileTypeId.StartCaveDownRight, TerrainFeature.StartCave);
        replace(TileTypeId.SpikesBrick, TerrainFeature.Spikes);
        replace(TileTypeId.SpikesCave, TerrainFeature.Spikes);
        [
            TileTypeId.SlimePoolBrick1,
            TileTypeId.SlimePoolBrick2,
            TileTypeId.SlimePoolBrick3,
            TileTypeId.SlimePoolCave1,
            TileTypeId.SlimePoolCave2,
            TileTypeId.SlimePoolCave3,
        ].forEach((id) => replace(id, TerrainFeature.SlimePool));

        // specify the tile animation sequences
        const setAnimationSequence =
            (forTileTypes: TileTypeId[], idSequence: TileTypeId[]) => {
                const sequence: ITileType[] = [];
                idSequence.forEach(id => sequence.push(tileTypes[id]));
                forTileTypes.forEach(type => tileTypes[type].animationSequence = sequence);
            }
        const torchBricks = [
            TileTypeId.TorchBrick1,
            TileTypeId.TorchBrick2,
            TileTypeId.TorchBrick3,
            TileTypeId.TorchBrick4,
        ];
        setAnimationSequence(torchBricks, torchBricks);
        const smallFires = [
            TileTypeId.SmallFire1,
            TileTypeId.SmallFire2,
            TileTypeId.SmallFire3,
            TileTypeId.SmallFire4,
        ];
        setAnimationSequence(smallFires, smallFires);
        const fungis1 = [
            TileTypeId.Fungi1a,
            TileTypeId.Fungi1b,
            TileTypeId.Fungi1c,
            TileTypeId.Fungi1d,
            TileTypeId.Fungi1e,
            TileTypeId.Fungi1f,
        ];
        setAnimationSequence(fungis1, fungis1);
        const fungis2 = [
            TileTypeId.Fungi2a,
            TileTypeId.Fungi2b,
            TileTypeId.Fungi2c,
            TileTypeId.Fungi2d,
            TileTypeId.Fungi2e,
            TileTypeId.Fungi2f,
        ];
        setAnimationSequence(fungis2, fungis2);
        const trashBricks = [
            TileTypeId.TrashBrick1,
            TileTypeId.TrashBrick2,
            TileTypeId.TrashBrick3,
            TileTypeId.TrashBrick4,
        ];
        setAnimationSequence(trashBricks, trashBricks);
        const trashCaves = [
            TileTypeId.TrashCave1,
            TileTypeId.TrashCave2,
            TileTypeId.TrashCave3,
            TileTypeId.TrashCave4,
        ];
        setAnimationSequence(trashCaves, trashCaves);
        const slimePoolBricks = [
            TileTypeId.SlimePoolBrick1,
            TileTypeId.SlimePoolBrick2,
            TileTypeId.SlimePoolBrick3,
        ];
        const slimePoolBrickSequence = [
            TileTypeId.SlimePoolBrick1,
            TileTypeId.SlimePoolBrick2,
            TileTypeId.SlimePoolBrick1,
            TileTypeId.SlimePoolBrick3,
        ];
        setAnimationSequence(slimePoolBricks, slimePoolBrickSequence);
        const slimePoolCaves = [
            TileTypeId.SlimePoolCave1,
            TileTypeId.SlimePoolCave2,
            TileTypeId.SlimePoolCave3,
        ];
        const slimePoolCaveSequence = [
            TileTypeId.SlimePoolCave1,
            TileTypeId.SlimePoolCave2,
            TileTypeId.SlimePoolCave1,
            TileTypeId.SlimePoolCave3,
        ];
        setAnimationSequence(slimePoolCaves, slimePoolCaveSequence);

        // replace those tile types which are associated with a terrain feature
        // which applies only to a region
        var replace1 =
            (id: number, feature: TerrainFeature, region: Region) =>
                tileTypes[id] = new TileType(id, feature, region);
        replace1(
            TileTypeId.SlideTerraceBrickLeft, TerrainFeature.SlideLeft,
            Region.downRightInclusive);
        replace1(
            TileTypeId.SlideTerraceBrickRight, TerrainFeature.SlideRight,
            Region.downLeftInclusive);
        replace1(
            TileTypeId.SlideTerraceRockLeft, TerrainFeature.SlideLeft,
            Region.downRightInclusive);
        replace1(
            TileTypeId.SlideTerraceRockRight, TerrainFeature.SlideRight,
            Region.downLeftInclusive);
        replace1(
            TileTypeId.SlideRockCaveLeft, TerrainFeature.SlideLeft,
            Region.downRightInclusive);
        replace1(
            TileTypeId.SlideRockCaveRight, TerrainFeature.SlideRight,
            Region.downLeftInclusive);
        replace1(
            TileTypeId.SlideRockBrickLeft, TerrainFeature.SlideLeft,
            Region.downRightInclusive);
        replace1(
            TileTypeId.SlideRockBrickRight, TerrainFeature.SlideRight,
            Region.downLeftInclusive);

        // specify the passible region for tiles which aren't all passible
        var setPassibleRegion =
            (types: TileTypeId[], region: Region) => {
                types.forEach(type => tileTypes[type].passibleRegion = region);
            }
        setPassibleRegion(impassibleTiles, Region.none);
        setPassibleRegion(downHalfImpassibleTiles, Region.up);
        setPassibleRegion(upHalfImpassibleTiles, Region.down);
        setPassibleRegion(leftDownSlopingTiles, Region.upLeftExclusive);
        setPassibleRegion(rightDownSlopingTiles, Region.upRightExclusive);
        setPassibleRegion(leftUpSlopingTiles, Region.downLeftExclusive);
        setPassibleRegion(rightUpSlopingTiles, Region.downRightExclusive);

        // specify fall-throughable-region values for those tile-types that don't 
        // use the default
        [
            TileTypeId.LadderBrick,
            TileTypeId.LadderCave,
        ].forEach(type => tileTypes[type].fallThroughableRegion = Region.none);

        // convert the sight-blocking tiles-types list into a map, for speedy
        // determination of which block sight; also, mark the tiles themselves
        // as being sight blockers
        sightBlockingTiles.forEach(
            tileId => {
                sightBlockingTilesMap[tileId.toString()] = tileId;
                tileTypes[tileId].isSightBlocking = true;
            });

        // specify animation-duration values for those tile-types that don't 
        // use the default
        var setAnimationDuration =
            (types: TileTypeId[], duration: number) => {
                types.forEach(type => tileTypes[type].animationDuration = duration);
            }
        var slimePoolDuration = 600;
        setAnimationDuration(
            [
                TileTypeId.SlimePoolBrick1,
                TileTypeId.SlimePoolBrick2,
                TileTypeId.SlimePoolBrick3,
                TileTypeId.SlimePoolCave1,
                TileTypeId.SlimePoolCave2,
                TileTypeId.SlimePoolCave3,
            ], slimePoolDuration);
    }

    initializeTileTypes();
}
