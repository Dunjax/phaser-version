/// <reference path="Sound.ts" />
var Dunjax;
(function (Dunjax) {
    var Sounds;
    (function (Sounds) {
        Sounds.shouldPlaySounds = true;
        var AllSounds = (function () {
            function AllSounds() {
                this.sounds = Sounds.Sound.sounds = [];
                this.playerMove = new Sounds.Sound("playerMove");
                this.playerJump = new Sounds.Sound("playerJump");
                this.playerThrust = new Sounds.Sound("playerThrust", true, true);
                this.playerLand = new Sounds.Sound("playerLand");
                this.playerSlide = new Sounds.Sound("playerSlide", true, true);
                this.playerSwitchGuns = new Sounds.Sound("playerSwitchGuns");
                this.playerClimb = new Sounds.Sound("playerClimb", false, true);
                this.playerDeath = new Sounds.Sound("playerDeath");
                this.playerHit = new Sounds.Sound("playerHit");
                this.playerWhirlwinded = new Sounds.Sound("playerWhirlwinded", false, true);
                this.playerWebbed = new Sounds.Sound("playerWebbed", false, true);
                this.monsterAttack = new Sounds.Sound("monsterAttack");
                this.monsterDeath = new Sounds.Sound("monsterDeath");
                this.greenGargoyleFly = new Sounds.Sound("greenGargoyleFly");
                this.stalagmiteWake = new Sounds.Sound("stalagmiteWake");
                this.spiderWake = new Sounds.Sound("spiderWake");
                this.masterWake = new Sounds.Sound("masterWake");
                this.slimeJump = new Sounds.Sound("slimeJump");
                this.pulseFire = new Sounds.Sound("pulseFire");
                this.grapeFire = new Sounds.Sound("grapeFire");
                this.outOfAmmo = new Sounds.Sound("outOfAmmo", false, true);
                this.halberdFire = new Sounds.Sound("halberdFire");
                this.webFire = new Sounds.Sound("webFire");
                this.webImpact = new Sounds.Sound("webImpact");
                this.shotImpact = new Sounds.Sound("shotImpact");
                this.fireballShot = new Sounds.Sound("fireballShot");
                this.fireballImpact = new Sounds.Sound("fireballImpact");
                this.slimeballShot = new Sounds.Sound("slimeballShot");
                this.slimeballImpact = new Sounds.Sound("slimeballImpact");
                this.levelEndReached = new Sounds.Sound("levelEndReached", false, true);
                this.itemTaken = new Sounds.Sound("itemTaken");
                this.door = new Sounds.Sound("door");
            }
            AllSounds.prototype.addSounds = function () {
                if (this.sounds[0].sound == null)
                    this.sounds.forEach(function (s) {
                        s.sound = Dunjax.Program.phaserGame.add.audio(s.fileName);
                        s.sound.allowMultiple = !s.shouldPlayOnlyOneClipAtATime;
                    });
            };
            return AllSounds;
        })();
        Sounds.AllSounds = AllSounds;
    })(Sounds = Dunjax.Sounds || (Dunjax.Sounds = {}));
})(Dunjax || (Dunjax = {}));
