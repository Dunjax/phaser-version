﻿/// <reference path="Sound.ts" />

module Dunjax.Sounds
{
    /**
     * All the sounds used in the gameplay.
     */
    export class AllSounds
    {
        /**
         * The collection of all the sounds, below.
         */
        sounds: Sound[] = Sound.sounds = [];

        playerMove: ISound = new Sound("playerMove");
        playerJump: ISound = new Sound("playerJump");
        playerThrust: ISound = new Sound("playerThrust");
        playerLand: ISound = new Sound("playerLand");
        playerSlide: ISound = new Sound("playerSlide");
        playerSwitchGuns: ISound = new Sound("playerSwitchGuns");
        playerClimb: ISound = new Sound("playerClimb", true);
        playerDeath: ISound = new Sound("playerDeath");
        playerHit: ISound = new Sound("playerHit");
        playerWhirlwinded: ISound = new Sound("playerWhirlwinded", true);
        playerWebbed: ISound = new Sound("playerWebbed", true);
        monsterAttack: ISound = new Sound("monsterAttack");
        monsterDeath: ISound = new Sound("monsterDeath");
        greenGargoyleFly: ISound = new Sound("greenGargoyleFly");
        stalagmiteWake: ISound = new Sound("stalagmiteWake");
        spiderWake: ISound = new Sound("spiderWake");
        masterWake: ISound = new Sound("masterWake");
        slimeJump: ISound = new Sound("slimeJump");
        pulseFire: ISound = new Sound("pulseFire");
        grapeFire: ISound = new Sound("grapeFire");
        outOfAmmo: ISound = new Sound("outOfAmmo", true);
        halberdFire: ISound = new Sound("halberdFire");
        webFire: ISound = new Sound("webFire");
        webImpact: ISound = new Sound("webImpact");
        shotImpact: ISound = new Sound("shotImpact");
        fireballShot: ISound = new Sound("fireballShot");
        fireballImpact: ISound = new Sound("fireballImpact");
        slimeballShot: ISound = new Sound("slimeballShot");
        slimeShotImpact: ISound = new Sound("slimeballImpact");
        levelEndReached: ISound = new Sound("levelEndReached", true);
        itemTaken: ISound = new Sound("itemTaken");
        door: ISound = new Sound("door");

        /**
         * Adds each of the above sounds to the Phaser game.
         */
        addSounds()
        {
            // if this method has been called before, there is nothing to do
            if (this.sounds[0].sound != null) return;

            // add every sound to the Phaser game
            this.sounds.forEach(
                s => {
                    s.sound = Program.game.add.audio(s.fileName);
                    s.sound.allowMultiple = !s.shouldPlayOnlyOneClipAtATime;
                });
        }
    }
} 