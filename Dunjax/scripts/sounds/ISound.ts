﻿module Dunjax.Sounds
{
    /**
     * A sound effect.
     */
    export interface ISound
    {
        /**
         * Plays a clip of this sound.
         */
        play(): void;

        /**
         * Has all currently-playing clips of this sound stop playing.
         */
        stop(): void;

        /**
         * Returns whether this sound is currently playing.
         */
        isPlaying: boolean;
    }
}