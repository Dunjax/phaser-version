var Dunjax;
(function (Dunjax) {
    var Sounds;
    (function (Sounds) {
        var Sound = (function () {
            function Sound(fileName, shouldLoop, shouldPlayOnlyOneClipAtATime) {
                if (shouldLoop === void 0) { shouldLoop = false; }
                if (shouldPlayOnlyOneClipAtATime === void 0) { shouldPlayOnlyOneClipAtATime = false; }
                this.fileName = fileName;
                this.shouldLoop = shouldLoop;
                this.shouldPlayOnlyOneClipAtATime = shouldPlayOnlyOneClipAtATime;
                var game = Dunjax.Program.phaserGame;
                game.load.audio(fileName, "sounds/" + fileName + ".mp3");
                Sound.sounds.push(this);
            }
            Sound.prototype.play = function () {
                if (!this.sound.isPlaying || !this.shouldPlayOnlyOneClipAtATime)
                    this.sound.play(null, 0, 1, this.shouldLoop);
            };
            Sound.prototype.stop = function () { this.sound.stop(); };
            return Sound;
        })();
        Sounds.Sound = Sound;
    })(Sounds = Dunjax.Sounds || (Dunjax.Sounds = {}));
})(Dunjax || (Dunjax = {}));
