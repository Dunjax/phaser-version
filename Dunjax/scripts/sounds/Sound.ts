﻿module Dunjax.Sounds
{
    /* Implementation */
    export class Sound implements ISound
    {
        /**
         * The name (minus extension) of this sound's file on disk.
         */
        fileName: string;

        /**
         * Whether only one instance of this sound should play at a time.
         */
        shouldPlayOnlyOneClipAtATime: boolean;

        /**
         * The Phaser sound object which this object wraps.
         */
        sound: Phaser.Sound;

        /**
         * Stores all the objects of this class which have been created, so far.
         */
        static sounds: ISound[];

        /**
         * Whether sounds should play, at all.  This may be set to false when the user 
         * wishes to play the game silently.
         */
        static soundsEnabled = true;

        constructor(fileName: string, shouldPlayOnlyOneClipAtATime: boolean = false)
        {
            this.fileName = fileName;
            this.shouldPlayOnlyOneClipAtATime = shouldPlayOnlyOneClipAtATime;
            Sound.sounds.push(this);

            // have Phaser start loading this sound
            const game = Program.game;
            if (Sound.soundsEnabled) game.load.audio(fileName, `sounds/${fileName}.mp3`);
        }

        /* Implementation */
        play()
        {
            if (!Sound.soundsEnabled) return;

            if (!this.sound.isPlaying || !this.shouldPlayOnlyOneClipAtATime)
                this.sound.play();
        }

        /* Implementation */
        get isPlaying()
        {
            if (!Sound.soundsEnabled) return false;

            return this.sound.isPlaying;
        }

        /* Implementation */
        stop()
        {
            if (!Sound.soundsEnabled) return;

            this.sound.stop();
        }
    }
}