var Dunjax;
(function (Dunjax) {
    var Time;
    (function (Time) {
        var Clock = (function () {
            function Clock(game) {
                this.game = game;
            }
            Object.defineProperty(Clock.prototype, "time", {
                get: function () { return this.game.time.now; },
                enumerable: true,
                configurable: true
            });
            Clock.prototype.start = function () {
                this.game.time.reset();
            };
            return Clock;
        })();
        Time.Clock = Clock;
        Time.clock;
    })(Time = Dunjax.Time || (Dunjax.Time = {}));
})(Dunjax || (Dunjax = {}));
