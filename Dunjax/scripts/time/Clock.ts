﻿module Dunjax.Time
{
    /* Implementation */
    export class Clock implements IClock
    {
        /**
         * The Phaser game whose timer this object wraps.
         */
        private game: Phaser.Game;

        /* Implementation */
        get time(): number { return this.game.time.now; }

        constructor(game: Phaser.Game)
        {
            this.game = game;
        }

        /* Implementation */
        start()
        {
            this.game.time.reset();
        }
    }
}