var Dunjax;
(function (Dunjax) {
    var Time;
    (function (Time) {
        var Duration = (function () {
            function Duration(length, representsPeriod) {
                if (length === void 0) { length = 0; }
                if (representsPeriod === void 0) { representsPeriod = true; }
                this.startTime = 0;
                this.length = length;
                this.representsPeriod = this.shouldRestartWhenDone = representsPeriod;
            }
            Object.defineProperty(Duration.prototype, "isDone", {
                get: function () {
                    var done = Time.clock.time - this.startTime >= this.length;
                    if (done && this.shouldRestartWhenDone)
                        this.start();
                    return done;
                },
                enumerable: true,
                configurable: true
            });
            Duration.prototype.start = function () {
                if (this.representsPeriod && this.startTime + this.length >=
                    Time.clock.time - this.length)
                    this.startTime += this.length;
                else
                    this.startTime = Time.clock.time;
                this.afterStart();
            };
            Duration.prototype.afterStart = function () { };
            Object.defineProperty(Duration.prototype, "lengthLeft", {
                get: function () {
                    return this.startTime + this.length - Time.clock.time;
                },
                enumerable: true,
                configurable: true
            });
            Duration.prototype.makeDone = function () {
                this.startTime = Time.clock.time - this.length;
            };
            Object.defineProperty(Duration.prototype, "isAtStart", {
                get: function () {
                    return this.startTime === Time.clock.time;
                },
                enumerable: true,
                configurable: true
            });
            return Duration;
        })();
        Time.Duration = Duration;
    })(Time = Dunjax.Time || (Dunjax.Time = {}));
})(Dunjax || (Dunjax = {}));
