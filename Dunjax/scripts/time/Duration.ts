﻿module Dunjax.Time
{
    /* Implementation */
    export class Duration implements IDuration
    {
        /* Implementation */
        length: number;
    	
        /**
         * The time (in ms) when this duration started.
         * 
         * We set this to a value far back in the past, such that if this duration's 
         * start() method is not called, IsDone() will return true.  This is necessary 
         * since, in such a case, this duration is likely being used as a period between 
         * executions of the same, repetitive action, and we normally want the first
         * such execution to happen right away. 
         */
        protected startTime = 0;
    	
        /**
         * Whether this duration is being used to keep a repetitive event occurring
         * at a regular interval.  In such a case, the period thus represented should 
         * be measured from the last period's end, rather than the current game time
         * when this duration is to be restarted.  The latter approach would make 
         * the periods uneven.  
         */
        protected representsPeriod: boolean;

        constructor(length: number = 0, representsPeriod: boolean = true)
        {
            this.length = length;
            this.representsPeriod = representsPeriod;
        }

        /* Implementation */
        get isDone(): boolean
        {
            const done = Program.clock.time - this.startTime >= this.length;

            // if this duration is done, and represents a period, restart it, to being the
            // next period
            if (done && this.representsPeriod) this.start();

            return done;
        }

        /* Implementation */
        start()
        {
            // if this call is an advance of this duration to a next successive period, and
            // the periods haven't gotten too far off track from the current game time, 
            // perform the advancing
            const clock = Program.clock;
            if (this.representsPeriod
                && this.startTime + this.length >= clock.time - this.length / 2)
                this.startTime += this.length;
            
            // otherwise, use the current game time as the starting time
            else this.startTime = clock.time;

            this.afterStart();
        }

        /* Hook */
        protected afterStart() { }

        /* Implementation */
        get lengthLeft(): number {
            return this.startTime + this.length - Program.clock.time; 
        }

        /* Implementation */
        makeDone()
        {
            this.startTime = Program.clock.time - this.length;
        }
    }
}
 