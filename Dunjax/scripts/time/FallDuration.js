/// <reference path="Duration.ts" />
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Dunjax;
(function (Dunjax) {
    var Time;
    (function (Time) {
        var FallDuration = (function (_super) {
            __extends(FallDuration, _super);
            function FallDuration() {
                _super.apply(this, arguments);
                this.startingDelayBetweenFallMoves = 64;
                this.minDelayBetweenFallMoves = 2;
            }
            FallDuration.prototype.fallStarted = function () {
                this.currentFallStartTime = Time.clock.time;
            };
            FallDuration.prototype.afterStart = function () {
                var accelerationInPixelsPerMilliSecond = .55;
                var timeFalling = Math.max(Time.clock.time - this.currentFallStartTime, 1);
                var fallSpeed = accelerationInPixelsPerMilliSecond * timeFalling / this.slowdownFactor;
                var durationLength = 1000 / fallSpeed;
                durationLength = Math.max(durationLength, this.minDelayBetweenFallMoves);
                durationLength = Math.min(durationLength, this.startingDelayBetweenFallMoves);
                this.length = durationLength;
            };
            return FallDuration;
        })(Time.Duration);
        Time.FallDuration = FallDuration;
    })(Time = Dunjax.Time || (Dunjax.Time = {}));
})(Dunjax || (Dunjax = {}));
