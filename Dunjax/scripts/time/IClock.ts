﻿module Dunjax.Time
{
    /**
     * Reports a current time relative to its inception.  
     */
    export interface IClock
    {
        /**
         * This clock's current value for the length of time in ms since it was started. 
         */
        time: number;

        /**
         * Starts (or restarts) this clock running with a time value of zero.
         */
        start(): void;
    }
}