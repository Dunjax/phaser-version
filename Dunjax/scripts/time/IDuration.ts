﻿module Dunjax.Time
{
    /**
     * Keeps track of whether a specified amount of time has elapsed.
     */
    export interface IDuration
    {
        /**
         * Returns whether this duration has completed.  
         */
        isDone: boolean;

        /**
         * If this duration represents a period, advances this duration's starting time
         * to the beginning of the next period.  Otherwise, uses the current time as 
         * the start time.
         */
        start(): void;

        /**
         * The length of this duration, in milliseconds.
         */
        length: number;

        /**
         * The length of this duration that is left, in milliseconds.
         */
        lengthLeft: number;

        /**
         * Makes this duration done. 
         */
        makeDone(): void;
    }
}