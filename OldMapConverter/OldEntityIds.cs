﻿namespace OldMapConverter
{
    enum OldEntityIds
    {
        GreenGargoyleLeftId = 5,
        GreenGargoyleRightId = 6,
        YellowGargoyleLeftId = 23,
        YellowGargoyleRightId = 24,
        StalagmiteId = 30,
        MasterId = 400,
        WhirlwindLeftId = 11,
        WhirlwindRightId = 12,
        SpiderLeftId = 27,
        SpiderRightId = 28,
        SlimeLeftId = 31,
        SlimeRightId = 32,
        BlueGargoyleLeftId = 25,
        BlueGargoyleRightId = 26,
        PulseAmmoBigId = 37,
        GrapeshotGunId = 39,
        PulseAmmoId = 38,
        PulseAmmoId2 = 151,
        PulseAmmoId3 = 33,
        PulseAmmoId4 = 34,
        PulseAmmoId5 = 35,
        PulseAmmoId6 = 36,
        GrapeshotAmmoId = 40,
        GrapeshotAmmoId2 = 45,
        ShieldPowerUpId = 42,
        ShieldPowerUpId2 = 43,
        ShieldPowerPackId = 41,
        ShieldPowerPackId2 = 44,
        KeyId = 47,
        FinalRoomTriggerId = 20
    }
}
