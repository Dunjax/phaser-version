﻿using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using Formatting = Newtonsoft.Json.Formatting;

namespace OldMapConverter
{
    class Program
    {
        private Dictionary<string, short> nameToTileMap;

        const int TileWidth = 32, TileHeight = 32;

        static void Main()
        {
            new Program().Run("2.wmp");
        }

        // ReSharper disable InconsistentNaming
        // ReSharper disable NotAccessedField.Local
        // ReSharper disable UnusedMember.Local
        #pragma warning disable 649
        #pragma warning disable 169
        #pragma warning disable 414
        class Map
        {
            public int width, height;
            public int tilewidth = TileWidth, tileheight = TileHeight;
            public string orientation = "orthogonal";
            public string renderorder = "right-down";
            public int nextobjectid = 1;
            public Tileset[] tilesets = {
                new Tileset {
                    firstgid = 1,
                    image = "tiles.png",
                    name = "tiles",
                    tilewidth = 32, 
                    tileheight = 32,
                    imagewidth = 1280, 
                    imageheight = 160
                }
            };
            public Layer[] layers = {
                new Layer {
                    name = "tiles",
                    type = "tilelayer",
                },
                new Layer {
                    name = "entities",
                    type = "objectgroup",
                }   
            };
        }

        class Layer
        {
            public int width, height;
            public string name;
            public string type;
            public bool visible = true;
            public float opacity = 1;
            public int[] data;
            public Entity[] objects;
        }

        class Tileset
        {
            public int firstgid;
            public string image;
            public string name;
            public int tilewidth, tileheight;
            public int imagewidth, imageheight;
            public int margin = 0, spacing = 0;
        }

        class Entity
        {
            public string name;
            public string type;
            public int x, y;
            public int width, height;

            public void CenterAt(int atX, int atY)
            {
                x = atX - width / 2;
                y = atY - height / 2;
            }
        }

        // ReSharper enable InconsistentNaming
        // ReSharper enable NotAccessedField.Local
        // ReSharper enable UnusedMember.Local
        #pragma warning restore 169
        #pragma warning restore 649
        #pragma warning restore 414

        void Run(string fileName)
        {
            LoadNameToTileMap();

            // load the old map
            var map = LoadOldMap(fileName);

            // modify the filename extension of the old map to create the
            // new map's filename
            var jsonFileName = 
                fileName.Substring(0, fileName.IndexOf(".")) + ".json";

            // store the new map
            StoreNewMap(map, jsonFileName);
        }

        Map LoadOldMap(string fileName)
        {
            // create an a new map
            var map = new Map();

            // load the map size, and size the map and layer to it
            var reader = new BinaryReader(File.OpenRead(fileName));
            reader.ReadInt16();
            var layer = map.layers[0];
            var width = map.width = layer.width = reader.ReadInt16();
            var height = map.height = layer.height = reader.ReadInt16();
            layer.data = new int[width * height];

            LoadTiles(width, height, reader, layer);

            // ignore the tile-types in the file
            for (int i = 0; i < 256; i++) reader.ReadInt16();

            LoadEntities(reader, map.layers[1]);

            return map;
        }

        void LoadTiles(int width, int height, BinaryReader reader, Layer layer)
        {
            // for each tile to be loaded
            for (var j = 0; j < height; j++) {
                for (var i = 0; i < width; i++) {
                    // load (and normalize if negative) this tile
                    var tile = reader.ReadInt16();
                    if (tile < 0) tile += 256;

                    // perform a pre-translation tile substition on this tile,
                    // if desired
                    CheckForTileSubstitution(layer, i, j, ref tile);

                    // map the old tile value to a tile name, then map that name
                    // to the position-index of the tile in the tilesheet, which is 
                    // the tile's new value
                    tile = (short)(nameToTileMap[OldTileMap.OldTileToName[tile]] + 1);

                    // set the translated tile into the map layer
                    layer.data[i + j * width] = tile;
                }
            }
        }

        void CheckForTileSubstitution(Layer layer, int x, int y, ref short tile)
        {
            // if the tile at the given location is for a non-wavy slime-pool 
            // (w/brick background)
            const int slimePoolBrick = 215;
            if (tile == slimePoolBrick) {
                // change the tile to one with a wave, alternating
                // wave direction with the like tile (if one exists) to the left
                var tileToLeft = layer.data[x - 1 + y * layer.width];
                tile = (short)
                    ((tileToLeft == slimePoolBrick + 1) ?
                        slimePoolBrick + 2 : slimePoolBrick + 1);
            }

            // if the tile at the given location is for a non-wavy slime-pool 
            // (w/cave background)
            const int slimePoolCave = 218;
            if (tile == slimePoolCave) {
                // change the tile to one with a wave, alternating
                // wave direction with the like tile (if one exists) to the left
                var tileToLeft = layer.data[x - 1 + y * layer.width];
                tile = (short)
                    ((tileToLeft == slimePoolCave + 1) ?
                        slimePoolCave + 2 : slimePoolCave + 1);
            }
        }

        void StoreNewMap(Map map, string fileName)
        {
            // convert the map to become JSON data
            var json = JsonConvert.SerializeObject(map, Formatting.Indented);

            // store the JSON data in a file of the given name
            File.WriteAllText(fileName, json);            
        }

        void LoadNameToTileMap()
        {
            // for each line in the data file generated by sprite sheet packer,
            // each of which corresponds to a tile
            var lines = File.ReadLines("tiles.txt");
            const int
                tileWidth = 32, tileHeight = 32, spriteSheetWidth = 1280, 
                tilesPerRow = spriteSheetWidth / tileWidth;
            nameToTileMap = new Dictionary<string, short>();
            foreach (var line in lines) {
                // compute the index of the image of this tile within the sprite sheet
                var fields = line.Split(' ');
                var row = int.Parse(fields[3]) / tileHeight;
                var column = int.Parse(fields[2]) / tileWidth;
                var tile = row * tilesPerRow + column;

                // key the tile name to the index computed above
                nameToTileMap[fields[0]] = (short)tile;
            }
        }

        void LoadEntities(BinaryReader reader, Layer layer)
        {
            var entityCount = reader.ReadInt16();
            var entities = new List<Entity>();
            for (var i = 0; i < entityCount; i++) {
                var entity = new Entity();
                reader.ReadByte();
                entity.x = reader.ReadInt16() * 2;
                entity.y = reader.ReadInt16() * 2;
                SetEntityPropertiesFromOldId(entity, reader.ReadInt16());

                if (entity.name == null || entity.type == null) continue;

                AlignEntity(entity);

                entities.Add(entity);
            }

            layer.objects = entities.ToArray();
        }

        void SetEntityPropertiesFromOldId(Entity entity, int id)
        {
            string name = null, type = null;
            int width = 0, height = 0;
            switch (id) {
                case (int)OldEntityIds.GreenGargoyleLeftId:
                case (int)OldEntityIds.GreenGargoyleRightId:
                    name = "GreenGargoyle";
                    type = "monster";
                    width = height = 32;
                    break;
                case (int)OldEntityIds.YellowGargoyleLeftId:
                case (int)OldEntityIds.YellowGargoyleRightId:
                    name = "YellowGargoyle";
                    type = "monster";
                    width = height = 32;
                    break;
                case (int)OldEntityIds.BlueGargoyleLeftId:
                case (int)OldEntityIds.BlueGargoyleRightId:
                    name = "BlueGargoyle";
                    type = "monster";
                    width = height = 64;
                    break;
                case (int)OldEntityIds.WhirlwindLeftId:
                case (int)OldEntityIds.WhirlwindRightId:
                    name = "Whirlwind";
                    type = "monster";
                    width = height = 32;
                    break;
                case (int)OldEntityIds.StalagmiteId:
                    name = "Stalagmite";
                    type = "monster";
                    width = height = 32;
                    break;
                case (int)OldEntityIds.MasterId:
                    name = "Master";
                    type = "monster";
                    width = height = 128;
                    break;
                case (int)OldEntityIds.SpiderLeftId:
                case (int)OldEntityIds.SpiderRightId:
                    name = "Spider";
                    type = "monster";
                    width = 64;
                    height = 32;
                    break;
                case (int)OldEntityIds.SlimeLeftId:
                case (int)OldEntityIds.SlimeRightId:
                    name = "Slime";
                    type = "monster";
                    width = height = 32;
                    break;
                case (int)OldEntityIds.PulseAmmoBigId:
                    name = "PulseAmmo";
                    type = "item";
                    width = 14;
                    height = 22;
                    break;
                case (int)OldEntityIds.GrapeshotGunId:
                    name = "GrapeshotGun";
                    type = "item";
                    width = height = 24;
                    break;
                case (int)OldEntityIds.PulseAmmoId:
                case (int)OldEntityIds.PulseAmmoId2:
                case (int)OldEntityIds.PulseAmmoId3:
                case (int)OldEntityIds.PulseAmmoId4:
                case (int)OldEntityIds.PulseAmmoId5:
                case (int)OldEntityIds.PulseAmmoId6:
                    name = "PulseAmmo";
                    type = "item";
                    width = 20;
                    height = 18;
                    break;
                case (int)OldEntityIds.GrapeshotAmmoId:
                case (int)OldEntityIds.GrapeshotAmmoId2:
                    name = "GrapeshotAmmo";
                    type = "item";
                    width = height = 12;
                    break;
                case (int)OldEntityIds.ShieldPowerUpId:
                case (int)OldEntityIds.ShieldPowerUpId2:
                    name = "ShieldPowerUp";
                    type = "item";
                    width = 10;
                    height = 5;
                    break;
                case (int)OldEntityIds.ShieldPowerPackId:
                case (int)OldEntityIds.ShieldPowerPackId2:
                    name = "ShieldPowerPack";
                    type = "item";
                    width = 22;
                    height = 17;
                    break;
                case (int)OldEntityIds.KeyId:
                    name = "Key";
                    type = "item";
                    width = 16;
                    height = 8;
                    break;
                case (int)OldEntityIds.FinalRoomTriggerId:
                    name = "FinalRoomTrigger";
                    type = "trigger";
                    width = height = 32;
                    break;
            }

            entity.name = name;
            entity.type = type;
            entity.width = width;
            entity.height = height;
        }

        void AlignEntity(Entity entity)
        {
            if (entity.name == "Key") AlignEntityWidthLedgeTop(entity);
        }

        void AlignEntityWidthLedgeTop(Entity entity)
        {
            var tileCenter =
                GetTileCenter(
                    entity.x + entity.width / 2,
                    entity.y + entity.height - 1 + 8);
            entity.CenterAt(tileCenter.X, tileCenter.Y - 8);
        }

        class Point
        {
            public int X, Y;
        }

        Point GetTileCenter(int x, int y)
        {
            return
                new Point {
                    X = (x / TileWidth) * TileWidth + TileWidth / 2,
                    Y = (y / TileHeight) * TileHeight + TileHeight / 2,
                };
        }
    }
}
